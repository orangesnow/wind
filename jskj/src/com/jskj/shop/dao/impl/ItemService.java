
package com.jskj.shop.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.dao.util.DataBaseService;
import com.jskj.shop.entity.resp.JskjArea;
import com.jskj.shop.entity.vo.DetailItem;
import com.jskj.shop.entity.vo.ItemSku;

public class ItemService {
	
	private DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");
	
	private static final Logger logger = Logger.getLogger(ItemService.class);
	public static Date LastTimeDate = new Date();
	public static long theSerialID = 0L;
	
	public void updateItemCategory(Integer itemid,Integer cid) {
		StringBuffer query = new StringBuffer();
		query.append("update jskj_item_list set c_id=").append(cid);
		query.append(" where item_id=").append(itemid);
		
		logger.info("update sql:"+query.toString());
		dataBaseService.executeUpdate(query.toString());
	}
	
	/**
	 * 查询热门标签列表
	 * @param num 热门标签数量
	 * @return
	 */
	public List<Map<String,Object>> findHotTag(int num){
		List<Map<String,Object>> tags = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_tag_list where status =1 order by hot_level desc limit 0,").append(num);
		
		tags = dataBaseService.executeQuery(query.toString());
		
		return tags;
	}
	
	/**
	 * 查询热门标签列表
	 * @param num 热门标签数量
	 * @return
	 */
	public List<Map<String,Object>> findRecommTag(int num){
		List<Map<String,Object>> tags = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_tag_list where status =1 order by recomm_level desc limit 0,").append(num);
		
		tags = dataBaseService.executeQuery(query.toString());
		
		return tags;
	}
	
	/**
	 * 加载广告
	 * @return
	 */
	public Map<String,Object> findLoadingAd(){
		
		StringBuffer query = new StringBuffer();
		query.append("select ad_id as adid,pic_url as imgurl,bind_flag as bindflag,bind_item as itemid,bind_url as adurl from jskj_ad where ad_type=1 and status=1");
		
		List<Map<String,Object>> loadings = dataBaseService.executeQuery(query.toString());
		if(loadings != null && loadings.size() >0){
			for(int i=0;i<loadings.size();i++){
				return loadings.get(0);
			}
		}
		
		return null;
		
	}
	
	/**
	 * 查询广告列表信息
	 * @param adtype 广告类型。1：首页启动加载广告，2：首页焦点图
	 * @param num
	 * @return
	 */
	public List<Map<String,Object>> findAds(int adtype,int num){
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_ad where status=1 and ad_type=").append(adtype);
		query.append(" limit 0,").append(num);
		
		List<Map<String,Object>> ads = dataBaseService.executeQuery(query.toString());
		return ads;
	}
	
	/**
	 * 查询当前置顶的专区
	 * @param istop
	 * @return
	 */
	public List<Map<String,Object>> findTopArea(int istop,int isrecomm,int num){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_area where status=1 ");
		
		if(istop >-1){
			query.append(" and is_top=").append(istop);
		}
		
		if(isrecomm >-1){
			query.append(" and is_recomm=").append(isrecomm);
		}
		
		query.append(" order by weight asc");
		query.append(" limit 0,").append(num);
		
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	/**
	 * 查询指定专区的商品列表
	 * @param areaId
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findItemFromAreaId(Integer areaId,Integer istop,int pageno,int pagesize){
		
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_area_item where status=1 ");
		
		if(areaId != null && areaId >-1){
			query.append(" and area_id=").append(areaId);
		}
		
		if(istop != null && istop >-1){
			query.append(" and is_top=").append(istop);
		}
		
		query.append(" order by weight asc");
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	
	/**
	 * 查询指定专区的置顶商品列表
	 * @param areaId
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findTopItemFromAreaId(Integer areaId,int num){
		
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_area_item where status=1 ");
		
		if(areaId != null && areaId >-1){
			query.append(" and area_id=").append(areaId);
		}

		query.append(" order by weight asc");
		query.append(" limit 0,").append(num);
		
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	
	public List<Map<String,Object>> findPicturesByItemId(Integer itemid,Integer ptype){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_picture where 1=1 ");
		
		if(itemid != null && itemid >-1){
			query.append(" and item_id=").append(itemid);
		}
		
		if(ptype != null && ptype>0){
			query.append(" and ptype=").append(ptype);
		}

		query.append(" order by weight asc,pic_id asc");
		
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	/**
	 * 统计当前记录说
	 * @param areaId
	 * @param istop
	 * @return
	 */
	public int totalItemFromAreaId(Integer areaId,Integer istop){
		List<Map<String,Object>> totals = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select count(*) cnt from jskj_area_item where status=1 ");
		
		if(areaId != null && areaId >-1){
			query.append(" and area_id=").append(areaId);
		}
		
		if(istop != null && istop >-1){
			query.append(" and is_top=").append(istop);
		}
		
		totals = dataBaseService.executeQuery(query.toString());
		if(totals != null && totals.size()>0){
			Map<String,Object> total =  totals.get(0);
			return (Integer)total.get("cnt");
		}
		
		return 0;
	}
	
	/**
	 * 查询当前商品标题中包含特定关键词的内容
	 * @param keyword
	 * @param ranktype
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findItemsFromKeyword(String keyword,int ranktype,int pageno,int pagesize){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_list where status=1 ");
		
		if(keyword != null && !keyword.equals("")){
			query.append(" and (instr(title,\"").append(keyword).append("\")>0");
			query.append(" or instr(tag_str,\"").append(keyword).append("\")>0)");
		}
		
		if(ranktype == 1){
			query.append(" order by sold_weight desc,sold_num desc");
		} else if(ranktype == 2){
			query.append(" order by sold_weight asc,sold_num asc");
		} else if(ranktype == 3){
			query.append(" order by origin_price desc");
		} else if(ranktype == 4){
			query.append(" order by origin_price asc");
		} else if(ranktype == 5){
			query.append(" order by recomm_weight desc");
		} else if(ranktype == 6){
			query.append(" ordre by recomm_weight asc");
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		logger.info("finditemfromkeywords:"+query.toString());
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	public int totalItemsFromKeyword(String keyword,int ranktype){
		List<Map<String,Object>> areas = null;
		StringBuffer query = new StringBuffer();
		query.append("select count(*) cnt from jskj_item_list where status=1 ");
		
		if(keyword != null && !keyword.equals("")){
			query.append(" and (instr(title,\"").append(keyword).append("\")>0");
			query.append(" or instr(tag_str,\"").append(keyword).append("\")>0)");
		}
		
		areas = dataBaseService.executeQuery(query.toString());
		if(areas != null && areas.size()>0){
			Map<String,Object> map = areas.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
	}
	
	/**
	 * 查询当前可用分类
	 * @param rootid
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findItemCategorys(Integer rootid,int pageno,int pagesize){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_category where status=1 ");
		
		if(rootid != null && rootid >0){
			query.append(" and root_id=").append(rootid);
		}
	
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	/**
	 * 统计分类总数
	 * @param rootid
	 * @return
	 */
	public int totalItemCategorys(Integer rootid) {
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select count(*) cnt from jskj_category where status=1 ");
		
		if(rootid != null && rootid >-1){
			query.append(" and root_id=").append(rootid);
		}
	
		areas = dataBaseService.executeQuery(query.toString());
		if(areas != null && areas.size() >0){
			Map<String,Object> map = areas.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
	}
	
	
	/**
	 * 按照分类查询指定的商品信息
	 * @param cid
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findItemDataByCategory(Integer cid,int ranktype,int pageno,int pagesize){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select i.* from jskj_item_list i,jskj_category c where i.status=1 and c.status=1 and i.c_id= c.c_id  ");
		
		if(cid != null && cid >-1){
			query.append(" and c.c_id=").append(cid);
		}
		
		if(ranktype == 1){
			query.append(" order by sold_weight desc,sold_num desc");
		} else if(ranktype == 2){
			query.append(" order by sold_weight asc,sold_num asc");
		} else if(ranktype == 3){
			query.append(" order by origin_price desc");
		} else if(ranktype == 4){
			query.append(" order by origin_price asc");
		} else if(ranktype == 5){
			query.append(" order by recomm_weight desc");
		} else if(ranktype == 6){
			query.append(" ordre by recomm_weight asc");
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	public List<Map<String,Object>> findArticleList(Integer cid,Integer pageno,int pagesize) {
		StringBuffer query = new StringBuffer();
		if(cid != null && cid >0){
			query.append("select * from jskj_article where status = 1 and cid=").append(cid);
		} else {
			query.append("select * from jskj_article where status = 1");	
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		return dataBaseService.executeQuery(query.toString());
	}
	
	public int totalArticleList(Integer cid) {
		StringBuffer query = new StringBuffer();
		
		if(cid != null && cid >0){
			query.append("select count(*) cnt from jskj_article where status = 1 and cid=").append(cid);
		} else {
			query.append("select count(*) cnt from jskj_article where status = 1 ");
		}

		List<Map<String,Object>> articles =  dataBaseService.executeQuery(query.toString());
		if(articles != null && articles.size() >0){
			Map<String,Object> article = articles.get(0);
			return (Integer)article.get("cnt");
		}
		
		return 0;
		
	}
	
	public List<Map<String,Object>> findArticleCategory() {
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_article_category where status = 1  and parent_id is null");
		
		return dataBaseService.executeQuery(query.toString());
	}
	
	
	public List<Map<String,Object>> findRelateByItemId(Integer itemid,int pagesize) {

		StringBuffer query = new StringBuffer();
		query.append("select  * from jskj_item_list k,(select distinct item_id from jskj_item_tag where tag_id in (select tag_id from jskj_item_tag where item_id=").append(itemid);
		query.append(")) t where k.item_id<>").append(itemid).append(" and k.item_id=t.item_id  limit 0,").append(pagesize);
	
		return dataBaseService.executeQuery(query.toString());
	}
	
	public Map<String,Object> findCategoryById(Integer id) {
		Map<String,Object> category = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_category where c_id=").append(id);
		
		List<Map<String,Object>> categorys = dataBaseService.executeQuery(query.toString());
		if(categorys != null && categorys.size()>0){
			category = categorys.get(0);
		}
		
		return category;
	}
	
	public Map<String,Object> findTagById(Integer id) {
		Map<String,Object> tag = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_tag_list where id=").append(id);
		
		List<Map<String,Object>> tags = dataBaseService.executeQuery(query.toString());
		if(tags != null && tags.size()>0){
			tag = tags.get(0);
		}
		
		return tag;
	}
	
	public Map<String,Object> findAreaById(Integer id) {
		Map<String,Object> area = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_area where status=1  and area_id=").append(id);
		
		List<Map<String,Object>> areas = dataBaseService.executeQuery(query.toString());
		if(areas != null && areas.size()>0){
			area = areas.get(0);
		}
		
		return area;
	}
	
	
	/**
	 * 
	 * @param cid
	 * @param ranktype
	 * @return
	 */
	public int totalItemsByCategory(Integer cid,int ranktype) {
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select count(i.item_id) cnt from jskj_item_list i,jskj_item_category c where i.status=1 and  and c.status=1  ");
		
		if(cid != null && cid >-1){
			query.append(" and c.c_id=").append(cid);
		}
		
		query.append(" and c.item_id = i.item_id");
		areas = dataBaseService.executeQuery(query.toString());
		if(areas != null && areas.size() >0){
			Map<String,Object> map = areas.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
		
	}
	
	/**
	 * 根据分区查询商品信息
	 * @param areaid
	 * @param ranktype
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findItemsByAreaId(Integer areaid,int ranktype,int pageno,int pagesize) {
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select i.* from jskj_item_list i,jskj_area_item a where i.status=1 ");
		
		if(areaid != null && areaid >-1){
			query.append(" and a.area_id=").append(areaid);
		}
		
		query.append(" and c.item_id = i.item_id");
		
		
		if(ranktype == 1){
			query.append(" order by sold_weight desc,sold_num desc");
		} else if(ranktype == 2){
			query.append(" order by sold_weight asc,sold_num asc");
		} else if(ranktype == 3){
			query.append(" order by origin_price desc");
		} else if(ranktype == 4){
			query.append(" order by origin_price asc");
		} else if(ranktype == 5){
			query.append(" order by recomm_weight desc");
		} else if(ranktype == 6){
			query.append(" ordre by recomm_weight asc");
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	/**
	 * 根据标签ID查询当前可用的商品
	 * @param tagid
	 * @param ranktype
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findItemDataByTagId(Integer tagid,int ranktype,int pageno,int pagesize){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select i.* from jskj_item_list i,jskj_item_tag it,jskj_tag_list t  where i.status=1 and t.status=1 ");
		query.append("  and it.item_id = i.item_id and t.id=it.tag_id ");
		 
		if(tagid != null && tagid >-1){
			query.append(" and it.tag_id=").append(tagid);
		}

		if(ranktype == 1){
			query.append(" order by sold_weight desc,sold_num desc");
		} else if(ranktype == 2){
			query.append(" order by sold_weight asc,sold_num asc");
		} else if(ranktype == 3){
			query.append(" order by origin_price desc");
		} else if(ranktype == 4){
			query.append(" order by origin_price asc");
		} else if(ranktype == 5){
			query.append(" order by recomm_weight desc");
		} else if(ranktype == 6){
			query.append(" ordre by recomm_weight asc");
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		logger.info("sql:"+query.toString());
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	/**
	 * 根据标签ID查询当前可用的商品
	 * @param tagid
	 * @param ranktype
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public int totalItemsByTagId(Integer tagid){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
//		query.append("select count(i.item_id) cnt from jskj_item_list i,jskj_item_tag t where i.status=1  and t.status=1 ");
		
		query.append("select count(i.item_id) cnt from jskj_item_list i,jskj_item_tag it,jskj_tag_list t  where i.status=1 and t.status=1 ");
		query.append("  and it.item_id = i.item_id and t.id=it.tag_id ");

		if(tagid != null && tagid >-1){
			query.append(" and it.tag_id=").append(tagid);
		}
		
		query.append(" and it.item_id = i.item_id");

		areas = dataBaseService.executeQuery(query.toString());
		if(areas != null && areas.size() >0){
			Map<String,Object> map = areas.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
	}
	
	/**
	 * 根据商品ID查询详细信息
	 * @param itemid
	 * @return
	 */
	public Map<String,Object> findItemById(Integer itemid){
		
		List<Map<String,Object>> items = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_list where 1=1");
		
		if(itemid != null && itemid >-1){
			query.append(" and item_id=").append(itemid);
		}
		
		items = dataBaseService.executeQuery(query.toString());
		
		if(items != null && items.size()>0){
			return items.get(0);
		}
		
		return null;
	}
	
	/**
	 * 查询商品SKU
	 * @param itemid
	 * @return
	 */
	public List<Map<String,Object>> findSkuFromItemId(Integer itemid){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select t.* from jskj_item_list i,jskj_item_sku t where i.status=1 ");
		
		if(itemid != null && itemid >-1){
			query.append(" and t.item_id=").append(itemid);
		}
		
		query.append(" and t.item_id = i.item_id");

		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	/**
	 * 查询当前商品简单购买记录
	 * @param itemid
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findSimpleTrades(Integer itemid,int pageno,int pagesize){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_trade_simple where 1=1 ");
		
		if(itemid != null && itemid >-1){
			query.append(" and item_id=").append(itemid);
		}

		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	public int totalSimpleTrades(Integer itemid){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select count(*) cnt from jskj_trade_simple where 1=1 ");
		
		if(itemid != null && itemid >-1){
			query.append(" and item_id=").append(itemid);
		}

		areas = dataBaseService.executeQuery(query.toString());
		if(areas != null && areas.size() >0){
			Map<String,Object> map = areas.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
	}
	
	
	/**
	 * 查询购买记录
	 * @param itemid
	 * @param userid
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findTradeByUserId(Integer itemid,Integer userid,int pageno,int pagesize){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select o.* from jskj_trade_info t,jskj_trade_order o where 1=1 ");
		
		if(itemid != null && itemid >-1){
			query.append(" and o.item_id=").append(itemid);
		}
		
		if(userid != null && userid >-1){
			query.append(" and t.user_id=").append(userid);
		}

		query.append(" and o.trade_id = t.t_id");
		query.append(" order by t.order_time desc ");
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);

		
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	
	/**
	 * 查询购买记录
	 * @param itemid
	 * @param userid
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findTradeInfoByUserId(String tradeId, Integer userid,int pageno,int pagesize){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select t.* from jskj_trade_info t where 1=1 ");
		
		if(tradeId != null && !tradeId.trim().equals("")){
			query.append(" and t.t_id = '").append(tradeId).append("'");
		}
		
		if(userid != null && userid >-1){
			query.append(" and t.user_id=").append(userid);
		}

		query.append(" order by t.order_time desc ");
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);

		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	
	/**
	 * 查询购买记录
	 * @param itemid
	 * @param userid
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findOrderInfoByTradeId(String tradeId){
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select o.* from jskj_trade_order o where 1=1 ");
		
		if(tradeId != null && !tradeId.trim().equals("")){
			query.append(" and o.trade_id='").append(tradeId).append("'");
		}

		query.append(" order by o.to_id desc ");
	
		areas = dataBaseService.executeQuery(query.toString());
		
		return areas;
	}
	
	
	
	public int totalTradeByUserId(String tradeId,Integer userid) {
		List<Map<String,Object>> areas = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select count(t.t_id) cnt from jskj_trade_info t where 1=1 ");
		
		
		if(tradeId != null && !tradeId.trim().equals("")){
			query.append(" and t.t_id=").append(tradeId);
		}
		
		if(userid != null && userid >-1){
			query.append(" and t.user_id=").append(userid);
		}

		areas = dataBaseService.executeQuery(query.toString());
		if(areas != null && areas.size() >0){
			Map<String,Object> map = areas.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
	}
	
	public List<Map<String,Object>> findTradesFromUserId(Integer uid,int pageno,int pagesize){
		List<Map<String,Object>> trades = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_trade_info where user_id=").append(uid);
		query.append(" order by order_time desc ");
		trades = dataBaseService.executeQuery(query.toString());
		
		return trades;
	}
	
	/**
	 * 用户收藏商品
	 * @param item
	 * @param userid
	 * @return
	 */
	public int saveItemCollect(int itemid,int userid,int action){
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_collect where 1=1");
		
		if(itemid >-1){
			query.append(" and item_id=").append(itemid);
		}
		
		if(userid >-1){
			query.append(" and user_id=").append(userid);
		}

		List<Map<String,Object>> collects = dataBaseService.executeQuery(query.toString());
		if(collects != null && collects.size() >0){
			if(action == 1){
				return -1;
			}else {
				StringBuffer sql = new StringBuffer("delete from jskj_item_collect where item_id="+itemid+" and user_id="+userid);
				dataBaseService.execute(sql.toString());
				return 0;
			}
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			StringBuffer sql = new StringBuffer("insert into jskj_item_collect(user_id,item_id,collect_type,collect_time,source)");
			sql.append(" values("+userid+","+itemid+",1,'"+sdf.format(new Date())+"',1)");
			dataBaseService.execute(sql.toString());
			return 0;
			
		}

	}
	
	
	public static String NewSid() {
		Date currDate = new Date();
		if (currDate.toString().equals(LastTimeDate.toString())) {
			theSerialID++;
		} else {
			theSerialID = 1L;
			LastTimeDate = new Date();
		}
		String theSerialIDStr = String.valueOf(theSerialID);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String mDateTime = formatter.format(new Date());
		String SerialIDStr = "";
		for (int i = 0; i < 6 - theSerialIDStr.length(); i++)
			SerialIDStr = (new StringBuilder(String.valueOf(SerialIDStr)))
					.append("0").toString();

		SerialIDStr = (new StringBuilder(String.valueOf(SerialIDStr))).append(
				theSerialIDStr).toString();
		return (new StringBuilder(String.valueOf(mDateTime))).append(
				SerialIDStr).toString();
	}
	
	public static String getPayNo(){
		StringBuffer payno =  new StringBuffer("J");
		String rand = ""+Math.random();
		payno.append(NewSid());
	//	payno.append(rand.substring(2,14));
		return payno.toString();
	}
	
	public String saveTradeInfo(String chid,String recev_name,String recev_mobile,String recev_district,String recev_address,
			Integer pay_type,double coupon,Integer coupon_id,String params,String buyer_message,double amount,String postFee,Integer userid) {
		
		String ghid = getGhid();
		String oid = getPayNo();
		
		Map<String,Object>  insertData = new HashMap<String,Object>();
	
		insertData.put("t_id", ghid);
		insertData.put("channel_code", chid);
		insertData.put("o_id", oid);
		insertData.put("source", 2);
		insertData.put("nums", 1);
		insertData.put("coupon_fee", coupon);
		insertData.put("price", amount);
		insertData.put("post_fee", postFee);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		insertData.put("order_time", sdf.format(new Date()));
		insertData.put("send_name", recev_name);
		insertData.put("recev_name", recev_name);
		insertData.put("recev_mobile", recev_mobile);
		insertData.put("recev_address", recev_address);
		insertData.put("buyer_message", buyer_message);
		insertData.put("pay_type", pay_type);
		
		if(userid != null && userid >0){
			insertData.put("user_id",userid);
		} else {
			insertData.put("user_id",1);
		}
		
		insertData.put("goods_status", 0);
		
		dataBaseService.insertSingleTable("jskj_trade_info", insertData, false, false);
		
		int totalNum = 0;
		String[] paramMap = params.split(",");
		if(paramMap != null && paramMap.length >0){
			for(int i=0;i<paramMap.length;i++){
				String[] comms = paramMap[i].split(":");
				Integer skuId = Integer.parseInt(comms[0]);
				Integer num = Integer.parseInt(comms[1]);
				
				Map<String,Object> sku = findSkuById(skuId);
				Integer itemId = (Integer)sku.get("item_id");
				
				Map<String,Object> skuu = new  HashMap<String,Object>();
				skuu.put("trade_id", ghid);
				skuu.put("item_id", itemId);
				skuu.put("sku_id",skuId);
				skuu.put("num",num);
				
				double price = Double.parseDouble((String)sku.get("price"));
				
				skuu.put("price",price);
				skuu.put("title", sku.get("sku_name"));
				skuu.put("payment", num*price);
				skuu.put("total_fee", num*price);
				skuu.put("item_type", 0);
				skuu.put("promotion", 0);
				skuu.put("discount_fee", 0);
				
				totalNum += num;
				
				dataBaseService.insertSingleTable("jskj_trade_order", skuu, false, false);
				//修改商品的数量
				dataBaseService.executeUpdate("update jskj_item_sku set num = num-"+num);
				dataBaseService.executeUpdate("update jskj_item_list set sold_num = sold_num+1,num = num-"+totalNum+" where item_id="+itemId);

			}
		}
		

		return oid;
	}
	
	public void saveSimpleTrade(String recev_district,String address,String params) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String addr = "未知地区";
		if(address != null && !address.equals("") && address.length() >2){
			if(address.indexOf("区") >0){
				addr = address.substring(0, address.indexOf("区")+1);
			} else if(address.indexOf("市") >0){
				addr = address.substring(0, address.indexOf("市")+1);
			} else {
				addr = addr.substring(0, 2);
			}
		} 
		
		String[] paramMap = params.split(",");
		if(paramMap != null && paramMap.length >0){
			for(int i=0;i<paramMap.length;i++){
				String[] comms = paramMap[i].split(":");
				Integer skuId = Integer.parseInt(comms[0]);
				Integer num = Integer.parseInt(comms[1]);
				
				Map<String,Object> sku = findSkuById(skuId);
				Integer itemId = (Integer)sku.get("item_id");
				String skuName = (String)sku.get("sku_name");
				
				Map<String,Object>  insertData = new HashMap<String,Object>();
				insertData.put("oprtime", sdf.format(new Date()));
				insertData.put("is_system", 0);
				
				insertData.put("item_id", itemId);
				insertData.put("title",skuName);
				insertData.put("address", addr);
				
				dataBaseService.insertSingleTable("jskj_trade_simple", insertData, false, false);
			}
		}

	}

	public String getGhid(){
		
		String cardNo = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_ids where status=0 limit 1");
		List<Map<String,Object>> ids = dataBaseService.executeQuery(query.toString());
		
		if(ids != null && ids.size() >0){
			Map<String,Object> id = ids.get(0);
			cardNo = (String)id.get("card_no");
			
			dataBaseService.executeUpdate("update jskj_ids set status=1 where card_no='"+cardNo+"'");
		}
		
		return cardNo;
	}

	public List<Map<String,Object>> findYunfei(){
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_yunfei");
		
		List<Map<String,Object>> yunfeis = dataBaseService.executeQuery(query.toString());
		
		return yunfeis;
	}
	
	public Map<String,Object> findSkuById(Integer skuid){
		
		StringBuffer query = new StringBuffer();
		query.append("select s.*,t.pic_url from jskj_item_sku s,jskj_item_list t where s.item_id=t.item_id and s.sku_id = ").append(skuid);
		
		List<Map<String,Object>> skus = dataBaseService.executeQuery(query.toString());
		if(skus != null && skus.size() >0){
			return skus.get(0);
		}
		
		return null;
	}
	
	/**
	 * 查询物流快递信息
	 * @param oid
	 * @return
	 */
	public List<Map<String,Object>> findTradeTraces(String oid){
		
		List<Map<String,Object>> traces = null;
		
		StringBuffer query = new StringBuffer();
		query.append(" select * from jskj_trade_trace where trade_id='").append(oid).append("'");
		
		traces = dataBaseService.executeQuery(query.toString());
		
		return traces;
	}
	
	/**
	 * 查询商品当前热点词
	 * @return
	 */
	public List<Map<String,Object>> findKeywords(){
		
		List<Map<String,Object>> traces = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_keyword where status=1");
		
		traces = dataBaseService.executeQuery(query.toString());
		
		return traces;
		
	}
	
	/**
	 * 查询商品当前热点词
	 * @return
	 */
	public int totalKeywords(){
		
		List<Map<String,Object>> traces = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select count(*) cnt from jskj_item_keyword where status=1");
		
		traces = dataBaseService.executeQuery(query.toString());
		if(traces != null && traces.size() >0){
			Map<String,Object> map = traces.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
	}
	/**
	 * 根据订单号查询订单详细
	 * @param oid
	 * @return
	 */
	public Map<String,Object> findTradeByOid(String oid){
		List<Map<String,Object>> traces = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_trade_info where o_id='").append(oid).append("'");
		
		traces = dataBaseService.executeQuery(query.toString());
		if(traces != null && traces.size() >0){
			return traces.get(0);
		}
		return null;
	}
	
	public List<Map<String,Object>> findOrderByTid(String tid){
		StringBuffer query = new StringBuffer();
		query.append("select s.*,i.pic_url,o.num count from jskj_trade_order o,jskj_item_sku s,jskj_item_list i where o.sku_id=s.sku_id and i.item_id=s.item_id and o.trade_id ='").append(tid).append("'");
		return dataBaseService.executeQuery(query.toString());
	}
	
	
	public List<Map<String,Object>> findCouponsFromUserId(Integer userid,Integer useFlag,Integer pageno,Integer pagesize){
		List<Map<String,Object>> coupons = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select u.q_id,u.nums count,u.oprtime,u.status,c.* from jskj_coupon_user u,jskj_coupon c where u.coupon_id=c.coupon_id");
		if(userid != null && userid >0){
			query.append(" and u.user_id=").append(userid);
		}
		
		if(useFlag != null && useFlag >-1){
			query.append(" and u.status=").append(useFlag);
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		coupons = dataBaseService.executeQuery(query.toString());
		
		return coupons;
		
	}
	
	/**
	 * 统计当前的优惠
	 * @param userid
	 * @param useFlag
	 * @return
	 */
	public int totalCouponsFromUserId(Integer userid,Integer useFlag){
		List<Map<String,Object>> coupons = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select count(u.q_id) cnt from jskj_coupon_user u,jskj_coupon c where u.coupon_id=c.coupon_id");
		if(userid != null && userid >0){
			query.append(" and u.user_id=").append(userid);
		}
		
		if(useFlag != null && useFlag >-1){
			query.append(" and u.status=").append(useFlag);
		}
		
		coupons = dataBaseService.executeQuery(query.toString());
		if(coupons != null && coupons.size()>0){
			Map<String,Object> map = coupons.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
		
	}
	
	public void updateTradeComment(String oid,Integer level,String content,String uid){
		
		Map<String,Object> setData = new HashMap<String,Object>();
		
		setData.put("comment_flag", 1);
		setData.put("comment_content", content);
		if(level != null){
			setData.put("comment_level", level);
		} else {
			setData.put("comment_level", 5);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		setData.put("comment_time", sdf.format(new Date()));
		
		Map<String,Object> whereData = new HashMap<String,Object>();
		whereData.put("o_id", oid);
		
		
		dataBaseService.updateTable("jskj_trade_info", setData, whereData);
	}
	
	public void updateOrderStatus(String oid,Integer status) {
		
		Map<String,Object> setData = new HashMap<String,Object>();
		setData.put("goods_status", status);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		setData.put("pay_time", sdf.format(new Date()));
		
		Map<String,Object> whereData = new HashMap<String,Object>();
		whereData.put("o_id",oid);
		
		dataBaseService.updateTable("jskj_trade_info", setData, whereData);
	}
	
	public Map<String,Object> findYunfeiList(String cost) {
		
		StringBuffer query = new StringBuffer();
		query.append(" select * from jskj_yunfei where min_val<=").append(cost);
		query.append(" and max_val>").append(cost);
		
		List<Map<String,Object>> yunfeis =  dataBaseService.executeQuery(query.toString());
		
		if(yunfeis != null && yunfeis.size() >0){
			return yunfeis.get(0);
		}
		
		return null;
	}
	
	
	public int saveItem(DetailItem item) {
		String out_item_id = item.getId();
		String title = item.getTitle();
		String market_price = item.getMarket_price();
		String retail_price = item.getRetail_price();
		String pic_url = item.getImage_grid();
		String pic_thumb_url = item.getImage_list();
		String tag_str = "";
		String[] tags = item.getLabels();
		if(tags != null && tags.length >0){
			tag_str = tags[0];
		}
		Map<String,Object> iitem = findItemByOut( out_item_id) ;
		
		if(iitem == null){
			
			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("out_item_id", out_item_id);
			insertData.put("title", title);
			insertData.put("origin_price", market_price);
			insertData.put("price", retail_price);
			insertData.put("pic_url", pic_url);
			insertData.put("pic_thumb_url", pic_thumb_url);
			insertData.put("tag_str", tag_str);
			insertData.put("remark", title);
			insertData.put("sold_num", 0);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String created = sdf.format(new Date());
			insertData.put("created", created);
			
			insertData.put("sold_weight", 0);
			insertData.put("recomm_weight", 0);
			insertData.put("detail_url", item.getDetail_url());
			insertData.put("status", 1);
			insertData.put("num", 100000);
			
			int item_id = dataBaseService.insertSingleTable("jskj_item_list", insertData, true, true);
			
			return item_id;
			
		} else {
			Integer item_id = (Integer)iitem.get("item_id");
			return item_id;
			
		}
		
	}
	
	public Map<String,Object> findItemByOut(String out_item_id) {
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_list where out_item_id='").append(out_item_id).append("'");
		
		List<Map<String,Object>> items = dataBaseService.executeQuery(query.toString());
		if(items != null && items.size() >0){
			return items.get(0);
		}
		
		return null;
		
	}
	
	
	
	
	
	public void saveItemTag(Integer item_id,String[] tags) {
		if( tags != null && tags.length >0 ){
			for(int i=0; i<tags.length; i++){
				Map<String,Object> tag =  findTagByName(tags[i]);
				Integer tag_id = null;
				if(tag == null){
					tag_id = saveTag(tags[i],0,0,1,"");
				} else {
					tag_id = (Integer)tag.get("id");
				}
				
				Map<String,Object> item_tag = findItemTag( item_id, tag_id);
				
				if(item_tag == null){
					
					Map<String,Object> insertData = new HashMap<String,Object>();
					insertData.put("item_id", item_id);
					insertData.put("tag_id", tag_id);
					insertData.put("hot_flag", 0);
					insertData.put("weight", 0);
					
					dataBaseService.insertSingleTable("jskj_item_tag", insertData, false, false);
					
				}

			}
		}
	}
	
	public Map<String,Object> findItemTag(Integer item_id,Integer tag_id) {
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_tag where item_id=").append(item_id).append(" and tag_id=").append(tag_id);
		
		List<Map<String,Object>> tags = dataBaseService.executeQuery(query.toString());
		if(tags != null && tags.size() >0){
			return tags.get(0);
		}
		
		return null;
	}
	
	public void savePicture(Integer item_id,String pic_url,Integer ptype,String out_image_id){

		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_picture where out_image_id='").append(out_image_id).append("'");
		List<Map<String,Object>>  pictures= dataBaseService.executeQuery(query.toString());
		if(pictures == null || pictures.size() == 0) {
			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("item_id", item_id);
			insertData.put("pic_url", pic_url);
			insertData.put("ptype", ptype);
			insertData.put("out_image_id", out_image_id);
			insertData.put("weight", 0);
			
			dataBaseService.insertSingleTable("jskj_item_picture", insertData, false, false);
		}

	}

	public int saveTag(String name,Integer hotflag,Integer recommflag,Integer status,String picurl){
		
		Map<String,Object> insertData = new HashMap<String,Object>();
		insertData.put("tag_name", name);
		insertData.put("tag_remark", name);
		insertData.put("hot_level", hotflag);
		insertData.put("status", status);
		insertData.put("recomm_level",recommflag);
		insertData.put("pic_url",picurl);
		
		int tag_id = dataBaseService.insertSingleTable("jskj_tag_list", insertData, true, false);
		
		return tag_id;
	}
	
	public Map<String,Object> findTagByName(String name){
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_tag_list where tag_name='").append(name).append("'");
		
		List<Map<String,Object>> tags =  dataBaseService.executeQuery(query.toString());
		if(tags != null && tags.size() >0){
			return tags.get(0);
		} else {
			return null;
		}
		
	}
	
	public void updateTag(Integer tagid,String name,String picurl,Integer recommflag,Integer hotlevel,Integer status){
		
		Map<String,Object> setData = new HashMap<String,Object>();
		setData.put("tag_name", name);
		setData.put("tag_remark", name);
		setData.put("pic_url", picurl);
		setData.put("recomm_level", recommflag);
		setData.put("hot_level", hotlevel);
		setData.put("status", status);
		
		Map<String,Object> whereData = new HashMap<String,Object>();
		whereData.put("id", tagid);
		
		dataBaseService.updateTable("jskj_tag_list", setData, whereData);
	}
	
	public void saveSku(Integer item_id,ItemSku sku) {
		String sku_name = sku.getName();
		String value = sku.getValue();
		String market_price = sku.getMarket_price();
		String retail_price = sku.getRetail_price();
		String out_sku_id = sku.getSkuid();
		
		Map<String,Object> ssku = findSku( out_sku_id);
		
		if(ssku == null){
			
			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("out_sku_id", out_sku_id);
			insertData.put("origin_price", market_price);
			insertData.put("price", retail_price);
			insertData.put("sku_name", value.replaceAll("'", "‘"));
			insertData.put("sku_remark", value);
			insertData.put("num", 100000);
			insertData.put("item_id", item_id);
			
			dataBaseService.insertSingleTable("jskj_item_sku", insertData, false, false);
			
		}
	}
	
	public Map<String,Object> findSku(String out_sku_id){
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_sku where out_sku_id='").append(out_sku_id).append("'");
		
		List<Map<String,Object>> skus = dataBaseService.executeQuery(query.toString());
		if(skus != null && skus.size() >0){
			return skus.get(0);
		}
		
		return null;
		
	}
	
	public void saveAd(String outid,Integer adtype,String contype,String picurl,Integer weight,Integer status,Integer bindflag,String bindurl,String name,String remark){
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_ad where name ='").append(name).append("'");
		
		List<Map<String,Object>> ads = dataBaseService.executeQuery(query.toString());
		if( ads == null || ads.isEmpty() ){
			
			if(contype != null && contype.equals("Article")){
				Map<String,Object> insertData = new HashMap<String,Object>();
				insertData.put("ad_type", adtype);
				insertData.put("pic_url", picurl);
				insertData.put("weight", weight);
				insertData.put("status",status);
				insertData.put("name", name);
				insertData.put("remark", name);
				insertData.put("bind_flag", 1);
				
				ArticleService articleService = (ArticleService) BeanFactory.getBean("articleService");
				logger.info("outid:"+outid);
				Map<String,Object> article = articleService.findArticleByOut(outid);
				
				insertData.put("bind_id", (Integer)article.get("article_id"));
				dataBaseService.insertSingleTable("jskj_ad", insertData, true, false);
				
			} else if(contype != null && contype.equals("Product")) {
				
				Map<String,Object> insertData = new HashMap<String,Object>();
				insertData.put("ad_type", adtype);
				insertData.put("pic_url", picurl);
				insertData.put("weight", weight);
				insertData.put("status",status);
				insertData.put("name", name);
				insertData.put("remark", name);
				insertData.put("bind_flag", 1);

				Map<String,Object> item = this.findItemByOut(outid);
				insertData.put("bind_id", (Integer)item.get("item_id"));
				dataBaseService.insertSingleTable("jskj_ad", insertData, true, false);

			} else if(contype != null && contype.equals("Tag")) {
				
				Map<String,Object> insertData = new HashMap<String,Object>();
				insertData.put("ad_type", adtype);
				insertData.put("pic_url", picurl);
				insertData.put("weight", weight);
				insertData.put("status",status);
				insertData.put("name", name);
				insertData.put("remark", name);
				insertData.put("bind_flag", 1);

				Map<String,Object> tag = this.findTagByName(name);
				insertData.put("bind_id", (Integer)tag.get("id"));
				dataBaseService.insertSingleTable("jskj_ad", insertData, true, false);
			}
			
		}
		
	}
	

	
	public void saveTag(String tagname,String picurl){
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_tag_list where tag_name='").append(tagname).append("'");
		List<Map<String,Object>> tags = dataBaseService.executeQuery(query.toString());
		if(tags != null && tags.size() >0){
			Map<String,Object> setData = new HashMap<String,Object>();
			setData.put("pic_url", picurl);
			setData.put("weight", System.currentTimeMillis()/1000);
			
			Map<String,Object> tag = tags.get(0);
			
			Map<String,Object> whereData = new HashMap<String,Object>();
			whereData.put("tag_id", tag.get("tag_id"));
			
			dataBaseService.updateTable("jskj_tag_list", setData, whereData);
		}
	}	
		
	public void saveItem(JskjArea area) {
			
			if(area == null) return;
			
			String image = area.getImage();
			String title = area.getTitle();
			String out_id = area.getOut_id();
			String type = area.getType();
			JSONObject[] items  = area.getItems();
			
			StringBuffer query = new StringBuffer();
			query.append("select * from jskj_area where area_name='").append(title).append("'");
			
			List<Map<String,Object>> areas = dataBaseService.executeQuery(query.toString());
			if( areas != null && areas.size() > 0 ){
				Map<String,Object> curr = areas.get(0);
				Integer areaid = (Integer)curr.get("area_id");
				
				Map<String,Object> setData = new HashMap<String,Object>();
				setData.put("is_top", 1);
				
				Map<String,Object> whereData = new HashMap<String,Object>();
				whereData.put("area_id", areaid);
			} else {
				
				if(type != null && type.equals("Article")){
					
				}
				
			}

		
	}
	
	public int saveItemCategory(String name,String image,int weight){
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_category where name='").append(name).append("'");
		
		List<Map<String,Object>> categorys = dataBaseService.executeQuery(query.toString());
		if(categorys == null || categorys.isEmpty()){
			
			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("name", name);
			insertData.put("pic_url", image);
			insertData.put("status", 0);
			insertData.put("weight", weight);
			
			return dataBaseService.insertSingleTable("jskj_category", insertData, true, false);
			
		} else {
			Map<String,Object> category = categorys.get(0);
			return (Integer)category.get("c_id");
		}
		
	}
	
	public int saveArea(String name,Integer istop,Integer isrecomm,Integer weight,String picurl,Integer status,String url,String lintype,Integer linkid){
		int result = 0;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_area where area_name='").append(name).append("'");
		
		List<Map<String,Object>> areas = dataBaseService.executeQuery(query.toString());
		if(areas  == null || areas.isEmpty()) {
			
			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("area_name", name);
			insertData.put("remark", name);
			insertData.put("is_top", istop);
			insertData.put("is_recomm", isrecomm);
			insertData.put("weight", weight);
			insertData.put("pic_url", picurl);
			insertData.put("status", 1);
			if(url != null && !url.equals("")){
				insertData.put("url", url);
			}
			
			insertData.put("show_type", 1);
			insertData.put("link_type", lintype);
			
			if(linkid != null && linkid>0){
				insertData.put("link_id", linkid);
			}
			
			result = dataBaseService.insertSingleTable("jskj_area", insertData, true, false);
			
		} else {
			Map<String,Object> area = areas.get(0);
			result = (Integer)area.get("area_id");
		}
		
		return result;
	}
	
	public int saveAreaItem(Integer areaid,Integer itemid,String itemtitle,String picurl,Integer weight,Integer istop,Integer status) {
		int result = 0;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_area_item where area_id=").append(areaid).append(" and item_id=").append(itemid);
		
		List<Map<String,Object>> areas = dataBaseService.executeQuery(query.toString());
		if( areas == null || areas.isEmpty() ){
			
			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("area_id",areaid);
			insertData.put("item_id", itemid);
			insertData.put("item_title", itemtitle);
			insertData.put("pic_url", picurl);
			insertData.put("weight", weight);
			insertData.put("is_top", istop);
			insertData.put("status", status);

			result = dataBaseService.insertSingleTable("jskj_area_item", insertData, true, false);
			
		} else {
			Map<String,Object> area = areas.get(0);
			result = (Integer)area.get("ai_id");
		}
		
		return result;
	}
	
	/**
	 * 查询优惠码列表
	 * @param num 热门标签数量
	 * @return
	 */
	public List<Map<String,Object>> findCouponCode(String code){
		List<Map<String,Object>> tags = null;
	
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_coupon_code where status=1 and coupon_code='").append(code).append("'");
		
		tags = dataBaseService.executeQuery(query.toString());
		
		return tags;
	}
	
	public void updateCouponStatus(String ccode) {
		StringBuffer query= new StringBuffer();
		query.append("update jskj_coupon_code set status = 0 where coupon_code='").append(ccode).append("'");
		
		dataBaseService.executeUpdate(query.toString());
		
	}
	
}