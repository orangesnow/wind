
package com.jskj.shop.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.dao.util.DataBaseService;

public class AppVersionService {
	
	private DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");
	private static final Logger logger = Logger.getLogger(AppVersionService.class);
	
	public Map<String,Object> findAppVersion(String vercode){
		Map<String,Object> version = null;
		logger.info("vercode:"+vercode);
		
		StringBuffer query = new StringBuffer();
		query.append(" select * from jskj_version where version_code >").append(vercode);
		
		List<Map<String,Object>> versions = dataBaseService.executeQuery(query.toString());
		if(versions != null && versions.size() >0){
			return versions.get(0);
		}
		return version;
	}

	public int insertAdvice(String nickname,String mobile,String content ,Map<String,String> headers){
		int result = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd H:mm:ss"); 
		StringBuffer query= new StringBuffer();
		query.append("insert into jskj_advice(content,nickname,mobile,oprtime) values(");
		query.append("'").append(content).append("','").append(nickname).append("','").append(mobile).append("','").append(sdf.format(new Date())).append("')");
		
		logger.info("query:"+query.toString());
		result = dataBaseService.insert(query.toString());
		return result;
	}

}
