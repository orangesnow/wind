
package com.jskj.shop.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.dao.util.DataBaseService;
import com.jskj.shop.util.SysConstants;
import com.jskj.shop.util.Tools;

/**
 * 用户服务接口
 * @author VAIO
 * @since 2015-11-22
 */
public class UserService {
	
	private DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");
	private static final Logger logger = Logger.getLogger(UserService.class);
	
	/**
	 * 保存用户信息
	 * @param nickname
	 * @param gender
	 * @param icon
	 * @param mobile
	 * @return
	 */
	public int saveUser(String nickname,Integer gender,String icon,String mobile,Map<String,String> headers){
		Map<String,Object> insertData = new HashMap<String,Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		insertData.put("gender", gender);
		insertData.put("head_img_url", icon);
		insertData.put("source", 2);
		insertData.put("nickname", nickname);
		insertData.put("login_time", sdf.format(new Date()));
		if(mobile != null && !mobile.equals("")){
			insertData.put("mobile", mobile);
		}

		String devicenum = headers.get(SysConstants.HEADER_DEVICENUM);
		String platform = headers.get(SysConstants.HEADER_PLATFORM);
		String code = headers.get(SysConstants.HEADER_CODE);
		String mobiletype = headers.get(SysConstants.HEADER_MOBILETYPE);
		String appver =  headers.get(SysConstants.HEADER_APPVERSION);
		String version =   headers.get(SysConstants.HEADER_VERSION);
		
		insertData.put("client_platform", platform);
		insertData.put("client_app_version", appver);
		insertData.put("client_deviceno",devicenum);
		insertData.put("client_mobiletype", mobiletype);
		insertData.put("client_version",version);
		
		int result = dataBaseService.insertSingleTable("jskj_user", insertData, true, true);
		return result;
	}
	

	public int getUserFromWeb(){
		
		Map<String,Object> insertData = new HashMap<String,Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		insertData.put("gender", 1);
		insertData.put("head_img_url", null);
		insertData.put("source", 1);
		insertData.put("nickname", Tools.getUserNickname());
		insertData.put("login_time", sdf.format(new Date()));
		insertData.put("mobile", 0);

		int result = dataBaseService.insertSingleTable("jskj_user", insertData, true, true);
		return result;
	}
	
	/**
	 * 更新用户基本资料
	 * @param uid
	 * @param nickname
	 * @param gender
	 * @param icon
	 * @param mobile
	 * @return
	 */
	public int updateUser(Integer uid,String nickname,Integer gender,String icon,String mobile){
		
		Map<String,Object> setData = new HashMap<String,Object>();
		if(nickname !=null && nickname.equals("")){
			setData.put("nickname", nickname);
		}
		
		if(gender >-1){
			setData.put("gender", gender);
		}
		
		if(icon != null && !icon.equals("")){
			setData.put("icon", icon);
		}
		
		if(mobile != null && !mobile.equals("")){
			setData.put("mobile", mobile);
		}
		
		Map<String,Object> whereData = new HashMap<String,Object>();
		whereData.put("user_id", uid);
		
		dataBaseService.updateTable("jskj_user", setData, whereData);
		
		return uid;
	}

	/**
	 * 更新用户邮寄地址
	 * @param uid
	 * @param nickname
	 * @param gender
	 * @param icon
	 * @param mobile
	 * @return
	 */
	public int updateUser(Integer uid,String contact,String name,String address){
		
		Map<String,Object> setData = new HashMap<String,Object>();
		if(contact !=null && contact.equals("")){
			setData.put("contact", contact);
		}
		
		if(name != null && !name.equals("")){
			setData.put("name", name);
		}
		
		if(address != null && !address.equals("")){
			setData.put("address", address);
		}
		
		Map<String,Object> whereData = new HashMap<String,Object>();
		whereData.put("user_id", uid);
		
		dataBaseService.updateTable("jskj_user", setData, whereData);
		
		return uid;
	}
	
	/**
	 * 根据用户ID查询当前用户信息
	 * @param uid
	 * @return
	 */
	public Map<String,Object> findUserById(Integer uid){
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_user where user_id=").append(uid);
		
		List<Map<String,Object>> users = dataBaseService.executeQuery(query.toString());
		if(users != null && users.size()>0){
			return users.get(0);
		}
		
		return null;
	}
	
	/**
	 * 根据用户ID查询当前用户信息
	 * @param uid
	 * @return
	 */
	public Map<String,Object> findUserById(String uid){
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_user where user_id=").append(uid);
		
		List<Map<String,Object>> users = dataBaseService.executeQuery(query.toString());
		if(users != null && users.size()>0){
			return users.get(0);
		}
		
		return null;
	}
	
	/**
	 * 根据用户key查询当前用户信息
	 * @param uid
	 * @return
	 */
	public Map<String,Object> findUserByKey(String key){
		
		
		if(key == null || key.equals("")){
			logger.info("user key is not exist...");
			return null;
		}
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_user where mkey='").append(key).append("'");
		
		List<Map<String,Object>> users = dataBaseService.executeQuery(query.toString());
		if(users != null && users.size()>0){
			return users.get(0);
		}
		
		return null;
	}
	
	/**
	 * 查询当前用户收藏的商品
	 * @param userid
	 * @return
	 */
	public List<Map<String,Object>> findCollectItems(Integer userid,int collectType,int pageno,int pagesize){
		StringBuffer query = new StringBuffer();
		query.append("select i.* from jskj_item_list i,jskj_item_collect c where 1=1");
		query.append(" and i.item_id=c.item_id");
		
		if(collectType >0){
			query.append(" and c.collect_type=").append(collectType);
		}
		
		query.append(" and user_id=").append(userid);
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		List<Map<String,Object>> collects = dataBaseService.executeQuery(query.toString());
		return collects;
		
	}
	
	public List<Map<String,Object>> findCollectArticles(Integer userid,int collectType,int pageno,int pagesize) {
		StringBuffer query = new StringBuffer();
		query.append("select i.* from jskj_article i,jskj_item_collect c where 1=1");
		query.append(" and i.article_id=c.item_id");
		
		if(collectType >0){
			query.append(" and c.collect_type=").append(collectType);
		}
		
		query.append(" and user_id=").append(userid);
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		List<Map<String,Object>> collects = dataBaseService.executeQuery(query.toString());
		return collects;
	}
	
	public int totalCollectArticles(Integer userid,int collectType) {
		StringBuffer query = new StringBuffer();
		query.append("select count(i.article_id) cnt from jskj_article i,jskj_item_collect c where 1=1");
		query.append(" and i.article_id=c.item_id");
		
		if(collectType >0){
			query.append(" and c.collect_type=").append(collectType);
		}
		
		query.append(" and user_id=").append(userid);
		
		List<Map<String,Object>> collects = dataBaseService.executeQuery(query.toString());
		if(collects != null && collects.size() >0){
			Map<String,Object> collect = collects.get(0);
			return (Integer)collect.get("cnt");
		}
		return 0;
	}

	/**
	 * 统计用户收藏的记录总数
	 * @param userid
	 * @param collectType
	 * @return
	 */
	public int totalCollectItems(Integer userid,int collectType) {
		StringBuffer query = new StringBuffer();
		query.append("select count(i.item_id) cnt from jskj_item_list i,jskj_item_collect c where 1=1");
		query.append(" and i.item_id=c.item_id");
		
		if(collectType >0){
			query.append(" and c.collect_type=").append(collectType);
		}
		
		query.append(" and user_id=").append(userid);

		List<Map<String,Object>> collects = dataBaseService.executeQuery(query.toString());
		if(collects != null && collects.size() >0){
			Map<String,Object> collect = collects.get(0);
			return (Integer)collect.get("cnt");
		}
		
		return 0;
		
	}
	
	/**
	 * 查询当前用户的可用优惠券
	 * @param userId
	 * @param userFlag
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findCouponsFromUserId(Integer userid,Integer status,int pageno,int pagesize){
		List<Map<String,Object>> coupons = null;
		StringBuffer query = new StringBuffer();
		
		query.append("select * from jskj_coupon_user where 1=1");
		query.append(" and user_id").append(userid);
		if(status >-1){
			query.append(" and status = ").append(status);
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		coupons = dataBaseService.executeQuery(query.toString());
		return coupons;
		
	}
	
	/**
	 * 统计用户可用券
	 * @param userid
	 * @param status
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public int totalCouponsFromUserId(Integer userid,Integer status,int pageno,int pagesize){
		List<Map<String,Object>> coupons = null;
		StringBuffer query = new StringBuffer();
		
		query.append("select count(*) cnt from jskj_coupon_user where 1=1");
		query.append(" and user_id").append(userid);
		if(status >-1){
			query.append(" and status = ").append(status);
		}
		
		coupons = dataBaseService.executeQuery(query.toString());
		if(coupons != null && coupons.size() >0){
			Map<String,Object> coupon = coupons.get(0);
			return (Integer)coupon.get("cnt");
		}
		
		return 0;
	}

	public Map<String,Object> findUserByDeviceNo(String device) {

		StringBuffer query = new StringBuffer();
		query.append(" select * from jskj_user where client_deviceno='").append(device).append("'");
		
		List<Map<String,Object>> users = dataBaseService.executeQuery(query.toString());
		if(users != null && users.size() >0){
			return users.get(0);
		}
		
		return null;
		
	}
	
	
	
	
}
