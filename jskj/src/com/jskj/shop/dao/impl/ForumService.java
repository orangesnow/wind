
package com.jskj.shop.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.dao.util.DataBaseService;
import com.jskj.shop.util.SysConstants;

/**
 * 论坛服务接口
 * @author VAIO
 * @since 2015-11-22
 */
public class ForumService {
	
	private DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");
	private static final Logger logger = Logger.getLogger(ForumService.class);
	
	/**
	 * 添加帖子
	 * @param content
	 * @param location
	 * @param groupid
	 * @param status
	 * @param attachments
	 * @param headers
	 * @return
	 */
	public int saveSubject(String content,String location,Integer groupid,Integer status,String[] attachments,Integer userid,Map<String,String> headers){
		Map<String,Object> insertData = new HashMap<String,Object>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		insertData.put("content", content);
		if(location != null && !location.equals("")){
			insertData.put("location", location);
		}

		if(groupid != null && groupid >-1){
			insertData.put("group_id", groupid);
		}

		insertData.put("status", status);
		insertData.put("oprtime",sdf.format(new Date()));
		
		insertData.put("is_top", 0);
		insertData.put("is_recomm", 0);
		insertData.put("is_tag", 0);
		
		if(attachments != null && attachments.length >0){
			insertData.put("pic_url", attachments[0]);
		}

		insertData.put("user_id", userid);
		
		int result = dataBaseService.insertSingleTable("jskj_subject", insertData, true, true);
		
		if(attachments != null && attachments.length >0){
			
			for(int i=0;i<attachments.length;i++){
				Map<String,Object> insertSubData = new HashMap<String,Object>();
				insertSubData.put("subject_id", result);
				insertSubData.put("pic_url", attachments[i]);
				insertSubData.put("weight", i*10);
				dataBaseService.insertSingleTable("jskj_subject_picture", insertSubData, false, true);
			}
			
		}

		Map<String,Object> insertTotalData = new HashMap<String,Object>();
		insertTotalData.put("subject_id", result);
		insertTotalData.put("agrees", 0);
		insertTotalData.put("replys", 0);
		insertTotalData.put("shares", 0);
		insertTotalData.put("follows", 0);
		dataBaseService.insertSingleTable("jskj_subject_total", insertTotalData, false, true);

		return result;
	}
	
	
	/**
	 * 查询当前主题的所有图片
	 * @param subjectId
	 * @return
	 */
	public Map<String,Object> findSubjectById(Integer subjectId){
		List<Map<String,Object>> subjects = null;
		StringBuffer query = new StringBuffer();
		
		query.append("select s.*,u.nickname,u.head_img_url,u.subjects from jskj_subject s, jskj_user u where s.user_id=u.user_id ");
		if(subjectId != null){
			query.append(" and subject_id=").append(subjectId);
		}

		subjects = dataBaseService.executeQuery(query.toString());
		if(subjects != null && subjects.size() >0){
			return subjects.get(0);
		}
		return null;
	}
	
	/**
	 * 查询当前主题的所有图片
	 * @param subjectId
	 * @return
	 */
	public List<Map<String,Object>> findSubjectPictures(Integer subjectId){
		List<Map<String,Object>> pictures = null;
		StringBuffer query = new StringBuffer();
		
		query.append("select * from jskj_subject_picture where 1=1");
		if(subjectId != null){
			query.append(" and subject_id=").append(subjectId);
		}
		
		query.append(" order by weight asc");
		
		pictures = dataBaseService.executeQuery(query.toString());
		
		return pictures;
	}
	
	/**
	 * 添加帖子回复
	 * @param content
	 * @param location
	 * @param groupid
	 * @param status
	 * @param attachments
	 * @param headers
	 * @return
	 */
	public int saveReply(String content,Integer replyId,Integer subjectId,Map<String,String> headers){
		Map<String,Object> insertData = new HashMap<String,Object>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		if(replyId != null){
			Map<String,Object> reply = findSubjectReply(replyId,headers);
			Integer replyRootId = (Integer)reply.get("root_id");
			Integer replySubjectId = (Integer)reply.get("subject_id");
			
			if(replyRootId != null){
				insertData.put("root_id", replyRootId);
			}else {
				insertData.put("root_id", replyId);
			}
			
			insertData.put("subject_id", replySubjectId);
			
		} else {
			insertData.put("subject_id", subjectId);
		}

		insertData.put("content", content);
		insertData.put("oprtime",sdf.format(new Date()));
		
		String uid = headers.get(SysConstants.HEADER_UID);
		insertData.put("user_id", uid);
		
		int result = dataBaseService.insertSingleTable("jskj_subject_reply", insertData, true, true);
		return result;
	}
	
	public List<Map<String,Object>> findSubjectReplys(Integer subjectid,int pageno,int pagesize){
		List<Map<String,Object>>  replys = null;
		StringBuffer query = new StringBuffer();
		
		query.append("select r.*,u.nickname,u.head_img_url,u.subjects from jskj_subject_reply r,jskj_user u where r.user_id=u.user_id and r.subject_id=").append(subjectid);
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		replys = dataBaseService.executeQuery(query.toString());
		return replys;
	}
	
	public int totalSubjectReplys(Integer subjectid){
		int count = 0;
		List<Map<String,Object>>  replys = null;
		StringBuffer query = new StringBuffer();
		
		query.append("select count(*) cnt from jskj_subject_reply where subject_id=").append(subjectid);
		
		replys = dataBaseService.executeQuery(query.toString());
		if(replys != null && replys.size()>0){
			Map<String,Object> map = replys.get(0);
			return (Integer)map.get("cnt");
		}

		return count;
	}
	
	
	/**
	 * 保存关注数据
	 * @param subjectid
	 * @param uid
	 */
	public void saveFollowSubject(Integer subjectid,Integer uid){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Map<String,Object> insertData = new HashMap<String,Object>();
		insertData.put("subject_id", subjectid);
		insertData.put("user_id", uid);
		insertData.put("action_type", 1);
		insertData.put("oprtime", sdf.format(new Date()));
		
		dataBaseService.insertSingleTable("jskj_subject_action", insertData, false, true);
		
		StringBuffer query = new StringBuffer();
		query.append(" update jskj_subject_total set follows = follows+1 where subject_id=").append(subjectid);
		dataBaseService.execute(query.toString());
		
	}
	
	/**
	 * 保存关注数据
	 * @param subjectid
	 * @param uid
	 */
	public void saveReportSubject(Integer subjectid,Integer replyid,Integer uid){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Map<String,Object> insertData = new HashMap<String,Object>();
		insertData.put("subject_id", subjectid);
		insertData.put("user_id", uid);
		insertData.put("reply_id", replyid);

		dataBaseService.insertSingleTable("jskj_subject_report", insertData, false, true);

	}
	
	/**
	 * 保存关注数据
	 * @param subjectid
	 * @param uid
	 */
	public void saveAgreeSubject(Integer subjectid,Integer uid){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Map<String,Object> insertData = new HashMap<String,Object>();
		insertData.put("subject_id", subjectid);
		insertData.put("user_id", uid);
		insertData.put("action_type", 2);
		insertData.put("oprtime", sdf.format(new Date()));
		
		dataBaseService.insertSingleTable("jskj_subject_action", insertData, false, true);
		
		StringBuffer query = new StringBuffer();
		query.append(" update jskj_subject_total set agrees = agrees+1 where subject_id=").append(subjectid);
		dataBaseService.execute(query.toString());

	}
	
	/**
	 * 保存关注数据
	 * @param subjectid
	 * @param uid
	 */
	public void saveShareSubject(Integer subjectid,Integer uid){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Map<String,Object> insertData = new HashMap<String,Object>();
		insertData.put("subject_id", subjectid);
		insertData.put("user_id", uid);
		insertData.put("action_type", 3);
		insertData.put("oprtime", sdf.format(new Date()));
		
		dataBaseService.insertSingleTable("jskj_subject_action", insertData, false, true);
		
		StringBuffer query = new StringBuffer();
		query.append(" update jskj_subject_total set shares = shares+1 where subject_id=").append(subjectid);
		dataBaseService.execute(query.toString());

	}
	
	/**
	 * 删除关注信息
	 * @param subjectid
	 * @param userid
	 */
	public void deleteFollowSubject(Integer subjectid,Integer userid){
		
		StringBuffer query = new StringBuffer();
		query.append(" delete from jskj_subject_action where subject_id=").append(subjectid);
		query.append(" and user_id=").append(userid);
		
		Map<String,Object>  result= dataBaseService.execute(query.toString());
		if(result != null){
			StringBuffer totalQuery = new StringBuffer();
			totalQuery.append("select * from jskj_subject_total where subject_id=").append(subjectid);
			List<Map<String,Object>> totals = dataBaseService.executeQuery(totalQuery.toString());
			if(totals != null && totals.size()>0){
				Map<String,Object> total = totals.get(0);
				Integer agrees = (Integer)total.get("agrees");
				StringBuffer updateQuery = new StringBuffer();
				
				if(agrees != null &&  agrees >0){
					updateQuery.append("update jskj_subject_total set agrees = agrees-1 where subject_id=").append(subjectid);
					dataBaseService.execute(updateQuery.toString());
				} else {
					updateQuery.append("update jskj_subject_total set agrees = 0 where subject_id=").append(subjectid);
					dataBaseService.execute(updateQuery.toString());
				}
			}
		}
		
	}
	
	
	/**
	 * 删除帖子
	 * @param subjectid
	 */
	public void deleteSubject(Integer subjectid,Map<String,String> headers){

		StringBuffer deletePicture=new StringBuffer();
		deletePicture.append("delete from jskj_subject_picture where subject_id=").append(subjectid);
		dataBaseService.execute(deletePicture.toString());
		
		StringBuffer deleteReply = new StringBuffer();
		deleteReply.append("delete from jskj_reply where subject_id=").append(subjectid);
		dataBaseService.execute(deleteReply.toString());
		
		StringBuffer deleteReport = new StringBuffer();
		deleteReport.append("delete from jskj_subject_report where subject_id").append(subjectid);
		dataBaseService.execute(deleteReport.toString());
		
		StringBuffer query = new StringBuffer();
		query.append("delete from jskj_subject where subject_id=").append(subjectid);
		dataBaseService.execute(query.toString());
		
		return ;
	}
	
	/**
	 * 根据ID查询当前回复信息
	 * @param replyId
	 * @return
	 */
	public Map<String,Object> findSubjectReply(Integer replyId,Map<String,String> headers){
		StringBuffer query = new StringBuffer();
		query.append("select r.*,u.nickname,u.head_img_url,u.subjects from jskj_subject_reply r,jskj_user u where r.user_id= u.user_id and reply_id=").append(replyId);
		List<Map<String,Object>> replys = dataBaseService.executeQuery(query.toString());
		
		if(replys != null && replys.size() >0){
			return replys.get(0);
		}
		
		return null;
	}
	
	/**
	 * 根据ID查询当前回复信息
	 * @param replyId
	 * @return
	 */
	public List<Map<String,Object>> findFollowSubject(Integer userid,Map<String,String> headers,int pageno,int pagesize){
		StringBuffer query = new StringBuffer();
		query.append("select a.oprtime follow_time,s.*,u.subjects,u.nickname,u.head_img_url from jskj_subject_action a,jskj_subject s,jskj_user u ");
		query.append("where s.user_id=u.user_id and a.subject_id=s.subject_id and a.user_id=").append(userid);
		query.append(" order by a.oprtime desc");
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		List<Map<String,Object>> replys = dataBaseService.executeQuery(query.toString());
	
		return replys;

	}
	
	public List<Map<String,Object>> findFollowSubject(String ids,Integer userid) {
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_subjetc_action where action_type=1 and userid=").append(userid);
		query.append(" and subject_id in (").append(ids).append(")");

		List<Map<String,Object>> actions = dataBaseService.executeQuery(query.toString());
		
		return actions;
	}
	
	
	/**
	 * 根据ID查询当前回复信息
	 * @param replyId
	 * @return
	 */
	public int tolalFollowSubject(Integer userid,Map<String,String> headers){
		StringBuffer query = new StringBuffer();
		query.append("select count(a.subject_id) cnt from jskj_subject_action a,jskj_subject s where a.subject_id=s.subject_id and a.user_id=").append(userid);
		List<Map<String,Object>> replys = dataBaseService.executeQuery(query.toString());
		
		if(replys != null && replys.size() >0){
			Map<String,Object> follow = replys.get(0);
			return (Integer)follow.get("cnt");
		}
		
		return 0;
	}
	
	
	
	/**
	 * 删除回复
	 * @param replyId
	 */
	public void deleteReplyById(Integer replyId,Map<String,String> headers){
		
		StringBuffer subQuery = new StringBuffer();
		subQuery.append("delete from jskj_subject_reply where root_id=").append(replyId);
		dataBaseService.execute(subQuery.toString());
		
		StringBuffer query = new StringBuffer();
		query.append("delete from jskj_subject_reply where reply_id=").append(replyId);
		dataBaseService.execute(query.toString());
		
	}
	
	/**
	 * 查询可用小组
	 * @return
	 */
	public List<Map<String,Object>> findSubjectGroup(Integer uid,int pageno,int pagesize){
		List<Map<String,Object>>  groups = null;
		
		StringBuffer query = new StringBuffer();
		
		if(uid != null){
			query.append("select g.* from jskj_subject_group g,jskj_subject_group_user u where g.group_id=u.group_id and g.status=1 ");
			query.append(" and u.user_id=").append(uid);
		} else {
			query.append("select * from jskj_subject_group g where g.status=1 ");
		}
		
		query.append(" order by g.weight asc");
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		logger.info("query.toString():"+query.toString());
		groups = dataBaseService.executeQuery(query.toString());
		
		return groups;
	}
	
	/**
	 * 查询可用小组
	 * @return
	 */
	public int  totalSubjectGroup(Integer uid){
		List<Map<String,Object>>  groups = null;
		
		StringBuffer query = new StringBuffer();
		
		if(uid != null){
			query.append("select count(g.group_id) cnt from jskj_subject_group g,jskj_subject_group_user u where g.group_id=u.group_id and g.status=1 ");
			query.append(" and u.user_id=").append(uid);
		} else {
			query.append("select count(*) cnt from jskj_subject_group where status=1 ");
		}

		groups = dataBaseService.executeQuery(query.toString());
		if(groups != null && groups.size() >0){
			Map<String,Object> map = groups.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
	}
	
	/**
	 * 用户进出论坛小组
	 * @param userid
	 * @param action
	 */
	public void enterSubjectGroup(Integer userid,Integer groupid,Integer action){

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		if(action == 1){
			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("user_id", userid);
			insertData.put("group_id", groupid);
			insertData.put("oprtime", sdf.format(new Date()));
			
			dataBaseService.insertSingleTable("jskj_subject_group_user", insertData, true, true);
		} else {
			StringBuffer query = new StringBuffer();
			query.append("delete from jskj_subject_group_user where user_id=").append(userid).append("&group_id=").append(groupid);
			dataBaseService.execute(query.toString());
			
		}
		
		return;
		
	}
	
	/**
	 * 查询帖子排行
	 * @param rtype
	 * @param groupid
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public List<Map<String,Object>> findSubjectByRank(Integer rtype,Integer groupid,int pageno,int pagesize){
		List<Map<String,Object>> subjects = null;
		StringBuffer query = new StringBuffer();
		
		if(groupid != null){
			query.append(" select s.*,c.agrees,c.replys,c.shares,c.follows,u.nickname,u.head_img_url,u.subjects from jskj_subject s,jskj_subject_total c,jskj_subject_group g,jskj_user u where 1=1");
			query.append(" and s.subject_id=c.subject_id and s.group_id=g.group_id and s.user_id=u.user_id");
			
			if(rtype == 1){
				query.append(" order by s.is_top desc,c.agrees desc");
			} else if(rtype == 2){
				query.append(" order by s.oprtime desc");
			} else if(rtype == 3){
				query.append(" order by s.is_tag desc,s.oprtime desc");
			} else {
				query.append(" order by s.is_recomm desc,s.oprtime desc");
			}
			
		} else {
			query.append(" select s.*,c.agrees,c.replys,c.shares,c.follows,u.nickname,u.head_img_url,u.subjects  from jskj_subject s,jskj_subject_total c,jskj_user u where 1=1");
			query.append(" and s.subject_id=c.subject_id and u.user_id=s.user_id");
			
			if(rtype == 1){
				query.append(" order by s.is_top desc,c.agrees desc");
			} else if(rtype == 2){
				query.append(" order by s.oprtime desc");
			} else if(rtype == 3){
				query.append(" order by s.is_tag desc,s.oprtime desc");
			} else {
				query.append(" order by s.is_recomm desc,s.oprtime desc");
			}
			
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		subjects = dataBaseService.executeQuery(query.toString());
		
		return subjects;
	}
	
	/**
	 * 查询帖子排行
	 * @param rtype
	 * @param groupid
	 * @param pageno
	 * @param pagesize
	 * @return
	 */
	public int totalSubjectByRank(Integer rtype,Integer groupid){
		List<Map<String,Object>> subjects = null;
		StringBuffer query = new StringBuffer();
		
		if(groupid != null){
			query.append(" select count(s.subject_id) cnt from jskj_subject s,jskj_subject_total c,jskj_subject_group g where 1=1");
			query.append(" and s.subject_id=c.subject_id and s.group_id=g.group_id");
		} else {
			query.append(" select count(s.subject_id) cnt from jskj_subject s,jskj_subject_total c where 1=1");
			query.append(" and s.subject_id=c.subject_id ");
		}
		
		subjects = dataBaseService.executeQuery(query.toString());
		if(subjects != null && subjects.size() >0){
			Map<String,Object> map = subjects.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
	}
	
	
	/**
	 * 查询当前用户的发帖列表
	 * @param uid
	 * @return
	 */
	public List<Map<String,Object>> findSubjectsByUid(Integer uid,int pageno,int pagesize){
		List<Map<String,Object>> subjects = null;
		
		StringBuffer query = new StringBuffer();
		query.append(" select s.*,c.agrees,c.replys,c.shares,c.follows,u.subjects,u.nickname,u.head_img_url from jskj_subject s,jskj_subject_total c,jskj_user u ");
		query.append(" where s.subject_id = c.subject_id and s.user_id=u.user_id");
		
		if(uid != null){
			query.append(" and s.user_id = ").append(uid);
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		subjects = dataBaseService.executeQuery(query.toString());
		
		return subjects;
	}
	
	
	/**
	 * 查询当前用户的发帖列表
	 * @param uid
	 * @return
	 */
	public List<Map<String,Object>> findReplyByUid(Integer uid,int pageno,int pagesize){
		List<Map<String,Object>> subjects = null;
		
		StringBuffer query = new StringBuffer();
		query.append(" select j.content subject_content ,s.*,u.subjects,u.nickname,u.head_img_url from jskj_subject j,jskj_subject_reply s,jskj_user u");
		query.append(" where j.subject_id=s.subject_id and s.user_id=u.user_id ");
		
		if(uid != null){
			query.append(" and s.user_id = ").append(uid);
		}
		
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize);
		
		subjects = dataBaseService.executeQuery(query.toString());
		
		return subjects;
	}
	
	public Map<String,Object> findGroupById(Integer groupid) {
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_subject_group where group_id=").append(groupid);
		List<Map<String,Object>> groups = dataBaseService.executeQuery(query.toString());
		
		if(groups != null && groups.size() >0){
			return groups.get(0);
		}
		
		return null;
	}
	
	/**
	 * 查询当前用户的发帖列表
	 * @param uid
	 * @return
	 */
	public int totalReplyByUid(Integer uid){
		List<Map<String,Object>> subjects = null;
		
		StringBuffer query = new StringBuffer();
		query.append(" select count(*) cnt from jskj_subject_reply ");
		query.append(" where 1=1");
		
		if(uid != null){
			query.append(" and user_id = ").append(uid);
		}

		subjects = dataBaseService.executeQuery(query.toString());
		
		if(subjects != null && subjects.size() >0){
			Map<String,Object> replyMap = subjects.get(0);
			return (Integer)replyMap.get("cnt");
		}

		return 0;
	}
	
	
	/**
	 * 查询当前用户的发帖列表
	 * @param uid
	 * @return
	 */
	public int totalSubjectsByUid(Integer uid){
		List<Map<String,Object>> subjects = null;
		
		StringBuffer query = new StringBuffer();
		query.append(" select count(s.subject_id) cnt from jskj_subject s,jskj_subject_total c ");
		query.append(" where s.subject_id = c.subject_id");
		
		if(uid != null){
			query.append(" and s.user_id = ").append(uid);
		}
		
		subjects = dataBaseService.executeQuery(query.toString());
		if(subjects != null && subjects.size()>0){
			Map<String,Object> map = subjects.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
	}
	
	/**
	 * 保存用户操作历史
	 * @param uid
	 * @param subjectid
	 * @param action
	 */
	public void saveSubjectAction(Integer userid,Integer subjectid,Integer action){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Map<String,Object> insertData = new HashMap<String,Object>();
		insertData.put("user_id", userid);
		insertData.put("subject_id", subjectid);
		insertData.put("action", action);
		insertData.put("oprtime", sdf.format(new Date()));
		
		dataBaseService.insertSingleTable("jskj_subject_action", insertData, true, true);
		
	}

	/**
	 * 发送一个小纸条
	 * @param content
	 * @param from
	 * @param to
	 */
	public void saveShortMsg(String content,Integer from, Integer to){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Map<String,Object> insertData = new HashMap<String,Object>();
		insertData.put("to_user", to);
		insertData.put("from_user", from);
		insertData.put("content", content);
		insertData.put("oprtime", sdf.format(new Date()));
		insertData.put("status", 0);
		insertData.put("from_del", 0);
		insertData.put("to_del", 0);
		
		dataBaseService.insertSingleTable("jskj_short_message", insertData, true, true);
		
	}
	/**
	 * 读取小纸条
	 * @param msgid
	 */
	public void readShortMessage(Integer msgid,Map<String,Object> headers){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Map<String,Object> setData = new HashMap<String,Object>();
		setData.put("status", 1);
		setData.put("read_time",sdf.format(new Date()));
		
		Map<String,Object> whereData = new HashMap<String,Object>();
		whereData.put("msg_id", msgid);
		
		dataBaseService.updateTable("jskj_short_message", setData, whereData);
	}
	
	/**
	 * 根据类型删除小纸条
	 * @param msgid
	 * @param stype 1：发送的小纸条，2：接收的小纸条
	 * @param headers
	 */
	public void deleteShortMessage(Integer msgid,Integer stype,Map<String,String> headers){
		
		Map<String,Object> setData = new HashMap<String,Object>();
		
		if(stype == 1){
			setData.put("from_del", 1);
			setData.put("status", 2);
		} else {
			setData.put("to_del", 1);
		}
		
		Map<String,Object> whereData = new HashMap<String,Object>();
		whereData.put("msg_id", msgid);
		
		dataBaseService.updateTable("jskj_short_message", setData, whereData);
		
	}
	
	public List<Map<String,Object>> findShortMessagesFromUser(Integer userid,int status,int pageno,int pagesize){
		
		List<Map<String,Object>> messages = null;
		StringBuffer query = new StringBuffer();
		query.append("select m.*,u.nickname,u.head_img_url from jskj_short_message m,jskj_user u where u.user_id = m.from_user and m.from_del=0 and m.from_user=").append(userid);
		if(status >-1){
			query.append(" and m.status=").append(status);
		}
		query.append(" order by m.oprtime desc ");
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize); 
		messages = dataBaseService.executeQuery(query.toString());
		
		return messages;
		
	}
	
	public List<Map<String,Object>> findShortMessagesFromLinkUser(Integer firstuser,Integer seconduser,int status,int pageno,int pagesize){
		
		List<Map<String,Object>> messages = null;
		StringBuffer query = new StringBuffer();
		query.append("select m.*,u.nickname,u.head_img_url from jskj_short_message m,jskj_user u  "); 
		query.append("where (m.from_user=").append(firstuser).append(" and m.to_user=").append(seconduser).append(")");
		query.append("or (m.from_user=").append(seconduser).append(" and m.to_user=").append(firstuser).append(")");
		query.append(" and m.from_del=0 and m.from_user=u.user_id");
		
		if(status >-1){
			query.append(" and m.status=").append(status);
		}
		
		query.append(" order by m.oprtime desc ");
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize); 
		
		messages = dataBaseService.executeQuery(query.toString());
		
		return messages;
		
	}
	
	public int totalShortMessage(Integer firstuser,Integer seconduser,int status) {
		List<Map<String,Object>> messages = null;
		StringBuffer query = new StringBuffer();
		query.append("select count(*) cnt from jskj_short_message "); 
		query.append("where (from_user=").append(firstuser).append(" and to_user=").append(seconduser).append(")");
		query.append("or (from_user=").append(seconduser).append(" and to_user=").append(firstuser).append(")");
		query.append(" and from_del=0 ");
		
		if(status >-1){
			query.append(" and status=").append(status);
		}
		
		messages = dataBaseService.executeQuery(query.toString());
		if(messages != null && messages.size() >0){
			Map<String,Object> message = messages.get(0);
			return (Integer)message.get("cnt");
		}
		
		return 0;
		
	}

	public int totalShortMessagesFromUser(Integer userid,int status){
		
		List<Map<String,Object>> messages = null;
		StringBuffer query = new StringBuffer();
		query.append("select count(*) cnt from jskj_short_message where from_del=0 and from_user=").append(userid);
		if(status >-1){
			query.append(" and status=").append(status);
		}
		
		messages = dataBaseService.executeQuery(query.toString());
		if(messages != null && messages.size() >0){
			Map<String,Object> map = messages.get(0);
			return (Integer)map.get("cnt");
		}
		
		return 0;
		
	}
	
	public List<Map<String,Object>> findShortMessagesToUser(Integer userid,int pageno,int pagesize){
		
		List<Map<String,Object>> messages = null;
		StringBuffer query = new StringBuffer();
		
		query.append("select m.*,u.nickname,u.head_img_url from jskj_short_message m,jskj_user u where m.from_user=u.user_id and m.to_del=0 and  m.to_user=").append(userid);
		messages = dataBaseService.executeQuery(query.toString());
		
		query.append(" order by m.oprtime desc ");
		query.append(" limit ").append((pageno-1)*pagesize).append(",").append(pagesize); 
		
		messages = dataBaseService.executeQuery(query.toString());
		
		return messages;
		
	}
	
	
	public int totalShortMessagesToUser(Integer userid){
		
		List<Map<String,Object>> messages = null;
		StringBuffer query = new StringBuffer();
		
		query.append("select count(*) cnt from jskj_short_message where to_del=0 and  to_user=").append(userid);
		messages = dataBaseService.executeQuery(query.toString());
		if(messages != null && messages.size() >0){
			Map<String,Object> map = messages.get(0);
			return (Integer)map.get("cnt");
		}

		return 0;
		
	}

}
