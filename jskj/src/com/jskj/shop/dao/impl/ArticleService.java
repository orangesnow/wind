
package com.jskj.shop.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.dao.util.DataBaseService;

/**
 * 用户服务接口
 * @author VAIO
 * @since 2015-11-22
 */
public class ArticleService {
	
	private DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");
	private static final Logger logger = Logger.getLogger(ArticleService.class);
	
	public int saveArticle(String outid,String image,String title,Integer reads,String created) {
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_article where out_article_id='").append(outid).append("'");
		List<Map<String,Object>> articles = dataBaseService.executeQuery(query.toString());
		
		if(articles == null || articles.isEmpty()) {
			
			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("out_article_id", outid);
			insertData.put("image", image);
			insertData.put("title", title);
			insertData.put("read_count", 0);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			insertData.put("created", sdf.format(new Date()));
			
			int resultid = dataBaseService.insertSingleTable("jskj_article", insertData, true, false);
			return resultid;
			
		} else {
			Map<String,Object> article = articles.get(0);
			return (Integer)article.get("article_id");
		}
	
	}
	
	public void updateArticleNum(Integer articleid){
		StringBuffer query = new StringBuffer();
		query.append("update jskj_article set read_count = read_count+1 where article_id=").append(articleid);
		dataBaseService.executeUpdate(query.toString());
	}
	
	public void saveArticle(Integer articleid,String content) {
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_article_ext where article_id=").append(articleid);
		List<Map<String,Object>> articles= dataBaseService.executeQuery(query.toString());
		
		if(articles == null ||  articles.isEmpty()) {

			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("article_id", articleid);
//			String body = StringEscapeUtils.escapeSql(content);
			insertData.put("body",content);
			
			dataBaseService.insertSingleTable("jskj_article_ext", insertData, true, false);
		}
		
		
	}


	public void saveArticleLabels(Integer articleid,Integer tagid) {
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_article_tag where article_id=").append(articleid).append(" and tag_id=").append(tagid);
		
		List<Map<String,Object>> articles = dataBaseService.executeQuery(query.toString());
		if(articles == null || articles.isEmpty()) {
			Map<String,Object> insertData = new HashMap<String,Object>();
			insertData.put("article_id", articleid);
			insertData.put("tag_id", tagid);
			insertData.put("hot_flag", 0);
			insertData.put("weight", 0);
			
			dataBaseService.insertSingleTable("jskj_article_tag", insertData, true, false);
		}
	}

	public Map<String,Object> findArticleByOut(String outid) {
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_article where out_article_id='").append(outid).append("'");
		
		List<Map<String,Object>> articles = dataBaseService.executeQuery(query.toString());
		if(articles != null && articles.size() >0){
			return articles.get(0);
		}
		
		return null;
		
	}
	
	public Map<String,Object> findArticleById(Integer articleid) {
		
		StringBuffer query = new StringBuffer();
		query.append("select a.*,e.body from jskj_article a,jskj_article_ext e where a.article_id=e.article_id and a.article_id=").append(articleid);
		
		List<Map<String,Object>> articles = dataBaseService.executeQuery(query.toString());
		if(articles != null && articles.size() >0){
			return articles.get(0);
		}
		
		return null;
	}
	
	

}
