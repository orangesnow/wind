package com.jskj.shop.dao.util;
import java.util.HashMap;import java.util.Map;import javax.xml.parsers.DocumentBuilderFactory;import org.w3c.dom.Document;import org.w3c.dom.Element;import org.w3c.dom.NodeList;import com.jskj.shop.util.FileHelper;public final class BeanFactory {
	private static Map<String, Object> map = null;	
	static {
		map = new HashMap<String, Object>();
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(
					FileHelper.getResourceAsStream("beanfactory.xml"));
			NodeList daos = doc.getElementsByTagName("bean");
			Element e = null;
			int length = daos.getLength();
			for (int i = 0; i < length; i++) {
				e = (Element) daos.item(i);
				map.put(e.getAttribute("name"), Class.forName(e.getAttribute("class")).newInstance());
			}			//			SAXReader reader = new SAXReader();//			URL url = ;//			System.out.println("url:"+url);//			File file = new File(url+"beanfactory.xml");//			//			Document doc =  reader.read(file);//			List<Element> beans= doc.selectNodes("/beans/bean");//			if(beans != null && beans.size() >0){//				for(int i=0;i<beans.size();i++){//					Element e = beans.get(i);//					map.put(e.attributeValue("name"), Class.forName(e.attributeValue("class")).newInstance());//				}//			}//						
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}	
	public static Object getBean(String beanName) {
		return map.get(beanName);
	}	
}