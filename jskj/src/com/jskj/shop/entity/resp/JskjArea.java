package com.jskj.shop.entity.resp;

import net.sf.json.JSONObject;

public class JskjArea {
	
	private String out_id;
	private String type;
	private String image;
	private String title;
	private JSONObject[] items;
	public String getOut_id() {
		return out_id;
	}
	public void setOut_id(String out_id) {
		this.out_id = out_id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public JSONObject[] getItems() {
		return items;
	}
	public void setItems(JSONObject[] items) {
		this.items = items;
	}
	
	
}
