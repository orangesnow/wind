package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class SimpleTradeResp implements Serializable {
	
	private int count;
	private List<Map<String,Object>> trades;
	private int pageno;
	private int pagesize;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<Map<String, Object>> getTrades() {
		return trades;
	}
	public void setTrades(List<Map<String, Object>> trades) {
		this.trades = trades;
	}
	public int getPageno() {
		return pageno;
	}
	public void setPageno(int pageno) {
		this.pageno = pageno;
	}
	public int getPagesize() {
		return pagesize;
	}
	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}
	
}
