package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class HotwordResp  implements Serializable{
	
	private int count;
	private List<Map<String,Object>> names;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<Map<String, Object>> getNames() {
		return names;
	}
	public void setNames(List<Map<String, Object>> names) {
		this.names = names;
	}

}
