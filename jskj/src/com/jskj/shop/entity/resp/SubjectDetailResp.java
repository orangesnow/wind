package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class SubjectDetailResp implements Serializable {
	
	private Map<String,Object> subject;
	private List<Map<String,Object>> pictures;
	private Map<String,Object> group;

	public Map<String, Object> getGroup() {
		return group;
	}
	public void setGroup(Map<String, Object> group) {
		this.group = group;
	}
	public Map<String, Object> getSubject() {
		return subject;
	}
	public void setSubject(Map<String, Object> subject) {
		this.subject = subject;
	}
	public List<Map<String, Object>> getPictures() {
		return pictures;
	}
	public void setPictures(List<Map<String, Object>> pictures) {
		this.pictures = pictures;
	}

	
}
