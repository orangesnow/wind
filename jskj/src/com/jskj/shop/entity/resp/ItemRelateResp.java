package com.jskj.shop.entity.resp;

import java.util.List;
import java.util.Map;

public class ItemRelateResp {

	private int num;
	private List<Map<String, Object>> relates;
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public List<Map<String, Object>> getRelates() {
		return relates;
	}
	public void setRelates(List<Map<String, Object>> relates) {
		this.relates = relates;
	}

}
