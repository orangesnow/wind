package com.jskj.shop.entity.resp;

public class IndexReq {
	
	private JskjAd[] ads;
	private JskjArea[] tops;
	private JskjArea[] recomms;
	private JskjTag[] hots;
	
	public JskjTag[] getHots() {
		return hots;
	}
	public void setHots(JskjTag[] hots) {
		this.hots = hots;
	}
	public JskjAd[] getAds() {
		return ads;
	}
	public void setAds(JskjAd[] ads) {
		this.ads = ads;
	}
	public JskjArea[] getTops() {
		return tops;
	}
	public void setTops(JskjArea[] tops) {
		this.tops = tops;
	}
	public JskjArea[] getRecomms() {
		return recomms;
	}
	public void setRecomms(JskjArea[] recomms) {
		this.recomms = recomms;
	}
	
}
