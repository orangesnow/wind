package com.jskj.shop.entity.resp;

import java.io.Serializable;

public class UploadResp implements Serializable {
	
	private String uploadPath;
	private Integer flag;
	
	public String getUploadPath() {
		return uploadPath;
	}
	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	
	
	
}
