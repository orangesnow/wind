package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TradeResp implements Serializable {
	
	private int count;
	private List<Trade> trades;
	private int pageno;
	private int pagesize;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	public List<Trade> getTrades() {
		return trades;
	}
	public void setTrades(List<Trade> trades) {
		this.trades = trades;
	}
	public int getPageno() {
		return pageno;
	}
	public void setPageno(int pageno) {
		this.pageno = pageno;
	}
	public int getPagesize() {
		return pagesize;
	}
	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

}
