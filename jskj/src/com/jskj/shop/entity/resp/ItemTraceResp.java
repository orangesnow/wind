package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ItemTraceResp implements Serializable {
	
	private String outsid;
	private String company;
	private String oid;
	private String status;
	private List<Map<String,Object>> traces;
	
	public String getOutsid() {
		return outsid;
	}
	public void setOutsid(String outsid) {
		this.outsid = outsid;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Map<String, Object>> getTraces() {
		return traces;
	}
	public void setTraces(List<Map<String, Object>> traces) {
		this.traces = traces;
	}
	
}
