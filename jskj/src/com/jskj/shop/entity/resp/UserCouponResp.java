package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class UserCouponResp implements Serializable {
	
	private int count;
	private List<Map<String,Object>> coupons;
	private int pagesize;
	private int pageno;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<Map<String, Object>> getCoupons() {
		return coupons;
	}
	public void setCoupons(List<Map<String, Object>> coupons) {
		this.coupons = coupons;
	}
	public int getPagesize() {
		return pagesize;
	}
	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}
	public int getPageno() {
		return pageno;
	}
	public void setPageno(int pageno) {
		this.pageno = pageno;
	}
	
}
