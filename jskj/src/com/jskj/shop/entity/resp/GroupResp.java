package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class GroupResp implements Serializable {
	
	private Integer count;
	private List<Map<String,Object>> groups;
	private Integer pageno;
	private Integer pagesize;
	
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public List<Map<String, Object>> getGroups() {
		return groups;
	}
	public void setGroups(List<Map<String, Object>> groups) {
		this.groups = groups;
	}
	public Integer getPageno() {
		return pageno;
	}
	public void setPageno(Integer pageno) {
		this.pageno = pageno;
	}
	public Integer getPagesize() {
		return pagesize;
	}
	public void setPagesize(Integer pagesize) {
		this.pagesize = pagesize;
	}
	
}
