package com.jskj.shop.entity.resp;

import java.io.Serializable;

public class UserRegisterResp implements Serializable {
	
	private Integer flag;
	private Integer uid;
	private String message;
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
