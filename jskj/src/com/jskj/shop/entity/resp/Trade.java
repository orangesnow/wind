package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Trade implements Serializable{
	
	private Map<String,Object> tradeInfo;
	private List<Map<String,Object>> orderInfos;
	
	public Map<String, Object> getTradeInfo() {
		return tradeInfo;
	}
	public void setTradeInfo(Map<String, Object> tradeInfo) {
		this.tradeInfo = tradeInfo;
	}
	public List<Map<String, Object>> getOrderInfos() {
		return orderInfos;
	}
	public void setOrderInfos(List<Map<String, Object>> orderInfos) {
		this.orderInfos = orderInfos;
	}
	
	
	
}
