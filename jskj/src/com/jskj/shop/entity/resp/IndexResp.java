package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.jskj.shop.entity.vo.ItemArea;

public class IndexResp implements Serializable {
	
	private List<Map<String,Object>> ads;
	private List<ItemArea> hots;
	private List<ItemArea> recomms;
	private List<Map<String,Object>> tags;
	
	public List<Map<String, Object>> getAds() {
		return ads;
	}
	public void setAds(List<Map<String, Object>> ads) {
		this.ads = ads;
	}
	public List<ItemArea> getHots() {
		return hots;
	}
	public void setHots(List<ItemArea> hots) {
		this.hots = hots;
	}
	public List<ItemArea> getRecomms() {
		return recomms;
	}
	public void setRecomms(List<ItemArea> recomms) {
		this.recomms = recomms;
	}
	public List<Map<String, Object>> getTags() {
		return tags;
	}
	public void setTags(List<Map<String, Object>> tags) {
		this.tags = tags;
	}

}
