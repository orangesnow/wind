package com.jskj.shop.entity.resp;

import java.util.Map;

public class ArticleResp {
	
	private Integer flag;
	private Map<String,Object> article;
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Map<String, Object> getArticle() {
		return article;
	}
	public void setArticle(Map<String, Object> article) {
		this.article = article;
	}

	
}
