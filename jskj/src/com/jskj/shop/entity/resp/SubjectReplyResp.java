package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class SubjectReplyResp implements Serializable {
	
	private int count;
	private List<Map<String,Object>> subjects;
	private int pageno;
	private int pagesize;
	
	private SubjectDetailResp detail;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<Map<String, Object>> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Map<String, Object>> subjects) {
		this.subjects = subjects;
	}

	public int getPageno() {
		return pageno;
	}

	public void setPageno(int pageno) {
		this.pageno = pageno;
	}

	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	public SubjectDetailResp getDetail() {
		return detail;
	}

	public void setDetail(SubjectDetailResp detail) {
		this.detail = detail;
	}

}
