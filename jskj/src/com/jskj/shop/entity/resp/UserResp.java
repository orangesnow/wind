package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.Map;

public class UserResp implements Serializable {
	
	private Map<String,Object> user;

	public Map<String, Object> getUser() {
		return user;
	}

	public void setUser(Map<String, Object> user) {
		this.user = user;
	}
	
	
}
