package com.jskj.shop.entity.resp;

import java.io.Serializable;

public class AppVersion implements Serializable {
	
	private int updateflag;
	private String downurl;
	private String remark;
	private int filesize;
	private String vername;
	private Integer vercode;
	private String md5Str;
	
	public String getMd5Str() {
		return md5Str;
	}

	public void setMd5Str(String md5Str) {
		this.md5Str = md5Str;
	}

	public Integer getVercode() {
		return vercode;
	}

	public void setVercode(Integer vercode) {
		this.vercode = vercode;
	}

	public int getFilesize() {
		return filesize;
	}
	
	public void setFilesize(int filesize) {
		this.filesize = filesize;
	}
	
	public String getVername() {
		return vername;
	}
	public void setVername(String vername) {
		this.vername = vername;
	}
	public int getUpdateflag() {
		return updateflag;
	}
	public void setUpdateflag(int updateflag) {
		this.updateflag = updateflag;
	}
	public String getDownurl() {
		return downurl;
	}
	public void setDownurl(String downurl) {
		this.downurl = downurl;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
