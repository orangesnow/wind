package com.jskj.shop.entity.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ItemDetail implements Serializable {
	
	private Map<String,Object> item;
	private List<Map<String,Object>> formats;
	private List<Map<String,Object>> pictures;
	private List<Map<String,Object>> intros;
	
	public List<Map<String, Object>> getIntros() {
		return intros;
	}
	public void setIntros(List<Map<String, Object>> intros) {
		this.intros = intros;
	}
	public Map<String, Object> getItem() {
		return item;
	}
	public void setItem(Map<String, Object> item) {
		this.item = item;
	}
	public List<Map<String, Object>> getFormats() {
		return formats;
	}
	public void setFormats(List<Map<String, Object>> formats) {
		this.formats = formats;
	}
	public List<Map<String, Object>> getPictures() {
		return pictures;
	}
	public void setPictures(List<Map<String, Object>> pictures) {
		this.pictures = pictures;
	}

}
