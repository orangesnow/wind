package com.jskj.shop.entity.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ItemArea implements Serializable {
	
	private Integer area_id ;
	private String area_name;
	private Integer show_type;
	private String pic_url;
	private String link_type;
	private Integer link_id;
	private List<Map<String,Object>> subs;
	
	public String getLink_type() {
		return link_type;
	}
	public void setLink_type(String link_type) {
		this.link_type = link_type;
	}
	public Integer getLink_id() {
		return link_id;
	}
	public void setLink_id(Integer link_id) {
		this.link_id = link_id;
	}
	public Integer getArea_id() {
		return area_id;
	}
	public void setArea_id(Integer area_id) {
		this.area_id = area_id;
	}
	public String getArea_name() {
		return area_name;
	}
	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}
	public Integer getShow_type() {
		return show_type;
	}
	public void setShow_type(Integer show_type) {
		this.show_type = show_type;
	}
	public String getPic_url() {
		return pic_url;
	}
	public void setPic_url(String pic_url) {
		this.pic_url = pic_url;
	}
	public List<Map<String, Object>> getSubs() {
		return subs;
	}
	public void setSubs(List<Map<String, Object>> subs) {
		this.subs = subs;
	}
	
}
