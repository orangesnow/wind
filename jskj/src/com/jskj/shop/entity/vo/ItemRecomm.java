package com.jskj.shop.entity.vo;

import java.util.List;
import java.util.Map;

public class ItemRecomm {
	
	private Map<String,Object> tag;
	private List<Map<String,Object>> items;
	
	public Map<String, Object> getTag() {
		return tag;
	}
	public void setTag(Map<String, Object> tag) {
		this.tag = tag;
	}
	public List<Map<String, Object>> getItems() {
		return items;
	}
	public void setItems(List<Map<String, Object>> items) {
		this.items = items;
	}
	
}
