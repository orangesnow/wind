package com.jskj.shop.entity.vo;

public class DetailItem {

	private String id;
	private String title;
	private String market_price;
	private String retail_price;
	private String[] labels;
	private boolean out_of_stock;
	private boolean auto_pick_up;
	private String image_list;
	private String image_grid;
	private String detail_url;
	private ItemSku[] skus;
	private HashImage[] photos;
	private String remark;
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMarket_price() {
		return market_price;
	}
	public void setMarket_price(String market_price) {
		this.market_price = market_price;
	}
	public String getRetail_price() {
		return retail_price;
	}
	public void setRetail_price(String retail_price) {
		this.retail_price = retail_price;
	}
	public String[] getLabels() {
		return labels;
	}
	public void setLabels(String[] labels) {
		this.labels = labels;
	}
	
	public boolean isOut_of_stock() {
		return out_of_stock;
	}
	public void setOut_of_stock(boolean out_of_stock) {
		this.out_of_stock = out_of_stock;
	}
	public boolean isAuto_pick_up() {
		return auto_pick_up;
	}
	public void setAuto_pick_up(boolean auto_pick_up) {
		this.auto_pick_up = auto_pick_up;
	}
	public String getImage_list() {
		return image_list;
	}
	public void setImage_list(String image_list) {
		this.image_list = image_list;
	}
	public String getImage_grid() {
		return image_grid;
	}
	public void setImage_grid(String image_grid) {
		this.image_grid = image_grid;
	}
	public String getDetail_url() {
		return detail_url;
	}
	public void setDetail_url(String detail_url) {
		this.detail_url = detail_url;
	}
	public ItemSku[] getSkus() {
		return skus;
	}
	public void setSkus(ItemSku[] skus) {
		this.skus = skus;
	}
	public HashImage[] getPhotos() {
		return photos;
	}
	public void setPhotos(HashImage[] photos) {
		this.photos = photos;
	}
	
}
