package com.jskj.shop.manager;

import org.apache.log4j.Logger;

import redis.clients.jedis.Jedis;

import com.daguu.lib.httpsqs4j.Httpsqs4j;
import com.daguu.lib.httpsqs4j.HttpsqsClient;
import com.daguu.lib.httpsqs4j.HttpsqsException;
import com.danga.MemCached.SockIOPool;
import com.jskj.shop.util.Md5Util;
import com.jskj.shop.util.SysConstants;

/**
 * 存储服务器配置信息管理
 * @author jordon
 * @since 2014-05-18
 */
public class StoreServerManager {
	private static boolean initMemcache = false;

	private static boolean initQueue = false;
	private static boolean isCheckPass = false;
	private static final Logger logger = Logger.getLogger(StoreServerManager.class);
	
	private static Jedis jedis = null;
	
	/**
	 * 初始化消息队列
	 */
	public static void initQueue(){
		
		if(initQueue == false){
			
			
			
			initQueue = true;
		}

	}
	
	/**
	 * 初始化memcache内存数据
	 */
	public static void initMemcache(String name){
		
		if(initMemcache == false) {
			String[] servers = { "127.0.0.1:12190" };//127.0.0.1
			SockIOPool pool = SockIOPool.getInstance(name);
			pool.setServers(servers);
			pool.setFailover(true);
			pool.setInitConn(10);
			pool.setMinConn(5);
			pool.setMaxConn(250);
			pool.setMaintSleep(30);
			pool.setNagle(false);
			pool.setSocketTO(3000);
			pool.setAliveCheck(true);
			pool.initialize();
		       
			initMemcache = true;
			logger.info("initMemcache success!!");
		}
		
	}
	
	public static boolean putString(String queueName, String content) {
		
		boolean flag = false;
		
	   	StoreServerManager.initQueue();
	   	HttpsqsClient httpSqsClient = Httpsqs4j.createNewClient();
	   	try {
	   		httpSqsClient.putString( queueName , content);
	   		flag = true;
		} catch (Exception e) {
			logger.error(e);
		}
	   	
	   	return flag;
	}
	
	public static String getString(String queueName){
		
		String result = null;
		
	   	StoreServerManager.initQueue();
	   	HttpsqsClient httpSqsClient = Httpsqs4j.createNewClient();
	   	
	   	try {
	   	 	result = httpSqsClient.getString(queueName);
		} catch (Exception e) {
			logger.error(e);
		}

	   	return result;
	}
	
	public  static Jedis initRedis(){
		if(jedis == null){
			Jedis jedis = new Jedis("116.255.147.42",16379);
			jedis.auth("1qaz@WSX3edc");//密码
		}
		
		return jedis ;
	}
	
	
	public static void setStringToQueue(String queueName,String key,String content){
		if(jedis != null){
			jedis.rpush(key, content);
		}
	}
	
	public static long getQueueSize(String queueName){
		long size = 0;
		
		if(jedis != null){
			size = jedis.llen(queueName);
		}
		
		return size;
	}
	
	public static String getStringFromQueue(String queueName){
		String result = null;
		
		if(jedis != null){
			jedis.lpop(queueName);
		}
		
		
		return result;
	}
	
	
	public static void addContent(String moduleName,String key,String value){
		String newKey = Md5Util.encode(moduleName+"_"+key);
		
		if(jedis != null){
			jedis.set(newKey, value);
		}

	}
	
	public static void addContent(String moduleName,String key,String value,int seconds){
		String newKey = Md5Util.encode(moduleName+"_"+key);
		
		if(jedis != null){
			jedis.set(newKey, value);
			jedis.expire(newKey, seconds);
		}

	}
	
	public static String getContent(String moduleName,String key){
		String value = null;
		
		String newKey = Md5Util.encode(moduleName+"_"+key);
		if(jedis != null){
			value = jedis.get(newKey);
		}
		
		return value;
		
	}

	
	
	
}
