package com.jskj.shop.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.entity.vo.ItemArea;


/**
 * 完成对业务生命周期缓存数据的管理功能
 * @author VAIO
 * @since 2014-08-06
 */
public class ItemManager {

	private static final Logger logger = Logger.getLogger(ItemManager.class);
	
	public static List<ItemArea> handleArea(List<Map<String,Object>> areas,boolean included) {
		List<ItemArea> itemAreas = new ArrayList<ItemArea>();
		
		if(areas != null && areas.size() >0){
			for(int i=0;i<areas.size();i++){
				Map<String,Object> area = areas.get(i);
				Integer area_id = (Integer)area.get("area_id");
				String pic_url = (String)area.get("pic_url");
				String area_name = (String)area.get("area_name");
				Integer show_type = (Integer)area.get("show_type");
				String link_type = (String)area.get("link_type");
				Integer link_id = (Integer)area.get("link_id");
				
				ItemArea ia = new ItemArea();
				ia.setArea_id(area_id);
				ia.setArea_name(area_name);
				ia.setPic_url(pic_url);
				ia.setShow_type(show_type);
				ia.setLink_id(link_id);
				ia.setLink_type(link_type);
				
				//查询热门商品列表
				if(included){
					ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
					List<Map<String,Object>> subs = itemService.findTopItemFromAreaId(area_id,5);
					ia.setSubs(subs);
				}

				itemAreas.add(ia);
			}
		}
		
		return itemAreas;
	}
	
}
