
package com.jskj.shop.util;

/**
 * 微信常量信息说明
 * @author VAIO
 * @since 2014-06-07
 */
public interface WxConstants {
	
	public static final String DEFAULT_BLANK_STRING = "";
	
	public static final String MESSAGE_FIELD_MSGTYPE = "MsgType";
	public static final String MESSAGE_FIELD_CONTENT = "Content";
	public static final String MESSAGE_FIELD_FROMUSERNAME = "FromUserName";
	public static final String MESSAGE_FIELD_TOUSERNAME  = "ToUserName";
	public static final String MESSAGE_FIELD_CREATETIME="CreateTime";
	public static final String MESSAGE_FIELD_EVENT_VIEW = "VIEW";
	public static final String MESSAGE_FIELD_EVENT_CLICK = "CLICK";
	
	
	public static final String PUBLIC_ACCOUNT_ID = "publicAccountId";
	public static final String PUBLIC_ACCOUNT_APPID = "appId";
	public static final String PUBLIC_ACCOUNT_SECRET = "appSecret";
	public static final String PUBLIC_ACCOUNT_TOKEN = "checkToken";
	public static final String PUBLIC_ACCOUNT_ATYPE = "atype";
	
	public static final String PUBLIC_ACCOUNT_FIELD_ID = "id";
	public static final String PUBLIC_ACCOUNT_FIELD_APPID = "appid";
	public static final String PUBLIC_ACCOUNT_FIELD_SECRET = "appsecret";
	public static final String PUBLIC_ACCOUNT_FIELD_TOKEN = "check_token";
	public static final String PUBLIC_ACCOUNT_FIELD_ATYPE = "atype";//1订阅号 2服务号 3测试号
	
	public static final String MESSAGE_FIELD_EVENT="Event";
	public static final String MESSAGE_FIELD_EVENTKEY="EventKey";
	
	public static final String REQ_MESSAGE_TYPE_TEXT = "text";
	public static final String REQ_MESSAGE_TYPE_IMAGE = "image";
	public static final String REQ_MESSAGE_TYPE_LOCATION = "location";
	public static final String REQ_MESSAGE_TYPE_LINK = "link";
	public static final String REQ_MESSAGE_TYPE_VOICE = "voice";
	public static final String REQ_MESSAGE_TYPE_EVENT = "event";
	
	public static final String EVENT_TYPE_SUBSCRIBE = "subscribe";
	public static final String EVENT_TYPE_UNSUBSCRIBE = "unsubscribe";
	public static final String EVENT_TYPE_CLICK = "click";
	public static final String EVENT_TYPE_VIEW = "view";
	
	public static final String RESP_MESSAGE_TYPE_TEXT = "text";
	public static final String RESP_MESSAGE_TYPE_IMAGE = "image";
	public static final String RESP_MESSAGE_TYPE_LINK = "link";
	public static final String RESP_MESSAGE_TYPE_VOICE = "voice";
	public static final String RESP_MESSAGE_TYPE_NEWS = "news";
	public static final String RESP_MESSAGE_TYPE_MUSIC = "music";
	
	public static final String DEVCHECK_PARAM_ECHOSTR = "echostr";
	public static final String DEVCHECK_PARAM_SIGNATURE = "signature";
	public static final String DEVCHECK_PARAM_TIMESTAMP = "timestamp";
	public static final String DEVCHECK_PARAM_NONCE = "nonce";

}
