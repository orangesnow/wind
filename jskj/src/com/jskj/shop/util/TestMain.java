package com.jskj.shop.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import net.sf.json.JSONObject;

import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.impl.UserService;
import com.jskj.shop.dao.util.BeanFactory;

public class TestMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
//		String requestUrl = "http://qs.win2blue.com/api/o_user_register.json?";
//		String outputStr = "nickname=东东&icon=/1/2.png&mobile=0&gender=1";
//		String requestMethod = "GET";
//		String result = httpRequest( requestUrl,  requestMethod,  outputStr);
//		System.out.println("result:"+result);
		
		String xml = "<xml><appid><![CDATA[wxb44a87b3362af380]]></appid><attach><![CDATA[211]]></attach><bank_type><![CDATA[CMB_CREDIT]]></bank_type><cash_fee><![CDATA[2900]]></cash_fee><fee_type><![CDATA[CNY]]></fee_type><is_subscribe><![CDATA[Y]]></is_subscribe><mch_id><![CDATA[1306452601]]></mch_id><nonce_str><![CDATA[2226453909]]></nonce_str><openid><![CDATA[oGusNwDgscvogWqSzWNktIHtJTzc]]></openid><out_trade_no><![CDATA[J20160218222636000001]]></out_trade_no><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><sign><![CDATA[A12E80635E564E07F185DFE4A07F8DE7]]></sign><time_end><![CDATA[20160218222437]]></time_end><total_fee>2900</total_fee><trade_type><![CDATA[JSAPI]]></trade_type><transaction_id><![CDATA[1000610033201602183369997342]]></transaction_id></xml>";
		
		//解析微信支付返回的结果
				try {
					Document document = DocumentHelper.parseText(xml);
					Element returnCode = (Element)document.selectSingleNode("/xml/return_code");
					Element returnMsg = (Element)document.selectSingleNode("/xml/return_msg");
					Element resultCode = (Element)document.selectSingleNode("/xml/result_code");
					
					if(returnCode != null && resultCode != null){
						
						String returnCodeStr = returnCode.getText();
						String resultCodeStr = resultCode.getText();
						
						if(returnCodeStr.equals("SUCCESS") && resultCodeStr.equals("SUCCESS") ){
							Element openidNode = (Element)document.selectSingleNode("/xml/openid");
							Element totalFeeNode = (Element)document.selectSingleNode("/xml/total_fee");
							Element transactionIdNode = (Element)document.selectSingleNode("/xml/transaction_id");
							Element attachNode = (Element)document.selectSingleNode("/xml/attach");
							Element timeEndNode = (Element)document.selectSingleNode("/xml/time_end");
							Element outTradeNoNode = (Element)document.selectSingleNode("/xml/out_trade_no");
							
							ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
							itemService.updateOrderStatus(outTradeNoNode.getText(),1);
						}
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			
		
	}

	
	/**
	 * 发起https请求并获取结果
	 *
	 * @param requestUrl 请求地址
	 * @param requestMethod 请求方式（GET、POST）
	 * @param outputStr 提交的数据
	 * @return JSONObject (通过JSONObject.get(key)的方式获取json对象的属性值)
	 */
	public static String httpRequest(String requestUrl, String requestMethod, String outputStr) {
	
	    try {
	       
	        URL url = new URL(requestUrl);
	        HttpURLConnection httpUrlConn=(HttpURLConnection) url.openConnection();
	        httpUrlConn.setRequestProperty("platform", "android");
	        httpUrlConn.setRequestProperty("_channelid", "test");
	        httpUrlConn.setRequestProperty("systemName", "android");
	        httpUrlConn.setRequestProperty("appversion", "1.0");
	        httpUrlConn.setRequestProperty("version", "5.1.1");
	        httpUrlConn.setRequestProperty("devicenum", "867840020080470");
	        httpUrlConn.setRequestProperty("mobiletype", "YQ603");
	        httpUrlConn.setRequestProperty("productid", "20160110");
	        httpUrlConn.setDoOutput(true);
	        httpUrlConn.setDoInput(true);
	        httpUrlConn.setUseCaches(false);
	        //设置请求方式（GET/POST）
	        httpUrlConn.setRequestMethod(requestMethod);
	     
	        /*if ("GET".equalsIgnoreCase(requestMethod))
	            httpUrlConn.connect();   */
	        //当有数据需要提交时(当outputStr不为null时，向输出流写数据)
	        if (null != outputStr) {
	            OutputStream outputStream = httpUrlConn.getOutputStream();
	            // 注意编码格式，防止中文乱码
	            outputStream.write(outputStr.getBytes("UTF-8"));
	            outputStream.close();
	        }

	        // 将返回的输入流转换成字符串
	        InputStream inputStream = httpUrlConn.getInputStream();
	        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
	        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	        String str = null;
	        StringBuffer buffer=new StringBuffer();
	        while ((str = bufferedReader.readLine()) != null) {
	            buffer.append(str);
	        }

	        //释放资源
	        bufferedReader.close();
	        inputStreamReader.close();
	        inputStream.close();
	        inputStream = null;
	        httpUrlConn.disconnect();
	       return buffer.toString();
	        
	    } catch (Exception e) {
	       e.printStackTrace();
	    }
	    return null;
	}

	
}
