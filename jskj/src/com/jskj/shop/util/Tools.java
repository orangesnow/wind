package com.jskj.shop.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.servlet.ServletInputStream;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

import com.jskj.shop.dao.impl.ItemService;

/**
 * 基本工具类
 * 
 * @author VAIO
 * @since 2014-06-08
 */
public class Tools {

	private static final Logger logger = Logger.getLogger(Tools.class);

	/**
	 * 检查整数
	 * 
	 * @param num
	 * @param type
	 *            "0+":非负整数 "+":正整数 "-0":非正整数 "-":负整数 "":整数
	 * @return
	 */
	public static boolean checkNumber(String num, String type) {
		String eL = "";

		if (type.equals("0+"))
			eL = "^\\d+$";// 非负整数
		else if (type.equals("+"))
			eL = "^\\d*[1-9]\\d*$";// 正整数
		else if (type.equals("-0"))
			eL = "^((-\\d+)|(0+))$";// 非正整数
		else if (type.equals("-"))
			eL = "^-\\d*[1-9]\\d*$";// 负整数
		else
			eL = "^-?\\d+$";// 整数

		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(num);
		boolean b = m.matches();

		return b;
	}

	// 从输入流读取post参数
	public static String readStreamParameter(ServletInputStream in) {

		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = null;

		try {

			reader = new BufferedReader(new InputStreamReader(in));
			String line = null;
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			if (null != reader) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return buffer.toString();

	}

	/**
	 * 发起https请求并获取结果
	 * 
	 * @param requestUrl
	 *            请求地址
	 * @param requestMethod
	 *            请求方式（GET、POST）
	 * @param outputStr
	 *            提交的数据
	 * @return JSONObject(通过JSONObject.get(key)的方式获取json对象的属性值)
	 */
	public static JSONObject httpRequest(String requestUrl,
			String requestMethod, String outputStr) {
		JSONObject jsonObject = null;
		StringBuffer buffer = new StringBuffer();
		try {
			// 创建SSLContext对象，并使用我们指定的信任管理器初始化
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new java.security.SecureRandom());
			// 从上述SSLContext对象中得到SSLSocketFactory对象
			SSLSocketFactory ssf = sslContext.getSocketFactory();

			URL url = new URL(requestUrl);
			HttpsURLConnection httpUrlConn = (HttpsURLConnection) url
					.openConnection();
			httpUrlConn.setSSLSocketFactory(ssf);

			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			// 设置请求方式（GET/POST）
			httpUrlConn.setRequestMethod(requestMethod);

			if ("GET".equalsIgnoreCase(requestMethod))
				httpUrlConn.connect();

			// 当有数据需要提交时
			if (null != outputStr) {
				OutputStream outputStream = httpUrlConn.getOutputStream();
				// 注意编码格式，防止中文乱码
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}

			// 将返回的输入流转换成字符串
			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(
					inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(
					inputStreamReader);

			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();
			// 释放资源
			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			jsonObject = JSONObject.fromObject(buffer.toString());
		} catch (ConnectException ce) {
			logger.error("Weixin server connection timed out.");
		} catch (Exception e) {
			logger.error("https request error:{}", e);
		}
		return jsonObject;
	}

	/**
	 * 发起https请求并获取结果
	 * 
	 * @param requestUrl
	 *            请求地址
	 * @param requestMethod
	 *            请求方式（GET、POST）
	 * @param outputStr
	 *            提交的数据
	 * @return JSONObject(通过JSONObject.get(key)的方式获取json对象的属性值)
	 */
	public static String httpRequest(String requestUrl,
			String requestMethod, String outputStr, boolean isComm) {
		JSONObject jsonObject = null;
		StringBuffer buffer = new StringBuffer();
		try {

			URL url = new URL(requestUrl);
			HttpURLConnection httpUrlConn = (HttpURLConnection) url
					.openConnection();

			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			// 设置请求方式（GET/POST）
			httpUrlConn.setRequestMethod(requestMethod);

			if ("GET".equalsIgnoreCase(requestMethod))
				httpUrlConn.connect();

			// 当有数据需要提交时
			if (null != outputStr) {
				OutputStream outputStream = httpUrlConn.getOutputStream();
				// 注意编码格式，防止中文乱码
				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}

			// 将返回的输入流转换成字符串
			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(
					inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(
					inputStreamReader);

			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();
			// 释放资源
			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			return buffer.toString();
		} catch (ConnectException ce) {
			logger.error("Weixin server connection timed out.");
		} catch (Exception e) {
			logger.error("https request error:{}", e);
		}
		return null;
	}

	// 数组转字符串
	public static String ArrayToString(String[] arr) {
		StringBuffer bf = new StringBuffer();
		for (int i = 0; i < arr.length; i++) {
			bf.append(arr[i]);
		}
		return bf.toString();
	}

	/**
	 * sha1加密
	 * 
	 * @param sourceString
	 *            原始串
	 * @return 加密串
	 */
	public static String SHA1Encode(String sourceString) {

		String resultString = null;

		try {
			resultString = new String(sourceString);
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			resultString = byte2hexString(md.digest(resultString.getBytes()));
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}

		return resultString;
	}

	/**
	 * 二进制转成l6进制
	 * 
	 * @param bytes
	 *            字节数组
	 * @return
	 */
	public static String byte2hexString(byte[] bytes) {

		StringBuffer buf = new StringBuffer(bytes.length * 2);

		for (int i = 0; i < bytes.length; i++) {
			if (((int) bytes[i] & 0xff) < 0x10) {
				buf.append("0");
			}
			buf.append(Long.toString((int) bytes[i] & 0xff, 16));
		}

		return buf.toString().toUpperCase();
	}

	/**
	 * 按照指定的编码进行解码
	 * 
	 * @param content
	 *            内容
	 * @param encode
	 *            编码格式 UTF-8 GBK等等
	 * @return
	 */
	public static String decodeContent(String content, String encode) {

		String keyword = "";
		try {
			keyword = URLEncoder.encode(content.trim(), encode);
		} catch (Exception e) {
			return "";
		}

		return keyword;
	}

	/**
	 * 将微信默认的返回信息为空字符串写到消息队列时需要改成“null”
	 * 
	 * @param respMessage
	 * @return
	 */
	public static String getFilterString(String respMessage) {
		if (respMessage == null || respMessage.equals("")) {
			return "null";
		} else {
			return respMessage;
		}
	}

	/**
	 * 将为null的改成“”
	 * 
	 * @param respMessage
	 *            响应内容
	 * @return
	 */
	public static String getConverseFilterString(String respMessage) {
		if (respMessage != null && respMessage.equals("null")) {
			return "";
		} else {
			return respMessage;
		}
	}

	public static boolean containsEmoji(String source) {

		if (source == null || source.equals("")) {
			return false;
		}

		int len = source.length();
		for (int i = 0; i < len; i++) {
			char codePoint = source.charAt(i);
			if (isEmojiCharacter(codePoint)) {
				return true;
			}
		}

		return false;

	}

	private static boolean isEmojiCharacter(char codePoint) {

		return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA)
				|| (codePoint == 0xD)
				|| ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
				|| ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
				|| ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));

	}

	/**
	 * 过滤emoji 或者 其他非文字类型的字符
	 * 
	 * @param source
	 * @return
	 */

	public static String filterEmoji(String source) {
		if(source != null){
			return source.replaceAll("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", "");
		} else {
			return null;
		}

//		if (!containsEmoji(source)) {
//			return source;// 如果不包含，直接返回
//		}
//
//		// 到这里铁定包含
//
//		StringBuilder buf = null;
//		int len = source.length();
//
//		for (int i = 0; i < len; i++) {
//			char codePoint = source.charAt(i);
//			if (isEmojiCharacter(codePoint)) {
//				if (buf == null) {
//					buf = new StringBuilder(source.length());
//				}
//				buf.append(codePoint);
//			} else {
//
//			}
//		}
//
//		if (buf == null) {
//			return source;// 如果没有找到 emoji表情，则返回源字符串
//		} else {
//			if (buf.length() == len) {// 这里的意义在于尽可能少的toString，因为会重新生成字符串
//				buf = null;
//				return source;
//			} else {
//				return buf.toString();
//			}
//		}

	}
	
	public static void main(String[] args) {
		String url = "http://h5.4007060700.com/regions.json";
		String result = Tools.httpRequest(url, "GET", null, false);
		System.out.println("result:"+result);
	}
	
	public static String getUserNickname(){
		UUID uuid = UUID.randomUUID();
		return "U"+uuid.toString();
	}

}
