package com.jskj.shop.util;

/**
 * 系统常量定义
 * @author jordon
 * @since 2014-06-06
 */
public  interface SysConstants {

	public static final String SPT = "/"; //路径分隔符
	public static final String INDEX = "index"; //索引页
	public static final String DEFAULT = "default"; // 默认模板
	public static final String UTF8 = "UTF-8"; //UTF-8编码
	public static final String MESSAGE = "message"; // 提示信息

	public static final String JSESSION_COOKIE = "JSESSIONID";//cookie中的JSESSIONID名称
	public static final String JSESSION_URL = "jsessionid"; // url中的jsessionid名称
	public static final String POST = "POST";//HTTP POST请求
	public static final String GET = "GET"; // HTTP GET请求


	public static final int SYNC_FAILURE = 1;
	public static final int REPEAT_COUNT = 3;
	public static final int INTERVAL_CYCLE = 5000;
	
	public static final int STATUS_SEND_OK = 1;
	public static final int STATUS_SEND_FAILURE = 9;
	
	public static final String MENU_QUEUE = "weixin_menu_queue";
	public static final String CUSTOM_MT_QUEUE = "weixin_custom_queue";
	public static final String MT_QUEUE = "weixin_mt_queue";
	public static final String MO_QUEUE = "weixin_mo_queue";
	
	public static String MT_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
	public static String MT_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
	
	public static int THREAD_SLEEP_TIME = 50;

	public static final int    MEMCACHED_SERVER_PORT = 12190;
	public static final String MEMCACHED_SERVER_IP = "127.0.0.1";

	public static final String MORE_RESULT_HEAD = "【更多】查看更多与“ ";
	public static final String MORE_RESULT_END = "”相关信息…";
	public static final String MORE_RESULT = "【更多】查看更多与“{content}”相关信息…";
	public static final String SEARCH_FEILD_TOTALCOUNT = "totalCount";
	public static final String SEARCH_FEILD_LIST = "list"; 
	public static final String SEARCH_FIELD_TITLE = "title";
	public static final String SEARCH_FIELD_CTGNAME= "ctgName";
	public static final String SEARCH_FIELD_URL = "url";
	public static final String SEARCH_FIELD_IMG = "imgUrl2";
	public static final String SEARCH_FIELD_DESCRIPTION = "description";
	
	public static final String WX_TOKEN_PREFIX = "wx_token_";
	
	public static final int WX_ALERT_FOLLOWMESSAGE_KEY = 1;
	public static final int WX_ALERT_SEARCHMESSAGE_KEY = 2;
	public static final int WX_ALERT_LEAVEWORDMESSAGE_KEY = 3;
	public static final int WX_ALERT_MOREMESSAGE_KEY = 4;
	
	public static final String HTTP_METHOD_GET = "GET";
	public static final String HTTP_METHOD_POST = "POST";
	
	public static final int MENU_DISPLAY_YES = 1;

	public static String HEADER_REQUEST_TIME = "_request_time";
	public static String HEADER_DEVICENUM = "_devicenum";
	public static String HEADER_MOBILETYPE = "_mobiletype";
	public static String HEADER_PLATFORM = "_platform";
	public static String HEADER_VERSION = "_version";
	public static String HEADER_APPVERSION = "_appversion";
	public static String HEADER_UID = "_uid";
	public static String HEADER_CODE = "_code";
	public static String HEADER_CHANNEL = "_channel_id";
	
	public static String HEADER_EXT_REQ_PARAM1 = "_ext_req_param1";
	public static String HEADER_EXT_REQ_PARAM2 = "_ext_req_param2";
	
	public static String HEADER_NDEVICENUM = "devicenum";
	public static String HEADER_NMOBILETYPE = "mobiletype";
	public static String HEADER_NPLATFORM = "platform";
	public static String HEADER_NVERSION = "version";
	public static String HEADER_NAPPVERSION = "appversion";
	public static String HEADER_NUID = "uid";
	public static String HEADER_NCODE = "code";
	public static String HEADER_NCHANNEL = "channelid";
	
	public static String HEADER_RESPONSE_TIME = "_response_time";
	public static String HEADER_ERROR_CODE = "_error_code";
	public static String HEADER_ERROR_MSG = "_error_msg";
	public static String HEADER_EXT_RESP_PARAM1 = "_ext_resp_param1";
	public static String HEADER_EXT_RESP_PARAM2 = "_ext_resp_param2";
	
	public static int HTTP_STATUSCODE_SUCCESS = 200;
	
	public static String CHECK_USERID = "_uid";
	public static String CHECK_CHID = "_chid";
	
	public static int DEFAULT_PAGESIZE = 20;
	public static String SHOP_NAME = "桔色春雪";
	public static String HOST_URL = "http://www.pinkstorm.cn";
	
}
