
package com.jskj.shop.util;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.dao.util.DataBaseService;

public class Common {

	private static DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");

	private static final char[] pregChars = {'.', '\\', '+', '*', '?', '[', '^', ']', '$', '(', ')', '{',
			'}', '=', '!', '<', '>', '|', ':'};
	private static final String randChars = "0123456789abcdefghigklmnopqrstuvtxyzABCDEFGHIGKLMNOPQRSTUVWXYZ";
	private static Random random = new Random();
	private static Map<String, String[]> timeZoneIDs = new LinkedHashMap<String, String[]>(32);
	static {
		timeZoneIDs.put("-12", new String[] {"GMT-12:00", "(GMT -12:00) Eniwetok, Kwajalein"});
		timeZoneIDs.put("-11", new String[] {"GMT-11:00", "(GMT -11:00) Midway Island, Samoa"});
		timeZoneIDs.put("-10", new String[] {"GMT-10:00", "(GMT -10:00) Hawaii"});
		timeZoneIDs.put("-9", new String[] {"GMT-09:00", "(GMT -09:00) Alaska"});
		timeZoneIDs.put("-8", new String[] {"GMT-08:00",
				"(GMT -08:00) Pacific Time (US &amp; Canada), Tijuana"});
		timeZoneIDs.put("-7", new String[] {"GMT-07:00",
				"(GMT -07:00) Mountain Time (US &amp; Canada), Arizona"});
		timeZoneIDs.put("-6", new String[] {"GMT-06:00",
				"(GMT -06:00) Central Time (US &amp; Canada), Mexico City"});
		timeZoneIDs.put("-5", new String[] {"GMT-05:00",
				"(GMT -05:00) Eastern Time (US &amp; Canada), Bogota, Lima, Quito"});
		timeZoneIDs.put("-4", new String[] {"GMT-04:00",
				"(GMT -04:00) Atlantic Time (Canada), Caracas, La Paz"});
		timeZoneIDs.put("-3.5", new String[] {"GMT-03:30", "(GMT -03:30) Newfoundland"});
		timeZoneIDs.put("-3", new String[] {"GMT-03:00",
				"(GMT -03:00) Brassila, Buenos Aires, Georgetown, Falkland Is"});
		timeZoneIDs.put("-2", new String[] {"GMT-02:00",
				"(GMT -02:00) Mid-Atlantic, Ascension Is., St. Helena"});
		timeZoneIDs.put("-1", new String[] {"GMT-01:00", "(GMT -01:00) Azores, Cape Verde Islands"});
		timeZoneIDs.put("0", new String[] {"GMT",
				"(GMT) Casablanca, Dublin, Edinburgh, London, Lisbon, Monrovia"});
		timeZoneIDs.put("1", new String[] {"GMT+01:00",
				"(GMT +01:00) Amsterdam, Berlin, Brussels, Madrid, Paris, Rome"});
		timeZoneIDs.put("2", new String[] {"GMT+02:00",
				"(GMT +02:00) Cairo, Helsinki, Kaliningrad, South Africa"});
		timeZoneIDs.put("3", new String[] {"GMT+03:00", "(GMT +03:00) Baghdad, Riyadh, Moscow, Nairobi"});
		timeZoneIDs.put("3.5", new String[] {"GMT+03:30", "(GMT +03:30) Tehran"});
		timeZoneIDs.put("4", new String[] {"GMT+04:00", "(GMT +04:00) Abu Dhabi, Baku, Muscat, Tbilisi"});
		timeZoneIDs.put("4.5", new String[] {"GMT+04:30", "(GMT +04:30) Kabul"});
		timeZoneIDs.put("5", new String[] {"GMT+05:00",
				"(GMT +05:00) Ekaterinburg, Islamabad, Karachi, Tashkent"});
		timeZoneIDs
				.put("5.5", new String[] {"GMT+05:30", "(GMT +05:30) Bombay, Calcutta, Madras, New Delhi"});
		timeZoneIDs.put("5.75", new String[] {"GMT+05:45", "(GMT +05:45) Katmandu"});
		timeZoneIDs.put("6", new String[] {"GMT+06:00", "(GMT +06:00) Almaty, Colombo, Dhaka, Novosibirsk"});
		timeZoneIDs.put("6.5", new String[] {"GMT+06:30", "(GMT +06:30) Rangoon"});
		timeZoneIDs.put("7", new String[] {"GMT+07:00", "(GMT +07:00) Bangkok, Hanoi, Jakarta"});
		timeZoneIDs.put("8", new String[] {"GMT+08:00",
				"(GMT +08:00) Beijing, Hong Kong, Perth, Singapore, Taipei"});
		timeZoneIDs
				.put("9", new String[] {"GMT+09:00", "(GMT +09:00) Osaka, Sapporo, Seoul, Tokyo, Yakutsk"});
		timeZoneIDs.put("9.5", new String[] {"GMT+09:30", "(GMT +09:30) Adelaide, Darwin"});
		timeZoneIDs.put("10", new String[] {"GMT+10:00",
				"(GMT +10:00) Canberra, Guam, Melbourne, Sydney, Vladivostok"});
		timeZoneIDs.put("11", new String[] {"GMT+11:00",
				"(GMT +11:00) Magadan, New Caledonia, Solomon Islands"});
		timeZoneIDs.put("12", new String[] {"GMT+12:00",
				"(GMT +12:00) Auckland, Wellington, Fiji, Marshall Island"});
	}
	@SuppressWarnings("unchecked")
	public static boolean empty(Object obj) {
		if (obj == null) {
			return true;
		} else if (obj instanceof String && (obj.equals("") || obj.equals("0"))) {
			return true;
		} else if (obj instanceof Number && ((Number) obj).doubleValue() == 0) {
			return true;
		} else if (obj instanceof Boolean && !((Boolean) obj)) {
			return true;
		} else if (obj instanceof Collection && ((Collection) obj).isEmpty()) {
			return true;
		} else if (obj instanceof Map && ((Map) obj).isEmpty()) {
			return true;
		} else if (obj instanceof Object[] && ((Object[]) obj).length == 0) {
			return true;
		}
		return false;
	}
	public static String trim(String text) {
		if (text == null) {
			return "";
		}
		return text.trim();
	}
	public static boolean in_array(Object source, Object ext) {
		return in_array(source, ext, false);
	}
	public static boolean in_array(Object source, Object ext, boolean strict) {
		if (source == null || ext == null) {
			return false;
		}
		if (source instanceof Collection) {
			for (Object s : (Collection) source) {
				if (s.toString().equals(ext.toString())) {
					if (strict) {
						if ((s.getClass().getName().equals(ext.getClass().getName()))) {
							return true;
						}
					} else {
						return true;
					}
				}
			}
		} else {
			for (Object s : (Object[]) source) {
				if (s.toString().equals(ext.toString())) {
					if (strict) {
						if ((s.getClass().getName().equals(ext.getClass().getName()))) {
							return true;
						}
					} else {
						return true;
					}
				}
			}
		}
		return false;
	}
	public static String urlEncode(String s) {
		return urlEncode(s, JavaCenterHome.JCH_CHARSET);
	}
	public static String urlEncode(String s, String enc) {
		if (!empty(s)) {
			try {
				return URLEncoder.encode(s, enc);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return s;
	}
	public static String urlDecode(String s) {
		return urlDecode(s, JavaCenterHome.JCH_CHARSET);
	}
	public static String urlDecode(String s, String enc) {
		if (!empty(s)) {
			try {
				return URLDecoder.decode(s, enc);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return s;
	}
	public static int rand(int max) {
		return rand(0, max);
	}
	public static int rand(int min, int max) {
		if (min < max) {
			return random.nextInt(max - min + 1) + min;
		} else {
			return min;
		}
	}

	@SuppressWarnings("unchecked")
	public static String implode(Object data, String separator) {
		if (data == null) {
			return "";
		}
		StringBuffer out = new StringBuffer();
		if (data instanceof Object[]) {
			boolean flag = false;
			for (Object obj : (Object[]) data) {
				if (flag) {
					out.append(separator);
				} else {
					flag = true;
				}
				out.append(obj);
			}
		} else if (data instanceof Map) {
			Map temp = (Map) data;
			Set<Object> keys = temp.keySet();
			boolean flag = false;
			for (Object key : keys) {
				if (flag) {
					out.append(separator);
				} else {
					flag = true;
				}
				out.append(temp.get(key));
			}
		} else if (data instanceof Collection) {
			boolean flag = false;
			for (Object obj : (Collection) data) {
				if (flag) {
					out.append(separator);
				} else {
					flag = true;
				}
				out.append(obj);
			}
		} else {
			return data.toString();
		}
		return out.toString();
	}
	public static String sImplode(Object ids) {
		return "'" + implode(ids, "','") + "'";
	}
	public static int range(Object value, int max, int min) {
		if (value instanceof String) {
			return Math.min(max, Math.max(intval((String) value), min));
		} else {
			return Math.min(max, Math.max((Integer) value, min));
		}
	}
	public static int intval(String s) {
		return intval(s, 10);
	}
	public static int intval(String s, int radix) {
		if (s == null || s.length() == 0) {
			return 0;
		}
		if (radix == 0) {
			radix = 10;
		} else if (radix < Character.MIN_RADIX) {
			return 0;
		} else if (radix > Character.MAX_RADIX) {
			return 0;
		}
		int result = 0;
		int i = 0, max = s.length();
		int limit;
		int multmin;
		int digit;
		boolean negative = false;
		if (s.charAt(0) == '-') {
			negative = true;
			limit = Integer.MIN_VALUE;
			i++;
		} else {
			limit = -Integer.MAX_VALUE;
		}
		if (i < max) {
			digit = Character.digit(s.charAt(i++), radix);
			if (digit < 0) {
				return 0;
			} else {
				result = -digit;
			}
		}
		multmin = limit / radix;
		while (i < max) {
			digit = Character.digit(s.charAt(i++), radix);
			if (digit < 0) {
				break;
			}
			if (result < multmin) {
				result = limit;
				break;
			}
			result *= radix;
			if (result < limit + digit) {
				result = limit;
				break;
			}
			result -= digit;
		}
		if (negative) {
			if (i > 1) {
				return result;
			} else {
				return 0;
			}
		} else {
			return -result;
		}
	}
	public static Map<String, String[]> getTimeZoneIDs() {
		return timeZoneIDs;
	}
	public static int time() {
		return (int) (System.currentTimeMillis() / 1000);
	}
	public static SimpleDateFormat getSimpleDateFormat(String format, String timeoffset) {
		SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
		sdf.setTimeZone(TimeZone.getTimeZone(timeZoneIDs.get(timeoffset)[0]));
		return sdf;
	}
	public static String gmdate(SimpleDateFormat sdf, int timestamp) {
		return sdf.format(timestamp * 1000l);
	}
	public static String gmdate(String format, int timestamp, String timeoffset) {
		return getSimpleDateFormat(format, timeoffset).format(timestamp * 1000l);
	}


	public static String getRandStr(int length, boolean isOnlyNum) {
		int size = isOnlyNum ? 10 : 62;
		StringBuffer hash = new StringBuffer(length);
		for (int i = 0; i < length; i++) {
			hash.append(randChars.charAt(random.nextInt(size)));
		}
		return hash.toString();
	}
	public static String stripTags(String content) {
		return content == null ? "" : content.replaceAll("<[\\s\\S]*?>", "");
	}
	public static String htmlSpecialChars(String string) {
		return htmlSpecialChars(string, 1);
	}
	public static String htmlSpecialChars(String text, int quotestyle) {
		if (text == null || text.equals("")) {
			return "";
		}
		StringBuffer sb = new StringBuffer(text.length() * 2);
		StringCharacterIterator iterator = new StringCharacterIterator(text);
		char character = iterator.current();
		while (character != StringCharacterIterator.DONE) {
			switch (character) {
				case '&':
					sb.append("&amp;");
					break;
				case '<':
					sb.append("&lt;");
					break;
				case '>':
					sb.append("&gt;");
					break;
				case '"':
					if (quotestyle == 1 || quotestyle == 2) {
						sb.append("&quot;");
					} else {
						sb.append(character);
					}
					break;
				case '\'':
					if (quotestyle == 2) {
						sb.append("&#039;");
					} else {
						sb.append(character);
					}
					break;
				default:
					sb.append(character);
					break;
			}
			character = iterator.next();
		}
		return sb.toString();
	}
	public static String addSlashes(String text) {
		if (text == null || text.equals("")) {
			return "";
		}
		StringBuffer sb = new StringBuffer(text.length() * 2);
		StringCharacterIterator iterator = new StringCharacterIterator(text);
		char character = iterator.current();
		while (character != StringCharacterIterator.DONE) {
			switch (character) {
				case '\'':
				case '"':
				case '\\':
					sb.append("\\");
				default:
					sb.append(character);
					break;
			}
			character = iterator.next();
		}
		return sb.toString();
	}
	public static String addCSlashes(String text, char[] characters) {
		if (text == null || text.equals("")) {
			return "";
		}
		StringBuffer sb = new StringBuffer(text.length() * 2);
		StringCharacterIterator iterator = new StringCharacterIterator(text);
		char character = iterator.current();
		while (character != StringCharacterIterator.DONE) {
			for (char c : characters) {
				if (character == c) {
					sb.append("\\");
					break;
				}
			}
			sb.append(character);
			character = iterator.next();
		}
		return sb.toString();
	}
	public static String stripSlashes(String text) {
		if (text == null || text.equals("")) {
			return "";
		}
		StringBuffer sb = new StringBuffer(text.length());
		StringCharacterIterator iterator = new StringCharacterIterator(text);
		char character = iterator.current();
		while (character != StringCharacterIterator.DONE) {
			switch (character) {
				case '\'':
					sb.append("'");
					break;
				case '"':
					sb.append('"');
					break;
				case '\\':
					sb.append(iterator.next());
					break;
				default:
					sb.append(character);
					break;
			}
			character = iterator.next();
		}
		return sb.toString();
	}
	public static String stripCSlashes(String text) {
		if (text == null || text.equals("")) {
			return "";
		}
		StringBuffer sb = new StringBuffer(text.length());
		StringCharacterIterator iterator = new StringCharacterIterator(text);
		char character = iterator.current();
		boolean flag = true;
		while (character != StringCharacterIterator.DONE) {
			if (character == '\\' && flag) {
				flag = false;
			} else {
				flag = true;
				sb.append(character);
			}
			character = iterator.next();
		}
		return sb.toString();
	}
	@SuppressWarnings("unchecked")
	public static Object sAddSlashes(Object obj) {
		if (obj instanceof String) {
			return addSlashes((String) obj);
		} else if (obj instanceof Map) {
			Map temp = (Map) obj;
			Set<Object> keys = temp.keySet();
			for (Object key : keys) {
				temp.put(key, sAddSlashes(temp.get(key)));
			}
			return temp;
		} else if (obj instanceof List) {
			List temp = new ArrayList();
			for (Object str : (List) obj) {
				temp.add(sAddSlashes(str));
			}
			return temp;
		} else if (obj instanceof Object[]) {
			Object[] temp = (Object[]) obj;
			for (int i = 0; i < temp.length; i++) {
				temp[i] = sAddSlashes(temp[i]);
			}
			return temp;
		} else {
			return obj;
		}
	}
	@SuppressWarnings("unchecked")
	public static Object sStripSlashes(Object obj) {
		if (obj instanceof String) {
			return stripSlashes((String) obj);
		} else if (obj instanceof Map) {
			Map temp = (Map) obj;
			Set<Object> keys = temp.keySet();
			for (Object key : keys) {
				temp.put(key, sStripSlashes(temp.get(key)));
			}
			return temp;
		} else if (obj instanceof List) {
			List temp = new ArrayList();
			for (Object str : (List) obj) {
				temp.add(sStripSlashes(str));
			}
			return temp;
		} else if (obj instanceof Set) {
			Set temp = new HashSet();
			for (Object str : (Set) obj) {
				temp.add(sStripSlashes(str));
			}
			return temp;
		} else if (obj instanceof Object[]) {
			Object[] temp = (Object[]) obj;
			for (int i = 0; i < temp.length; i++) {
				temp[i] = sStripSlashes(temp[i]);
			}
			return temp;
		} else {
			return obj;
		}
	}
	@SuppressWarnings("unchecked")
	public static Object sHtmlSpecialChars(Object obj) {
		if (obj instanceof String) {
			return htmlSpecialChars((String) obj).replaceAll(
					"&amp;((#(\\d{3,5}|x[a-fA-F0-9]{4})|[a-zA-Z][a-z0-9]{2,5});)", "&$1");
		} else if (obj instanceof Map) {
			Map temp = (Map) obj;
			Set<Object> keys = temp.keySet();
			for (Object key : keys) {
				temp.put(key, sHtmlSpecialChars(temp.get(key)));
			}
			return temp;
		} else if (obj instanceof List) {
			List temp = new ArrayList();
			for (Object str : (List) obj) {
				temp.add(sHtmlSpecialChars(str));
			}
			return temp;
		} else if (obj instanceof Object[]) {
			Object[] temp = (Object[]) obj;
			for (int i = 0; i < temp.length; i++) {
				temp[i] = sHtmlSpecialChars(temp[i]);
			}
			return temp;
		} else {
			return obj;
		}
	}
	public static boolean isArray(Object obj) {
		if (obj instanceof Object[]) {
			return true;
		} else if (obj instanceof Collection) {
			return true;
		} else if (obj instanceof Map) {
			return true;
		} else {
			return false;
		}
	}
	public static int strlen(String text) {
		return strlen(text, JavaCenterHome.JCH_CHARSET);
	}
	public static int strlen(String text, String charsetName) {
		if (text == null || text.length() == 0) {
			return 0;
		}
		int length = 0;
		try {
			length = text.getBytes(charsetName).length;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return length;
	}
	public static String cutstr(String text, int length) {
		return cutstr(text, length, " ...");
	}
	public static String cutstr(String text, int length, String dot) {
		int strBLen = strlen(text);
		if (strBLen <= length) {
			return text;
		}
		int temp = 0;
		StringBuffer sb = new StringBuffer(length);
		char[] ch = text.toCharArray();
		for (char c : ch) {
			sb.append(c);
			if (c > 256) {
				temp += 2;
			} else {
				temp += 1;
			}
			if (temp >= length) {
				if (dot != null) {
					sb.append(dot);
				}
				break;
			}
		}
		return sb.toString();
	}
	public static boolean isNumeric(Object obj) {
		if (obj instanceof String && !obj.equals("")) {
			String temp = (String) obj;
			if (temp.endsWith("d") || temp.endsWith("f")) {
				return false;
			} else {
				try {
					Double.parseDouble(temp);
				} catch (Exception e) {
					return false;
				}
			}
			return true;
		} else if (obj instanceof Number) {
			return true;
		} else {
			return false;
		}
	}
	public static boolean isEmail(String email) {
		return Common.strlen(email) > 6 && email.matches("^[\\w\\-\\.]+@[\\w\\-\\.]+(\\.\\w+)+$");
	}
	public static String pregQuote(String text, char... delimiter) {
		StringBuffer sb = new StringBuffer(text.length() * 2);
		StringCharacterIterator iterator = new StringCharacterIterator(text);
		char character = iterator.current();
		while (character != StringCharacterIterator.DONE) {
			boolean flag = false;
			for (char c : pregChars) {
				if (character == c) {
					flag = true;
					break;
				}
			}
			if (!flag && delimiter != null) {
				for (char d : delimiter) {
					if (character == d) {
						flag = true;
						break;
					}
				}
			}
			if (flag) {
				sb.append('\\');
			}
			sb.append(character);
			character = iterator.next();
		}
		return sb.toString();
	}
	public static String nl2br(String text) {
		if (text == null || text.length() == 0) {
			return text;
		}
		StringBuffer sb = new StringBuffer(text.length() * 2);
		StringCharacterIterator iterator = new StringCharacterIterator(text);
		char character = iterator.current();
		while (character != StringCharacterIterator.DONE) {
			switch (character) {
				case '\r':
					sb.append("<br/>");
					sb.append(character);
					character = iterator.next();
					if (character == '\n') {
						character = iterator.next();
					}
					break;
				case '\n':
					sb.append("<br/>");
					sb.append(character);
					character = iterator.next();
					if (character == '\r') {
						sb.append(character);
						character = iterator.next();
					}
					break;
				default:
					sb.append(character);
					character = iterator.next();
					break;
			}
		}
		return sb.toString();
	}
	public static String sprintf(String format, double number) {
		return new DecimalFormat(format).format(number);
	}
	public static String formatSize(long dataSize) {
		dataSize = Math.abs(dataSize);
		if (dataSize >= 1099511627776d) {
			return (Math.round(dataSize / 1099511627776d * 100) / 100d) + " TB";
		} else if (dataSize >= 1073741824) {
			return (Math.round(dataSize / 1073741824d * 100) / 100d) + " GB";
		} else if (dataSize >= 1048576) {
			return (Math.round(dataSize / 1048576d * 100) / 100d) + " MB";
		} else if (dataSize >= 1024) {
			return (Math.round(dataSize / 1024d * 100) / 100d) + " KB";
		} else if (dataSize > 0) {
			return dataSize + " B ";
		} else {
			return "   0 B ";
		}
	}
	public static String getOnlineIP(HttpServletRequest request) {
		return getOnlineIP(request, false);
	}
	@SuppressWarnings("unchecked")
	public static String getOnlineIP(HttpServletRequest request, boolean format) {
		Map<String, Object> sGlobal = (Map<String, Object>) request.getAttribute("sGlobal");
		String onlineip = (String) sGlobal.get("onlineip");
		if (onlineip == null) {
			onlineip = request.getHeader("x-forwarded-for");
			if (Common.empty(onlineip) || "unknown".equalsIgnoreCase(onlineip)) {
				onlineip = request.getHeader("X-Real-IP");
			}
			if (Common.empty(onlineip) || "unknown".equalsIgnoreCase(onlineip)) {
				onlineip = request.getRemoteAddr();
			}
			onlineip = onlineip != null && onlineip.matches("^[\\d\\.]{7,15}$") ? onlineip : "unknown";
			sGlobal.put("onlineip", onlineip);
		}
		if (format) {
			String[] ips = onlineip.split("\\.");
			String stip = "000";
			StringBuffer temp = new StringBuffer();
			for (int i = 0; i < 3; i++) {
				int ip = 0;
				if (i < ips.length) {
					ip = intval(ips[i]);
				}
				temp.append(Common.sprintf(stip, ip));
			}
			return temp.toString();
		} else {
			return onlineip;
		}
	}
	
	public static String getSiteUrl(HttpServletRequest request) {
		Map<String, Object> sConfig = (Map<String, Object>) request.getAttribute("sConfig");
		Object siteAllURL = null;
		if (sConfig != null) {
			siteAllURL = sConfig.get("siteallurl");
		}
		System.out.println("siteAllURL:"+siteAllURL);
		if (Common.empty(siteAllURL)) {
			int port = request.getServerPort();
			return request.getScheme() + "://" + request.getServerName() + (port == 80 ? "" : ":" + port)
					+ request.getContextPath() + "/";
		} else {
			return siteAllURL.toString();
		}
	}
	
	public static void showMySQLMessage(HttpServletResponse response, String message, String sql,
			SQLException e) {
		String dbError = e.getMessage();
		int dbErrno = e.getErrorCode();
		try {
			PrintWriter out = response.getWriter();
			out
					.write("<div style=\"position:absolute;font-size:11px;font-family:verdana,arial;background:#BFBFBF;padding:0.5em;\">");
			out.write("<b>MySQL Error</b><br>");
			out.write("<b>Message</b>: <font color=\"red\">" + message + "</font><br>");
			if (sql != null) {
				out.write("<b>SQL</b>: " + sql + "<br>");
			}
			out.write("<b>Error</b>: <font color=\"red\">" + dbError + "<br></font>");
			out.write("<b>Errno.</b>: <font color=\"red\">" + dbErrno + "<br></font>");
			out.write("</div>");
			out.flush();
			out.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	public static String template(Map<String, Object> sConfig, Map<String, Object> sGlobal, String pageName) {
		String tpl = null;
		if (empty(sGlobal.get("mobile"))) {
			if (pageName.startsWith("/")) {
				tpl = pageName;
			} else {
				tpl = "/template/" + sConfig.get("template") + "/" + pageName;
			}
			File file = new File(JavaCenterHome.jchRoot + tpl);
			if (!file.exists()) {
				tpl = "/template/default/" + pageName;
			}
		} else {
			tpl = "/api/mobile/tpl_" + pageName;
		}
		return tpl;
	}
	public static String getCount(String tableName, Map<String, Object> whereArr, String get) {
		StringBuffer whereSql = new StringBuffer();
		if (empty(get)) {
			get = "COUNT(*)";
		}
		if (empty(whereArr)) {
			whereSql.append("1");
		} else {
			String key = null, mod = "";
			for (Iterator<String> it = whereArr.keySet().iterator(); it.hasNext();) {
				key = it.next();
				whereSql.append(mod + "`" + key + "`='" + whereArr.get(key) + "'");
				mod = " AND ";
			}
		}
		return dataBaseService.findFirst("SELECT " + get + " FROM " + JavaCenterHome.getTableName(tableName)
				+ " WHERE " + whereSql + " LIMIT 1", 1);
	}
	
	public static String getData(String var) {
		return (String) getData(var, false);
	}
	public static Object getData(String var, boolean isArray) {
		List<Map<String, Object>> values = dataBaseService.executeQuery("SELECT * FROM "
				+ JavaCenterHome.getTableName("data") + " WHERE var='" + var + "' LIMIT 1");
		if (values.size() > 0) {
			Map<String, Object> value = values.get(0);
			return isArray ? value : value.get("datavalue");
		}
		return null;
	}
	public static boolean ipAccess(String ip, Object ipAccess) {
		return empty(ipAccess) ? true : ip.matches("^("
				+ pregQuote(String.valueOf(ipAccess), '/').replaceAll("\r\n", "|").replaceAll(" ", "")
				+ ").*");
	}

	public static Object[] reNum(Map<Integer, Integer> array) {
		Map<Integer, List<Integer>> newnums = new HashMap<Integer, List<Integer>>();
		List<Integer> nums = new ArrayList<Integer>();
		Set<Integer> keys = array.keySet();
		for (Integer key : keys) {
			int num = array.get(key);
			List<Integer> newnum = newnums.get(num);
			if (newnum == null) {
				newnum = new ArrayList<Integer>();
				newnums.put(num, newnum);
			}
			newnum.add(key);
			nums.add(num);
		}
		Object[] arrayObject = new Object[] {nums, newnums};
		return arrayObject;
	}
	public static String stripSearchKey(String string) {
		if (string == null || "".equals(string)) {
			return "";
		}
		string = string.trim();
		string = Common.addSlashes(string).replace("*", "%");
		string = string.replace("_", "\\_");
		return string;
	}
	
	public static int strToTime(String string, String timeoffset) {
		return strToTime(string, timeoffset, "yyyy-MM-dd");
	}
	public static int strToTime(String string, String timeoffset, String format) {
		if (string == null || string.length() == 0) {
			return 0;
		}
		SimpleDateFormat sdf = getSimpleDateFormat(format, timeoffset);
		try {
			Date ndate = sdf.parse(string);
			if (sdf.format(ndate).equals(string)) {
				return (int) (ndate.getTime() / 1000);
			} else {
				return 0;
			}
		} catch (Exception e) {
			return 0;
		}
	}
	public static String pic_get(Map<String, Object> sConfig, String filePath, int thumb, int remote,
			boolean return_thumb) {
		String url = null;
		if (empty(filePath)) {
			url = "image/nopic.gif";
		} else {
			url = filePath;
			if (return_thumb && thumb > 0) {
				url += ".thumb.jpg";
			}
			if (remote > 0) {
				url = sConfig.get("ftpurl") + url;
			} else {
				Map<String, String> jchConfig = JavaCenterHome.jchConfig;
				url = jchConfig.get("attachUrl") + url;
			}
		}
		return url;
	}
	@SuppressWarnings("unchecked")
	public static String pic_cover_get(Map<String, Object> sConfig, String pic, int picFlag) {
		String url = null;
		if (empty(pic)) {
			url = "image/nopic.gif";
		} else {
			if (picFlag == 1) {
				Map<String, String> jchConfig = JavaCenterHome.jchConfig;
				url = jchConfig.get("attachUrl") + pic;
			} else if (picFlag == 2) {
				url = sConfig.get("ftpurl") + pic;
			} else {
				url = pic;
			}
		}
		return url;
	}

	@SuppressWarnings("unchecked")
	public static String getColor(Map<String, Object> userGroup) {
		Object color = userGroup.get("color");
		if (empty(color)) {
			return null;
		} else {
			return " style=\"color:" + color + ";\"";
		}
	}
	
	@SuppressWarnings("unchecked")
	public static String getIcon(Map<String, Object> userGroup) {
		Object icon = userGroup.get("icon");
		if (empty(icon)) {
			return null;
		} else {
			return " <img src=\"" + icon + "\" align=\"absmiddle\"> ";
		}
	}
	
	public static File[] readDir(String dir, final String... extarr) {
		File supDir = new File(dir);
		if (supDir.isDirectory()) {
			if (extarr == null || extarr.length == 0) {
				return supDir.listFiles();
			} else {
				FilenameFilter filenameFilter = new FilenameFilter() {
					public boolean accept(File dir, String name) {
						int tempI = name.lastIndexOf(".");
						String postfix = null;
						if (tempI >= 0) {
							postfix = name.substring(tempI + 1);
						} else {
							postfix = name;
						}
						return in_array(extarr, postfix);
					}
				};
				return supDir.listFiles(filenameFilter);
			}
		}
		return null;
	}
	
	public static String multi(HttpServletRequest request, int num, int perPage, int currPage, int maxPage, String ajaxDiv, String toDiv) {
		Map<String, Object> sGlobal = (Map<String, Object>) request.getAttribute("sGlobal");
		StringBuffer multiPage = new StringBuffer();
		int inAjax = (Integer) sGlobal.get("inajax");
		if (empty(ajaxDiv) && inAjax > 0) {
			ajaxDiv = request.getParameter("ajaxdiv");
		}
		int page = 5;
		if (!empty(sGlobal.get("showpage"))) {
			page = (Integer) sGlobal.get("showpage");
		}
		int realPages = 1;
		if (num > perPage) {
			int offset = 2;
			realPages = (int) Math.ceil((float) num / (float) perPage);
			int pages = maxPage > 0 && maxPage < realPages ? maxPage : realPages;
			int from = 0, to = 0;
			if (page > pages) {
				from = 1;
				to = pages;
			} else {
				from = currPage - offset;
				to = from + page - 1;
				if (from < 1) {
					to = currPage + 1 - from;
					from = 1;
					if (to - from < page) {
						to = page;
					}
				} else if (to > pages) {
					from = pages - page + 1;
					to = pages;
				}
			}

			if (currPage - offset > 1 && pages > page) {
					multiPage.append("<a href=\"#\" onclick=\"select_page('1');\" ");
					multiPage.append(" class=\"first\">1 ...</a>");
			}
			if (currPage > 1) {
					multiPage.append("<a  href=\"#\" onclick=\"select_page('");
					multiPage.append(currPage - 1);
					multiPage.append("');\" ");
					multiPage.append(" class=\"prev\">&lsaquo;&lsaquo;</a>");
			}
			for (int i = from; i <= to; i++) {
				if (i == currPage) {
					multiPage.append("<a  class=\"pager_current\" href=\"#\" onclick=\"select_page('");
					multiPage.append(i);
					multiPage.append("');\" >");
					multiPage.append(i);
					multiPage.append("</a>");
				} else {
					multiPage.append("<a  href=\"#\" onclick=\"select_page('");
					multiPage.append(i);
					multiPage.append("');\" >");
					multiPage.append(i);
					multiPage.append("</a>");
				}
			}
			if (currPage < pages) {

				multiPage.append("<a  href=\"#\" onclick=\"select_page('");
				multiPage.append(currPage + 1);
				multiPage.append("');\" ");
				multiPage.append(" class=\"next\">&rsaquo;&rsaquo;</a>");

			}
			if (to < pages) {
				multiPage.append("<a  href=\"#\" onclick=\"select_page('");
				multiPage.append(pages);
				multiPage.append("');\"");
				multiPage.append(" class=\"last\">... ");
				multiPage.append(realPages);
				multiPage.append("</a>");
			}
		}
		return multiPage.toString();
	}
	
	public static String multi(HttpServletRequest request, int num, int perPage, int currPage, int maxPage,
			String url, String ajaxDiv, String toDiv) {
		Map<String, Object> sGlobal = (Map<String, Object>) request.getAttribute("sGlobal");
		StringBuffer multiPage = new StringBuffer();
		int inAjax = (Integer) sGlobal.get("inajax");
		if (empty(ajaxDiv) && inAjax > 0) {
			ajaxDiv = request.getParameter("ajaxdiv");
		}
		int page = 5;
		if (!empty(sGlobal.get("showpage"))) {
			page = (Integer) sGlobal.get("showpage");
		}
		int realPages = 1;
		if (num > perPage) {
			int offset = 2;
			realPages = (int) Math.ceil((float) num / (float) perPage);
			int pages = maxPage > 0 && maxPage < realPages ? maxPage : realPages;
			int from = 0, to = 0;
			if (page > pages) {
				from = 1;
				to = pages;
			} else {
				from = currPage - offset;
				to = from + page - 1;
				if (from < 1) {
					to = currPage + 1 - from;
					from = 1;
					if (to - from < page) {
						to = page;
					}
				} else if (to > pages) {
					from = pages - page + 1;
					to = pages;
				}
			}
			url += url.indexOf("?") == -1 ? "?" : "&";
			String urlPlus = !empty(toDiv) ? "#" + toDiv : "";
			if (currPage - offset > 1 && pages > page) {
				if (inAjax > 0) {
					multiPage.append("<a href=\"javascript:;\" onclick=\"ajaxget('");
					multiPage.append(url);
					multiPage.append("page=1&ajaxdiv=");
					multiPage.append(ajaxDiv);
					multiPage.append("','");
					multiPage.append(ajaxDiv);
					multiPage.append("')\" class=\"first\">1 ...</a>");
				} else {
					multiPage.append("<a href=\"");
					multiPage.append(url);
					multiPage.append("page=1");
					multiPage.append(urlPlus);
					multiPage.append("\" class=\"first\">1 ...</a>");
				}
			}
			if (currPage > 1) {
				if (inAjax > 0) {
					multiPage.append("<a href=\"javascript:;\" onclick=\"ajaxget('");
					multiPage.append(url);
					multiPage.append("page=");
					multiPage.append(currPage - 1);
					multiPage.append("&ajaxdiv=");
					multiPage.append(ajaxDiv);
					multiPage.append("','");
					multiPage.append(ajaxDiv);
					multiPage.append("')\" class=\"prev\">&lsaquo;&lsaquo;</a>");
				} else {
					multiPage.append("<a href=\"");
					multiPage.append(url);
					multiPage.append("page=");
					multiPage.append(currPage - 1);
					multiPage.append(urlPlus);
					multiPage.append("\" class=\"prev\">&lsaquo;&lsaquo;</a>");
				}
			}
			for (int i = from; i <= to; i++) {
				if (i == currPage) {
					multiPage.append("<strong>");
					multiPage.append(i);
					multiPage.append("</strong>");
				} else {
					if (inAjax > 0) {
						multiPage.append("<a href=\"javascript:;\" onclick=\"ajaxget('");
						multiPage.append(url);
						multiPage.append("page=");
						multiPage.append(i);
						multiPage.append("&ajaxdiv=");
						multiPage.append(ajaxDiv);
						multiPage.append("','");
						multiPage.append(ajaxDiv);
						multiPage.append("')\">");
					} else {
						multiPage.append("<a href=\"");
						multiPage.append(url);
						multiPage.append("page=");
						multiPage.append(i);
						multiPage.append(urlPlus);
						multiPage.append("\">");
					}
					multiPage.append(i);
					multiPage.append("</a>");
				}
			}
			if (currPage < pages) {
				if (inAjax > 0) {
					multiPage.append("<a href=\"javascript:;\" onclick=\"ajaxget('");
					multiPage.append(url);
					multiPage.append("page=");
					multiPage.append(currPage + 1);
					multiPage.append("&ajaxdiv=");
					multiPage.append(ajaxDiv);
					multiPage.append("','");
					multiPage.append(ajaxDiv);
					multiPage.append("')\" class=\"next\">&rsaquo;&rsaquo;</a>");
				} else {
					multiPage.append("<a href=\"");
					multiPage.append(url);
					multiPage.append("page=");
					multiPage.append(currPage + 1);
					multiPage.append(urlPlus);
					multiPage.append("\" class=\"next\">&rsaquo;&rsaquo;</a>");
				}
			}
			if (to < pages) {
				if (inAjax > 0) {
					multiPage.append("<a href=\"javascript:;\" onclick=\"ajaxget('");
					multiPage.append(url);
					multiPage.append("page=");
					multiPage.append(pages);
					multiPage.append("&ajaxdiv=");
					multiPage.append(ajaxDiv);
					multiPage.append("','");
					multiPage.append(ajaxDiv);
					multiPage.append("')\" class=\"last\">... ");
				} else {
					multiPage.append("<a href=\"");
					multiPage.append(url);
					multiPage.append("page=");
					multiPage.append(pages);
					multiPage.append(urlPlus);
					multiPage.append("\" class=\"last\">... ");
				}
				multiPage.append(realPages);
				multiPage.append("</a>");
			}
			if (multiPage.length() > 0) {
				multiPage.insert(0, "<em>&nbsp;" + num + "&nbsp;</em>");
			}
		}
		return multiPage.toString();
	}
	
	
	public static String fileext(String filePath) {
		int sl = filePath.length();
		if (sl < 2) {
			return "";
		}
		int lastPoint = filePath.lastIndexOf(".");
		if (lastPoint < 0) {
			return "";
		}
		return filePath.substring(lastPoint + 1, filePath.length());
	}
	
	public static void realname_set(Map<String, Object> sGlobal, Map<String, Object> sConfig,
			Map<Integer, String> sNames, int uid, String username, String name, int namestatus) {
		if (!empty(name)) {
			sNames.put(uid, (Integer) sConfig.get("realname") > 0 && namestatus > 0 ? name : username);
		} else if (empty(sNames.get(uid))) {
			sNames.put(uid, username);
			Map<Integer, Integer> select_realname = (Map<Integer, Integer>) sGlobal.get("select_realname");
			if (select_realname == null) {
				select_realname = new HashMap<Integer, Integer>();
			}
			select_realname.put(uid, uid);
			sGlobal.put("select_realname", select_realname);
		}
	}
	public static void realname_get(Map<String, Object> sGlobal, Map<String, Object> sConfig,
			Map<Integer, String> sNames, Map<String, Object> space) {
		if (empty(sGlobal.get("_realname_get")) && !empty(sConfig.get("realname"))
				&& !empty(sGlobal.get("select_realname"))) {
			sGlobal.put("_realname_get", 1);
			Map<Integer, Integer> select_realname = (Map<Integer, Integer>) sGlobal.get("select_realname");
			if (space != null && select_realname != null && select_realname.get(space.get("uid")) != null) {
				select_realname.remove(space.get("uid"));
			}
			Map<String, Object> member = (Map<String, Object>) sGlobal.get("member");
			if (member != null) {
				if (!empty(member.get("uid")) && select_realname != null
						&& select_realname.get(member.get("uid")) != null) {
					select_realname.remove(member.get("uid"));
				}
			}
			Set<Integer> uids = empty(select_realname) ? null : select_realname.keySet();
			if (!empty(uids)) {
				List<Map<String, Object>> spaceList = dataBaseService
						.executeQuery("SELECT uid, name, namestatus FROM "
								+ JavaCenterHome.getTableName("space") + " WHERE uid IN (" + sImplode(uids)
								+ ")");
				for (Map<String, Object> value : spaceList) {
					if (!empty(value.get("name")) && (Integer) value.get("namestatus") > 0) {
						sNames.put((Integer) value.get("uid"), (String) value.get("name"));
					}
				}
			}
		}
	}
	
	
	public static void setResponseHeader(HttpServletResponse response) {
		setResponseHeader(response, "text/html");
	}
	public static void setResponseHeader(HttpServletResponse response, String type) {
		response.setContentType(type + "; charset=" + JavaCenterHome.JCH_CHARSET);
		response.setHeader("Cache-Control", "no-store"); 
		response.setHeader("Program", "no-cache"); 
		response.setDateHeader("Expirse", 0);
	}
	
	public static Calendar getCalendar(String timeoffset) {
		return Calendar.getInstance(TimeZone.getTimeZone(timeZoneIDs.get(timeoffset)[0]));
	}
	public static String getTimeOffset(Map<String, Object> sGlobal, Map<String, Object> sConfig) {
		Map<String, Object> member = (Map<String, Object>) sGlobal.get("member");
		String timeoffset = null;
		if (member != null) {
			timeoffset = (String) member.get("timeoffset");
		}
		if (Common.empty(timeoffset)) {
			timeoffset = sConfig.get("timeoffset").toString();
		}
		return timeoffset;
	}

	public static String debugInfo(Map<String, Object> sGlobal, Map<String, Object> sConfig) {
		int debuginfo = (Integer) sConfig.get("debuginfo");
		if (debuginfo > 0) {
			long startTime = (Long) sGlobal.get("starttime");
			long endTime = System.currentTimeMillis();
			String totalTime = sprintf("0.0000", (endTime - startTime) / 1000f);
			String gzipCompress = Common.intval(JavaCenterHome.jchConfig.get("gzipCompress")) > 0 ? ", Gzip enabled"
					: "";
			return "Processed in " + totalTime + " second(s) " + gzipCompress + ".";
		} else {
			return null;
		}
	}
	public static String getImageType(File imgFile) {
		String imageType = null;
		try {
			ImageInputStream iis = ImageIO.createImageInputStream(imgFile);
			Iterator<ImageReader> iter = ImageIO.getImageReaders(iis);
			if (iter.hasNext()) {
				ImageReader reader = iter.next();
				imageType = reader.getFormatName().toLowerCase();
			}
			iis.close();
		} catch (IOException e) {
		}
		return imageType;
	}
	
	public static Object sarrayRand(Object arr, int num) {
		if (Common.empty(arr)) {
			return arr;
		}
		if (arr instanceof Object[]) {
			Object[] temp = (Object[]) arr;
			if (temp.length > num) {
				Object[] r_values = new Object[num];
				if (num > 1) {
					Integer[] r_keys = (Integer[]) arrayRand(temp, num);
					for (int i = 0; i < num; i++) {
						r_values[i] = temp[r_keys[i]];
					}
				} else {
					Integer[] r_keys = (Integer[]) arrayRand(temp, 1);
					r_values[0] = temp[r_keys[0]];
				}
				return r_values;
			}
		} else if (arr instanceof List) {
			List temp = (List) arr;
			if (temp.size() > num) {
				try {
					List r_values = new ArrayList();
					if (num > 1) {
						Integer[] r_keys = (Integer[]) arrayRand(temp, num);
						for (Integer key : r_keys) {
							r_values.add(temp.get(key));
						}
					} else {
						Integer[] r_keys = (Integer[]) arrayRand(temp, 1);
						r_values.add(temp.get(r_keys[0]));
					}
					return r_values;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else if (arr instanceof Map) {
			Map temp = (Map) arr;
			if (temp.size() > num) {
				Map r_values = new LinkedHashMap();
				if (num > 1) {
					Object[] r_keys = (Object[]) arrayRand(temp, num);
					for (Object key : r_keys) {
						r_values.put(key, temp.get(key));
					}
				} else {
					Object[] r_keys = (Object[]) arrayRand(temp, 1);
					r_values.put(r_keys[0], temp.get(r_keys[0]));
				}
				return r_values;
			}
		}
		return arr;
	}
	public static Object[] arrayRand(Object input, int num) {
		if (input == null) {
			return new Object[] {};
		}
		num = num <= 0 ? 1 : num;
		int max = 0;
		Object[] objs = null;
		if (input instanceof Object[]) {
			objs = (Object[]) input;
			max = objs.length;
			if (num == 1) {
				return new Integer[] {Common.rand(0, max - 1)};
			}
			if (max <= num) {
				Integer[] returnKeys = new Integer[max];
				for (int i = 0; i < max; i++) {
					returnKeys[i] = i;
				}
				return returnKeys;
			}
		} else if (input instanceof Collection) {
			Collection<Object> temp = (Collection<Object>) input;
			max = temp.size();
			if (num == 1) {
				return new Integer[] {Common.rand(0, max - 1)};
			}
			if (max <= num) {
				Integer[] returnKeys = new Integer[max];
				for (int i = 0; i < max; i++) {
					returnKeys[i] = i;
				}
				return returnKeys;
			}
		} else if (input instanceof Map) {
			Map<Object, Object> temp = (Map<Object, Object>) input;
			Set<Object> keys = temp.keySet();
			max = keys.size();
			objs = keys.toArray();
			if (num == 1) {
				return new Object[] {objs[Common.rand(0, max - 1)]};
			}
			if (max <= num) {
				return objs;
			}
		} else {
			return new Object[] {};
		}
		int i = 0;
		Set<Integer> set = new HashSet<Integer>();
		while (i < num) {
			set.add(Common.rand(0, max - 1));
			i = set.size();
		}
		if (input instanceof Object[] || input instanceof Collection) {
			return set.toArray(new Integer[num]);
		} else {
			Object[] returnKeys = new Object[num];
			int j = 0;
			for (Integer randKey : set) {
				returnKeys[j] = objs[randKey];
				j++;
			}
			return returnKeys;
		}
	}
	@SuppressWarnings("unchecked")
	public static List getRandList(List list, int num) {
		int size = list == null ? 0 : list.size();
		if (num > 0 && num <= size) {
			List temp = new ArrayList(num);
			for (int i = 0; i < num; i++) {
				int rand = rand(size - 1);
				if (temp.indexOf(rand) == -1) {
					temp.add(list.get(rand));
				} else {
					i--;
				}
			}
			return temp;
		}
		return null;
	}
	public static int mkTime(int hour, int min, int sec, int mon, int day, int year) {
		if (0 >= (mon -= 2)) {
			mon += 12;
			year -= 1;
		}
		int y = (year - 1) * 365 + year / 4 - year / 100 + year / 400;
		int m = 367 * mon / 12 - 30 + 59;
		int d = day - 1;
		int x = y + m + d - 719162;
		int t = ((x * 24 + hour) * 60 + min) * 60 + sec;
		return t;
	}
	public static String smulti(Map<String, Object> sGlobal, int start, int perPage, int count, String url,
			String ajaxDiv) throws Exception {
		Map<String, Integer> multi = new HashMap<String, Integer>();
		multi.put("last", -1);
		multi.put("next", -1);
		multi.put("begin", -1);
		multi.put("end", -1);
		String html = null;
		if (start > 0) {
			if (count == 0) {
				throw new Exception("no_data_pages");
			} else {
				multi.put("last", start - perPage);
			}
		}
		ajaxDiv = ajaxDiv == null ? "" : ajaxDiv;
		boolean showHtml = false;
		if (count == perPage) {
			multi.put("next", start + perPage);
		}
		multi.put("begin", start + 1);
		multi.put("end", start + count);
		if (multi.get("begin") >= 0) {
			if (multi.get("last") >= 0) {
				showHtml = true;
				if (!empty(sGlobal.get("inajax"))) {
					html = "<a href=\"javascript:;\" onclick=\"ajaxget('" + url + "&ajaxdiv=" + ajaxDiv
							+ "', '" + ajaxDiv
							+ "')\">|&lt;</a> <a href=\"javascript:;\" onclick=\"ajaxget('" + url + "&start="
							+ multi.get("last") + "&ajaxdiv=" + ajaxDiv + "', '" + ajaxDiv + "')\">&lt;</a> ";
				} else {
					html = "<a href=\"" + url + "\">|&lt;</a> <a href=\"" + url + "&start="
							+ multi.get("last") + "\">&lt;</a> ";
				}
			} else {
				html = "&lt;";
			}
			html += " " + multi.get("begin") + "~" + multi.get("end") + " ";
			if (multi.get("next") >= 0) {
				showHtml = true;
				if (!empty(sGlobal.get("inajax"))) {
					html += " <a href=\"javascript:;\" onclick=\"ajaxget('" + url + "&start="
							+ multi.get("next") + "&ajaxdiv=" + ajaxDiv + "', '" + ajaxDiv + "')\">&gt;</a> ";
				} else {
					html += " <a href=\"" + url + "&start=" + multi.get("next") + "\">&gt;</a>";
				}
			} else {
				html += " &gt;";
			}
		}
		return showHtml ? html : "";
	}
	public static Object siconv(String str, String outCharset, String inCharset, String jchConfigCharset) {
		return str;
	}
	
	public static long getByteSizeByBKMG(String unitSize) {
		long maxSize = 0;
		String maxFileSize = unitSize;
		Matcher matcher = Pattern.compile("\\d+([bkmg]?)").matcher(maxFileSize.toLowerCase());
		if (matcher.matches()) {
			String ch = matcher.replaceAll("$1");
			if (ch.equals("k")) {
				maxSize = Common.intval(maxFileSize) * 1024;
			} else if (ch.equals("m")) {
				maxSize = Common.intval(maxFileSize) * 1024 * 1024;
			} else if (ch.equals("g")) {
				maxSize = Common.intval(maxFileSize) * 1024 * 1024;
			} else {
				maxSize = Common.intval(maxFileSize);
			}
		}
		return maxSize;
	}
	public static String sub_url(String url, int length) throws UnsupportedEncodingException {
		if (url == null) {
			return "";
		}
		if (url.length() > length) {
			url = URLEncoder.encode(url, JavaCenterHome.JCH_CHARSET);
			url = url.replace("%3A", ":").replace("%2F", "/");
			url = url.substring(0, length / 2) + " ... "
					+ url.substring(url.length() - (int) (length * 0.3F));
		}
		return url;
	}
	
	
	public static void main(String[] args){
	
	}
	
}