package com.jskj.shop.action.web.front;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.shop.dao.impl.ArticleService;
import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.impl.UserService;
import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.entity.vo.ItemRecomm;
import com.jskj.shop.pay.GetWxOrderno;
import com.jskj.shop.pay.RequestHandler;
import com.jskj.shop.pay.Sha1Util;
import com.jskj.shop.pay.TenpayUtil;
import com.jskj.shop.pay.http.HttpConnect;
import com.jskj.shop.pay.http.HttpResponse;
import com.jskj.shop.util.RegionData;
import com.jskj.shop.util.RequestUtils;
import com.jskj.shop.util.ResponseUtils;
import com.jskj.shop.util.SysConstants;

@Controller
public class FrontAct {
	
	public static String REGION_URL = "http://h5.4007060700.com/regions.json";
	public static Logger logger = Logger.getLogger(FrontAct.class);
	
	@RequestMapping(value = "/index.jspx")
	public String jskj_index(String chid,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);
		ItemService itemService = (ItemService)BeanFactory.getBean("itemService");
		
		//查询焦点广告
		List<Map<String,Object>> ads = itemService.findAds(2, 5);
		
		//查询热门标签
		List<Map<String,Object>> hots =itemService.findHotTag(4);
		
		//查询推荐标签
		List<ItemRecomm> recomms = new ArrayList<ItemRecomm>();
		List<Map<String,Object>> recommTags = itemService.findRecommTag(5);
		
		//根据推荐标签查询热门产品
		if(recommTags != null && recommTags.size() >0){
			for(int i=0;i<recommTags.size();i++){
				ItemRecomm ir = new ItemRecomm();
				Map<String,Object> tag = recommTags.get(i);
				List<Map<String,Object>> items =  itemService.findItemDataByTagId((Integer)tag.get("id"), 1, 1, 4);
				
				ir.setItems(items);
				ir.setTag(tag);
				
				recomms.add(ir);
			}
		}
		
		model.addAttribute("ads", ads);
		model.addAttribute("hots", hots);
		model.addAttribute("recomms", recomms);
		
		return "main/shouye.html";
	}
	
	@RequestMapping(value = "/category.jspx")
	public String category_index(String chid,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _uid = getCurrUser( request,  response);
		String _chid = getChannel(chid, request,  response);
		
		ItemService itemService = (ItemService)BeanFactory.getBean("itemService");
		List<Map<String,Object>> categorys= itemService.findItemCategorys(-1, 1, 100);
		
		model.addAttribute("categorys", categorys);
		
		return "main/category.html";
	}
	
	@RequestMapping(value = "/article.jspx")
	public String article_detail(String chid,Integer articleid,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);
		
		ArticleService articleService = (ArticleService)BeanFactory.getBean("articleService");
		articleService.updateArticleNum(articleid);

		Map<String,Object> article = articleService.findArticleById(articleid);
		
		model.addAttribute("article", article);
		
		return "main/article.html";
	}
	
	@RequestMapping(value = "/article_list.jspx")
	public String article_list(String chid,Integer cid,Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);

		ItemService itemService = (ItemService)BeanFactory.getBean("itemService");
		if(pageno == null || pageno<1) pageno=1;
		if(pagesize == null || pagesize>100) pagesize=20;
		
		int count = 0;
	
		List<Map<String,Object>> articles= null;
	
		List<Map<String,Object>> categorys = itemService.findArticleCategory();

		articles = itemService.findArticleList(cid, pageno, pagesize);
		count = itemService.totalArticleList(cid);

		model.addAttribute("count", count);
		model.addAttribute("cid", cid);

		model.addAttribute("pageno", pageno);
		model.addAttribute("pagesize", pagesize);
		model.addAttribute("articles", articles);
		model.addAttribute("categorys", categorys);
		
		model.addAttribute("pages",(count+pagesize-1)/pagesize);
		
		return "main/article_list.html";
		
	}
	
	
	@RequestMapping(value = "/search.jspx")
	public String search_index(String chid,String keyword,Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		logger.info("keyword:"+keyword);
		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);

		if(keyword == null || keyword.equals("") || keyword.indexOf("'")>-1){
			keyword = "套";
		} 
		
		ItemService itemService = (ItemService)BeanFactory.getBean("itemService");
		if(pageno == null || pageno<1) pageno=1;
		if(pagesize == null || pagesize>100) pagesize=20;
		
		List<Map<String,Object>> items = itemService.findItemsFromKeyword(keyword, 1, pageno, pagesize);
		int count = itemService.totalItemsFromKeyword(keyword, 1);
		
		model.addAttribute("keyword", keyword);
		model.addAttribute("pageno", pageno);
		model.addAttribute("pagesize", pagesize);
		model.addAttribute("pages", (count+pagesize-1)/pagesize);
		model.addAttribute("items", items);

		return "main/search_list.html";
	}
	
	@RequestMapping(value = "/ranks.jspx")
	public String rank_list(String chid,Integer stype,Integer id,Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);
		
		if(stype == null || stype <1){
			return null;
		}
		
		ItemService itemService = (ItemService)BeanFactory.getBean("itemService");
		if(pageno == null || pageno<1) pageno=1;
		if(pagesize == null || pagesize>100) pagesize=20;
		
		int count = 0;
		String name = "";
		List<Map<String,Object>> items= null;
		if(stype == 1){
			items = itemService.findItemDataByCategory(id, 1, pageno, pagesize);
			count = itemService.totalItemsByCategory(id, 1);
			Map<String,Object> rankObj = itemService.findCategoryById(id);
			name = (String)rankObj.get("name");
		} else if(stype == 2){
			items = itemService.findItemDataByTagId(id, 1, pageno, pagesize);
			count = itemService.totalItemsByTagId(id);
			Map<String,Object> rankObj = itemService.findTagById(id);
			name = (String)rankObj.get("tag_name");
		} else if(stype == 3){
			items = itemService.findItemFromAreaId(id, -1, pageno,pagesize);
			count = itemService.totalItemFromAreaId(id, -1);
			Map<String,Object> rankObj = itemService.findAreaById(id);
			name = (String)rankObj.get("area_name");
		}
		
		model.addAttribute("stype", stype);
		model.addAttribute("count", count);
		model.addAttribute("id", id);
		model.addAttribute("name", name);
		model.addAttribute("pageno", pageno);
		model.addAttribute("pagesize",pagesize);
		model.addAttribute("items", items);
		
		model.addAttribute("pages",(count+pagesize-1)/pagesize);
		
		return "main/rank_list.html";
	}
	
	
	@RequestMapping(value = "/detail.jspx")
	public String item_detail(String chid,Integer id,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _uid = getCurrUser( request,  response);
		String _chid = getChannel(chid, request,  response);
		
		ItemService itemService = (ItemService)BeanFactory.getBean("itemService");
		
		Map<String,Object> detail = itemService.findItemById(id);
		List<Map<String,Object>> pictures = itemService.findPicturesByItemId(id,1);
		List<Map<String,Object>> intros = itemService.findPicturesByItemId(id,2);
		List<Map<String,Object>> skus = itemService.findSkuFromItemId(id);
		
		if(skus != null && skus.size() >0){
			model.addAttribute("sku", skus.get(0));
		}
		
		List<Map<String,Object>> buys = itemService.findSimpleTrades(id, 1, SysConstants.DEFAULT_PAGESIZE);
		
		model.addAttribute("detail", detail);
		model.addAttribute("pictures", pictures);
		model.addAttribute("intros", intros);
		model.addAttribute("skus", skus);
		
		model.addAttribute("buys", buys);
		
		return "main/detail.html";
		
	}
	

	@RequestMapping(value = "/check_couponcode.jspx")
	public void check_couponcode(String chid,String ccode,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _uid = getCurrUser( request,  response);
		String _chid = getChannel(chid, request,  response);
		
		ItemService itemService = (ItemService)BeanFactory.getBean("itemService");
		
		double amount = 0;
		if(ccode != null && !ccode.equals("") && ccode.indexOf("'")==-1){
			List<Map<String,Object>> codes = itemService.findCouponCode( ccode.trim());
			if(codes != null && codes.size() >0){
				Map<String,Object> codeItem = codes.get(0);
				amount = Double.parseDouble((String)codeItem.get("amount"));
			}
		}

		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		ResponseUtils.renderJson(response, "{\"amount\":"+amount+"}");

		return ;
		
		
	}
	
	
	@RequestMapping(value = "/regions.jspx")
	public void region(HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _uid = getCurrUser( request,  response);
		
		//String regions = Tools.httpRequest(REGION_URL, "GET", null, true);
		logger.info("regions:");
		String regions = RegionData.city;
		ResponseUtils.renderText(response, regions);

	}
	
	@RequestMapping(value = "/shipping.jspx")
	public void shipping_charge(String chid,String cost,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _uid = getCurrUser( request,  response);
		String _chid = getChannel(chid, request,  response);
		
		logger.info("cost:"+cost);
		ItemService itemService = (ItemService)BeanFactory.getBean("itemService");
		
		Map<String,Object> yunfeiMap = itemService.findYunfeiList(cost);
		if(yunfeiMap != null){
			model.addAttribute("yunfei", yunfeiMap.get("amount"));
			int yf =  (Integer)yunfeiMap.get("amount");
			ResponseUtils.renderJson(response, "{\"yunfei\":"+yf+"}");
		}
		
		ResponseUtils.renderJson(response, "{\"yunfei\":"+10+"}");
		
	}

	@RequestMapping(value = "/carts.jspx")
	public String carts_list(String chid,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){

		String _uid = getCurrUser( request,  response);
		String _chid = getChannel(chid, request,  response);
		
		return "carts/cart_list.html";
	}
	
	@RequestMapping(value = "/no_order.jspx")
	public String order_no(String chid,String keyword,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);
		model.addAttribute("test", "test conent");
		
		return "order/no_order.html";
	}
	
	@RequestMapping(value = "/orders.jspx")
	public String order_list(String chid,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){

		UserService userService = (UserService) BeanFactory.getBean("userService");
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);
		Map<String,Object> user = userService.findUserById(_uid);
		
		if(user != null){
			Integer uid = (Integer)user.get("user_id");
			List<Map<String,Object>> trades = itemService.findTradesFromUserId(uid, 1, 100);
			model.addAttribute("trades", trades);
		}
		
		return "main/trade_list.html";
	}

	@RequestMapping(value = "/enter_pay.jspx")
	public String pay_enter(String chid,String from,String oid,Integer skuid,String item_ids,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);
		ItemService itemService = (ItemService)BeanFactory.getBean("itemService");
		
		
		if(skuid != null && skuid > 0){
			Map<String,Object> sku = itemService.findSkuById(skuid);
			model.addAttribute("sku", sku);
		}

		model.addAttribute("item_ids", item_ids);
	
		if(from != null && from.equals("orders")){
			Map<String,Object> trade = itemService.findTradeByOid(oid);
			model.addAttribute("trade", trade);
			
			List<Map<String,Object>> tradeOrders =  itemService.findOrderByTid((String)trade.get("t_id"));
			model.addAttribute("orders", tradeOrders);
			
		}

		return "trade/enter_pay.html";
		
	}
	
	@RequestMapping(value = "/order_detail.jspx")
	public String order_detail(String chid,String oid,HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		String _uid = getCurrUser( request,  response);
		String _chid = getChannel(chid, request,  response);
		
		Integer userid = Integer.parseInt(_uid);
		
		Map<String,Object> trade = itemService.findTradeByOid(oid);
		model.addAttribute("trade", trade);
		
		List<Map<String,Object>> tradeOrders =  itemService.findOrderByTid((String)trade.get("t_id"));
		model.addAttribute("orders", tradeOrders);
		
		return "trade/confirm_pay.html";
		
	}
	
	@RequestMapping(value = "/confirm_pay.jspx")
	public String pay_confirm(String chid,String user_name,String user_phone,String user_address,String user_paytype,String user_items,
			String user_fee,String user_charge,String ccode,String user_coupon,String buyer_message,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){

		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);
		Integer userid = Integer.parseInt(_uid);
		
		logger.info("user_name:"+user_name);
		logger.info("user_phone:"+user_phone);
		logger.info("user_address:"+user_address);
		
		logger.info("user_paytype:"+user_paytype);
		logger.info("user_fee:"+user_fee);
		logger.info("user_charge:"+user_charge);
		logger.info("user_items:"+user_items);
		logger.info("user_coupon:"+user_coupon);
		
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		double coupon = 0;
		if(ccode != null && !ccode.equals("") && ccode.indexOf("'")==-1){
			List<Map<String,Object>> codes = itemService.findCouponCode( ccode.trim());
			if(codes != null && codes.size() >0){
				Map<String,Object> codeItem = codes.get(0);
				coupon = Double.parseDouble((String)codeItem.get("amount"));
			}
			itemService.updateCouponStatus(ccode);
		}

		Integer coupon_id = null;
		if(user_coupon != null && !user_coupon.equals("")) {
			coupon_id = Integer.parseInt(user_coupon);
		}
		double amount = 0;
		double userFee = Double.parseDouble(user_fee);
		
		if(userFee > coupon){
			amount = userFee - coupon;
		} else {
			amount = 0;
		}
		
		String oid = itemService.saveTradeInfo(_chid,user_name, user_phone, "", user_address,
				Integer.parseInt(user_paytype),coupon,coupon_id, user_items, buyer_message, 
				amount,user_charge, userid);

		//简单订单信息保存成功
		itemService.saveSimpleTrade( null, user_address, user_items);
		
		if(user_paytype.equals("0") ){
			String url = "/get_authorize.jspx?oid="+oid;
			try {
				response.sendRedirect(url);
			} catch (Exception e) {
				logger.error("/get_authorize.jspx exception");
			}
			return null;
		}
		
		Map<String,Object> trade = itemService.findTradeByOid(oid);
		model.addAttribute("trade", trade);
		
		List<Map<String,Object>> tradeOrders =  itemService.findOrderByTid((String)trade.get("t_id"));
		model.addAttribute("orders", tradeOrders);
		
		return "trade/confirm_pay.html";
		
	}

	@RequestMapping(value = "/result_pay.jspx")
	public String result_pay(String chid,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _chid = getChannel(chid, request,  response);
		String _uid = getCurrUser( request,  response);
		
		return "trade/result_pay.html";
		
	}

	public String getCurrUser(HttpServletRequest request, HttpServletResponse response){
		
		String mkey = "";
		Cookie[] cookies =  request.getCookies();
		if(cookies != null && cookies.length >0){
			for(int i=0;i<cookies.length;i++){
				
				String name = cookies[i].getName();
				String value = cookies[i].getValue();
				
				if(name.equals(SysConstants.CHECK_USERID) ){
					if(value != null && !value.equals("")){
						mkey = value;
					}
				}

			}
		}
		
		if(mkey == null || mkey.equals("")){
			UserService userService = (UserService) BeanFactory.getBean("userService");
			mkey = ""+userService.getUserFromWeb();
			Cookie cookie = new Cookie(SysConstants.CHECK_USERID,""+mkey);
			response.addCookie(cookie);
		}
		
		return mkey;
		
	}
	
public String getChannel(String chid,HttpServletRequest request, HttpServletResponse response){
		
		String mkey = "";
		Cookie[] cookies =  request.getCookies();
		if(cookies != null && cookies.length >0){
			for(int i=0;i<cookies.length;i++){
				
				String name = cookies[i].getName();
				String value = cookies[i].getValue();
				
				if(name.equals(SysConstants.CHECK_CHID) ){
					if(value != null && !value.equals("")){
						mkey = value;
					}
				}

			}
		}
		
		if(chid != null && !chid.equals("")){
			Cookie cookie = new Cookie(SysConstants.CHECK_CHID,""+chid);
			response.addCookie(cookie);
			mkey = chid;
		}
		
		return mkey;
		
	}
	
	@RequestMapping(value = "/get_authorize.jspx")
	public void get_authorize(String oid,HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		String _uid = getCurrUser( request,  response);
		
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		Map<String,Object> trade = itemService.findTradeByOid(oid);
		
		//共账号及商户相关参数
		String appid = "wxb44a87b3362af380";
		String backUri = SysConstants.HOST_URL+"/get_openid.jspx";
		String money = (String)trade.get("price");
		String describe = SysConstants.SHOP_NAME;
		
		//授权后要跳转的链接所需的参数一般有会员号，金额，订单号之类，
		//最好自己带上一个加密字符串将金额加上一个自定义的key用MD5签名或者自己写的签名,
		//比如 Sign = %3D%2F%CS% 
		backUri = backUri+"?userId="+_uid+"&orderNo="+oid+"&describe="+describe+"&money="+money;
		//URLEncoder.encode 后可以在backUri 的url里面获取传递的所有参数
		backUri = URLEncoder.encode(backUri);
		int state = RandomUtils.nextInt(10000);
		//scope 参数视各自需求而定，这里用scope=snsapi_base 不弹出授权页面直接授权目的只获取统一支付接口的openid
		String url = "https://open.weixin.qq.com/connect/oauth2/authorize?" +
				"appid=" + appid+
				"&redirect_uri=" +
				 backUri+
				"&response_type=code&scope=snsapi_base&state="+state+"#wechat_redirect";
		try {
			response.sendRedirect(url);
		} catch (Exception e) {
			logger.error("get_authorize exception");
		}

		return;
	}
	
	@RequestMapping(value = "/get_openid.jspx")
	public String get_openid(String userId,String orderNo,String money,String code,
			HttpServletRequest request,	HttpServletResponse response, ModelMap model){

		//金额转化为分为单位
		float sessionmoney = Float.parseFloat(money);
		String finalmoney = String.format("%.2f", sessionmoney);
		finalmoney = finalmoney.replace(".", "");
		
		//商户相关资料 
		String appid = "wxb44a87b3362af380";
		String appsecret = "ad2799a9f9cca12dbd3d77f8d6b5c1f8";

		
		String URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+appid+"&secret="+appsecret+"&code="+code+"&grant_type=authorization_code";
		
		HttpResponse temp = HttpConnect.getInstance().doGetStr(URL);		
		String tempValue="";
		if( temp == null){
				return null;
		} else {
			try {
				tempValue = temp.getStringResult();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			JSONObject  jsonObj = JSONObject.fromObject(tempValue);
			if(jsonObj.containsKey("errcode")){
				System.out.println(tempValue);
				return null;
			}
			
			String openId = jsonObj.getString("openid");
			model.addAttribute("openid", openId);
			
			ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
			Map<String,Object> trade = itemService.findTradeByOid(orderNo);
			model.addAttribute("trade", trade);
			
			List<Map<String,Object>> tradeOrders =  itemService.findOrderByTid((String)trade.get("t_id"));
			model.addAttribute("orders", tradeOrders);
			
			return "trade/confirm_pay.html";
		}

	}
	
	@RequestMapping(value = "/prepay.jspx")
	public void prepay(String chid,String orderNo,String money,String openid,
			HttpServletRequest request,	HttpServletResponse response, ModelMap model){
		
		String _uid = getCurrUser( request,  response);
		String _chid = getChannel(chid, request,  response);
		
		//金额转化为分为单位
		float sessionmoney = Float.parseFloat(money);
		String finalmoney = String.format("%.2f", sessionmoney);
		finalmoney = finalmoney.replace(".", "");
		
		//商户相关资料 
		String appid = "wxb44a87b3362af380";
		String appsecret = "ad2799a9f9cca12dbd3d77f8d6b5c1f8";
		
		String partner = "1306452601";
		String partnerkey = "49e056afa06f6ae4342ff8c8ebf54d83";

		//获取openId后调用统一支付接口https://api.mch.weixin.qq.com/pay/unifiedorder
		String currTime = TenpayUtil.getCurrTime();
		
		//8位日期
		String strTime = currTime.substring(8, currTime.length());
		//四位随机数
		
		String strRandom = TenpayUtil.buildRandom(4) + "";
		//10位序列号,可以自行调整。
		String strReq = strTime + strRandom;

		//商户号
		String mch_id = partner;

		//设备号   非必输
		String device_info="";
		
		//随机数 
		String nonce_str = strReq;

		//商品描述根据情况修改
		String body = SysConstants.SHOP_NAME;
				
		//商户订单号
		String out_trade_no = orderNo;
		int intMoney = Integer.parseInt(finalmoney);
				
		//总金额以分为单位，不带小数点
		int total_fee = intMoney;
		
		//订单生成的机器 IP
		String spbill_create_ip = request.getRemoteAddr();

		//这里notify_url是 支付完成后微信发给该链接信息，可以判断会员是否支付成功，改变订单状态等。
		String notify_url =SysConstants.HOST_URL+"/pay_notify.jspx";

		String trade_type = "JSAPI";

		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", appid);  
		packageParams.put("mch_id", mch_id);  
		packageParams.put("nonce_str", nonce_str);  
		packageParams.put("body", body);  
		packageParams.put("attach", _uid);  
		packageParams.put("out_trade_no", out_trade_no);  

		packageParams.put("total_fee", ""+total_fee);  
		packageParams.put("spbill_create_ip", spbill_create_ip);  
		packageParams.put("notify_url", notify_url);  
				
		packageParams.put("trade_type", trade_type);  
		packageParams.put("openid", openid);  

		RequestHandler reqHandler = new RequestHandler(request, response);
		reqHandler.init(appid, appsecret, partnerkey);
				
		String sign = reqHandler.createSign(packageParams);
		String xml="<xml>"+
						"<appid>"+appid+"</appid>"+
						"<mch_id>"+mch_id+"</mch_id>"+
						"<nonce_str>"+nonce_str+"</nonce_str>"+
						"<sign>"+sign+"</sign>"+
						"<body><![CDATA["+body+"]]></body>"+
						"<attach>"+_uid+"</attach>"+
						"<out_trade_no>"+out_trade_no+"</out_trade_no>"+
						"<total_fee>"+total_fee+"</total_fee>"+
						"<spbill_create_ip>"+spbill_create_ip+"</spbill_create_ip>"+
						"<notify_url>"+notify_url+"</notify_url>"+
						"<trade_type>"+trade_type+"</trade_type>"+
						"<openid>"+openid+"</openid>"+
						"</xml>";
				
		String allParameters = "";
		try {
			allParameters =  reqHandler.genPackage(packageParams);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String createOrderURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
			
		String prepay_id="";
		try {
			prepay_id = GetWxOrderno.getPayNo(createOrderURL, xml);
			if(prepay_id.equals("")){
				request.setAttribute("ErrorMsg", "统一支付接口获取预支付订单出错");
				return ;
			}
		} catch (Exception e1) {
					e1.printStackTrace();
		}
				
		SortedMap<String, String> finalpackage = new TreeMap<String, String>();
		String appid2 = appid;
		String timestamp = Sha1Util.getTimeStamp();
		String nonceStr2 = nonce_str;
		String prepay_id2 = "prepay_id="+prepay_id;
		String packages = prepay_id2;
		finalpackage.put("appId", appid2);  
		finalpackage.put("timeStamp", timestamp);  
		finalpackage.put("nonceStr", nonceStr2);  
		finalpackage.put("package", packages);  
		finalpackage.put("signType", "MD5");
		
		String finalsign = reqHandler.createSign(finalpackage);
				
		Map<String,String> params = new HashMap<String,String>();
		params.put("appid", appid2);
		params.put("timestamp", timestamp);
		params.put("nonceStr", nonceStr2);
		params.put("packages", packages);
		params.put("sign", finalsign);
				
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

		JSONObject json = JSONObject.fromObject(params, config);
		ResponseUtils.renderJson(response, json.toString());
				
		return ;
	}
	

	@RequestMapping(value = "/pay_notify.jspx")
	public void pay_notify(HttpServletRequest request,
			HttpServletResponse response, ModelMap model){
		
		StringBuffer content = new StringBuffer();

		try {
			InputStream in = request.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String result  = reader.readLine();
			while(result != null){
				content.append(result);
				result = reader.readLine();
			}
			
		} catch (Exception e) {
				logger.error("exception");
		}
		
		logger.info("result:"+content.toString());
		
		String text="<xml>"+
				"<return_code>SUCCESS</return_code>"+
				"<return_msg></return_msg>"+
				"</xml>";
		
		ResponseUtils.renderXml(response, text);
		
		try {
			Document document = DocumentHelper.parseText(content.toString());
			Element returnCode = (Element)document.selectSingleNode("/xml/return_code");
			Element returnMsg = (Element)document.selectSingleNode("/xml/return_msg");
			Element resultCode = (Element)document.selectSingleNode("/xml/result_code");
			
			if(returnCode != null && resultCode != null){
				
				String returnCodeStr = returnCode.getText();
				String resultCodeStr = resultCode.getText();
				
				if(returnCodeStr.equals("SUCCESS") && resultCodeStr.equals("SUCCESS") ){
					Element openidNode = (Element)document.selectSingleNode("/xml/openid");
					Element totalFeeNode = (Element)document.selectSingleNode("/xml/total_fee");
					Element transactionIdNode = (Element)document.selectSingleNode("/xml/transaction_id");
					Element attachNode = (Element)document.selectSingleNode("/xml/attach");
					Element timeEndNode = (Element)document.selectSingleNode("/xml/time_end");
					Element outTradeNoNode = (Element)document.selectSingleNode("/xml/out_trade_no");
					
					ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
					itemService.updateOrderStatus(outTradeNoNode.getText(),1);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return;
	}
	
	
}
