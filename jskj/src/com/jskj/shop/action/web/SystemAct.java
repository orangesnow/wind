package com.jskj.shop.action.web;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.jskj.shop.dao.impl.AppVersionService;
import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.entity.resp.AppVersion;
import com.jskj.shop.entity.resp.UploadResp;
import com.jskj.shop.util.FileRepository;
import com.jskj.shop.util.RequestUtils;
import com.jskj.shop.util.ResponseUtils;
import com.jskj.shop.util.SysConstants;

/**
 * 公共服务接口
 * @author VAIO
 * @since 2015-11-18
 */
@Controller
public class SystemAct {
	
	private static final Logger logger = Logger.getLogger(SystemAct.class);

	@RequestMapping(value = "/api/update_check.json",method = {RequestMethod.GET,RequestMethod.POST})//, method = RequestMethod.POST
	public void update_check(String vername,String vercode,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {

		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		
		AppVersionService appVersionService = (AppVersionService) BeanFactory.getBean("appVersionService");
	
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 
		Map<String,Object> appVersion = appVersionService.findAppVersion(vercode);
		
		if(appVersion == null){
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
			response.setUpdateflag(0);
		} else {
			
			String downUrl = (String)appVersion.get("down_url");
			String remark = (String)appVersion.get("remark");
			Integer filesize = (Integer)appVersion.get("filesize");
			String versionName = (String)appVersion.get("version_name");
			Integer versionCode = (Integer)appVersion.get("version_code");
			String md5str = (String)appVersion.get("md5_str");
			
			long responseTime = System.currentTimeMillis();
			
			response.setUpdateflag(1);
			response.setDownurl(downUrl);
			response.setRemark(remark);
			response.setFilesize(filesize);
			response.setVername(versionName);
			response.setVercode(versionCode);
			response.setMd5Str(md5str);
			
			RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);
			
		}
		
		JSONObject json = JSONObject.fromObject(response,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

	@RequestMapping(value = "/api/advice.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void advice(String nickname,String mobile,String content,HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		
		logger.info("nickname:"+nickname);
		logger.info("mobile:"+mobile);
		logger.info("content:"+content);
		
		AppVersionService appVersionService = (AppVersionService) BeanFactory.getBean("appVersionService");
		int result = appVersionService.insertAdvice(nickname, mobile, content, headers);
		
		if(result >0){
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);
			ResponseUtils.renderJson(response, "{response:{\"flag\":1}}");
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);
			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":0,\"message\":\"当前意见存在非法数据，保存失败~\"}}");
		}
		
	}
	
	@RequestMapping(value = "/api/test.json")
	public String test_unload(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		
//		Map<String,String> headers = RequestUtils.parseRequest(request,false);
//		long responseTime = System.currentTimeMillis();
//		RequestUtils.fillResponse(response,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);
//		ResponseUtils.renderJson(response, "{response:{\"flag\":1}}");
		
		return "index.html";
	}

	@RequestMapping(value = "/api/item_yf.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_yf(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		
		ItemService userService = (ItemService) BeanFactory.getBean("itemService");
		List<Map<String,Object>> yunfeis = userService.findYunfei();
		Map<String,Object> charge = new HashMap<String,Object>();
		
		if(yunfeis != null && yunfeis.size() >0){
			charge.put("charges", yunfeis);
			charge.put("count", yunfeis.size());
		}else {
			charge.put("count", 0);
			charge.put("charges", null);
		}
		
//		JSONArray yfJson = JSONArray.fromObject(yunfeis);
		JSONObject yfJson = JSONObject.fromObject(charge);
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(response,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);
		ResponseUtils.renderJson(response, "{response:"+yfJson.toString()+"}");

	}
	
	@RequestMapping("/api/upload_pic.json")
	public void upload_pic(@RequestParam("uploadFile") MultipartFile file,
			HttpServletRequest request, HttpServletResponse response,ModelMap model) {

		String origName = file.getOriginalFilename();
		String ext = FilenameUtils.getExtension(origName).toLowerCase(Locale.ENGLISH);
		String fileUrl = "";
		try {
			String ctx = request.getContextPath();
			FileRepository fileRepository = (FileRepository)BeanFactory.getBean("fileRepository");
			fileRepository.setServletContext(request.getServletContext());
			fileUrl = fileRepository.storeByExt("", ext, file);
			fileUrl = ctx + fileUrl;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		UploadResp uploadResp = new UploadResp();
		uploadResp.setFlag(1);
		uploadResp.setUploadPath(fileUrl);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 
		JSONObject json = JSONObject.fromObject(uploadResp,config );

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(response,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);
		ResponseUtils.renderJson(response, "{\"response\":"+json.toString()+"}");
		
	}
	
	@RequestMapping(value = "/api/loading.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void loading(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		
		Map<String,String> headers = RequestUtils.parseRequest(request,false);

		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		Map<String,Object> loading= itemService.findLoadingAd();
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 
		JSONObject json = JSONObject.fromObject(loading,config );
		
		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(response,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);
		ResponseUtils.renderJson(response, "{\"response\":"+json.toString()+"}");
		
	}

	
	
	
	public static void main(String[] args){
		
	}

}
