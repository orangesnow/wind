package com.jskj.shop.action.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.impl.UserService;
import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.entity.resp.AppVersion;
import com.jskj.shop.entity.resp.Trade;
import com.jskj.shop.entity.resp.TradeResp;
import com.jskj.shop.entity.resp.UserCollectResp;
import com.jskj.shop.entity.resp.UserCouponResp;
import com.jskj.shop.entity.resp.UserRegisterResp;
import com.jskj.shop.entity.resp.UserResp;
import com.jskj.shop.util.RequestUtils;
import com.jskj.shop.util.ResponseUtils;
import com.jskj.shop.util.SysConstants;

/**
 * 异步任务处理接口程序
 * @author VAIO
 * @since 2014-11-2
 */
@Controller
public class UserAct {
	
	private static final Logger logger = Logger.getLogger(UserAct.class);

	@RequestMapping(value = "/api/o_user_register.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void user_register(String nickname,String icon,String mobile,Integer gender,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {

		logger.info("nickname:"+nickname);
		logger.info("icon:"+icon);
		logger.info("mobile:"+mobile);
		logger.info("gender:"+gender);
		
		Map<String,String> headers =  RequestUtils.parseRequest(request,false);

		UserService userService = (UserService) BeanFactory.getBean("userService");
		
		if(userService == null){
			logger.info("userService is null");
		}
		
		int userid = userService.saveUser(nickname, gender, icon, mobile,headers);
		
		UserRegisterResp userRegister = new UserRegisterResp();
		userRegister.setFlag(1);
		userRegister.setUid(userid);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
	

		JSONObject json = JSONObject.fromObject(userRegister,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

	@RequestMapping(value = "/api/o_user_modify.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void user_modify(String nickname,String icon,String mobile,Integer gender,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {

		Map<String,String> headers =  RequestUtils.parseRequest(request,true);
		
		UserService userService = (UserService) BeanFactory.getBean("userService");
		String uid = headers.get(SysConstants.HEADER_UID);
		int userid = 0;
		
		try {
			userid = Integer.parseInt(uid);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		userService.updateUser(userid, nickname, gender, icon, mobile);
		
		UserRegisterResp userRegister = new UserRegisterResp();
		userRegister.setFlag(1);
		userRegister.setUid(userid);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			

		JSONObject json = JSONObject.fromObject(userRegister,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	@RequestMapping(value = "/api/o_address_modify.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void address_modify(String name,String address,String contact,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers =  RequestUtils.parseRequest(request,true);
		
		UserService userService = (UserService) BeanFactory.getBean("userService");
		String uid = headers.get(SysConstants.HEADER_UID);
		int userid = 0;
		
		try {
			userid = Integer.parseInt(uid);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		userService.updateUser(userid,contact, name, address);
		
		UserRegisterResp userRegister = new UserRegisterResp();
		userRegister.setFlag(1);
		userRegister.setUid(userid);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		response.setUpdateflag(0);

		JSONObject json = JSONObject.fromObject(userRegister,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	@RequestMapping(value = "/api/v_user_detail.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void user_detail(HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {

		Map<String,String> headers =  RequestUtils.parseRequest(request,true);
		
		UserService userService = (UserService) BeanFactory.getBean("userService");
		String uid =headers.get(SysConstants.HEADER_UID);
		int userid = Integer.parseInt(uid);
		
		Map<String,Object> user =userService.findUserById(userid);	
		
		UserResp userResp = new UserResp();
		userResp.setUser(user);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		

		JSONObject json = JSONObject.fromObject(userResp,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	@RequestMapping(value = "/api/v_itemcollect_list.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void itemcollect_list(Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		Map<String,String> headers =  RequestUtils.parseRequest(request,true);
		
		UserService userService = (UserService) BeanFactory.getBean("userService");
		String uid =headers.get(SysConstants.HEADER_UID);
		int userid = Integer.parseInt(uid);
		
		if(pageno == null) pageno=1;
		if(pagesize == null) pagesize=20;
		
		int count = userService.totalCollectItems(userid, 1);
		List<Map<String,Object>> collects =userService.findCollectItems(userid, 1, pageno, pagesize);
		
		UserCollectResp userCollect = new UserCollectResp();
		userCollect.setCollects(collects);
		userCollect.setPageno(pageno);
		userCollect.setPagesize(pagesize);
		userCollect.setCount(count);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		
		JSONObject json = JSONObject.fromObject(userCollect,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

	@RequestMapping(value = "/api/v_resourcecollect_list.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void resourcecollect_list(Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		Map<String,String> headers =  RequestUtils.parseRequest(request,true);
		
		UserService userService = (UserService) BeanFactory.getBean("userService");
		String uid =headers.get(SysConstants.HEADER_UID);
//		int userid = Integer.parseInt(uid);
		int userid = 1;
		if(pageno == null)   pageno=1;
		if(pagesize == null) pagesize=20;
		int count = userService.totalCollectArticles(userid, 2);
		List<Map<String,Object>> collects =userService.findCollectArticles(userid, 2, pageno, pagesize);
		
		UserCollectResp userCollect = new UserCollectResp();
		userCollect.setCollects(collects);
		userCollect.setPageno(pageno);
		userCollect.setPagesize(pagesize);
		userCollect.setCount(count);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		
		JSONObject json = JSONObject.fromObject(userCollect,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	@RequestMapping(value = "/api/v_trade_list.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void trade_list(String tid,Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		Map<String,String> headers =  RequestUtils.parseRequest(request,true);
		
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		String uid =headers.get(SysConstants.HEADER_UID);
		int userid = Integer.parseInt(uid);
		
		if(pageno == null) pageno=1;
		if(pagesize == null) pagesize=20;
		
		List<Trade> tradeList = new ArrayList<Trade>();
		List<Map<String,Object>> trades = itemService.findTradeInfoByUserId(tid,userid, pageno, pagesize);
		int count = itemService.totalTradeByUserId(null, userid);
		if(trades != null && trades.size() >0){
			for(int i=0;i<trades.size();i++){
				Map<String,Object> tradeInfo = trades.get(i);
				String tradeId = (String)tradeInfo.get("t_id");
				List<Map<String,Object>> orders = itemService.findOrderInfoByTradeId(tradeId);
				Trade trade = new Trade();
				trade.setTradeInfo(tradeInfo);
				trade.setOrderInfos(orders);
				tradeList.add(trade);
			}
		}

		TradeResp tradeResp = new TradeResp();
		tradeResp.setCount(count);
		tradeResp.setPageno(pageno);
		tradeResp.setPagesize(pagesize);
		tradeResp.setTrades(tradeList);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		
		JSONObject json = JSONObject.fromObject(tradeResp,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

	@RequestMapping(value = "/api/v_coupon_list.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void coupon_list(Integer use_flag,Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		Map<String,String> headers =  RequestUtils.parseRequest(request,true);
		
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		String uid =headers.get(SysConstants.HEADER_UID);
		int userid = Integer.parseInt(uid);
		
		if(pageno == null) pageno=1;
		if(pagesize == null) pagesize=20;
	
		List<Map<String,Object>> coupons = itemService.findCouponsFromUserId(userid, use_flag, pageno, pagesize);
		int count = itemService.totalCouponsFromUserId(userid, use_flag);
		
		UserCouponResp userCoupon = new UserCouponResp();
		userCoupon.setCount(count);
		userCoupon.setCoupons(coupons);
		userCoupon.setPageno(pageno);
		userCoupon.setPagesize(pagesize);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		
		JSONObject json = JSONObject.fromObject(userCoupon,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

	@RequestMapping(value = "/api/o_trade_comment.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_rank(String oid,Integer level,String content ,HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		Map<String,String> headers = RequestUtils.parseRequest(request,true);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		String uid =headers.get(SysConstants.HEADER_UID);

		if(uid != null && !uid.equals("")){
			itemService.updateTradeComment(oid,level,content,uid);
		}
			
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(response,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		
		ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");

	}


}
