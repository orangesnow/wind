package com.jskj.shop.action.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jskj.shop.dao.impl.ArticleService;
import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.entity.resp.AppVersion;
import com.jskj.shop.entity.resp.ArticleResp;
import com.jskj.shop.entity.resp.CategoryResp;
import com.jskj.shop.entity.resp.HotwordResp;
import com.jskj.shop.entity.resp.IndexResp;
import com.jskj.shop.entity.resp.ItemDetail;
import com.jskj.shop.entity.resp.ItemRelateResp;
import com.jskj.shop.entity.resp.ItemResp;
import com.jskj.shop.entity.resp.ItemTraceResp;
import com.jskj.shop.entity.resp.SimpleTradeResp;
import com.jskj.shop.entity.resp.Trade;
import com.jskj.shop.entity.vo.ItemArea;
import com.jskj.shop.manager.ItemManager;
import com.jskj.shop.util.RequestUtils;
import com.jskj.shop.util.ResponseUtils;
import com.jskj.shop.util.SysConstants;

/**
 * 商品处理接口
 * @author VAIO
 * @since 2014-12-20
 */
@Controller
public class ItemAct {
	
	private  static final Logger logger = Logger.getLogger(ItemAct.class);

	@RequestMapping(value = "/api/v_article_detail.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void article_detail(Integer articleid,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {

		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ArticleService articleService = (ArticleService) BeanFactory.getBean("articleService");
		
		Map<String,Object> article = articleService.findArticleById(articleid);
		
		ArticleResp articleResp = new ArticleResp();
		if(article != null){
			articleResp.setFlag(1);
			articleResp.setArticle(article);
		} else {
			articleResp.setFlag(0);
		}

		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		
		JSONObject json = JSONObject.fromObject(articleResp,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	@RequestMapping(value = "/api/v_index.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_index(HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		List<Map<String,Object>> ads = itemService.findAds(2, 6);//焦点广告
		List<Map<String,Object>> tags = itemService.findHotTag(3);//热门标签
		
		List<ItemArea> hots = ItemManager.handleArea(itemService.findTopArea(1, -1, 3) , false); //热门专区
		List<ItemArea> recomms = ItemManager.handleArea(itemService.findTopArea(-1, 1,3),true) ;//推荐专区

		IndexResp indexResp = new IndexResp();
		indexResp.setAds(ads);
		indexResp.setHots(hots);
		indexResp.setRecomms(recomms);
		indexResp.setTags(tags);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		response.setUpdateflag(0);

		JSONObject json = JSONObject.fromObject(indexResp,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

	@RequestMapping(value = "/api/v_item_search.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_search(String keyword,Integer rtype,Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		if(pageno == null) pageno = 1;
		if(pagesize == null) pagesize = 20;
		if(rtype == null) rtype = 0;
		
		List<Map<String,Object>> items = itemService.findItemsFromKeyword(keyword, rtype, pageno, pagesize);
		int count = itemService.totalItemsFromKeyword(keyword, rtype);
		
		ItemResp itemResp = new ItemResp();
		itemResp.setItems(items);
		itemResp.setCount(count);
		itemResp.setPageno(pageno);
		itemResp.setPagesize(pagesize);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		response.setUpdateflag(0);

		JSONObject json = JSONObject.fromObject(itemResp,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

	@RequestMapping(value = "/api/v_item_category.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_category(Integer rootid,Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		if(pageno == null) pageno = 1;
		if(pagesize == null) pagesize = 20;

		List<Map<String,Object>> items = itemService.findItemCategorys(rootid, pageno, pagesize);
		int count = itemService.totalItemCategorys(rootid);
		
		CategoryResp itemResp = new CategoryResp();
		itemResp.setItems(items);
		itemResp.setCount(count);
		itemResp.setPageno(pageno);
		itemResp.setPagesize(pagesize);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		response.setUpdateflag(0);

		JSONObject json = JSONObject.fromObject(itemResp,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

	@RequestMapping(value = "/api/v_item_rank.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_rank(Integer stype,Integer cid,Integer tagid,Integer areaid,Integer rtype,
			Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		if(pageno == null) pageno = 1;
		if(pagesize == null) pagesize = 20;
		if(rtype == null) rtype = 0;
		
		List<Map<String,Object>> items = null;
		int count = 0;
		
		if(stype == 1){
			items = itemService.findItemDataByCategory(cid, rtype, pageno, pagesize);
			count = itemService.totalItemsByCategory(cid, rtype);
		} else if(stype == 2){
			items = itemService.findItemDataByTagId(tagid, rtype, pageno, pagesize);
			count = itemService.totalItemsByTagId(tagid);
		} else if(stype == 3){
			
			Map<String,Object> area = itemService.findAreaById(areaid);
			if(area != null) {
				String linktype = (String)area.get("link_type");
				if(linktype == null || linktype.equals("")){
					items = itemService.findItemFromAreaId(areaid, -1, pageno, pagesize);
					count = itemService.totalItemFromAreaId(areaid, -1);
				} else if(linktype.equals("tag") || linktype.equals("Tag")) {
					items = itemService.findItemDataByTagId((Integer)area.get("link_id"), rtype, pageno, pagesize);
					count = itemService.totalItemsByTagId((Integer)area.get("link_id"));
				} else if(linktype.equals("category") || linktype.equals("Category")) {
					items = itemService.findItemDataByCategory((Integer)area.get("link_id"), rtype, pageno, pagesize);
					count = itemService.totalItemsByCategory((Integer)area.get("link_id"), rtype);
				}
				
			} 
			
		}
		
		CategoryResp itemResp = new CategoryResp();
		itemResp.setItems(items);
		itemResp.setCount(count);
		itemResp.setPageno(pageno);
		itemResp.setPagesize(pagesize);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		response.setUpdateflag(0);

		JSONObject json = JSONObject.fromObject(itemResp,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	@RequestMapping(value = "/api/v_item_detail.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_detail(Integer itemid,String fields,
			Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {

		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		if(pageno == null) pageno = 1;
		if(pagesize == null) pagesize = 20;
		
		//商品信息
		Map<String,Object> item = itemService.findItemById(itemid);
		
		//商品SKU列表（包含名称、价格、可用数量）
		List<Map<String,Object>> formats = itemService.findSkuFromItemId(itemid);
		
		//商品图片列表
		List<Map<String,Object>> pictures = itemService.findPicturesByItemId(itemid,1);
		
		//商品描述图片列表
		List<Map<String,Object>> intros = itemService.findPicturesByItemId(itemid,2);
		ItemDetail detail = new ItemDetail();
	
		detail.setFormats(formats);
		detail.setItem(item);
		detail.setPictures(pictures);
		detail.setIntros(intros);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			

		JSONObject json = JSONObject.fromObject(detail,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	@RequestMapping(value = "/api/v_item_relate.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_relate(Integer itemid,
			Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {

		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		if(pageno == null) pageno = 1;
		if(pagesize == null) pagesize = 5;
		
		//商品信息
		List<Map<String,Object>> items = itemService.findRelateByItemId(itemid, pagesize);
		ItemRelateResp itemRelate = new ItemRelateResp();
		if(!items.isEmpty() && items.size() >0){
			itemRelate.setNum(items.size());
			itemRelate.setRelates(items);
		} else {
			itemRelate.setNum(0);
		}
	
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			

		JSONObject json = JSONObject.fromObject(itemRelate,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	@RequestMapping(value = "/api/v_simple_trade.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void simple_trade(Integer itemid,String start,String end,
			Integer pageno,Integer pagesize,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		if(pageno == null) pageno = 1;
		if(pagesize == null) pagesize = 20;
		
		List<Map<String,Object>> trades = itemService.findSimpleTrades(itemid, pageno, pagesize);
		int count = itemService.totalSimpleTrades(itemid);
		
		SimpleTradeResp simpleTrade = new SimpleTradeResp();
		simpleTrade.setCount(count);
		simpleTrade.setPageno(pageno);
		simpleTrade.setPagesize(pagesize);
		simpleTrade.setTrades(trades);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		response.setUpdateflag(0);

		JSONObject json = JSONObject.fromObject(simpleTrade,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	
	
	@RequestMapping(value = "/api/o_item_collect.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_collect(Integer itemid,Integer action,
			HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,true);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		String uid = headers.get(SysConstants.HEADER_UID);
		
		if(uid != null && !uid.equals("")){
			
			Integer userid = Integer.parseInt(uid);
			itemService.saveItemCollect(itemid, userid, action);
			
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
			response.setUpdateflag(0);

			ResponseUtils.renderJson(res, "{\"response\":{\"flag\":1}}");
		} else {
			
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
			response.setUpdateflag(0);
			ResponseUtils.renderJson(res, "{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return ;
	}
	
	@RequestMapping(value = "/api/o_item_pay.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_pay(String recev_name,String recev_mobile,String ccode,
			String recev_province,String recev_city,String recev_district,String recev_address,
			Integer pay_type,Integer coupin_id,String params,String buyer_message,Double amount,String postFee,
			HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {

		Map<String,String> headers = RequestUtils.parseRequest(request,true);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		Integer userid = 0;
		String uid = headers.get(SysConstants.HEADER_UID);
	
		//生成订单，生成订单明细，返回订单编号
		if(uid != null && !uid.equals("") ) {
			userid = Integer.parseInt(uid);
		}
		String address = getAddr( recev_province, recev_city, recev_district, recev_address);
		String oid = itemService.saveTradeInfo("",recev_name, recev_mobile, recev_district, address ,
					pay_type,0, coupin_id, params, buyer_message, amount, postFee,userid);
		
		//简单订单信息保存成功
		itemService.saveSimpleTrade( null, address, params);
		
		JSONObject jsonObj = new JSONObject();

		Map<String,Object> tradeInfo = itemService.findTradeByOid(oid);
		String tid = (String)tradeInfo.get("t_id");
		List<Map<String,Object>> orders = itemService.findOrderInfoByTradeId(tid);


		Trade trade = new Trade();
		trade.setTradeInfo(tradeInfo);
		trade.setOrderInfos(orders);
		
		jsonObj.put("oid", oid);
		jsonObj.put("flag", "1");
		jsonObj.put("trade", trade);

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			

		ResponseUtils.renderJson(res, "{\"response\":"+jsonObj.toString()+"}");
	
		return ;
	}
	
	private String getAddr(String province,String city,String district,String address) {
		return province+" "+city+" "+district+" "+address;
	}
	
	@RequestMapping(value = "/api/v_item_trace.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_trace(String oid,HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		Map<String,Object> trade =  itemService.findTradeByOid(oid);
		String company = (String)trade.get("goods_company");
		String outsid = (String)trade.get("goods_num");
		String status = (String)trade.get("goods_result");
		List<Map<String,Object>> traces = itemService.findTradeTraces(oid);

		ItemTraceResp itemTrace = new ItemTraceResp();
		itemTrace.setCompany(company);
		itemTrace.setOid(oid);
		itemTrace.setOutsid(outsid);
		itemTrace.setStatus(status);
		itemTrace.setTraces(traces);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		response.setUpdateflag(0);

		JSONObject json = JSONObject.fromObject(itemTrace,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

	@RequestMapping(value = "/api/v_item_hotword.json",method = {RequestMethod.GET,RequestMethod.POST})
	public void item_hotword(HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		List<Map<String,Object>> names = itemService.findKeywords();
		int count = itemService.totalKeywords();
		
		HotwordResp hotword = new HotwordResp();
		hotword.setCount(count);
		hotword.setNames(names);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		response.setUpdateflag(0);

		JSONObject json = JSONObject.fromObject(hotword,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}
	
	
	
	@RequestMapping(value = "/api/v_delete.json",method={RequestMethod.HEAD})
	public void v_delete(HttpServletRequest request,
			HttpServletResponse res, ModelMap model) {
		
		AppVersion response = new AppVersion();
		Map<String,String> headers = RequestUtils.parseRequest(request,false);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		List<Map<String,Object>> names = itemService.findKeywords();
		int count = itemService.totalKeywords();
		
		HotwordResp hotword = new HotwordResp();
		hotword.setCount(count);
		hotword.setNames(names);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(res,responseTime,SysConstants.HTTP_STATUSCODE_SUCCESS,null);			
		response.setUpdateflag(0);

		JSONObject json = JSONObject.fromObject(hotword,config );
		ResponseUtils.renderJson(res, "{\"response\":"+json.toString()+"}");

		return ;
	}

}
