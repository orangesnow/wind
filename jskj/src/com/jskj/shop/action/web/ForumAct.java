package com.jskj.shop.action.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jskj.shop.dao.impl.ForumService;
import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.entity.resp.GroupResp;
import com.jskj.shop.entity.resp.ShortMessageResp;
import com.jskj.shop.entity.resp.SubjectDetailResp;
import com.jskj.shop.entity.resp.SubjectReplyResp;
import com.jskj.shop.entity.resp.SubjectResp;
import com.jskj.shop.util.RequestUtils;
import com.jskj.shop.util.ResponseUtils;
import com.jskj.shop.util.SysConstants;

/**
 * 论坛处理接口
 * 
 * @author VAIO
 * @since 2014-11-2
 */
@Controller
public class ForumAct {

	private static final Logger logger = Logger.getLogger(ForumAct.class);

	@RequestMapping(value = "/api/o_post_subject.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_post_subject(String content, String images,
			String location, Integer groupid, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {

			Integer userid = Integer.parseInt(uid);
			String[] pics = null;
			if(images != null){
				logger.info("images:"+images);
				pics = images.split(";");
			}
			forumService.saveSubject(content, location, groupid, 1, pics, userid,headers);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/v_check_follow.json", method = {RequestMethod.GET, RequestMethod.POST })
	public void v_subject_follow(String ids,HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		
		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory.getBean("forumService");
		
		String uid = headers.get(SysConstants.HEADER_UID);
		Integer userid = Integer.parseInt(uid);
		
		List<Map<String,Object>> actions = forumService.findFollowSubject(ids, userid);
		
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		JSONObject json = JSONObject.fromObject(actions, config);

		long responseTime = System.currentTimeMillis();
		RequestUtils.fillResponse(response, responseTime,
				SysConstants.HTTP_STATUSCODE_SUCCESS, null);
		
		ResponseUtils.renderJson(response,
				"{\"response\":" + json.toString() + "}");
	}
	
	
	@RequestMapping(value = "/api/o_reply_subject.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_post_subject(String content, Integer replyid,
			Integer subjectid, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {

			Integer userid = Integer.parseInt(uid);
			forumService.saveReply(content, replyid, subjectid, headers);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");
			
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime, SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/o_delete_subject.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_delete_subject(Integer subjectid, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {

			forumService.deleteSubject(subjectid, headers);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/o_follow_subject.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_follow_subject(Integer subjectid, Integer action,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);
			if (action == 1) {
				forumService.saveFollowSubject(subjectid, userid);
			} else {
				forumService.deleteFollowSubject(subjectid, userid);
			}

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/o_report_subject.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_report_subject(Integer subjectid, Integer replyid,
			Integer rtype, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);
			forumService.saveReportSubject(subjectid, replyid, userid);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/o_agree_subject.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_agree_subject(Integer subjectid, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);
			forumService.saveAgreeSubject(subjectid, userid);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/o_share_subject.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_share_subject(Integer subjectid, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);
			forumService.saveShareSubject(subjectid, userid);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/o_join_group.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_join_group(Integer groupid, Integer action,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);
			forumService.enterSubjectGroup(userid, groupid, action);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/v_user_group.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void v_user_group(Integer pageno, Integer pagesize,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (pageno == null)
			pageno = 1;
		if (pagesize == null)
			pagesize = 20;

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);

			List<Map<String, Object>> groups = forumService.findSubjectGroup(
					userid, pageno, pagesize);
			int count = groups.size();

			GroupResp groupResp = new GroupResp();
			groupResp.setCount(count);
			groupResp.setGroups(groups);

			JsonConfig config = new JsonConfig();
			config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

			JSONObject json = JSONObject.fromObject(groupResp, config);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response,
					"{\"response\":" + json.toString() + "}");

		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/v_user_subject_list.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void user_subject_list(Integer pageno, Integer pagesize,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);

			if (pageno == null)
				pageno = 1;
			if (pagesize == null)
				pagesize = 20;

			List<Map<String, Object>> subjects = forumService
					.findSubjectsByUid(userid, pageno, pagesize);
			int count = forumService.totalSubjectsByUid(userid);

			SubjectResp subjectResp = new SubjectResp();
			subjectResp.setCount(count);
			subjectResp.setPageno(pageno);
			subjectResp.setPagesize(pagesize);
			subjectResp.setSubjects(subjects);

			JsonConfig config = new JsonConfig();
			config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

			JSONObject json = JSONObject.fromObject(subjectResp, config);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response,
					"{\"response\":" + json.toString() + "}");

		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	// 用户回复帖子历史列表
	@RequestMapping(value = "/api/v_reply_list.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void reply_list(Integer pageno, Integer pagesize,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);

			if (pageno == null)
				pageno = 1;
			if (pagesize == null)
				pagesize = 20;

			List<Map<String, Object>> replys = forumService.findReplyByUid(
					userid, pageno, pagesize);
			int count = forumService.totalReplyByUid(userid);

			SubjectResp subjectResp = new SubjectResp();
			subjectResp.setCount(count);
			subjectResp.setPageno(pageno);
			subjectResp.setPagesize(pagesize);
			subjectResp.setSubjects(replys);

			JsonConfig config = new JsonConfig();
			config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

			JSONObject json = JSONObject.fromObject(subjectResp, config);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response,
					"{\"response\":" + json.toString() + "}");

		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/v_subject_follow.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void follow_list(Integer pageno, Integer pagesize,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);
 
			if (pageno == null)
				pageno = 1;
			if (pagesize == null)
				pagesize = 20;

			List<Map<String, Object>> replys = forumService.findFollowSubject(
					userid, headers, pageno, pagesize);
			int count = forumService.tolalFollowSubject(userid, headers);

			SubjectResp subjectResp = new SubjectResp();
			subjectResp.setCount(count);
			subjectResp.setPageno(pageno);
			subjectResp.setPagesize(pagesize);
			subjectResp.setSubjects(replys);

			JsonConfig config = new JsonConfig();
			config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

			JSONObject json = JSONObject.fromObject(subjectResp, config);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response,
					"{\"response\":" + json.toString() + "}");

		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/v_subject_rank.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void subject_rank(Integer rtype, Integer groupid, Integer pageno,
			Integer pagesize, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (rtype == null)
			rtype = 1;

//		if (uid != null && !uid.equals("")) {
//			Integer userid = Integer.parseInt(uid);

			if (pageno == null)
				pageno = 1;
			if (pagesize == null)
				pagesize = 20;

			List<Map<String, Object>> subjects = forumService
					.findSubjectByRank(rtype, groupid, pageno, pagesize);
			int count = forumService.totalSubjectByRank(rtype, groupid);

			SubjectResp subjectResp = new SubjectResp();
			subjectResp.setCount(count);
			subjectResp.setPageno(pageno);
			subjectResp.setPagesize(pagesize);
			subjectResp.setSubjects(subjects);

			JsonConfig config = new JsonConfig();
			config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

			JSONObject json = JSONObject.fromObject(subjectResp, config);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response,
					"{\"response\":" + json.toString() + "}");

//		} else {
//			long responseTime = System.currentTimeMillis();
//			RequestUtils.fillResponse(response, responseTime,
//					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
//			ResponseUtils.renderJson(response,
//					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
//		}

		return;
	}

	@RequestMapping(value = "/api/v_subject_detail.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void subject_detail(Integer subjectid, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

//		String uid = headers.get(SysConstants.HEADER_UID);

//		if (uid != null && !uid.equals("")) {
//			Integer userid = Integer.parseInt(uid);

			Map<String, Object> subject = forumService
					.findSubjectById(subjectid);
			List<Map<String, Object>> pictures = forumService
					.findSubjectPictures(subjectid);

			
			SubjectDetailResp subjectDetailResp = new SubjectDetailResp();
			subjectDetailResp.setPictures(pictures);
			subjectDetailResp.setSubject(subject);

			Integer groupid = (Integer)subject.get("group_id");
			if(groupid != null && groupid >0){
				Map<String,Object> group = forumService.findGroupById(groupid);
				subjectDetailResp.setGroup(group);
			}
			
			JsonConfig config = new JsonConfig();
			config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

			JSONObject json = JSONObject.fromObject(subjectDetailResp, config);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response,
					"{\"response\":" + json.toString() + "}");

//		} else {
//			long responseTime = System.currentTimeMillis();
//			RequestUtils.fillResponse(response, responseTime,
//					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
//			ResponseUtils.renderJson(response,
//					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
//		}

		return;
	}

	// 帖子回复列表
	@RequestMapping(value = "/api/v_subject_reply.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void subject_reply(Integer subjectid, Integer pageno,
			Integer pagesize, HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

//		String uid = headers.get(SysConstants.HEADER_UID);
//
//		if (uid != null && !uid.equals("")) {
//			Integer userid = Integer.parseInt(uid);

			if (pageno == null)
				pageno = 1;
			if (pagesize == null)
				pagesize = 20;

			// 当前帖子
			Map<String, Object> subject = forumService
					.findSubjectById(subjectid);
			List<Map<String, Object>> pictures = forumService
					.findSubjectPictures(subjectid);

			SubjectDetailResp subjectDetailResp = new SubjectDetailResp();
			subjectDetailResp.setPictures(pictures);
			subjectDetailResp.setSubject(subject);

			Integer groupid = (Integer)subject.get("group_id");
			if(groupid != null && groupid >0){
				Map<String,Object> group = forumService.findGroupById(groupid);
				subjectDetailResp.setGroup(group);
			}
			
			// 回复信息列表
			List<Map<String, Object>> replys = forumService.findSubjectReplys(
					subjectid, pageno, pagesize);
			int count = forumService.totalSubjectReplys(subjectid);

			SubjectReplyResp reply = new SubjectReplyResp();
			reply.setCount(count);
			reply.setPageno(pageno);
			reply.setPagesize(pagesize);
			reply.setSubjects(replys);
			reply.setDetail(subjectDetailResp);

			JsonConfig config = new JsonConfig();
			config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

			JSONObject json = JSONObject.fromObject(reply, config);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response,
					"{\"response\":" + json.toString() + "}");

//		} else {
//			long responseTime = System.currentTimeMillis();
//			RequestUtils.fillResponse(response, responseTime,
//					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
//			ResponseUtils.renderJson(response,
//					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
//		}

		return;
	}

	@RequestMapping(value = "/api/v_subject_group.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void subject_group(Integer pageno, Integer pagesize,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);

			if (pageno == null)
				pageno = 1;
			if (pagesize == null)
				pagesize = 20;

			List<Map<String, Object>> groups = forumService.findSubjectGroup(
					null, pageno, pagesize);
			int count = forumService.totalSubjectGroup(null);

			GroupResp subjectResp = new GroupResp();
			subjectResp.setCount(count);
			subjectResp.setPageno(pageno);
			subjectResp.setPagesize(pagesize);
			subjectResp.setGroups(groups);

			JsonConfig config = new JsonConfig();
			config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);

			JSONObject json = JSONObject.fromObject(subjectResp, config);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response,
					"{\"response\":" + json.toString() + "}");

		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return ;
	}

	@RequestMapping(value = "/api/o_send_shortmsg.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_send_shortmsg(String content, Integer touser,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);
			forumService.saveShortMsg(content, userid, touser);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);

			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");
			
		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}

	@RequestMapping(value = "/api/v_user_shortmsg.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void user_shortmsg(Integer mtype,Integer linkuser, Integer pageno, Integer pagesize,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);

			if (pageno == null)
				pageno = 1;
			if (pagesize == null)
				pagesize = 20;
			if (mtype == null)
				mtype = 1;

			int count = 0;
			List<Map<String, Object>> messages = null;
			
			if(linkuser != null && linkuser >0){
				
				messages = forumService.findShortMessagesFromLinkUser(linkuser, userid, 1, pageno, pagesize);
				count = forumService.totalShortMessage(linkuser, userid, 1);
				
			} else {
				
				if (mtype == 1) {
					messages = forumService.findShortMessagesFromUser(userid, -1,pageno,pagesize);
					count = forumService.totalShortMessagesFromUser(userid, -1);
				} else {
					messages = forumService.findShortMessagesToUser(userid,pageno,pagesize);
					count = forumService.totalShortMessagesToUser(userid);
				}
				
			}

			ShortMessageResp messageResp = new ShortMessageResp();
			messageResp.setCount(count);
			messageResp.setMessages(messages);
			messageResp.setPageno(pageno);
			messageResp.setPagesize(pagesize);

			JsonConfig config = new JsonConfig();
			config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
			JSONObject json = JSONObject.fromObject(messageResp, config);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":" + json.toString() + "}");

		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}


	@RequestMapping(value = "/api/o_delete_shortmsg.json", method = {
			RequestMethod.GET, RequestMethod.POST })
	public void o_delete_shortmsg(Integer msgid, Integer mtype,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {

		Map<String, String> headers = RequestUtils.parseRequest(request,true);
		ForumService forumService = (ForumService) BeanFactory
				.getBean("forumService");

		String uid = headers.get(SysConstants.HEADER_UID);

		if (uid != null && !uid.equals("")) {
			Integer userid = Integer.parseInt(uid);

			forumService.deleteShortMessage(msgid, mtype, headers);

			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response, "{\"response\":{\"flag\":1}}");

		} else {
			long responseTime = System.currentTimeMillis();
			RequestUtils.fillResponse(response, responseTime,
					SysConstants.HTTP_STATUSCODE_SUCCESS, null);
			ResponseUtils.renderJson(response,
					"{\"response\":{\"flag\":0,\"message\":\"当前用户没有登录~\"}}");
		}

		return;
	}
	
}
