package com.jskj.shop.schedule;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

public class GhypScheduleLister implements ServletContextListener{

	public static final String SYNC_ORDER_JOB_NAME = "syncOrderJob";
	public static final String SYNC_PRODUCT_JOB_NAME = "syncProductJob";

	private static final Logger logger = Logger.getLogger(GhypScheduleLister.class);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
		try {
			QuartzManager.removeJob(SYNC_ORDER_JOB_NAME);
			QuartzManager.removeJob(SYNC_PRODUCT_JOB_NAME);
		} catch (Exception e) {
			logger.error("destory job!!");
		}

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		SimpleDateFormat DateFormat = new SimpleDateFormat("yyyyMMddHHmmss");  
		String returnstr = DateFormat.format(new Date());         
		
		logger.info(returnstr+"【系统启动】");


		SyncOrderJob orderJob = new SyncOrderJob();
		SyncProductJob productJob = new SyncProductJob();
		
		try {

			QuartzManager.addJob(SYNC_ORDER_JOB_NAME, orderJob,"0/5 * * * * ?"); //每5分钟执行一次
		
			QuartzManager.addJob(SYNC_PRODUCT_JOB_NAME,productJob,"0 0 0 * * ?"); //每天凌晨0点执行一次
			
			
		} catch (Exception e) {
			logger.error("sync exception:"+e.getMessage());
		}
		
	}
	
	public static void main(String[] args) throws Exception{
		SimpleDateFormat DateFormat = new SimpleDateFormat("yyyyMMddHHmmss");  
		String returnstr = DateFormat.format(new Date());         

		System.out.println(returnstr+"【系统启动】");
//	
//			GoodsStatusTraceJob goodsUserJob = new GoodsStatusTraceJob();
//		try {
//			QuartzManager.addJob(SYNC_HANDLE_GOODS,goodsUserJob,"0/1 * * * * ?"); //每5秒钟执行一次
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

	}

}