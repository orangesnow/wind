package com.jskj.shop.schedule;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jskj.shop.dao.impl.ArticleService;
import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.util.BeanFactory;

/**
 * 同步处理程序
 * @author jordon
 * @since 2014-05-18
 */
public class SyncIndexJob implements Job{
	
	private static Logger logger = Logger.getLogger(SyncIndexJob.class);

	public static void main(String[] args) throws Exception{
		new SyncIndexJob().execute(null);
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		logger.info("SyncIndexJob="+sdf.format(new Date()));
		
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		ArticleService articleService = (ArticleService) BeanFactory.getBean("articleService");
		
		try {
			
			ThirdApiClient client = new ThirdApiClient(null,null);
	    	String response = client.getIndex();
	    	logger.info("response:"+response);
	    	JSONObject respJson = JSONObject.fromObject(response);
	    	
	    	JSONArray banners = respJson.getJSONArray("banners");
	    	if(banners != null && !banners.isEmpty()) {
	    		for(int i=0;i<banners.size();i++){
	    			JSONObject banner = banners.getJSONObject(i);
	    			
	    			String id = banner.getString("id");
	    			String type = banner.getString("type");
	    			String title = banner.getString("title");
	    			String name = banner.getString("name");
	    			
	    			JSONObject backJson = banner.getJSONObject("background");
	    			String image = backJson.getString("default");
	    			
	    			itemService.saveAd(id, 2, type, image, (i+1), 1, 1, null, name, name);
	    		}
	    	}
	    	
	    	JSONArray submenus = respJson.getJSONArray("submenus");
	    	if(submenus != null && !submenus.isEmpty()){
	    		for(int i=0;i<submenus.size();i++){
	    			JSONObject submenu = submenus.getJSONObject(i);
	    			String id = submenu.getString("id");
	    			String type = submenu.getString("type");
	    			String title = submenu.getString("title");
	    			String name = submenu.getString("name");
	    			String image = submenu.getString("image");
	    			
	    			int areaid = 0;
    				if(type != null && type.equals("Tag")){
    					int tagid = 0;
    					Map<String,Object> tagItem = itemService.findTagByName(title);
    					if(tagItem == null || tagItem.isEmpty()){
    						tagid = itemService.saveTag(title, 0, 0, 1, image);
    					} else {
    						tagid = (Integer)tagItem.get("id");
    					}
    					areaid = itemService.saveArea(name, 1, 0, (i+1), image, 1, null, "tag", tagid);
    					
    				} else if(type != null && type.equals("Article")){
    					
    					Map<String,Object> article = articleService.findArticleByOut(id);
    					Integer articleid = (Integer)article.get("article_id");
    					
    					areaid = itemService.saveArea(name, 1, 0, (i+1), image, 1, null, "article", articleid);
    				}   else {
    					;//其他
    				}
	    			
	    		}
	    	}
	    	
	    	
	    	//推荐专区
	    	JSONArray sections = respJson.getJSONArray("sections");
	    	if(sections != null && !sections.isEmpty()){
	    		for(int i=0;i<sections.size();i++){
	    			JSONObject section = sections.getJSONObject(i);
	    			String name = section.getString("name");
	    			JSONArray objects = section.getJSONArray("objects");
	    			
	    			if(objects != null && !objects.isEmpty()){
	    				JSONObject tag = objects.getJSONObject(0);
	    				String tagImage = tag.getString("image");
	    				String tagType = tag.getString("type");
	    				String title = tag.getString("title");
	    				String id = tag.getString("id");
	    				
	    				int areaid = 0;
	    				if(tagType != null && tagType.equals("Tag")){
	    					int tagid = 0;
	    					Map<String,Object> tagItem = itemService.findTagByName(title);
	    					if(tagItem == null || tagItem.isEmpty()){
	    						tagid = itemService.saveTag(title, 0, 0, 1, tagImage);
	    					} else {
	    						tagid = (Integer)tagItem.get("id");
	    					}
	    					areaid = itemService.saveArea(name, 0, 1, (i+1), tagImage, 1, null, "tag", tagid);
	    				} else if(tagType != null && tagType.equals("Article")){
	    					
	    					Map<String,Object> article = articleService.findArticleByOut(id);
	    					Integer articleid = (Integer)article.get("article_id");
	    					
	    					areaid = itemService.saveArea(name, 0, 1, (i+1), tagImage, 1, null, "article", articleid);
	    				}   else {
	    					;//其他
	    				}
	    				
	    				for(int j=1;j<objects.size();j++){
	    					JSONObject item = objects.getJSONObject(j);
	    					String outitemid = item.getString("id");
	    					String image = item.getString("image");
	    					String itemtitle = item.getString("title");
	    					String type = item.getString("type");
	    					
	    					Map<String,Object> itemMap = itemService.findItemByOut(outitemid);
	    					if(itemMap != null && !itemMap.isEmpty()){
	    						Integer itemid = (Integer)itemMap.get("item_id");
	    						itemService.saveAreaItem(areaid, itemid, itemtitle, image, j+1, 1, 1);
	    					}
	    				}
	    				
	    			}
	    			
	    		}
	    	}
	      	
	    	//热门标签
	    	JSONArray brands = respJson.getJSONArray("brands");
	      	if(brands != null && !brands.isEmpty()){
	      		for(int i=0;i<brands.size();i++){
	      			JSONObject brand = brands.getJSONObject(i);
	      			String id = brand.getString("id");
	      			String type = brand.getString("type");
	      			String title = brand.getString("title");
	      			String image = brand.getString("image");
	      			
	      			Map<String,Object> tags = itemService.findTagByName(title);
	      			if(tags != null && !tags.isEmpty()){
	      				Integer tagid = (Integer)tags.get("id");
	      				itemService.updateTag(tagid, title, image, 0, 1, 1);
	      			} else {
	      				itemService.saveTag(title, 1, 0, 1, image);
	      			}
	      			
	      		}
	      	}
	    	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.info("SyncIndexJob end..."+sdf.format(new Date()));
		
	}

}

