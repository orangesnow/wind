package com.jskj.shop.schedule;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.entity.vo.DetailItem;
import com.jskj.shop.entity.vo.HashImage;
import com.jskj.shop.entity.vo.ItemSku;
import com.jskj.shop.entity.vo.SimpleItem;
import com.jskj.shop.util.Md5Util;

/**
 * 同步处理程序
 * @author jordon
 * @since 2014-05-18
 */
public class SyncProductJob implements Job{
	
	private static Logger logger = Logger.getLogger(SyncProductJob.class);

	public static void main(String[] args) throws Exception{
		new SyncProductJob().execute(null);
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		logger.info("SyncProductJob="+sdf.format(new Date()));
		
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		 
		int pageno =1;
		int pagesize = 12;
		
		try {
			
			ThirdApiClient client = new ThirdApiClient(null,null);
	    	String response = client.getProduct(pageno,pagesize);
	    	logger.info("response:"+response);
	    	SimpleItem[]  items = ThirdApiParser.parseItemList(response);
	    	
	    	while(items != null){
	    		for(int i=0;i<items.length;i++){
	    			String itemid = items[i].getId();
	    			String title = items[i].getTitle();
	    			
	    			String item_detail = client.getProductDetail(itemid);
	    			logger.info("item_detail:"+item_detail);
	    			DetailItem detail = ThirdApiParser.parseDetailItem(item_detail);
	    			detail.setTitle(title);
	    			//保存商品
	    			int item_id = itemService.saveItem(detail);
	    			
	    			//保存sku
	    			ItemSku[] skus = detail.getSkus();
	    			if(skus != null && skus.length >0){
	    				for(int j=0;j<skus.length;j++){
	    					itemService.saveSku(item_id, skus[j]);
	    				}
	    			}
	    			
	    			//保存标签
	    			String[] labels = detail.getLabels();
	    			itemService.saveItemTag(item_id, labels);
	    			
	    			//保存焦点图
	    			HashImage[]  focus_images = detail.getPhotos();
	    			if(focus_images != null && focus_images.length >0){
	    				for(int j=0;j<focus_images.length;j++){
	    					String out_image_id = focus_images[j].getKey();
	    					String pic_url = focus_images[j].getUrl();
	    					itemService.savePicture( item_id, pic_url, 1, out_image_id);
	    				}
	    			}
	    			//保存详细页图
	    			String detail_url = detail.getDetail_url();
	    			logger.info("detail_url:"+detail_url);
	    			
	    			String[]  pictures = ThirdApiParser.parseDetailUrl(detail_url);
	    			 if(pictures != null && pictures.length >0){
	    				 for(int j=0;j<pictures.length;j++){
	    					 String out_image_id = Md5Util.encode(pictures[j]);
	    					 String pic_url = pictures[j];
	    					 itemService.savePicture( item_id, pic_url, 2, out_image_id);
	    				 }
	    			 }
	    			 
	    		}
	    		
	    		pageno++;
	    	 	response = client.getProduct(pageno,pagesize);
		    	logger.info("response:"+response);
		    	items = ThirdApiParser.parseItemList(response);
	    	}
	    	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.info("SyncProductJob end..."+sdf.format(new Date()));
		
	}


}

