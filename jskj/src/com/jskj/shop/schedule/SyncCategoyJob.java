package com.jskj.shop.schedule;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.util.BeanFactory;
import com.jskj.shop.entity.vo.DetailItem;
import com.jskj.shop.entity.vo.HashImage;
import com.jskj.shop.entity.vo.ItemSku;
import com.jskj.shop.util.Md5Util;

/**
 * 同步处理程序
 * @author jordon
 * @since 2014-05-18
 */
public class SyncCategoyJob implements Job{
	
	private static Logger logger = Logger.getLogger(SyncCategoyJob.class);

	public static void main(String[] args) throws Exception{
		new SyncCategoyJob().execute(null);
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		logger.info("SyncCategoyJob="+sdf.format(new Date()));
		
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");

		try {
			
			ThirdApiClient client = new ThirdApiClient(null,null);
	    	String response = client.getCategory();
	    	logger.info("response:"+response);
	    	JSONObject respJson = JSONObject.fromObject(response);
	    	JSONArray array =respJson.getJSONArray("tabs");
	    	if(array != null && !array.isEmpty()){
	    		for( int i=0;i<array.size() ;i++){
	    			
	    			JSONObject category = array.getJSONObject(i);
	    			String title  = category.getString("title");
	    			String name = category.getString("name");
	    			String image = category.getString("image");
	    			
	    			int cid = itemService.saveItemCategory(title, image, i+1);
	    			int page = 1;
	    			int per_page=20;
	    			
	    			String products = client.getTag(name, page, per_page);
	    			JSONObject tagsJson = JSONObject.fromObject(products);
	    			JSONArray productArray = tagsJson.getJSONArray("products");
	    			
	    			while(!productArray.isEmpty() &&  productArray.size() >0) {
	    				logger.info("productArray.size() :"+productArray.size() );
	    				if(!productArray.isEmpty() && productArray.size() >0 ){
	    					for(int j=0;j<productArray.size();j++){
	    						logger.info("===="+page+":"+j+"===============");
	    						JSONObject product = productArray.getJSONObject(j);
	    						String outid = product.getString("id");
	    						String itemname = product.getString("title");
	    						Map<String,Object> productData = itemService.findItemByOut(outid);
	    						
	    						if(productData == null){
	    							 saveProduct(outid, itemname) ;
	    							 logger.info("outid :"+outid+"---------------------->>>>");
	    						} else {
	    							Integer itemid = (Integer)productData.get("item_id");
		    						itemService.updateItemCategory(itemid, cid);
	    						}
	    						
	    					}
	    				}
	    				
	    				page++;
	    				 products = client.getTag(title, page, per_page);
	 	    			 tagsJson = JSONObject.fromObject(products);
	 	    			productArray = tagsJson.getJSONArray("products");
	 	    			 System.out.println("============end===");
	    			}
	    			
	    		}
	    	}
	    	
	    
	    	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.info("SyncCategoyJob end..."+sdf.format(new Date()));
		
	}

	private void saveProduct(String itemid,String title) throws Exception{
		ThirdApiClient client = new ThirdApiClient(null,null);
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		String item_detail = client.getProductDetail(itemid);
		logger.info("item_detail:"+item_detail);
		DetailItem detail = ThirdApiParser.parseDetailItem(item_detail);
		detail.setTitle(title);
		//保存商品
		int item_id = itemService.saveItem(detail);
		
		//保存sku
		ItemSku[] skus = detail.getSkus();
		if(skus != null && skus.length >0){
			for(int j=0;j<skus.length;j++){
				itemService.saveSku(item_id, skus[j]);
			}
		}
		
		//保存标签
		String[] labels = detail.getLabels();
		itemService.saveItemTag(item_id, labels);
		
		//保存焦点图
		HashImage[]  focus_images = detail.getPhotos();
		if(focus_images != null && focus_images.length >0){
			for(int j=0;j<focus_images.length;j++){
				String out_image_id = focus_images[j].getKey();
				String pic_url = focus_images[j].getUrl();
				itemService.savePicture( item_id, pic_url, 1, out_image_id);
			}
		}
		//保存详细页图
		String detail_url = detail.getDetail_url();
		logger.info("detail_url:"+detail_url);
		
		String[]  pictures = ThirdApiParser.parseDetailUrl(detail_url);
		 if(pictures != null && pictures.length >0){
			 for(int j=0;j<pictures.length;j++){
				 String out_image_id = Md5Util.encode(pictures[j]);
				 String pic_url = pictures[j];
				 itemService.savePicture( item_id, pic_url, 2, out_image_id);
			 }
		 }
	}
	
	
	
	
}

