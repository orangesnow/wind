package com.jskj.shop.schedule;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;

import com.jskj.shop.dao.impl.ArticleService;
import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.util.BeanFactory;


public class ThirdApiClient {
	
	private static final String Version = "1.0";

    private static final String apiEntry = "http://api.aihuo360.com";

    private static final String format = "json";

    private static final String signMethod = "md5";
    
    private static final String APIKey = "849ad695";

    private String appId;
    private String appSecret;

    public static final Logger logger = Logger.getLogger(ThirdApiClient.class);
    
    public ThirdApiClient(String appId, String appSecret) throws Exception{
        if ("".equals(appId) || "".equals(appSecret)){
            throw new Exception("appId 和 appSecret 不能为空");
        }
        
        this.appId = appId;
        this.appSecret = appSecret;
    }
    
    public static void main(String[] args) throws Exception{
    	ThirdApiClient client = new ThirdApiClient(null,null);
    	//String str = client.getArticle(2, 10);
//    	String detail =  client.getTag("男用玩具",1,20);
    	//String str = client.getCategory();
//   	System.out.println("str:"+detail);
//    	str = client.getArticle(3, 10);
//    	System.out.println("str:"+str);
//   		for(int i=0;i<20;i++){
//   			String detail =  client.getTag("送礼玩具",i+1,20);
//   			System.out.println("str:"+detail);
//   		}
    	//保存标签
   		String detail =  client.getTag("男用玩具",1,20);
   		System.out.println("str:"+detail);
   		
   		detail =  client.getTag("创意产品",1,20);
   		System.out.println("str:"+detail);

    	
    }
    
    /**
     * 获取
     * @param method
     * @param parames
     * @return
     * @throws Exception
     */
    public String getIndex() throws Exception{
    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/home";
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
       
        getMethod.addRequestHeader("APIKey", APIKey);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    /**
     * 获取
     * @param method
     * @param parames
     * @return
     * @throws Exception
     */
    public String getArticleDetail(String articleId) throws Exception{
    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/articles/"+articleId;
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
       
        getMethod.addRequestHeader("APIKey", APIKey);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    
    
    /**
     * 获取
     * @param method
     * @param parames
     * @return
     * @throws Exception
     */
    public String getProduct(int pageno,int pagesize) throws Exception{
    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/products?page="+pageno+"&per_page="+pagesize;
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
       
        getMethod.addRequestHeader("APIKey", APIKey);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    /**
     * 获取
     * @param method
     * @param parames
     * @return
     * @throws Exception
     */
    public String getArticle(int pageno,int pagesize) throws Exception{
    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/articles?page="+pageno+"&per_page="+pagesize;
    	logger.info("url:"+url);
    	
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
       
        getMethod.addRequestHeader("APIKey", APIKey);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    /**
     * 获取
     * @param method
     * @param parames
     * @return
     * @throws Exception
     */
    public String getProductDetail(String productid) throws Exception{
    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/products/"+productid;
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
       
        getMethod.addRequestHeader("APIKey", APIKey);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    
    
//    public String post(String method, HashMap<String, String> parames, List<String> filePaths, String fileKey) throws Exception{
//    	String result = "";
//    	String url = apiEntry + getParamStr(method, parames);
//    	
//    	HttpClient client = new HttpClient();
//    	PostMethod postMethod = new PostMethod(url);
//    	postMethod.addRequestHeader("APIKey", APIKey);
//    	
//    	if(null != filePaths && filePaths.size() > 0 && null != fileKey && !"".equals(fileKey)){
//    		
//    		Part[] parts = new FilePart[filePaths.size()];
//    	
//    		for(int i = 0; i < filePaths.size(); i++){
//	    		File file = new File(filePaths.get(i));
//	            parts[i] = new FilePart(fileKey, file);
//	    	}
//    		
//    		postMethod.setRequestEntity(new MultipartRequestEntity(parts,postMethod.getParams()));
//    		
//    	}
//    	
//		int statusCode = client.executeMethod(postMethod);  
//		if(statusCode == HttpStatus.SC_OK) {  
//			result = postMethod.getResponseBodyAsString();
//		}
//        
//        return result;
//    }
    
//    public String getParamStr(String method, HashMap<String, String> parames){
//        String str = "";
//        try {
//            str = URLEncoder.encode(buildParamStr(buildCompleteParams(method, parames)), "UTF-8")
//                    .replace("%3A", ":")
//                    .replace("%2F", "/")
//                    .replace("%26", "&")
//                    .replace("%3D", "=")
//                    .replace("%3F", "?");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return str;
//    }
    
    public String getCategory() throws Exception{
    	
    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/tabs";
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
        getMethod.addRequestHeader("APIKey", APIKey);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    public String getTag(String tagName,int page,int per_page)  throws Exception{
    	StringBuffer result = new StringBuffer();
    	logger.info("tagName:"+tagName);
    	String url = "http://api.aihuo360.com/v2/products?page="+page+"&per_page="+per_page+"&tag="+URLEncoder.encode(tagName, "utf8");
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
       
        getMethod.addRequestHeader("APIKey", APIKey);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    
    private String buildParamStr(HashMap<String, String> param){
        String paramStr = "";
        Object[] keyArray = param.keySet().toArray();
        
        for(int i = 0; i < keyArray.length; i++){
            String key = (String)keyArray[i];

            if(0 == i){
                paramStr += (key + "=" + param.get(key));
            }
            else{
                paramStr += ("&" + key + "=" + param.get(key));
            }
            
        }

        return paramStr;
    }


//    private HashMap<String, String> buildCompleteParams(String method, HashMap<String, String> parames) throws Exception{
//        HashMap<String, String> commonParams = getCommonParams(method);
//        for (String key : parames.keySet()) {
//			if(commonParams.containsKey(key)){
//				throw new Exception("参数名冲突");
//			}
//			
//			commonParams.put(key, parames.get(key));
//		}
//        
//        commonParams.put(KdtApiProtocol.SIGN_KEY, KdtApiProtocol.sign(appSecret, commonParams));
//        return commonParams;
//    }

//    private HashMap<String, String> getCommonParams(String method){
//    	HashMap<String, String> parames = new HashMap<String, String>();
//        parames.put(KdtApiProtocol.APP_ID_KEY, appId);
//        parames.put(KdtApiProtocol.METHOD_KEY, method);
//        parames.put(KdtApiProtocol.TIMESTAMP_KEY, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
//        parames.put(KdtApiProtocol.FORMAT_KEY, format);
//        parames.put(KdtApiProtocol.SIGN_METHOD_KEY, signMethod);
//        parames.put(KdtApiProtocol.VERSION_KEY, Version);
//        return parames;
//    }
}
