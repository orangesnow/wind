package com.jskj.shop.schedule;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jskj.shop.dao.impl.ArticleService;
import com.jskj.shop.dao.impl.ItemService;
import com.jskj.shop.dao.util.BeanFactory;

/**
 * 同步处理程序
 * @author jordon
 * @since 2014-05-18
 */
public class SyncArticleJob implements Job{
	
	private static Logger logger = Logger.getLogger(SyncArticleJob.class);

	public static void main(String[] args) throws Exception{
		new SyncArticleJob().execute(null);
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		logger.info("SyncArticleJob="+sdf.format(new Date()));
		
		ArticleService articleService = (ArticleService) BeanFactory.getBean("articleService");
		ItemService itemService = (ItemService) BeanFactory.getBean("itemService");
		
		int pageno =1 ;
		int pagesize = 10;
	
		try {
				
				ThirdApiClient client = new ThirdApiClient(null,null);
		    	String response = client.getArticle(pageno,pagesize);
		    	
		    	JSONObject respJson = JSONObject.fromObject(response);
		    	JSONArray articles = respJson.getJSONArray("articles");
		    	
		    	while(articles != null && !articles.isEmpty()){
		    		
		    		for(int i=0;i<articles.size();i++){
		    			logger.info("=============="+pageno+"====================");
		    			JSONObject articleJson = articles.getJSONObject(i);
		    			//保存单个文章内容
		    			String id = articleJson.getString("id");
		    			String title = articleJson.getString("title");
		    			Integer reads = articleJson.getInt("reading_count");
		    			String created = articleJson.getString("created_at");
		    			JSONArray labels = articleJson.getJSONArray("labels");
		    			
		    			JSONObject images = articleJson.getJSONObject("image");
		    			
		    			logger.info(images);
		    			String image = "";
		    			if(images != null && !images.isEmpty()){
		    				image = images.getString("default");
		    			}
		    			
		    			int articleid = articleService.saveArticle(id, image, title, reads, created);
		    			
		    			//保存标签
		    			if(labels != null && !labels.isEmpty()){
		    				for(int j=0;j<labels.size();j++){
		    					
		    					String label = labels.getString(j);
		    					Map<String,Object> labelMap = itemService.findTagByName(label);
		    					
		    					if(labelMap == null){
		    						int tagid = itemService.saveTag(label,0,0,1,"");
		    						articleService.saveArticleLabels(articleid, tagid);
		    					} else {
		    						int tagid = (Integer)labelMap.get("id");
		    						articleService.saveArticleLabels(articleid, tagid);
		    					}
		    					
		    				}
		    			}
		    			
		    			//保存文章明细内容
		    			String detail = client.getArticleDetail(id);
		    			JSONObject detailJson = JSONObject.fromObject(detail);
		    			String content  = detailJson.getString("body");

		    			articleService.saveArticle(articleid, content);
		    			
		    		}
		    		
		    		logger.info("=============="+pageno+"====================");
		    		logger.info("=============="+pageno+"====================");
		    		pageno++;
		    	 	response = client.getArticle(pageno,pagesize);
			    	respJson = JSONObject.fromObject(response);
			    	articles = respJson.getJSONArray("articles");
			    	
		    	}
		    	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.info("SyncArticleJob end....");
	}
	
	
}

