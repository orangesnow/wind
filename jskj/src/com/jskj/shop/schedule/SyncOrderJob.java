package com.jskj.shop.schedule;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 同步处理程序
 * @author jordon
 * @since 2014-05-18
 */
public class SyncOrderJob implements Job{
	
	private static Logger logger = Logger.getLogger(SyncOrderJob.class);
	public static final int SYNC_TYPE_SHARE = 4;
	public static final int SYNC_TYPE_GOODS = 6;
	public static final String SERVICE_PATH = "http://sv.yuanpin100.com/";
	
	
	public static void main(String[] args) throws Exception{
		new SyncOrderJob().execute(null);
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		logger.info("SyncOrderJob="+sdf.format(new Date()));

//		//同步任务
//		SyncResultService syncResultService = (SyncResultService) BeanFactory.getBean("syncResultService");
//		List<Map<String,Object>> results = syncResultService.findSyncDuty();

		logger.info("SyncOrderJob end..."+sdf.format(new Date()));
	}
	
	

}

