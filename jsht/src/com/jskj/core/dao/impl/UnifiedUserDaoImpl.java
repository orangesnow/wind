package com.jskj.core.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;
import com.jskj.core.dao.UnifiedUserDao;
import com.jskj.core.entity.UnifiedUser;

@Repository
public class UnifiedUserDaoImpl extends HibernateBaseDao<UnifiedUser, Integer>
		implements UnifiedUserDao {
	public UnifiedUser getByUsername(String username) {
		return findUniqueByProperty("username", username);
	}

	public List<UnifiedUser> getByEmail(String email) {
		return findByProperty("email", email);
	}
	
	public int countByEmail(String email) {
		String hql = "select count(*) from UnifiedUser bean where bean.email=:email";
		Query query = getSession().createQuery(hql);
		query.setParameter("email", email);
		return ((Number) query.iterate().next()).intValue();
	}
	
	public List<UnifiedUser> getByMobile(String mobile) {
		return findByProperty("mobile", mobile);
	}
	
	public int countByMobile(String mobile) {
		String hql = "select count(*) from UnifiedUser bean where bean.mobile=:mobile";
		Query query = getSession().createQuery(hql);
		query.setParameter("mobile", mobile);
		return ((Number) query.iterate().next()).intValue();
	}
	
	@SuppressWarnings("unchecked")
	public List<UnifiedUser> valid(String username,String email,String mobile) {
		Finder f = Finder.create("select bean from UnifiedUser bean  ");
		f.append(" where bean.username = :username or bean.email = :email or bean.mobile = :mobile");
		
		f.setParam("username", username);
		f.setParam("email", email);
		f.setParam("mobile", mobile);

		return find(f);
	}

	public Pagination getPage(int pageNo, int pageSize) {
		Criteria crit = createCriteria();
		Pagination page = findByCriteria(crit, pageNo, pageSize);
		return page;
	}

	public UnifiedUser findById(Integer id) {
		UnifiedUser entity = get(id);
		return entity;
	}

	public UnifiedUser save(UnifiedUser bean) {
		getSession().save(bean);
		return bean;
	}

	public UnifiedUser deleteById(Integer id) {
		UnifiedUser entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<UnifiedUser> getEntityClass() {
		return UnifiedUser.class;
	}

}