package com.jskj.core.dao;

import java.util.List;

import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;
import com.jskj.core.entity.UnifiedUser;

public interface UnifiedUserDao {
	public UnifiedUser getByUsername(String username);

	public List<UnifiedUser> getByEmail(String email);

	public int countByEmail(String email);

	public Pagination getPage(int pageNo, int pageSize);

	public UnifiedUser findById(Integer id);

	public UnifiedUser save(UnifiedUser bean);

	public UnifiedUser updateByUpdater(Updater<UnifiedUser> updater);

	public UnifiedUser deleteById(Integer id);

	public List<UnifiedUser> valid(String username,String email,String mobile);
	
	public int countByMobile(String mobile);
	
	public List<UnifiedUser> getByMobile(String mobile);
	
}