
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信文本消息实体类
 * @author VAIO
 * @since 2014-06-03
 */
public class CmsWxMessage implements Serializable {
	
	public static int STATUS_NOHANDLE = 0;
	public static int STATUS_RELPY_NOSEND = 2;
	public static int STATUS_REPLY_OK = 1;
	public static int STATUS_FAILURE = 9;

	private static final long serialVersionUID = 1L;
	private String id;
	private String content;
	private String msgType;
	private long createtime;
	private String openid;
	private String username;
	private Date oprtime;
	private int status;//0 表示未处理 2表示已回复但未下发 1表示下发成功 9表示下发失败
	private String reply;
	private Date replyTime;
	private WxPublicAccount account;
	private WxFollowUser user;

	public WxFollowUser getUser() {
		return user;
	}
	public void setUser(WxFollowUser user) {
		this.user = user;
	}
	public WxPublicAccount getAccount() {
		return account;
	}
	public void setAccount(WxPublicAccount account) {
		this.account = account;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}
	/**
	 * @param msgType the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	/**
	 * @return the createtime
	 */
	public long getCreatetime() {
		return createtime;
	}
	/**
	 * @param createtime the createtime to set
	 */
	public void setCreatetime(long createtime) {
		this.createtime = createtime;
	}
	/**
	 * @return the openid
	 */
	public String getOpenid() {
		return openid;
	}
	/**
	 * @param openid the openid to set
	 */
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the oprtime
	 */
	public Date getOprtime() {
		return oprtime;
	}
	/**
	 * @param oprtime the oprtime to set
	 */
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the replay
	 */
	public String getReply() {
		return reply;
	}
	/**
	 * @param replay the replay to set
	 */
	public void setReply(String reply) {
		this.reply = reply;
	}
	/**
	 * @return the replyTime
	 */
	public Date getReplyTime() {
		return replyTime;
	}
	/**
	 * @param replyTime the replyTime to set
	 */
	public void setReplyTime(Date replyTime) {
		this.replyTime = replyTime;
	}

}
