package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class WxSyncRule implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private Date oprtime;
	private WxSyncType stype;
	private Integer isRepeat;
	private String cond;
	private String remark;
	private Date startTime;
	private Integer isHistory;
	private Integer status;
	private Integer isDelay;
	private WxPublicAccount account;
	private String url;
	private String params;

	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public WxPublicAccount getAccount() {
		return account;
	}
	public void setAccount(WxPublicAccount account) {
		this.account = account;
	}
	public Integer getIsHistory() {
		return isHistory;
	}
	public void setIsHistory(Integer isHistory) {
		this.isHistory = isHistory;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIsDelay() {
		return isDelay;
	}
	public void setIsDelay(Integer isDelay) {
		this.isDelay = isDelay;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public WxSyncType getStype() {
		return stype;
	}
	public void setStype(WxSyncType stype) {
		this.stype = stype;
	}
	public Integer getIsRepeat() {
		return isRepeat;
	}
	public void setIsRepeat(Integer isRepeat) {
		this.isRepeat = isRepeat;
	}
	
	public String getCond() {
		return cond;
	}
	public void setCond(String cond) {
		this.cond = cond;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
