
package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class WxQrcodeType implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private int status;
	private Integer isSystem;
	private String remark;

	public Integer getIsSystem() {
		return isSystem;
	}
	public void setIsSystem(Integer isSystem) {
		this.isSystem = isSystem;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
