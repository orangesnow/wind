package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjSubjectReport implements Serializable{
	
	private Integer id;
	private String content;
	private Integer rtype;
	private Date oprtime;
	private JskjUser user;
	private JskjSubject subject;
	private JskjSubjectReply reply;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getRtype() {
		return rtype;
	}
	public void setRtype(Integer rtype) {
		this.rtype = rtype;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public JskjUser getUser() {
		return user;
	}
	public void setUser(JskjUser user) {
		this.user = user;
	}
	public JskjSubject getSubject() {
		return subject;
	}
	public void setSubject(JskjSubject subject) {
		this.subject = subject;
	}
	public JskjSubjectReply getReply() {
		return reply;
	}
	public void setReply(JskjSubjectReply reply) {
		this.reply = reply;
	}

}
