package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class JskjArticleCategory implements Serializable{
	
	private Integer id;
	private String name;
	private Integer status;
	private Integer clevel;
	private JskjArticleCategory parent;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getStatus() {
		return status;
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Integer getClevel() {
		return clevel;
	}
	
	public void setClevel(Integer clevel) {
		this.clevel = clevel;
	}
	
	public JskjArticleCategory getParent() {
		return parent;
	}
	
	public void setParent(JskjArticleCategory parent) {
		this.parent = parent;
	}
	
}
