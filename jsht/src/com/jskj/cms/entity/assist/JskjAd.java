package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class JskjAd implements Serializable{
	
	private Integer id;
	private Integer adtype;
	private String picurl;
	private Integer weight;
	private Integer status;
	private Integer bindflag;
	private Integer bindid;
	private String bindurl;
	private String name;
	private String remark;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAdtype() {
		return adtype;
	}
	public void setAdtype(Integer adtype) {
		this.adtype = adtype;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getBindflag() {
		return bindflag;
	}
	public void setBindflag(Integer bindflag) {
		this.bindflag = bindflag;
	}
	public Integer getBindid() {
		return bindid;
	}
	public void setBindid(Integer bindid) {
		this.bindid = bindid;
	}
	public String getBindurl() {
		return bindurl;
	}
	public void setBindurl(String bindurl) {
		this.bindurl = bindurl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
