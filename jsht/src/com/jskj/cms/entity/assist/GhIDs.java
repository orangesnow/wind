/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * 订单数据库
 * @author VAIO
 * @since 2014-08-14
 */
public class GhIDs implements Serializable{

	private static final long serialVersionUID = 1L;
	private String cardno;
	private String batch;
	private Integer status;
	
	public String getCardno() {
		return cardno;
	}
	public void setCardno(String cardno) {
		this.cardno = cardno;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

}
