/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * @author VAIO
 * @since 2014-08-11
 */
public class GhBindLeave implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String ghid;
	private String userid;
	private GhLeave leave;
	
	private String recevName;
	private String recevMobile;
	private int bindStatus;
	private int checkStatus;
	private int notifyStatus;
	
	private Date notifyTime;
	private Date bindTime;
	private Date readTime;
	private String remark;
	
	private Integer readCount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGhid() {
		return ghid;
	}

	public void setGhid(String ghid) {
		this.ghid = ghid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public GhLeave getLeave() {
		return leave;
	}

	public void setLeave(GhLeave leave) {
		this.leave = leave;
	}

	public String getRecevName() {
		return recevName;
	}

	public void setRecevName(String recevName) {
		this.recevName = recevName;
	}

	public String getRecevMobile() {
		return recevMobile;
	}

	public void setRecevMobile(String recevMobile) {
		this.recevMobile = recevMobile;
	}

	public int getBindStatus() {
		return bindStatus;
	}

	public void setBindStatus(int bindStatus) {
		this.bindStatus = bindStatus;
	}

	public int getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(int checkStatus) {
		this.checkStatus = checkStatus;
	}

	public int getNotifyStatus() {
		return notifyStatus;
	}

	public void setNotifyStatus(int notifyStatus) {
		this.notifyStatus = notifyStatus;
	}

	public Date getNotifyTime() {
		return notifyTime;
	}

	public void setNotifyTime(Date notifyTime) {
		this.notifyTime = notifyTime;
	}

	public Date getBindTime() {
		return bindTime;
	}

	public void setBindTime(Date bindTime) {
		this.bindTime = bindTime;
	}

	public Date getReadTime() {
		return readTime;
	}

	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getReadCount() {
		return readCount;
	}

	public void setReadCount(Integer readCount) {
		this.readCount = readCount;
	}
	
}
