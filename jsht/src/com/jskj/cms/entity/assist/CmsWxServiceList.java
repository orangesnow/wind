
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * 微信业务对象
 * @author VAIO
 * @since 2014-05-13
 */
public class CmsWxServiceList   implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 业务ID
	 */
	private int serviceid;
	/**
	 * 业务名称
	 */
	private String name;
	/**
	 * 业务类型
	 */
	private int type;
	/**
	 * 业务处理地址 
	 */
	private String url;
	/**
	 * 业务描述
	 */
	private String remark;
	
	/**
	 * @return the serviceid
	 */
	public int getServiceid() {
		return serviceid;
	}



	/**
	 * @param serviceid the serviceid to set
	 */
	public void setServiceid(int serviceid) {
		this.serviceid = serviceid;
	}



	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}



	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}



	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}



	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}



	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}



	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}



	/**
	 * 
	 */
	public CmsWxServiceList() {
		// TODO Auto-generated constructor stub
	}

}
