/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * @author VAIO
 * @since 2014-08-12
 */
public class GhVisitRecord implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer userid;
	private Date visitTime;
	private Integer visitType;
	private String merits;
	private String faults;
	private Date oprtime;
	
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public Date getVisitTime() {
		return visitTime;
	}
	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}
	public Integer getVisitType() {
		return visitType;
	}
	public void setVisitType(Integer visitType) {
		this.visitType = visitType;
	}
	public String getMerits() {
		return merits;
	}
	public void setMerits(String merits) {
		this.merits = merits;
	}
	public String getFaults() {
		return faults;
	}
	public void setFaults(String faults) {
		this.faults = faults;
	}
	
	
	
}
