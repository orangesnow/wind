package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjHotKeyword implements Serializable{
	
	private Integer id;
	private String content;
	private Integer status;
	private Integer htype;
	private Integer conid;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getHtype() {
		return htype;
	}
	public void setHtype(Integer htype) {
		this.htype = htype;
	}
	public Integer getConid() {
		return conid;
	}
	public void setConid(Integer conid) {
		this.conid = conid;
	}

}
