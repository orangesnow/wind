/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * 配置key管理
 * @author VAIO
 * @since 2014-08-17
 */
public class GhConfigCategory implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
