package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class JskjItemSku implements Serializable{
	
	private Integer id;
	private Integer itemid;
	private String skuname;
	private Integer num;
	private Double price;
	private Double originprice;
	private String remark;
	private String outskuid;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getItemid() {
		return itemid;
	}
	public void setItemid(Integer itemid) {
		this.itemid = itemid;
	}
	public String getSkuname() {
		return skuname;
	}
	public void setSkuname(String skuname) {
		this.skuname = skuname;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getOriginprice() {
		return originprice;
	}
	public void setOriginprice(Double originprice) {
		this.originprice = originprice;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOutskuid() {
		return outskuid;
	}
	public void setOutskuid(String outskuid) {
		this.outskuid = outskuid;
	}
	
}
