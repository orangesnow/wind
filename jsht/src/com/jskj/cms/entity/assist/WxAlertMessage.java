
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * 微信配置提醒信息
 * @author VAIO
 * @since 2014-06-19
 */
public class WxAlertMessage implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String cname;
	private String ckey;
	private String cvalue;
	private GhConfigSource source;
	private int status;
	private WxPublicAccount account;

	public WxPublicAccount getAccount() {
		return account;
	}

	public void setAccount(WxPublicAccount account) {
		this.account = account;
	}

	public GhConfigSource getSource() {
		return source;
	}

	public void setSource(GhConfigSource source) {
		this.source = source;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the cname
	 */
	public String getCname() {
		return cname;
	}
	/**
	 * @param cname the cname to set
	 */
	public void setCname(String cname) {
		this.cname = cname;
	}
	/**
	 * @return the ckey
	 */
	public String getCkey() {
		return ckey;
	}
	/**
	 * @param ckey the ckey to set
	 */
	public void setCkey(String ckey) {
		this.ckey = ckey;
	}
	/**
	 * @return the cvalue
	 */
	public String getCvalue() {
		return cvalue;
	}
	/**
	 * @param cvalue the cvalue to set
	 */
	public void setCvalue(String cvalue) {
		this.cvalue = cvalue;
	}
	
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
}
