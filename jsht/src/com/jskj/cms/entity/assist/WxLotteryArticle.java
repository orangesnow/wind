
package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class WxLotteryArticle implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String reward;
	private WxLotteryProduct product;
	private Integer amount;
	private Integer remain;
	private Integer minNum;
	private String tailNum;
	private Integer status;
	private String remark;
	private Integer angle;
	private Integer weight;
	private Integer nolottery;

	/**
	 * @return the reward
	 */
	public String getReward() {
		return reward;
	}
	/**
	 * @param reward the reward to set
	 */
	public void setReward(String reward) {
		this.reward = reward;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the product
	 */
	public WxLotteryProduct getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(WxLotteryProduct product) {
		this.product = product;
	}
	/**
	 * @return the amount
	 */
	public Integer getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	/**
	 * @return the remain
	 */
	public Integer getRemain() {
		return remain;
	}
	/**
	 * @param remain the remain to set
	 */
	public void setRemain(Integer remain) {
		this.remain = remain;
	}
	/**
	 * @return the minNum
	 */
	public Integer getMinNum() {
		return minNum;
	}
	/**
	 * @param minNum the minNum to set
	 */
	public void setMinNum(Integer minNum) {
		this.minNum = minNum;
	}
	/**
	 * @return the tailNum
	 */
	public String getTailNum() {
		return tailNum;
	}
	/**
	 * @param tailNum the tailNum to set
	 */
	public void setTailNum(String tailNum) {
		this.tailNum = tailNum;
	}
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * @return the angle
	 */
	public Integer getAngle() {
		return angle;
	}
	/**
	 * @param angle the angle to set
	 */
	public void setAngle(Integer angle) {
		this.angle = angle;
	}
	/**
	 * @return the weight
	 */
	public Integer getWeight() {
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	/**
	 * @return the nolottery
	 */
	public Integer getNolottery() {
		return nolottery;
	}
	/**
	 * @param nolottery the nolottery to set
	 */
	public void setNolottery(Integer nolottery) {
		this.nolottery = nolottery;
	}
	
}
