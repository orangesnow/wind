
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 微信用户
 * @author VAIO
 * @since2014-09-09
 */
public class WxFollowUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int userid;
	private String openid;
	
	private String subType;
	private Date subDate;
	private Date unsubDate;
	
	private WxPublicAccount account;
	private String name;
	private String nickname;
	private Integer sex;
	
	private String city;
	private String province;
	private String country;
	
	private String language;
	private String imgUrl;
	private Integer status;
	
	private WxUserGroup group;
	private String remark;
	private Date lastOprtime;
	
	private Integer isSign;
	private Integer isClub;
	private String unionid;

	public String getUnionid() {
		return unionid;
	}
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
	public Integer getIsSign() {
		return isSign;
	}
	public void setIsSign(Integer isSign) {
		this.isSign = isSign;
	}
	public Integer getIsClub() {
		return isClub;
	}
	public void setIsClub(Integer isClub) {
		this.isClub = isClub;
	}
	private List<WxTagList> tags;
	
	public List<WxTagList> getTags() {
		return tags;
	}
	public void setTags(List<WxTagList> tags) {
		this.tags = tags;
	}
	public Date getLastOprtime() {
		return lastOprtime;
	}
	public void setLastOprtime(Date lastOprtime) {
		this.lastOprtime = lastOprtime;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	
	public Date getSubDate() {
		return subDate;
	}
	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}
	public Date getUnsubDate() {
		return unsubDate;
	}
	public void setUnsubDate(Date unsubDate) {
		this.unsubDate = unsubDate;
	}
	
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public WxUserGroup getGroup() {
		return group;
	}
	public void setGroup(WxUserGroup group) {
		this.group = group;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public WxPublicAccount getAccount() {
		return account;
	}
	public void setAccount(WxPublicAccount account) {
		this.account = account;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
