/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单关系信息
 * @author VAIO
 * @since 2014-08-12
 */
public class GhOrderRelation implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	
	private GhOrderInfo ghOrder;
	
	private WxFollowUser owner;
	
	private Date oprtime;
	
	private int rtype;

	private Integer visitFlag;
	
	private GhVisitRecord ghVisit;
	
	public GhVisitRecord getGhVisit() {
		return ghVisit;
	}

	public void setGhVisit(GhVisitRecord ghVisit) {
		this.ghVisit = ghVisit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GhOrderInfo getGhOrder() {
		return ghOrder;
	}

	public void setGhOrder(GhOrderInfo ghOrder) {
		this.ghOrder = ghOrder;
	}

	public WxFollowUser getOwner() {
		return owner;
	}

	public void setOwner(WxFollowUser owner) {
		this.owner = owner;
	}

	public Date getOprtime() {
		return oprtime;
	}

	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}

	public int getRtype() {
		return rtype;
	}

	public void setRtype(int rtype) {
		this.rtype = rtype;
	}

	public Integer getVisitFlag() {
		return visitFlag;
	}

	public void setVisitFlag(Integer visitFlag) {
		this.visitFlag = visitFlag;
	}

}
