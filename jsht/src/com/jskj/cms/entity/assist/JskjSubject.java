package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjSubject implements Serializable{
	
	private Integer id;
	private String content;
	private String location;
	private JskjSubjectGroup group;
	private Integer status;
	private Date oprtime;
	private JskjUser user;
	private Integer istop;
	private Integer isrecomm;
	private Integer istag;
	private String picurl;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public JskjSubjectGroup getGroup() {
		return group;
	}
	public void setGroup(JskjSubjectGroup group) {
		this.group = group;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public JskjUser getUser() {
		return user;
	}
	public void setUser(JskjUser user) {
		this.user = user;
	}
	public Integer getIstop() {
		return istop;
	}
	public void setIstop(Integer istop) {
		this.istop = istop;
	}
	public Integer getIsrecomm() {
		return isrecomm;
	}
	public void setIsrecomm(Integer isrecomm) {
		this.isrecomm = isrecomm;
	}
	public Integer getIstag() {
		return istag;
	}
	public void setIstag(Integer istag) {
		this.istag = istag;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}

}
