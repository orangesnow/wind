package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class SfuDataCompany implements Serializable{
	
	private Integer companyId;
	private String name;
	private String industryField;
	private String emplyees;
	private String websiteUrl;
	private String investStatus;
	private String founderName;
	private String founderWeibo;
	private String founderWeixin;
	private Date foundTime;
	private String founderEmail;
	private String founderMobile;
	private String city;
	private String address;
	private String collectSrc;
	private Date collectTime;
	private Date lastUpdate;
	private Double avgWage;
	private Date lastNew;
	private Integer status;
	private String remark;
	private String sourceUrl;
	private Integer findFlag;
	private Integer keyFlag;
	private String ourUrl;
	
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIndustryField() {
		return industryField;
	}
	public void setIndustryField(String industryField) {
		this.industryField = industryField;
	}
	public String getEmplyees() {
		return emplyees;
	}
	public void setEmplyees(String emplyees) {
		this.emplyees = emplyees;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}
	public String getInvestStatus() {
		return investStatus;
	}
	public void setInvestStatus(String investStatus) {
		this.investStatus = investStatus;
	}
	public String getFounderName() {
		return founderName;
	}
	public void setFounderName(String founderName) {
		this.founderName = founderName;
	}
	public String getFounderWeibo() {
		return founderWeibo;
	}
	public void setFounderWeibo(String founderWeibo) {
		this.founderWeibo = founderWeibo;
	}
	public String getFounderWeixin() {
		return founderWeixin;
	}
	public void setFounderWeixin(String founderWeixin) {
		this.founderWeixin = founderWeixin;
	}
	public Date getFoundTime() {
		return foundTime;
	}
	public void setFoundTime(Date founderTime) {
		this.foundTime = founderTime;
	}
	public String getFounderEmail() {
		return founderEmail;
	}
	public void setFounderEmail(String founderEmail) {
		this.founderEmail = founderEmail;
	}
	public String getFounderMobile() {
		return founderMobile;
	}
	public void setFounderMobile(String founderMobile) {
		this.founderMobile = founderMobile;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCollectSrc() {
		return collectSrc;
	}
	public void setCollectSrc(String collectSrc) {
		this.collectSrc = collectSrc;
	}
	public Date getCollectTime() {
		return collectTime;
	}
	public void setCollectTime(Date collectTime) {
		this.collectTime = collectTime;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Double getAvgWage() {
		return avgWage;
	}
	public void setAvgWage(Double avgWage) {
		this.avgWage = avgWage;
	}
	public Date getLastNew() {
		return lastNew;
	}
	public void setLastNew(Date lastNew) {
		this.lastNew = lastNew;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSourceUrl() {
		return sourceUrl;
	}
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	public Integer getFindFlag() {
		return findFlag;
	}
	public void setFindFlag(Integer findFlag) {
		this.findFlag = findFlag;
	}
	public Integer getKeyFlag() {
		return keyFlag;
	}
	public void setKeyFlag(Integer keyFlag) {
		this.keyFlag = keyFlag;
	}
	public String getOurUrl() {
		return ourUrl;
	}
	public void setOurUrl(String ourUrl) {
		this.ourUrl = ourUrl;
	}

}
