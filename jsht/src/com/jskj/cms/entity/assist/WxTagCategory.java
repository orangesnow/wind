
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * 微信用户
 * @author VAIO
 * @since 2014-12-11
 */
public class WxTagCategory implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private int status;
	private int rootId;
	private String remark;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getRootId() {
		return rootId;
	}
	public void setRootId(int rootId) {
		this.rootId = rootId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
