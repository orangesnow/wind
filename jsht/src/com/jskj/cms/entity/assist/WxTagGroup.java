package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.List;

public class WxTagGroup implements Serializable{
	
	private int id;
	private String name;
	private String remark;
	private WxPublicAccount account;
	private List<WxTagList> tags;
	
	public List<WxTagList> getTags() {
		return tags;
	}
	public void setTags(List<WxTagList> tags) {
		this.tags = tags;
	}
	public WxPublicAccount getAccount() {
		return account;
	}
	public void setAccount(WxPublicAccount account) {
		this.account = account;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
