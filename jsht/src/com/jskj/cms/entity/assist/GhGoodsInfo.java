/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * 物流用户信息
 * @author VAIO
 * @since 2015-03-28
 * 
 */
public class GhGoodsInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	private Date oprtime;

	private String mobile;
	private String address;

	private WxFollowUser user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getOprtime() {
		return oprtime;
	}

	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public WxFollowUser getUser() {
		return user;
	}

	public void setUser(WxFollowUser user) {
		this.user = user;
	}

}
