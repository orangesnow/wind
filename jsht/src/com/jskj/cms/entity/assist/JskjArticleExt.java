package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class JskjArticleExt implements Serializable{
	
	private Integer id;
	private String body;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
}
