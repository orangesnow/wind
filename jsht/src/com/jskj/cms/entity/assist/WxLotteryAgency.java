
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class WxLotteryAgency implements Serializable{
	
	private String weixin;
	private String name;
	private String mobile;
	private Date oprtime;
	private String remark;
	private String address;
	/**
	 * @return the weixin
	 */
	public String getWeixin() {
		return weixin;
	}
	/**
	 * @param weixin the weixin to set
	 */
	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @return the oprtime
	 */
	public Date getOprtime() {
		return oprtime;
	}
	/**
	 * @param oprtime the oprtime to set
	 */
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
}
