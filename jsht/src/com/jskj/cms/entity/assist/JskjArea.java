package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class JskjArea implements Serializable{
	
	private Integer id;
	private String name;
	private Integer strongflag;
	private Integer istop;
	private Integer isrecomm;
	private Integer weight;
	private String picurl;
	private Integer status;
	private String url;
	private Integer showtype;
	private String remark;
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStrongflag() {
		return strongflag;
	}
	public void setStrongflag(Integer strongflag) {
		this.strongflag = strongflag;
	}
	public Integer getIstop() {
		return istop;
	}
	public void setIstop(Integer istop) {
		this.istop = istop;
	}
	public Integer getIsrecomm() {
		return isrecomm;
	}
	public void setIsrecomm(Integer isrecomm) {
		this.isrecomm = isrecomm;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getShowtype() {
		return showtype;
	}
	public void setShowtype(Integer showtype) {
		this.showtype = showtype;
	}

}
