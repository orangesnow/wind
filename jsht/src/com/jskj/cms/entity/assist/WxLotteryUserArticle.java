
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class WxLotteryUserArticle implements Serializable {
	
	private Integer id;
	private WxLotteryUser user;
	private WxLotteryArticle article;
	private WxLotteryAgency agency;
	private Date oprtime;
	private Integer isConfirm;
	private Date confirmTime;
	private String code;
	private Integer sendTimes;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the user
	 */
	public WxLotteryUser getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(WxLotteryUser user) {
		this.user = user;
	}
	/**
	 * @return the article
	 */
	public WxLotteryArticle getArticle() {
		return article;
	}
	/**
	 * @param article the article to set
	 */
	public void setArticle(WxLotteryArticle article) {
		this.article = article;
	}
	/**
	 * @return the agency
	 */
	public WxLotteryAgency getAgency() {
		return agency;
	}
	/**
	 * @param agency the agency to set
	 */
	public void setAgency(WxLotteryAgency agency) {
		this.agency = agency;
	}
	/**
	 * @return the oprtime
	 */
	public Date getOprtime() {
		return oprtime;
	}
	/**
	 * @param oprtime the oprtime to set
	 */
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	/**
	 * @return the isConfirm
	 */
	public Integer getIsConfirm() {
		return isConfirm;
	}
	/**
	 * @param isConfirm the isConfirm to set
	 */
	public void setIsConfirm(Integer isConfirm) {
		this.isConfirm = isConfirm;
	}
	/**
	 * @return the confirmTime
	 */
	public Date getConfirmTime() {
		return confirmTime;
	}
	/**
	 * @param confirmTime the confirmTime to set
	 */
	public void setConfirmTime(Date confirmTime) {
		this.confirmTime = confirmTime;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the sendTimes
	 */
	public Integer getSendTimes() {
		return sendTimes;
	}
	/**
	 * @param sendTimes the sendTimes to set
	 */
	public void setSendTimes(Integer sendTimes) {
		this.sendTimes = sendTimes;
	}

}
