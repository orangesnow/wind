package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjVersion implements Serializable{
	
	private Integer appid;
	private Date oprtime;
	private String downurl;
	private Integer versioncode;
	private String remark;
	private long filesize;
	private String md5str;
	private String versionName;
	private Integer status;
	private String channelStr;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getChannelStr() {
		return channelStr;
	}
	public void setChannelStr(String channelStr) {
		this.channelStr = channelStr;
	}
	public long getFilesize() {
		return filesize;
	}
	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}
	public String getMd5str() {
		return md5str;
	}
	public void setMd5str(String md5str) {
		this.md5str = md5str;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public Integer getAppid() {
		return appid;
	}
	public void setAppid(Integer appid) {
		this.appid = appid;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public String getDownurl() {
		return downurl;
	}
	public void setDownurl(String downurl) {
		this.downurl = downurl;
	}
	public Integer getVersioncode() {
		return versioncode;
	}
	public void setVersioncode(Integer versioncode) {
		this.versioncode = versioncode;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
