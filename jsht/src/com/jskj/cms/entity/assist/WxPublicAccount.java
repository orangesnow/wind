
package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class WxPublicAccount implements Serializable {
	
	private int id;
	private String name;
	private String pseudoCode;
	private String nickname;
	private String code;
	private String appId;
	private String appSecret;
	private Integer status;
	private Integer atype;
	private String checkToken;
	
	public String getPseudoCode() {
		return pseudoCode;
	}
	public void setPseudoCode(String pseudoCode) {
		this.pseudoCode = pseudoCode;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAppSecret() {
		return appSecret;
	}
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getAtype() {
		return atype;
	}
	public void setAtype(Integer atype) {
		this.atype = atype;
	}
	public String getCheckToken() {
		return checkToken;
	}
	public void setCheckToken(String checkToken) {
		this.checkToken = checkToken;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
}
