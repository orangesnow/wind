
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class WxUserStatDay implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String totalTime;
	private Integer subs;
	private Integer unsubs;
	private Integer totalSubs;
	private Integer totalUnsubs;
	private WxPublicAccount account;
	private Date oprtime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}
	public Integer getSubs() {
		return subs;
	}
	public void setSubs(Integer subs) {
		this.subs = subs;
	}
	public Integer getUnsubs() {
		return unsubs;
	}
	public void setUnsubs(Integer unsubs) {
		this.unsubs = unsubs;
	}
	public WxPublicAccount getAccount() {
		return account;
	}
	public void setAccount(WxPublicAccount account) {
		this.account = account;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public Integer getTotalSubs() {
		return totalSubs;
	}
	public void setTotalSubs(Integer totalSubs) {
		this.totalSubs = totalSubs;
	}
	public Integer getTotalUnsubs() {
		return totalUnsubs;
	}
	public void setTotalUnsubs(Integer totalUnsubs) {
		this.totalUnsubs = totalUnsubs;
	}
	
}
