package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjItemList implements Serializable{
	
	private Integer id;
	private String title;
	private double price;
	private double originprice;
	private String detailurl;
	private Date created;
	private String picurl;
	private String thumb;
	private String remark;
	private Integer num;
	private Integer soldnum;
	private Integer status;
	private String tagstr;
	private JskjCategory category;
	private Integer soldweight;
	private Integer recommweight;
	private String outitemid;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getOriginprice() {
		return originprice;
	}
	public void setOriginprice(double originprice) {
		this.originprice = originprice;
	}
	public String getDetailurl() {
		return detailurl;
	}
	public void setDetailurl(String detailurl) {
		this.detailurl = detailurl;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Integer getSoldnum() {
		return soldnum;
	}
	public void setSoldnum(Integer soldnum) {
		this.soldnum = soldnum;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getTagstr() {
		return tagstr;
	}
	public void setTagstr(String tagstr) {
		this.tagstr = tagstr;
	}
	public JskjCategory getCategory() {
		return category;
	}
	public void setCategory(JskjCategory category) {
		this.category = category;
	}
	public Integer getSoldweight() {
		return soldweight;
	}
	public void setSoldweight(Integer soldweight) {
		this.soldweight = soldweight;
	}
	public Integer getRecommweight() {
		return recommweight;
	}
	public void setRecommweight(Integer recommweight) {
		this.recommweight = recommweight;
	}
	public String getOutitemid() {
		return outitemid;
	}
	public void setOutitemid(String outitemid) {
		this.outitemid = outitemid;
	}

}
