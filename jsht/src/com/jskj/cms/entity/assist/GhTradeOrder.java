/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * @author VAIO
 * @since 2014-08-12
 */
public class GhTradeOrder implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String ghid;
	
	private Integer itemid;
	private Integer skuid;
	private Integer num;

	private String title;
	
	private Double fenxiaoPrice;
	private Double fenxiaoPayment;
	
	private Double price;
	private Double totalFee;
	private Double payment;
	
	private String buyerMessage;
	private String picPath;
	private int itemType;
	private String picThumbPath;
	private Double promotion;
	private String buyerMessages;
	
	private Double discountFee;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGhid() {
		return ghid;
	}

	public void setGhid(String ghid) {
		this.ghid = ghid;
	}

	public Integer getItemid() {
		return itemid;
	}

	public void setItemid(Integer itemid) {
		this.itemid = itemid;
	}

	public Integer getSkuid() {
		return skuid;
	}

	public void setSkuid(Integer skuid) {
		this.skuid = skuid;
	}


	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public Double getFenxiaoPrice() {
		return fenxiaoPrice;
	}

	public void setFenxiaoPrice(Double fenxiaoPrice) {
		this.fenxiaoPrice = fenxiaoPrice;
	}

	public Double getFenxiaoPayment() {
		return fenxiaoPayment;
	}

	public void setFenxiaoPayment(Double fenxiaoPayment) {
		this.fenxiaoPayment = fenxiaoPayment;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Double totalFee) {
		this.totalFee = totalFee;
	}

	public Double getPayment() {
		return payment;
	}

	public void setPayment(Double payment) {
		this.payment = payment;
	}

	public String getBuyerMessage() {
		return buyerMessage;
	}

	public void setBuyerMessage(String buyerMessage) {
		this.buyerMessage = buyerMessage;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public int getItemType() {
		return itemType;
	}

	public void setItemType(int itemType) {
		this.itemType = itemType;
	}

	public String getPicThumbPath() {
		return picThumbPath;
	}

	public void setPicThumbPath(String picThumbPath) {
		this.picThumbPath = picThumbPath;
	}

	public Double getPromotion() {
		return promotion;
	}

	public void setPromotion(Double promotion) {
		this.promotion = promotion;
	}

	public String getBuyerMessages() {
		return buyerMessages;
	}

	public void setBuyerMessages(String buyerMessages) {
		this.buyerMessages = buyerMessages;
	}

	public Double getDiscountFee() {
		return discountFee;
	}

	public void setDiscountFee(Double discountFee) {
		this.discountFee = discountFee;
	}

}
