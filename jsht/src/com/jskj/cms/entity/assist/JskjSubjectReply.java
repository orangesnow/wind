package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjSubjectReply implements Serializable{
	
	private Integer id;
	private JskjSubject subject;
	private JskjUser user;
	private Date oprtime;
	private String content;
	private Integer rootid;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public JskjSubject getSubject() {
		return subject;
	}
	public void setSubject(JskjSubject subject) {
		this.subject = subject;
	}
	public JskjUser getUser() {
		return user;
	}
	public void setUser(JskjUser user) {
		this.user = user;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getRootid() {
		return rootid;
	}
	public void setRootid(Integer rootid) {
		this.rootid = rootid;
	}

}
