package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjSubjectAction implements Serializable{
	
	private Integer id;
	private JskjSubject subject;
	private JskjUser user;
	private Integer atype;
	private Date oprtime;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public JskjSubject getSubject() {
		return subject;
	}
	public void setSubject(JskjSubject subject) {
		this.subject = subject;
	}
	public JskjUser getUser() {
		return user;
	}
	public void setUser(JskjUser user) {
		this.user = user;
	}
	public Integer getAtype() {
		return atype;
	}
	public void setAtype(Integer atype) {
		this.atype = atype;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}

}
