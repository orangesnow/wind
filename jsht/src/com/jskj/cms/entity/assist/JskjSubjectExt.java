package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjSubjectExt implements Serializable{
	
	private Integer id;
	private String remark;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
