package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjItemPicture implements Serializable{
	
	private Integer id;
	private Integer itemid;
	private Integer weight;
	private String picurl;
	private Integer ptype;
	private String outimageid;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getItemid() {
		return itemid;
	}
	public void setItemid(Integer itemid) {
		this.itemid = itemid;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
	public Integer getPtype() {
		return ptype;
	}
	public void setPtype(Integer ptype) {
		this.ptype = ptype;
	}
	public String getOutimageid() {
		return outimageid;
	}
	public void setOutimageid(String outimageid) {
		this.outimageid = outimageid;
	}

}
