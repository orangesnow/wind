/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * @author VAIO
 * @since 2014-08-12
 */
public class GhOrderInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String oid;
	
	private Integer source;
	private Integer ptype;
	private Integer nums;
	private double price;
	
	private Date odTime;
	
	private String recevName;
	
	private String recevMobile;
	private String recevAddress;
	private String goodsNum;
	
	private String goodsCompany;
	private Integer goodsStatus;
	private String sendName;
	
	private String buyerMessage;
	private String sellerMessage;
	
	private Integer payType;
	private String pcode;
	
	private Integer claimFlag;
	private Integer claimType;
	
	private WxFollowUser user;
	private int kdtUserId;
	
	private String couponInfo;
	private Integer feeType;
	private Integer ownerId;
	private Integer power;
	private Integer recevStatus;
	private Integer dutyStatus;
	private Double postFee;
	private Double showPrice;
	private Integer demoFlag;
	private Integer addressFlag;
	private String recevProv;
	private Double printPrice;
	private String channelCode;
	private Double couponFee;
	private Double promotionFee;
	private Double adjustFee;
	private Double itemFee;
	private Double yikouFee;
	private Date payTime;

	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	public Double getYikouFee() {
		return yikouFee;
	}

	public void setYikouFee(Double yikouFee) {
		this.yikouFee = yikouFee;
	}

	public Double getPrintPrice() {
		return printPrice;
	}

	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	public void setPrintPrice(Double printPrice) {
		this.printPrice = printPrice;
	}
	public String getRecevProv() {
		return recevProv;
	}
	public void setRecevProv(String recevProv) {
		this.recevProv = recevProv;
	}
	public Double getShowPrice() {
		return showPrice;
	}
	public void setShowPrice(Double showPrice) {
		this.showPrice = showPrice;
	}
	public Integer getDutyStatus() {
		return dutyStatus;
	}
	public void setDutyStatus(Integer dutyStatus) {
		this.dutyStatus = dutyStatus;
	}
	public Integer getRecevStatus() {
		return recevStatus;
	}
	public void setRecevStatus(Integer recevStatus) {
		this.recevStatus = recevStatus;
	}
	public Integer getPower() {
		return power;
	}
	public void setPower(Integer power) {
		this.power = power;
	}
	public Integer getFeeType() {
		return feeType;
	}
	public void setFeeType(Integer feeType) {
		this.feeType = feeType;
	}
	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public String getCouponInfo() {
		return couponInfo;
	}
	public void setCouponInfo(String couponInfo) {
		this.couponInfo = couponInfo;
	}
	public String getPcode() {
		return pcode;
	}
	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public Integer getSource() {
		return source;
	}
	public void setSource(Integer source) {
		this.source = source;
	}
	public Integer getPtype() {
		return ptype;
	}
	public void setPtype(Integer ptype) {
		this.ptype = ptype;
	}
	public Integer getNums() {
		return nums;
	}
	public void setNums(Integer nums) {
		this.nums = nums;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getOdTime() {
		return odTime;
	}
	public void setOdTime(Date orderTime) {
		this.odTime = orderTime;
	}
	
	public String getRecevName() {
		return recevName;
	}
	public void setRecevName(String recevName) {
		this.recevName = recevName;
	}
	public String getRecevMobile() {
		return recevMobile;
	}
	public void setRecevMobile(String recevMobile) {
		this.recevMobile = recevMobile;
	}
	public String getRecevAddress() {
		return recevAddress;
	}
	public void setRecevAddress(String recevAddress) {
		this.recevAddress = recevAddress;
	}
	public String getGoodsNum() {
		return goodsNum;
	}
	public void setGoodsNum(String goodsNum) {
		this.goodsNum = goodsNum;
	}
	public String getGoodsCompany() {
		return goodsCompany;
	}
	public void setGoodsCompany(String goodsCompany) {
		this.goodsCompany = goodsCompany;
	}
	public Integer getGoodsStatus() {
		return goodsStatus;
	}
	public void setGoodsStatus(Integer goodsStatus) {
		this.goodsStatus = goodsStatus;
	}
	public String getSendName() {
		return sendName;
	}
	public void setSendName(String sendName) {
		this.sendName = sendName;
	}
	public String getBuyerMessage() {
		return buyerMessage;
	}
	public void setBuyerMessage(String buyerMessage) {
		this.buyerMessage = buyerMessage;
	}
	public String getSellerMessage() {
		return sellerMessage;
	}
	public void setSellerMessage(String sellerMessage) {
		this.sellerMessage = sellerMessage;
	}
	public Integer getPayType() {
		return payType;
	}
	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	public Integer getClaimFlag() {
		return claimFlag;
	}
	public void setClaimFlag(Integer claimFlag) {
		this.claimFlag = claimFlag;
	}
	public Integer getClaimType() {
		return claimType;
	}
	public void setClaimType(Integer claimType) {
		this.claimType = claimType;
	}
	public WxFollowUser getUser() {
		return user;
	}
	public void setUser(WxFollowUser user) {
		this.user = user;
	}
	public int getKdtUserId() {
		return kdtUserId;
	}
	public void setKdtUserId(int kdtUserId) {
		this.kdtUserId = kdtUserId;
	}
	public Double getPostFee() {
		return postFee;
	}
	public void setPostFee(Double postFee) {
		this.postFee = postFee;
	}
	public Integer getDemoFlag() {
		return demoFlag;
	}
	public void setDemoFlag(Integer demoFlag) {
		this.demoFlag = demoFlag;
	}
	public Integer getAddressFlag() {
		return addressFlag;
	}
	public void setAddressFlag(Integer addressFlag) {
		this.addressFlag = addressFlag;
	}

	public Double getCouponFee() {
		return couponFee;
	}

	public void setCouponFee(Double couponFee) {
		this.couponFee = couponFee;
	}

	public Double getPromotionFee() {
		return promotionFee;
	}

	public void setPromotionFee(Double promotionFee) {
		this.promotionFee = promotionFee;
	}

	public Double getAdjustFee() {
		return adjustFee;
	}

	public void setAdjustFee(Double adjustFee) {
		this.adjustFee = adjustFee;
	}

	public Double getItemFee() {
		return itemFee;
	}

	public void setItemFee(Double itemFee) {
		this.itemFee = itemFee;
	}

}
