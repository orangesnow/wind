package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjShortMessage implements Serializable{
	
	private Integer id;
	private String content;
	private Date oprtime;
	private Integer status;
	private Date readtime;
	private Integer fromuser;
	private Integer touser;
	private Integer fromdel;
	private Integer todel;
	private Date lasttime;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getReadtime() {
		return readtime;
	}
	public void setReadtime(Date readtime) {
		this.readtime = readtime;
	}
	public Integer getFromuser() {
		return fromuser;
	}
	public void setFromuser(Integer fromuser) {
		this.fromuser = fromuser;
	}
	public Integer getTouser() {
		return touser;
	}
	public void setTouser(Integer touser) {
		this.touser = touser;
	}
	public Integer getFromdel() {
		return fromdel;
	}
	public void setFromdel(Integer fromdel) {
		this.fromdel = fromdel;
	}
	public Integer getTodel() {
		return todel;
	}
	public void setTodel(Integer todel) {
		this.todel = todel;
	}
	public Date getLasttime() {
		return lasttime;
	}
	public void setLasttime(Date lasttime) {
		this.lasttime = lasttime;
	}

}
