package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class JskjArticle implements Serializable{
	
	private Integer id;
	private String title;
	private String picurl;
	private Integer reads;
	private String outarticleid;
	private String created;
	private String url;
	private JskjArticleCategory category;
	private Integer flag;
	private Integer showType;

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public JskjArticleCategory getCategory() {
		return category;
	}
	public void setCategory(JskjArticleCategory category) {
		this.category = category;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
	public Integer getShowType() {
		return showType;
	}
	public void setShowType(Integer showType) {
		this.showType = showType;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
	public Integer getReads() {
		return reads;
	}
	public void setReads(Integer reads) {
		this.reads = reads;
	}
	public String getOutarticleid() {
		return outarticleid;
	}
	public void setOutarticleid(String outarticleid) {
		this.outarticleid = outarticleid;
	}

}
