package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class SfuDataContact implements Serializable{
	
	private Integer contactId;
	private Integer companyId;
	private String contactName;
	
	private String contactMobile;
	private String contactEmail;
	private String contactQq;
	
	private String contactWeixin;
	private String contactWeibo;
	private String contactTitle;
	
	private String contactType;
	private Date collectTime;
	private String collectUrl;
	
	private Integer iscom;
	private Integer bestFlag;
	private String remark;
	
	public Integer getContactId() {
		return contactId;
	}
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactMobile() {
		return contactMobile;
	}
	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getContactQq() {
		return contactQq;
	}
	public void setContactQq(String contactQq) {
		this.contactQq = contactQq;
	}
	public String getContactWeixin() {
		return contactWeixin;
	}
	public void setContactWeixin(String contactWeixin) {
		this.contactWeixin = contactWeixin;
	}
	public String getContactWeibo() {
		return contactWeibo;
	}
	public void setContactWeibo(String contactWeibo) {
		this.contactWeibo = contactWeibo;
	}
	public String getContactTitle() {
		return contactTitle;
	}
	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}
	public String getContactType() {
		return contactType;
	}
	public void setContactType(String contactType) {
		this.contactType = contactType;
	}
	public Date getCollectTime() {
		return collectTime;
	}
	public void setCollectTime(Date collectTime) {
		this.collectTime = collectTime;
	}
	public String getCollectUrl() {
		return collectUrl;
	}
	public void setCollectUrl(String collectUrl) {
		this.collectUrl = collectUrl;
	}
	public Integer getIscom() {
		return iscom;
	}
	public void setIscom(Integer iscom) {
		this.iscom = iscom;
	}
	public Integer getBestFlag() {
		return bestFlag;
	}
	public void setBestFlag(Integer bestFlag) {
		this.bestFlag = bestFlag;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
