
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信用户
 * @author VAIO
 * @since2014-09-09
 */
public class WxFollowRecord implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int msgid;
	private String toUsername;
	private String fromUsername;
	private String createtime;
	private String msgType;
	private String eventType;
	private WxPublicAccount account;
	
	private Date oprtime;
	
	public int getMsgid() {
		return msgid;
	}
	public void setMsgid(int msgid) {
		this.msgid = msgid;
	}
	public String getToUsername() {
		return toUsername;
	}
	public void setToUsername(String toUsername) {
		this.toUsername = toUsername;
	}
	public String getFromUsername() {
		return fromUsername;
	}
	public void setFromUsername(String fromUsername) {
		this.fromUsername = fromUsername;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	public WxPublicAccount getAccount() {
		return account;
	}
	public void setAccount(WxPublicAccount account) {
		this.account = account;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}

}
