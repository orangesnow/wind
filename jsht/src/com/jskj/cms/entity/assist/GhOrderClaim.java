/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单认领信息
 * @author VAIO
 * @since 2014-08-12
 */
public class GhOrderClaim implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	
	private GhOrderInfo ghOrder;

	private Integer claimFlag;
	private Integer claimType;
	
	private WxFollowUser user;

	private Integer power;
	private Integer recevStatus;
	private Date claimOprtime;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getClaimOprtime() {
		return claimOprtime;
	}
	public void setClaimOprtime(Date claimOprtime) {
		this.claimOprtime = claimOprtime;
	}
	public GhOrderInfo getGhOrder() {
		return ghOrder;
	}
	public void setGhOrder(GhOrderInfo ghOrder) {
		this.ghOrder = ghOrder;
	}
	public Integer getClaimFlag() {
		return claimFlag;
	}
	public void setClaimFlag(Integer claimFlag) {
		this.claimFlag = claimFlag;
	}
	public Integer getClaimType() {
		return claimType;
	}
	public void setClaimType(Integer claimType) {
		this.claimType = claimType;
	}
	public WxFollowUser getUser() {
		return user;
	}
	public void setUser(WxFollowUser user) {
		this.user = user;
	}
	public Integer getPower() {
		return power;
	}
	public void setPower(Integer power) {
		this.power = power;
	}
	public Integer getRecevStatus() {
		return recevStatus;
	}
	public void setRecevStatus(Integer recevStatus) {
		this.recevStatus = recevStatus;
	}

}
