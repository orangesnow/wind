
package com.jskj.cms.entity.assist;

public class WxLotteryProductStatus {
	
	private Integer id;
	private WxLotteryProduct product;
	private Integer nums;
	private Integer status;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the product
	 */
	public WxLotteryProduct getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(WxLotteryProduct product) {
		this.product = product;
	}
	/**
	 * @return the nums
	 */
	public Integer getNums() {
		return nums;
	}
	/**
	 * @param nums the nums to set
	 */
	public void setNums(Integer nums) {
		this.nums = nums;
	}
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
