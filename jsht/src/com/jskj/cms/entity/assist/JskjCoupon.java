package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjCoupon implements Serializable{
	
	private Integer id;
	private Integer couponType;
	private String  name;
	private double couponFee;
	private String couponInfo;
	private Date start;
	private Date end;
	private Integer status;
	private String remark;
	private int rate;
	private Integer itemid;
	private Integer isbind;
	private Integer nums;
	private Date oprtime;
	
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCouponType() {
		return couponType;
	}
	public void setCouponType(Integer couponType) {
		this.couponType = couponType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCouponFee() {
		return couponFee;
	}
	public void setCouponFee(double couponFee) {
		this.couponFee = couponFee;
	}
	public String getCouponInfo() {
		return couponInfo;
	}
	public void setCouponInfo(String couponInfo) {
		this.couponInfo = couponInfo;
	}
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	public Integer getItemid() {
		return itemid;
	}
	public void setItemid(Integer itemid) {
		this.itemid = itemid;
	}
	public Integer getIsbind() {
		return isbind;
	}
	public void setIsbind(Integer isbind) {
		this.isbind = isbind;
	}
	public Integer getNums() {
		return nums;
	}
	public void setNums(Integer nums) {
		this.nums = nums;
	}

}
