/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * 配置key管理
 * @author VAIO
 * @since 2014-08-17
 */
public class GhConfigSource implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String md5Key;
	private String name;
	private GhConfigCategory category;
	private String remark;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMd5Key() {
		return md5Key;
	}
	public void setMd5Key(String md5Key) {
		this.md5Key = md5Key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public GhConfigCategory getCategory() {
		return category;
	}
	public void setCategory(GhConfigCategory category) {
		this.category = category;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
