/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * @author VAIO
 * @since 2014-08-12
 */
public class GhProduct implements Serializable{

	private Integer pid;
	private String title;
	private String remark;
	private String originPrice;
	private String outerid;
	private String detailUrl;
	private Date created;
	private String picUrl;
	private String picThumbUrl;
	private int num;
	private int soldNum;
	private double price;
	private double postPrice;
	private String formats;
	private String nickname;
	private Integer status;
	
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOriginPrice() {
		return originPrice;
	}
	public void setOriginPrice(String originPrice) {
		this.originPrice = originPrice;
	}
	public String getOuterid() {
		return outerid;
	}
	public void setOuterid(String outerid) {
		this.outerid = outerid;
	}
	public String getDetailUrl() {
		return detailUrl;
	}
	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getPicThumbUrl() {
		return picThumbUrl;
	}
	public void setPicThumbUrl(String picThumbUrl) {
		this.picThumbUrl = picThumbUrl;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getSoldNum() {
		return soldNum;
	}
	public void setSoldNum(int soldNum) {
		this.soldNum = soldNum;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getFormats() {
		return formats;
	}
	public void setFormats(String formats) {
		this.formats = formats;
	}
	public double getPostPrice() {
		return postPrice;
	}
	public void setPostPrice(double postPrice) {
		this.postPrice = postPrice;
	}

}
