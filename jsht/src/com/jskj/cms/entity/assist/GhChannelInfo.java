/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * @author VAIO
 * @since 2014-08-12
 */
public class GhChannelInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String pcode;
	private String name;
	private int amount;
	private Integer status;

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getPcode() {
		return pcode;
	}
	public void setPcode(String pcode) {
		this.pcode = pcode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	
	
}
