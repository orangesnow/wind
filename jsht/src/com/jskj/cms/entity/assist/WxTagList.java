package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class WxTagList implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private int nums;
	private WxTagCategory category;
	
	public WxTagCategory getCategory() {
		return category;
	}
	public void setCategory(WxTagCategory category) {
		this.category = category;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNums() {
		return nums;
	}
	public void setNums(int nums) {
		this.nums = nums;
	}

}
