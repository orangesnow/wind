
package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

/**
 * 微信用户
 * @author VAIO
 * @since2014-09-09
 */
public class WxMenuRecord implements Serializable {

	private static final long serialVersionUID = 1L;
	private int msgid;
	private String toUsername;
	private String fromUsername;
	private String createtime;
	private String msgType;
	private String event;
	private String eventKey;
	private Date oprtime;
	private String remark;
	private WxFollowUser user;
	private WxPublicAccount account;

	public WxFollowUser getUser() {
		return user;
	}
	public void setUser(WxFollowUser user) {
		this.user = user;
	}
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	public int getMsgid() {
		return msgid;
	}
	public void setMsgid(int msgid) {
		this.msgid = msgid;
	}
	public String getToUsername() {
		return toUsername;
	}
	public void setToUsername(String toUsername) {
		this.toUsername = toUsername;
	}
	public String getFromUsername() {
		return fromUsername;
	}
	public void setFromUsername(String fromUsername) {
		this.fromUsername = fromUsername;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	
	public WxPublicAccount getAccount() {
		return account;
	}
	public void setAccount(WxPublicAccount account) {
		this.account = account;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
