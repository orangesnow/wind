package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class WxSyncResult implements Serializable{
	
	private int id;
	private Date oprtime;
	private WxSyncRule rule;
	private String result;
	private Integer status;
	private Date exetime;
	private String params;
	
	public Date getExetime() {
		return exetime;
	}
	public void setExetime(Date exetime) {
		this.exetime = exetime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getOprtime() {
		return oprtime;
	}
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}
	public WxSyncRule getRule() {
		return rule;
	}
	public void setRule(WxSyncRule rule) {
		this.rule = rule;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}

}
