
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * 微信菜单
 * @author VAIO
 * @since 2014-07-07
 */
public class WxMenuList  implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	private String code;
	private String name;
	private int status;
	private int level;
	private int weight;
	private WxMenuList parent;
	private int isDisplay;
	private String actionType;
	private int groupid;
	private String picUrl;
	private String url;
	private int serviceid;
	private Integer keyid;
	private String remark;
	private WxPublicAccount pub;
	
	public void init() {
		blankToNull();
	}

	public void blankToNull() {
//		if (StringUtils.isBlank(getTitleImg())) {
//			setTitleImg(null);
//		}
//		if (StringUtils.isBlank(getContentImg())) {
//			setContentImg(null);
//		}
//		if (StringUtils.isBlank(getShortName())) {
//			setShortName(null);
//		}
	}
	
	/**
	 * @return the pub
	 */
	public WxPublicAccount getPub() {
		return pub;
	}

	/**
	 * @param pub the pub to set
	 */
	public void setPub(WxPublicAccount pub) {
		this.pub = pub;
	}

	/**
	 * @return the keyid
	 */
	public Integer getKeyid() {
		return keyid;
	}

	/**
	 * @param keyid the keyid to set
	 */
	public void setKeyid(Integer keyid) {
		this.keyid = keyid;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}
	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	/**
	 * @return the parent
	 */
	public WxMenuList getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(WxMenuList parent) {
		this.parent = parent;
	}

	/**
	 * @return the isDispaly
	 */
	public int getIsDisplay() {
		return isDisplay;
	}
	/**
	 * @param isDispaly the isDispaly to set
	 */
	public void setIsDisplay(int isDisplay) {
		this.isDisplay = isDisplay;
	}
	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return actionType;
	}
	/**
	 * @param actionType the actionType to set
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	/**
	 * @return the groupid
	 */
	public int getGroupid() {
		return groupid;
	}
	/**
	 * @param groupid the groupid to set
	 */
	public void setGroupid(int groupid) {
		this.groupid = groupid;
	}
	/**
	 * @return the picUrl
	 */
	public String getPicUrl() {
		return picUrl;
	}
	/**
	 * @param picUrl the picUrl to set
	 */
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the serviceid
	 */
	public int getServiceid() {
		return serviceid;
	}
	/**
	 * @param serviceid the serviceid to set
	 */
	public void setServiceid(int serviceid) {
		this.serviceid = serviceid;
	}
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
