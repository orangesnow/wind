package com.jskj.cms.entity.assist;

import java.io.Serializable;
import java.util.Date;

public class JskjItemTag implements Serializable{
	
	private Integer id;

	private JskjItemList item;
	private JskjTagList tag;
	private Integer weight;
	private Integer hotflag;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public JskjItemList getItem() {
		return item;
	}
	public void setItem(JskjItemList item) {
		this.item = item;
	}
	public JskjTagList getTag() {
		return tag;
	}
	public void setTag(JskjTagList tag) {
		this.tag = tag;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public Integer getHotflag() {
		return hotflag;
	}
	public void setHotflag(Integer hotflag) {
		this.hotflag = hotflag;
	}

}
