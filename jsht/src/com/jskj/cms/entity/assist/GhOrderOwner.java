/**
 * 
 */
package com.jskj.cms.entity.assist;

import java.io.Serializable;

/**
 * @author VAIO
 * @since 2014-08-12
 * 订单回访用户规则表
 */
public class GhOrderOwner implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer rid;
	private String keyword;
	private int userid;
	private int status;
	private Integer pubType;

	public Integer getPubType() {
		return pubType;
	}
	public void setPubType(Integer pubType) {
		this.pubType = pubType;
	}
	public Integer getRid() {
		return rid;
	}
	public void setRid(Integer rid) {
		this.rid = rid;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
