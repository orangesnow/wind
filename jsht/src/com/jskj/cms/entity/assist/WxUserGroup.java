package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class WxUserGroup implements Serializable{
	
	private int groupid;
	private String name;
	private String remark;
	private WxPublicAccount account;
	
	public WxPublicAccount getAccount() {
		return account;
	}
	public void setAccount(WxPublicAccount account) {
		this.account = account;
	}
	public int getGroupid() {
		return groupid;
	}
	public void setGroupid(int groupid) {
		this.groupid = groupid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
