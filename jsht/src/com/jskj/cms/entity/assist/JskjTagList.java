package com.jskj.cms.entity.assist;

import java.io.Serializable;

public class JskjTagList implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String remark;
	private Integer hotlevel;
	private Integer status;
	private String picurl;
	private Integer recommlevel;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getHotlevel() {
		return hotlevel;
	}
	public void setHotlevel(Integer hotlevel) {
		this.hotlevel = hotlevel;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
	public Integer getRecommlevel() {
		return recommlevel;
	}
	public void setRecommlevel(Integer recommlevel) {
		this.recommlevel = recommlevel;
	}
	
	
}
