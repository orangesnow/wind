package com.jskj.cms.entity.main.base;

import java.io.Serializable;
import java.util.Date;

import com.jskj.cms.entity.main.CmsUserExt;


/**
 * This is an object that contains data related to the jc_user_ext table.
 * Do not modify this class because it will be overwritten if the configuration file
 * related to this class is modified.
 *
 * @hibernate.class
 *  table="jc_user_ext"
 */

public abstract class BaseCmsThirdAccount  implements Serializable {

	public static String REF = "BaseCmsThirdAccount";

	public static String PROP_USER = "user";
	public static String PRO_ID = "id";
	public static String PRO_THIRD_UID = "third_uid";
	public static String PRO_THIRD_TYPE = "third_type";
	public static String PRO_STATUS = "status";
	
	// constructors
	public BaseCmsThirdAccount () {
		initialize();
	}

	/**
	 * Constructor for primary key
	 */
	public BaseCmsThirdAccount (java.lang.Integer id) {
		this.setId(id);
		initialize();
	}

	protected void initialize () {}



	private int hashCode = Integer.MIN_VALUE;

	// primary key
	private java.lang.Integer id;

	// fields
	private String thirdUid;
	private Integer thirdType;
	private Integer status;
	private Date oprtime;
	
//	// one to one
//	private com.jiuxf.cms.entity.main.CmsUser user;

	/**
	 * @return the thirdUid
	 */
	public String getThirdUid() {
		return thirdUid;
	}

	/**
	 * @param thirdUid the thirdUid to set
	 */
	public void setThirdUid(String thirdUid) {
		this.thirdUid = thirdUid;
	}

	/**
	 * @return the thirdType
	 */
	public Integer getThirdType() {
		return thirdType;
	}

	/**
	 * @param thirdType the thirdType to set
	 */
	public void setThirdType(Integer thirdType) {
		this.thirdType = thirdType;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the oprtime
	 */
	public Date getOprtime() {
		return oprtime;
	}

	/**
	 * @param oprtime the oprtime to set
	 */
	public void setOprtime(Date oprtime) {
		this.oprtime = oprtime;
	}

	/**
	 * Return the unique identifier of this class
     * @hibernate.id
     *  generator-class="foreign"
     *  column="user_id"
     */
	public java.lang.Integer getId () {
		return id;
	}

	/**
	 * Set the unique identifier of this class
	 * @param id the new ID
	 */
	public void setId (java.lang.Integer id) {
		this.id = id;
		this.hashCode = Integer.MIN_VALUE;
	}






//	/**
//	 * Return the value associated with the column: user
//	 */
//	public com.jiuxf.cms.entity.main.CmsUser getUser () {
//		return user;
//	}
//
//	/**
//	 * Set the value related to the column: user
//	 * @param user the user value
//	 */
//	public void setUser (com.jiuxf.cms.entity.main.CmsUser user) {
//		this.user = user;
//	}
//
//

	public boolean equals (Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof CmsUserExt)) return false;
		else {
			CmsUserExt cmsUserExt = (CmsUserExt) obj;
			if (null == this.getId() || null == cmsUserExt.getId()) return false;
			else return (this.getId().equals(cmsUserExt.getId()));
		}
	}

	public int hashCode () {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}

	

	public String toString () {
		return super.toString();
	}


}