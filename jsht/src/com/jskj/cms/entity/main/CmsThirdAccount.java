package com.jskj.cms.entity.main;

import com.jskj.cms.entity.main.base.BaseCmsThirdAccount;

public class CmsThirdAccount extends BaseCmsThirdAccount {
	private static final long serialVersionUID = 1L;

	public void blankToNull() {
		// 将空串设置为null，便于前台处理。

		
	}

	/* [CONSTRUCTOR MARKER BEGIN] */
	public CmsThirdAccount () {
		super();
	}

	/**
	 * Constructor for primary key
	 */
	public CmsThirdAccount (java.lang.Integer id) {
		super(id);
	}

	/* [CONSTRUCTOR MARKER END] */

}