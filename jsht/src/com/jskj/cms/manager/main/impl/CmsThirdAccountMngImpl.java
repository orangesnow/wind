package com.jskj.cms.manager.main.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.main.CmsThirdAccountDao;
import com.jskj.cms.entity.main.CmsThirdAccount;
import com.jskj.cms.entity.main.CmsUser;
import com.jskj.cms.manager.main.CmsThirdAccountMng;

@Service
@Transactional
public class CmsThirdAccountMngImpl implements CmsThirdAccountMng {
	
	public CmsThirdAccount findById(Integer id) {
		return dao.findById(id);
	}
	
	public CmsThirdAccount save(CmsThirdAccount ext, CmsUser user) {
		ext.blankToNull();
		dao.save(ext);
		return ext;
	}

	public CmsThirdAccount save(CmsThirdAccount ext) {
		ext.blankToNull();
		dao.save(ext);
		return ext;
	}
	public CmsThirdAccount findByThirdUid(String uid) {
		return dao.findByThirdUid(uid);
	}
	
//	public CmsUserExt update(CmsUserExt ext, CmsUser user) {
//		CmsUserExt entity = dao.findById(user.getId());
//		if (entity == null) {
//			entity = save(ext, user);
//			user.getUserExtSet().add(entity);
//			return entity;
//		} else {
//			Updater<CmsUserExt> updater = new Updater<CmsUserExt>(ext);
//			updater.include("gender");
//			updater.include("birthday");
//			ext = dao.updateByUpdater(updater);
//			ext.blankToNull();
//			return ext;
//		}
//	}

	private CmsThirdAccountDao dao;

	@Autowired
	public void setDao(CmsThirdAccountDao dao) {
		this.dao = dao;
	}
}