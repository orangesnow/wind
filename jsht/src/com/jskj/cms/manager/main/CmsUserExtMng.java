package com.jskj.cms.manager.main;

import com.jskj.cms.entity.main.CmsUser;
import com.jskj.cms.entity.main.CmsUserExt;

public interface CmsUserExtMng {
	public CmsUserExt findById(Integer id);
	
	public CmsUserExt save(CmsUserExt ext, CmsUser user);

	public CmsUserExt update(CmsUserExt ext, CmsUser user);
}