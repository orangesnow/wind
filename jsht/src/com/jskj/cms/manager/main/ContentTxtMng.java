package com.jskj.cms.manager.main;

import com.jskj.cms.entity.main.Content;
import com.jskj.cms.entity.main.ContentTxt;

public interface ContentTxtMng {
	public ContentTxt save(ContentTxt txt, Content content);

	public ContentTxt update(ContentTxt txt, Content content);
}