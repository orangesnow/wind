package com.jskj.cms.manager.main;

import com.jskj.cms.entity.main.Channel;
import com.jskj.cms.entity.main.ChannelExt;

public interface ChannelExtMng {
	public ChannelExt save(ChannelExt ext, Channel channel);

	public ChannelExt update(ChannelExt ext);
}