package com.jskj.cms.manager.main;

import com.jskj.cms.entity.main.CmsThirdAccount;
import com.jskj.cms.entity.main.CmsUser;

public interface CmsThirdAccountMng {
	public CmsThirdAccount findById(Integer id);
	
	public CmsThirdAccount findByThirdUid(String uid) ;
	
	public CmsThirdAccount save(CmsThirdAccount ext, CmsUser user);

	public CmsThirdAccount save(CmsThirdAccount ext) ;
}