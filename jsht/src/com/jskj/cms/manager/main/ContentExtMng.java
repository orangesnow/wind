package com.jskj.cms.manager.main;

import com.jskj.cms.entity.main.Content;
import com.jskj.cms.entity.main.ContentExt;

public interface ContentExtMng {
	public ContentExt save(ContentExt ext, Content content);

	public ContentExt update(ContentExt ext);
}