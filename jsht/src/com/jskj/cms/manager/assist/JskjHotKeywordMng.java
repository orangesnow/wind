
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjHotKeyword;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjHotKeywordMng {
	
	public Pagination getPage(String name,Integer htype,int pageNo, int pageSize);
	
	public JskjHotKeyword findById(Integer id);

	public JskjHotKeyword save(JskjHotKeyword bean);

	public JskjHotKeyword update(JskjHotKeyword bean);

	public JskjHotKeyword deleteById(Integer id);
	
	public JskjHotKeyword[] deleteByIds(Integer[] ids);

}
