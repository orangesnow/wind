
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.GhOrderRelation;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhOrderRelationMng {

	
	public Pagination getPage(Integer userid,String staDate,String endDate, int pageNo,
			int pageSize);
	
	public Pagination getPage(Integer userid,Integer visitFlag,String staDate,String endDate, int pageNo,
			int pageSize);
	
	public GhOrderRelation findById(Integer id);

	public GhOrderRelation save(GhOrderRelation bean);

	public GhOrderRelation update(GhOrderRelation bean);

	public GhOrderRelation deleteById(Integer id);
	
	public GhOrderRelation[] deleteByIds(Integer[] ids);
	
}
