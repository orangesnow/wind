/**
 * 
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhIDsDao;
import com.jskj.cms.entity.assist.GhIDs;
import com.jskj.cms.manager.assist.GhIDsMng;
import com.jskj.common.hibernate3.Updater;

/**
 * @author VAIO
 * @since 2014-08-11
 */
@Service
@Transactional
class GhIDsMngImpl implements GhIDsMng {

	private GhIDsDao dao;
	
	@Autowired
	public void setDao(GhIDsDao dao) {
		this.dao = dao;
	}

	@Override
	public String findNextCardno() {
		String cardno = null;
		GhIDs ghIDs =  findNextCardno(1);
		cardno = ghIDs.getCardno();
		ghIDs.setStatus(0);
		update(ghIDs);
		return cardno;
	}

	@Override
	public GhIDs update(GhIDs bean) {
		Updater<GhIDs> updater = new Updater<GhIDs>(bean);
		GhIDs entity = dao.updateByUpdater(updater);
		return entity;
	}
	
	@Override
	public GhIDs findNextCardno(Integer status) {
		GhIDs ghIDs =  dao.findNextCardno(status);
		ghIDs.setStatus(0);
		GhIDs nbean = update(ghIDs);
		return nbean;
	}


}
