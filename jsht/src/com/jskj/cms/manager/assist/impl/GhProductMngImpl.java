
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhProductDao;
import com.jskj.cms.entity.assist.GhProduct;
import com.jskj.cms.entity.assist.WxShare;
import com.jskj.cms.manager.assist.GhProductMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2015-05-13
 */
@Service
@Transactional
public class GhProductMngImpl implements GhProductMng {

	@Autowired
	private GhProductDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(GhProductDao dao) {
		this.dao = dao;
	}

	@Override
	public GhProduct findByOuterId(String id) {
		return dao.findByOuterId(id);
	}

	@Override
	public Pagination getPage(String name,int pageNo, int pageSize) {
		return dao.getPage(name,pageNo, pageSize);
	}

	@Override
	public GhProduct findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public GhProduct save(GhProduct bean) {
		// TODO Auto-generated method stub
		return dao.save(bean);
	}

	@Override
	public GhProduct update(GhProduct product) {
		// TODO Auto-generated method stub
		Updater<GhProduct> updater = new Updater<GhProduct>(product);
		GhProduct entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhProduct deleteById(Integer id) {
		// TODO Auto-generated method stub
		return dao.deleteById(id);
	}

	
	@Override
	public GhProduct[] deleteByIds(Integer[] ids) {
		GhProduct[] beans = new GhProduct[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public List<GhProduct> getList(Integer status) {
		return dao.getList(status);
	}
	
}
