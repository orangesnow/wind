
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhGoodsInfoDao;
import com.jskj.cms.entity.assist.GhGoodsInfo;
import com.jskj.cms.manager.assist.GhGoodsInfoMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 物流用户信息代理类实现
 * @author VAIO
 * @since 2015-03-28
 */
@Service
@Transactional
public class GhGoodsInfoMngImpl implements GhGoodsInfoMng {

	@Autowired
	private GhGoodsInfoDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(GhGoodsInfoDao dao) {
		this.dao = dao;
	}

	@Override
	public GhGoodsInfo findById(Integer id) {
		if(id == null || id.equals("")) return null;
		return dao.findById(id);
	}

	@Override
	public GhGoodsInfo save(GhGoodsInfo bean) {
		return dao.save(bean);
	}

	@Override
	public GhGoodsInfo update(GhGoodsInfo bean) {
		Updater<GhGoodsInfo> updater = new Updater<GhGoodsInfo>(bean);
		GhGoodsInfo entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhGoodsInfo deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,String mobile,String staDate,String endDate,int pageNo, int pageSize) {
		return dao.getPage(name,mobile,staDate,endDate, pageNo, pageSize);
	}
	
	@Override
	public GhGoodsInfo[] deleteByIds(Integer[] ids) {
		GhGoodsInfo[] beans = new GhGoodsInfo[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


}
