
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjShortMessageDao;
import com.jskj.cms.entity.assist.JskjShortMessage;
import com.jskj.cms.manager.assist.JskjShortMessageMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjShortMessageMngImpl implements JskjShortMessageMng {

	@Autowired
	private JskjShortMessageDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjShortMessageDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjShortMessage findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjShortMessage save(JskjShortMessage bean) {
		return dao.save(bean);
	}

	@Override
	public JskjShortMessage update(JskjShortMessage bean) {
		Updater<JskjShortMessage> updater = new Updater<JskjShortMessage>(bean);
		JskjShortMessage entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjShortMessage deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,String staDate,String endDate, Integer status, int pageNo, int pageSize) {
		return dao.getPage(name,staDate, endDate,status,pageNo, pageSize);
	}
	
	@Override
	public JskjShortMessage[] deleteByIds(Integer[] ids) {
		JskjShortMessage[] beans = new JskjShortMessage[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
