/**
 * 
 */
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhBindLeaveDao;
import com.jskj.cms.entity.assist.GhBindLeave;
import com.jskj.cms.manager.assist.GhBindLeaveMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-11
 */
@Service
@Transactional
class GhBindLeaveMngImpl implements GhBindLeaveMng {

	private GhBindLeaveDao dao;
	
	@Autowired
	public void setDao(GhBindLeaveDao dao) {
		this.dao = dao;
	}

	@Override
	public GhBindLeave findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public GhBindLeave save(GhBindLeave bean) {
		return dao.save(bean);
	}

	@Override
	public GhBindLeave update(GhBindLeave bean) {
		Updater<GhBindLeave> updater = new Updater<GhBindLeave>(bean);
		GhBindLeave entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhBindLeave deleteById(Integer id) {
		return dao.deleteById(id);
	}

	@Override
	public GhBindLeave[] deleteByIds(Integer[] ids) {

		GhBindLeave[] beans = new GhBindLeave[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;

	}

	@Override
	public List<GhBindLeave> findByGhId(String id) {
		return dao.findByGhId(id);
	}
	
	@Override
	public Pagination getPage(String ghid,int pageNo, int pageSize) {
		return dao.getPage(ghid, pageNo, pageSize);
	}
	
}
