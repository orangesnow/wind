
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjCategoryDao;
import com.jskj.cms.entity.assist.JskjCategory;
import com.jskj.cms.manager.assist.JskjCategoryMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjCategoryMngImpl implements JskjCategoryMng {

	@Autowired
	private JskjCategoryDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjCategoryDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjCategory findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjCategory save(JskjCategory bean) {
		return dao.save(bean);
	}

	@Override
	public JskjCategory update(JskjCategory bean) {
		Updater<JskjCategory> updater = new Updater<JskjCategory>(bean);
		JskjCategory entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjCategory deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,Integer status, int pageNo,
			int pageSize) {
		return dao.getPage(name, status,pageNo, pageSize);
	}
	
	@Override
	public JskjCategory[] deleteByIds(Integer[] ids) {
		JskjCategory[] beans = new JskjCategory[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public List<JskjCategory> findCategorys(Integer status) {
		return dao.findCategorys(status);
	}

}
