
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhOrderClaimDao;
import com.jskj.cms.entity.assist.GhOrderClaim;
import com.jskj.cms.manager.assist.GhOrderClaimMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2015-05-13
 */
@Service
@Transactional
public class GhOrderClaimMngImpl implements GhOrderClaimMng {

	@Autowired
	private GhOrderClaimDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(GhOrderClaimDao dao) {
		this.dao = dao;
	}

	@Override
	public GhOrderClaim findById(int id) {
		if(id < 0) return null;
		return dao.findById(id);
	}

	@Override
	public GhOrderClaim save(GhOrderClaim bean) {
		return dao.save(bean);
	}

	@Override
	public GhOrderClaim update(GhOrderClaim bean) {
		Updater<GhOrderClaim> updater = new Updater<GhOrderClaim>(bean);
		GhOrderClaim entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhOrderClaim deleteById(int id) {
		return dao.deleteById(id);
	}
	
	
	@Override
	public GhOrderClaim[] deleteByIds(int[] ids) {
		GhOrderClaim[] beans = new GhOrderClaim[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public Pagination getPage(String orderId, Integer source,
			Integer claimFlag, Integer claimType, String staDate,
			String endDate, int pageNo, int pageSize) {
		return dao.getPage(orderId, source, claimFlag, claimType, staDate, endDate, pageNo, pageSize, true);
	}


}
