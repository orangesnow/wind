
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxQrcodeTypeDao;
import com.jskj.cms.entity.assist.WxQrcodeType;
import com.jskj.cms.manager.assist.WxQrcodeTypeMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxQrcodeTypeMngImpl implements WxQrcodeTypeMng {

	@Override
	public List<WxQrcodeType> getList() {
		return dao.getList();
	}

	@Autowired
	private WxQrcodeTypeDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(WxQrcodeTypeDao dao) {
		this.dao = dao;
	}

	@Override
	public WxQrcodeType findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public WxQrcodeType save(WxQrcodeType bean) {
		return dao.save(bean);
	}

	@Override
	public WxQrcodeType update(WxQrcodeType bean) {
		Updater<WxQrcodeType> updater = new Updater<WxQrcodeType>(bean);
		WxQrcodeType entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public WxQrcodeType deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(int pageNo,
			int pageSize) {
		return dao.getPage( pageNo, pageSize, false);
	}
	
	@Override
	public WxQrcodeType[] deleteByIds(Integer[] ids) {
		WxQrcodeType[] beans = new WxQrcodeType[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}
}
