
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhCouponInfoDao;
import com.jskj.cms.entity.assist.GhCouponInfo;
import com.jskj.cms.manager.assist.GhCouponInfoMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class GhCouponInfoMngImpl implements GhCouponInfoMng {

	@Autowired
	private GhCouponInfoDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(GhCouponInfoDao dao) {
		this.dao = dao;
	}

	@Override
	public GhCouponInfo findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public GhCouponInfo save(GhCouponInfo bean) {
		return dao.save(bean);
	}

	@Override
	public GhCouponInfo update(GhCouponInfo bean) {
		Updater<GhCouponInfo> updater = new Updater<GhCouponInfo>(bean);
		GhCouponInfo entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhCouponInfo deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public GhCouponInfo[] deleteByIds(Integer[] ids) {
		GhCouponInfo[] beans = new GhCouponInfo[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public Pagination getPage(String unionid,int pageNo,
			int pageSize) {
		return dao.getPage(unionid,pageNo, pageSize);
	}

	@Override
	public GhCouponInfo findByUnionId(String unionid) {
		return dao.findByUnionId(unionid);
	}
	
}
