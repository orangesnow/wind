
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjSubjectTotalDao;
import com.jskj.cms.entity.assist.JskjSubjectTotal;
import com.jskj.cms.manager.assist.JskjSubjectTotalMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjSubjectTotalMngImpl implements JskjSubjectTotalMng {

	@Autowired
	private JskjSubjectTotalDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjSubjectTotalDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjSubjectTotal findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjSubjectTotal save(JskjSubjectTotal bean) {
		return dao.save(bean);
	}

	@Override
	public JskjSubjectTotal update(JskjSubjectTotal bean) {
		Updater<JskjSubjectTotal> updater = new Updater<JskjSubjectTotal>(bean);
		JskjSubjectTotal entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjSubjectTotal deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(int pageNo, int pageSize) {
		return dao.getPage(pageNo, pageSize);
	}
	
	@Override
	public JskjSubjectTotal[] deleteByIds(Integer[] ids) {
		JskjSubjectTotal[] beans = new JskjSubjectTotal[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
