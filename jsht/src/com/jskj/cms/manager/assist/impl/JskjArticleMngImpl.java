
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjArticleDao;
import com.jskj.cms.entity.assist.JskjArticle;
import com.jskj.cms.manager.assist.JskjArticleMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjArticleMngImpl implements JskjArticleMng {

	@Autowired
	private JskjArticleDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjArticleDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjArticle findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjArticle save(JskjArticle bean) {
		return dao.save(bean);
	}

	@Override
	public JskjArticle update(JskjArticle bean) {
		Updater<JskjArticle> updater = new Updater<JskjArticle>(bean);
		JskjArticle entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjArticle deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,Integer status,int pageNo, int pageSize) {
		return dao.getPage(name, status, pageNo, pageSize);
	}
	
	@Override
	public JskjArticle[] deleteByIds(Integer[] ids) {
		JskjArticle[] beans = new JskjArticle[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
