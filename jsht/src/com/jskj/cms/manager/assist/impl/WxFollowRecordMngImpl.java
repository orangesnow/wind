package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxFollowRecordDao;
import com.jskj.cms.entity.assist.WxFollowRecord;
import com.jskj.cms.manager.assist.WxFollowRecordMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

@Service
@Transactional
public class WxFollowRecordMngImpl implements WxFollowRecordMng {

	@Transactional(readOnly = true)
	public WxFollowRecord findById(Integer id) {
		WxFollowRecord entity = dao.findById(id);
		return entity;
	}

	public WxFollowRecord save(WxFollowRecord bean) {
		dao.save(bean);
		return bean;
	}

	public WxFollowRecord update(WxFollowRecord bean) {
		Updater<WxFollowRecord> updater = new Updater<WxFollowRecord>(bean);
		WxFollowRecord entity = dao.updateByUpdater(updater);
		return entity;
	}

	public WxFollowRecord deleteById(Integer id) {
		WxFollowRecord bean = dao.deleteById(id);
		return bean;
	}
	
	
	@Override
	public WxFollowRecord[] deleteByIds(Integer[] ids) {
		WxFollowRecord[] beans = new WxFollowRecord[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


	private WxFollowRecordDao dao;

	@Autowired
	public void setDao(WxFollowRecordDao dao) {
		this.dao = dao;
	}

	@Override
	public Pagination getPage(String openid, Integer pubType,int pageNo,
			int pageSize) {
		if(openid == null) openid="";
		if(pubType == null) pubType=0;
		return dao.getPage(openid, pubType, pageNo, pageSize);
	}

}