/**
 * 
 */
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhTradeOrderDao;
import com.jskj.cms.entity.assist.GhTradeOrder;
import com.jskj.cms.manager.assist.GhTradeOrderMng;
import com.jskj.common.hibernate3.Updater;

/**
 * @author VAIO
 * @since 2014-08-11
 */
@Service
@Transactional
class GhTradeOrderMngImpl implements GhTradeOrderMng {

	private GhTradeOrderDao dao;
	
	@Autowired
	public void setDao(GhTradeOrderDao dao) {
		this.dao = dao;
	}

	@Override
	public GhTradeOrder findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public GhTradeOrder save(GhTradeOrder bean) {
		return dao.save(bean);
	}

	@Override
	public GhTradeOrder update(GhTradeOrder bean) {
		Updater<GhTradeOrder> updater = new Updater<GhTradeOrder>(bean);
		GhTradeOrder entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhTradeOrder deleteById(Integer id) {
		return dao.deleteById(id);
	}

	@Override
	public GhTradeOrder[] deleteByIds(Integer[] ids) {

		GhTradeOrder[] beans = new GhTradeOrder[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;

	}

	@Override
	public List<GhTradeOrder> findByGhId(String id) {
		return dao.findByGhId(id);
	}

}
