
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjSubjectDao;
import com.jskj.cms.entity.assist.JskjSubject;
import com.jskj.cms.manager.assist.JskjSubjectMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjSubjectMngImpl implements JskjSubjectMng {

	@Autowired
	private JskjSubjectDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjSubjectDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjSubject findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjSubject save(JskjSubject bean) {
		return dao.save(bean);
	}

	@Override
	public JskjSubject update(JskjSubject bean) {
		Updater<JskjSubject> updater = new Updater<JskjSubject>(bean);
		JskjSubject entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjSubject deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String staDate,String endDate,Integer groupid,Integer rtype,Integer status,int pageNo, int pageSize){
		return dao.getPage(staDate,endDate,groupid,rtype, status,pageNo, pageSize);
	}
	
	@Override
	public JskjSubject[] deleteByIds(Integer[] ids) {
		JskjSubject[] beans = new JskjSubject[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
