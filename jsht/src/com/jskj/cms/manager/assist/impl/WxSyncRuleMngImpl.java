
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxSyncRuleDao;
import com.jskj.cms.entity.assist.WxSyncRule;
import com.jskj.cms.manager.assist.WxSyncRuleMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxSyncRuleMngImpl implements WxSyncRuleMng {

	@Autowired
	private WxSyncRuleDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(WxSyncRuleDao dao) {
		this.dao = dao;
	}

	@Override
	public WxSyncRule findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public WxSyncRule save(WxSyncRule bean) {
		return dao.save(bean);
	}

	@Override
	public WxSyncRule update(WxSyncRule bean) {
		Updater<WxSyncRule> updater = new Updater<WxSyncRule>(bean);
		WxSyncRule entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public WxSyncRule deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(Integer stype, int pageNo,
			int pageSize) {
		return dao.getPage(stype, pageNo, pageSize);
	}
	
	@Override
	public WxSyncRule[] deleteByIds(Integer[] ids) {
		WxSyncRule[] beans = new WxSyncRule[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}
}
