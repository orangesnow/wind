
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjSubjectActionDao;
import com.jskj.cms.entity.assist.JskjSubjectAction;
import com.jskj.cms.manager.assist.JskjSubjectActionMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjSubjectActionMngImpl implements JskjSubjectActionMng {

	@Autowired
	private JskjSubjectActionDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjSubjectActionDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjSubjectAction findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjSubjectAction save(JskjSubjectAction bean) {
		return dao.save(bean);
	}

	@Override
	public JskjSubjectAction update(JskjSubjectAction bean) {
		Updater<JskjSubjectAction> updater = new Updater<JskjSubjectAction>(bean);
		JskjSubjectAction entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjSubjectAction deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String staDate,String endDate,Integer atype, int pageNo,
			int pageSize) {
		return dao.getPage(staDate,endDate, atype,pageNo, pageSize);
	}
	
	@Override
	public JskjSubjectAction[] deleteByIds(Integer[] ids) {
		JskjSubjectAction[] beans = new JskjSubjectAction[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
