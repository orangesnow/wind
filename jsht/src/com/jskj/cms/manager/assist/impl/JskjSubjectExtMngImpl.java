
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjSubjectExtDao;
import com.jskj.cms.entity.assist.JskjSubject;
import com.jskj.cms.entity.assist.JskjSubjectExt;
import com.jskj.cms.manager.assist.JskjSubjectExtMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjSubjectExtMngImpl implements JskjSubjectExtMng {

	@Autowired
	private JskjSubjectExtDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjSubjectExtDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjSubjectExt findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjSubjectExt save(JskjSubjectExt bean) {
		return dao.save(bean);
	}

	@Override
	public JskjSubjectExt update(JskjSubjectExt bean) {
		Updater<JskjSubjectExt> updater = new Updater<JskjSubjectExt>(bean);
		JskjSubjectExt entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjSubjectExt deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public JskjSubjectExt[] deleteByIds(Integer[] ids) {
		JskjSubjectExt[] beans = new JskjSubjectExt[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
