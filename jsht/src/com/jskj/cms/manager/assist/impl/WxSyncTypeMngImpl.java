
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxSyncTypeDao;
import com.jskj.cms.entity.assist.WxSyncType;
import com.jskj.cms.manager.assist.WxSyncTypeMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxSyncTypeMngImpl implements WxSyncTypeMng {

	@Autowired
	private WxSyncTypeDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(WxSyncTypeDao dao) {
		this.dao = dao;
	}

	@Override
	public WxSyncType findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public WxSyncType save(WxSyncType bean) {
		return dao.save(bean);
	}

	@Override
	public WxSyncType update(WxSyncType bean) {
		Updater<WxSyncType> updater = new Updater<WxSyncType>(bean);
		WxSyncType entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public WxSyncType deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(int pageNo,
			int pageSize) {
		return dao.getPage(pageNo, pageSize);
	}
	
	@Override
	public WxSyncType[] deleteByIds(Integer[] ids) {
		WxSyncType[] beans = new WxSyncType[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public List<WxSyncType> getList() {
		return dao.getList();
	}
}
