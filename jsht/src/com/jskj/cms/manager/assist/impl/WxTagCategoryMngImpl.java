
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxTagCategoryDao;
import com.jskj.cms.entity.assist.WxTagCategory;
import com.jskj.cms.manager.assist.WxTagCategoryMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxTagCategoryMngImpl implements WxTagCategoryMng {

	@Autowired
	private WxTagCategoryDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(WxTagCategoryDao dao) {
		this.dao = dao;
	}

	@Override
	public WxTagCategory findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public WxTagCategory save(WxTagCategory bean) {
		return dao.save(bean);
	}

	@Override
	public WxTagCategory update(WxTagCategory bean) {
		Updater<WxTagCategory> updater = new Updater<WxTagCategory>(bean);
		WxTagCategory entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public WxTagCategory deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(Integer rootId,Integer status,int pageNo,
			int pageSize) {
		return dao.getPage(rootId,status,pageNo, pageSize);
	}
	
	@Override
	public WxTagCategory[] deleteByIds(Integer[] ids) {
		WxTagCategory[] beans = new WxTagCategory[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public List<WxTagCategory> getList(Integer rootId,Integer status) {
		return dao.getList(rootId,status);
	}
	
}
