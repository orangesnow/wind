
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjItemCollectDao;
import com.jskj.cms.entity.assist.JskjItemCollect;
import com.jskj.cms.manager.assist.JskjItemCollectMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjItemCollectMngImpl implements JskjItemCollectMng {

	@Autowired
	private JskjItemCollectDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjItemCollectDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjItemCollect findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjItemCollect save(JskjItemCollect bean) {
		return dao.save(bean);
	}

	@Override
	public JskjItemCollect update(JskjItemCollect bean) {
		Updater<JskjItemCollect> updater = new Updater<JskjItemCollect>(bean);
		JskjItemCollect entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjItemCollect deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String staDate, String endDate, Integer collecttype, int pageNo, int pageSize) {
		return dao.getPage(staDate, endDate,collecttype,pageNo, pageSize);
	}
	
	@Override
	public JskjItemCollect[] deleteByIds(Integer[] ids) {
		JskjItemCollect[] beans = new JskjItemCollect[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
