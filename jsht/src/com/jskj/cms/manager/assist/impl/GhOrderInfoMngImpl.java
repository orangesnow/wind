
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhOrderInfoDao;
import com.jskj.cms.entity.assist.GhOrderInfo;
import com.jskj.cms.manager.assist.GhOrderInfoMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2015-05-13
 */
@Service
@Transactional
public class GhOrderInfoMngImpl implements GhOrderInfoMng {

	@Autowired
	private GhOrderInfoDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(GhOrderInfoDao dao) {
		this.dao = dao;
	}

	@Override
	public GhOrderInfo findById(String id) {
		if(id == null || id.equals("")) return null;
		return dao.findById(id);
	}

	@Override
	public GhOrderInfo save(GhOrderInfo bean) {
		return dao.save(bean);
	}

	@Override
	public GhOrderInfo update(GhOrderInfo bean) {
		Updater<GhOrderInfo> updater = new Updater<GhOrderInfo>(bean);
		GhOrderInfo entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhOrderInfo deleteById(String id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String orderId,Integer source,String channelId,String staDate,String endDate, int pageNo,
			int pageSize) {
		return dao.getPage(orderId,source,channelId,staDate,endDate, pageNo, pageSize, false);
	}
	
	@Override
	public GhOrderInfo[] deleteByIds(String[] ids) {
		GhOrderInfo[] beans = new GhOrderInfo[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public Pagination getPage(String orderId, Integer source,
			Integer claimFlag, Integer claimType, String staDate,
			String endDate, int pageNo, int pageSize) {
		return dao.getPage(orderId, source, claimFlag, claimType, staDate, endDate, pageNo, pageSize, true);
	}

	@Override
	public Pagination getPage(Integer userId, String staDate, String endDate,
			int pageNo, int pageSize) {
		return dao.getPage(userId, staDate, endDate, pageNo, pageSize);
	}
	
}
