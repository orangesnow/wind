
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxSyncResultDao;
import com.jskj.cms.entity.assist.WxSyncResult;
import com.jskj.cms.manager.assist.WxSyncResultMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxSyncResultMngImpl implements WxSyncResultMng {

	
	@Autowired
	private WxSyncResultDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(WxSyncResultDao dao) {
		this.dao = dao;
	}

	@Override
	public WxSyncResult findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public WxSyncResult save(WxSyncResult bean) {
		return dao.save(bean);
	}

	@Override
	public WxSyncResult update(WxSyncResult bean) {
		Updater<WxSyncResult> updater = new Updater<WxSyncResult>(bean);
		WxSyncResult entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public WxSyncResult deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(Integer stype, int pageNo,
			int pageSize) {
		return dao.getPage(stype, pageNo, pageSize);
	}
	
	@Override
	public WxSyncResult[] deleteByIds(Integer[] ids) {
		WxSyncResult[] beans = new WxSyncResult[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}
}
