
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjTagListDao;
import com.jskj.cms.entity.assist.JskjTagList;
import com.jskj.cms.manager.assist.JskjTagListMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjTagListMngImpl implements JskjTagListMng {

	@Autowired
	private JskjTagListDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjTagListDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjTagList findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjTagList save(JskjTagList bean) {
		return dao.save(bean);
	}

	@Override
	public JskjTagList update(JskjTagList bean) {
		Updater<JskjTagList> updater = new Updater<JskjTagList>(bean);
		JskjTagList entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjTagList deleteById(Integer id) {
		
		dao.deleteItemRef(id);
		return dao.deleteById(id);
	}
	

	@Override
	public JskjTagList[] deleteByIds(Integer[] ids) {
		JskjTagList[] beans = new JskjTagList[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public Pagination getPage(String name,Integer  orderBy, int pageNo, int pageSize) {
		return dao.getPage(name, orderBy, pageNo, pageSize);
	}

	@Override
	public List<JskjTagList> getList(Integer status) {
		return dao.getList(status);
	}

	@Override
	public int deleteItemRef(Integer tagId) {
		return dao.deleteItemRef(tagId);
	}

	@Override
	public int deleteTagRefFromItemId(Integer itemid) {
		return dao.deleteTagRefFromItemId(itemid);
	}

}
