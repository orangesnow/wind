/**
 * 
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhBindLeaveDao;
import com.jskj.cms.dao.assist.GhLeaveDao;
import com.jskj.cms.entity.assist.GhBindLeave;
import com.jskj.cms.entity.assist.GhLeave;
import com.jskj.cms.manager.assist.GhLeaveMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-11
 */
@Service
@Transactional
class GhLeaveMngImpl implements GhLeaveMng {

	private GhLeaveDao dao;
	private GhBindLeaveDao bindDao;
	
	@Autowired
	public void setBindDao(GhBindLeaveDao bindDao) {
		this.bindDao = bindDao;
	}

	@Autowired
	public void setDao(GhLeaveDao dao) {
		this.dao = dao;
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.GhLeaveMng#getPage(java.lang.String, int, int)
	 */
	@Override
	public Pagination getPage(String userid, int pageNo, int pageSize) {
		return dao.getPage(userid, pageNo, pageSize, false);
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.GhLeaveMng#findById(java.lang.Integer)
	 */
	@Override
	public GhLeave findById(Integer id) {
		return dao.findById(id);
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.GhLeaveMng#save(com.jskj.cms.entity.assist.GhOrderInfo)
	 */
	@Override
	public GhLeave save(GhLeave bean) {
		return dao.save(bean);
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.GhLeaveMng#update(com.jskj.cms.entity.assist.GhOrderInfo)
	 */
	@Override
	public GhLeave update(GhLeave bean) {
		Updater<GhLeave> updater = new Updater<GhLeave>(bean);
		GhLeave entity = dao.updateByUpdater(updater);
		return entity;
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.GhLeaveMng#deleteById(java.lang.Integer)
	 */
	@Override
	public GhLeave deleteById(Integer id) {
		
		GhBindLeave bindLeave = bindDao.findByLeaveId(id);
		if(bindLeave != null){
			bindDao.deleteById(bindLeave.getId());
		}
		
		return dao.deleteById(id);
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.GhLeaveMng#deleteByIds(java.lang.Integer[])
	 */
	@Override
	public GhLeave[] deleteByIds(Integer[] ids) {

		GhLeave[] beans = new GhLeave[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;

	}

}
