
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjCouponDao;
import com.jskj.cms.entity.assist.JskjCoupon;
import com.jskj.cms.manager.assist.JskjCouponMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjCouponMngImpl implements JskjCouponMng {

	@Autowired
	private JskjCouponDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjCouponDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjCoupon findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjCoupon save(JskjCoupon bean) {
		return dao.save(bean);
	}

	@Override
	public JskjCoupon update(JskjCoupon bean) {
		Updater<JskjCoupon> updater = new Updater<JskjCoupon>(bean);
		JskjCoupon entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjCoupon deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,String staDate, String endDate, Integer status, int pageNo, int pageSize) {
		return dao.getPage(name,staDate, endDate,status,pageNo, pageSize);
	}
	
	@Override
	public JskjCoupon[] deleteByIds(Integer[] ids) {
		JskjCoupon[] beans = new JskjCoupon[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
