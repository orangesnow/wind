
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.SfuDataContactDao;
import com.jskj.cms.entity.assist.SfuDataContact;
import com.jskj.cms.manager.assist.SfuDataContactMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

@Service
@Transactional
public class SfuDataContactMngImpl implements SfuDataContactMng {

	@Override
	public SfuDataContact findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public SfuDataContact save(SfuDataContact bean) {
		return dao.save(bean);
	}

	@Override
	public SfuDataContact deleteById(Integer id) {
		return dao.deleteById(id);
	}

	private SfuDataContactDao dao;
	
	@Autowired
	public void setDao(SfuDataContactDao dao) {
		this.dao = dao;
	}

	@Override
	public SfuDataContact[] deleteByIds(Integer[] ids) {
		SfuDataContact[] beans = new SfuDataContact[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}
	
	@Override
	public SfuDataContact update(SfuDataContact bean) {
		Updater<SfuDataContact> updater = new Updater<SfuDataContact>(bean);
		SfuDataContact entity = dao.updateByUpdater(updater);
	
		return entity;
	}
	
	@Override
	public Pagination getPage(Integer companyId,int pageNo, int pageSize){
		return dao.getPage(companyId, pageNo, pageSize);
	}

	@Override
	public List<SfuDataContact> getList(Integer companyId) {
		return dao.getList(companyId);
	}

}
