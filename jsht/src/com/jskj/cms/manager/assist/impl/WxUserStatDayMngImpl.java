
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxUserStatDayDao;
import com.jskj.cms.manager.assist.WxUserStatDayMng;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxUserStatDayMngImpl implements WxUserStatDayMng {

	@Autowired
	private WxUserStatDayDao dao;

	@Override
	public Pagination getPage(Integer accountId,String staDate,String endDate, int pageNo,
			int pageSize) {
		
		return dao.getPage(accountId, staDate,endDate, pageNo, pageSize);
		
	}
	
}
