
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxShareDao;
import com.jskj.cms.entity.assist.WxShare;
import com.jskj.cms.manager.assist.WxShareMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxShareMngImpl implements WxShareMng {

	@Autowired
	private WxShareDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(WxShareDao dao) {
		this.dao = dao;
	}

	@Override
	public WxShare findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public WxShare save(WxShare bean) {
		return dao.save(bean);
	}

	@Override
	public WxShare update(WxShare bean) {
		Updater<WxShare> updater = new Updater<WxShare>(bean);
		WxShare entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public WxShare deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public WxShare[] deleteByIds(Integer[] ids) {
		WxShare[] beans = new WxShare[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}
	
	
	@Override
	public Pagination getPage(int pageNo,
			int pageSize) {
		return dao.getPage(pageNo, pageSize);
	}
	
}
