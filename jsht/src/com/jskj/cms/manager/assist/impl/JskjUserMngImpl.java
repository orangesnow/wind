
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjUserDao;
import com.jskj.cms.entity.assist.JskjUser;
import com.jskj.cms.manager.assist.JskjUserMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjUserMngImpl implements JskjUserMng {

	@Autowired
	private JskjUserDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjUserDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjUser findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjUser save(JskjUser bean) {
		return dao.save(bean);
	}

	@Override
	public JskjUser update(JskjUser bean) {
		Updater<JskjUser> updater = new Updater<JskjUser>(bean);
		JskjUser entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjUser deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String staDate,String endDate,String nickname,int pageNo, int pageSize) {
		return dao.getPage(staDate, endDate,nickname,pageNo, pageSize);
	}
	
	@Override
	public JskjUser[] deleteByIds(Integer[] ids) {
		JskjUser[] beans = new JskjUser[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
