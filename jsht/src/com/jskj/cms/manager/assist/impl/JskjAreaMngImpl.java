
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjAreaDao;
import com.jskj.cms.entity.assist.JskjArea;
import com.jskj.cms.manager.assist.JskjAreaMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjAreaMngImpl implements JskjAreaMng {

	@Autowired
	private JskjAreaDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjAreaDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjArea findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjArea save(JskjArea bean) {
		return dao.save(bean);
	}

	@Override
	public JskjArea update(JskjArea bean) {
		Updater<JskjArea> updater = new Updater<JskjArea>(bean);
		JskjArea entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjArea deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,Integer orderBy,Integer status,int pageNo, int pageSize) {
		return dao.getPage(name, orderBy,status, pageNo, pageSize);
	}
	
	@Override
	public JskjArea[] deleteByIds(Integer[] ids) {
		JskjArea[] beans = new JskjArea[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
