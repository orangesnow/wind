/**
 * 
 */
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhConfigSourceDao;
import com.jskj.cms.entity.assist.GhConfigSource;
import com.jskj.cms.manager.assist.GhConfigSourceMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 
 * @author VAIO
 * @since 2014-08-11
 */
@Service
@Transactional
class GhConfigSourceMngImpl implements GhConfigSourceMng {

	private GhConfigSourceDao dao;
	
	@Autowired
	public void setDao(GhConfigSourceDao dao) {
		this.dao = dao;
	}

	@Override
	public Pagination getPage(String name,Integer category, int pageNo, int pageSize) {
		return dao.getPage(name,category, pageNo, pageSize, false);
	}

	@Override
	public GhConfigSource findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public GhConfigSource save(GhConfigSource bean) {
		return dao.save(bean);
	}

	@Override
	public GhConfigSource update(GhConfigSource bean) {
		Updater<GhConfigSource> updater = new Updater<GhConfigSource>(bean);
		GhConfigSource entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhConfigSource deleteById(Integer id) {
		return dao.deleteById(id);
	}

	@Override
	public GhConfigSource[] deleteByIds(Integer[] ids) {

		GhConfigSource[] beans = new GhConfigSource[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;

	}

	@Override
	public List<GhConfigSource> findByCategory(Integer categoryId) {
		return dao.findByCategory(categoryId);
	}

}
