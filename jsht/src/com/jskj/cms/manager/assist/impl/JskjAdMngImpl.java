
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjAdDao;
import com.jskj.cms.entity.assist.JskjAd;
import com.jskj.cms.manager.assist.JskjAdMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjAdMngImpl implements JskjAdMng {

	@Autowired
	private JskjAdDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjAdDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjAd findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjAd save(JskjAd bean) {
		return dao.save(bean);
	}

	@Override
	public JskjAd update(JskjAd bean) {
		Updater<JskjAd> updater = new Updater<JskjAd>(bean);
		JskjAd entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjAd deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(Integer adtype,Integer status, int pageNo,
			int pageSize) {
		return dao.getPage(adtype, status,pageNo, pageSize);
	}
	
	@Override
	public JskjAd[] deleteByIds(Integer[] ids) {
		JskjAd[] beans = new JskjAd[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
