package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxPublicAccountDao;
import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.common.hibernate3.Updater;

@Service
@Transactional
public class WxPublicAccountMngImpl implements WxPublicAccountMng {

	@Transactional(readOnly = true)
	public WxPublicAccount findById(Integer id) {
		WxPublicAccount entity = dao.findById(id);
		return entity;
	}

	public WxPublicAccount save(WxPublicAccount bean) {
		dao.save(bean);
		return bean;
	}

	public WxPublicAccount update(WxPublicAccount bean) {
		Updater<WxPublicAccount> updater = new Updater<WxPublicAccount>(bean);
		WxPublicAccount entity = dao.updateByUpdater(updater);
		return entity;
	}

	public WxPublicAccount deleteById(Integer id) {
		WxPublicAccount bean = dao.deleteById(id);
		return bean;
	}

	private WxPublicAccountDao dao;

	@Autowired
	public void setDao(WxPublicAccountDao dao) {
		this.dao = dao;
	}

	@Override
	public List<WxPublicAccount> getList(Integer status) {
		return dao.getList(status);
	}

}