
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjTradeTraceDao;
import com.jskj.cms.manager.assist.JskjTradeTraceMng;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2015-05-13
 */
@Service
@Transactional
public class JskjTradeTraceMngImpl implements JskjTradeTraceMng {

	@Autowired
	private JskjTradeTraceDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjTradeTraceDao dao) {
		this.dao = dao;
	}

	@Override
	public Pagination getPage(String tradeid, String sid, int pageNo,
			int pageSize) {
		return dao.getPage(tradeid, sid, pageNo, pageSize);
	}

	
	
}
