
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjSubjectPictureDao;
import com.jskj.cms.entity.assist.JskjSubjectPicture;
import com.jskj.cms.manager.assist.JskjSubjectPictureMng;
import com.jskj.common.hibernate3.Updater;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjSubjectPictureMngImpl implements JskjSubjectPictureMng {

	@Autowired
	private JskjSubjectPictureDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjSubjectPictureDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjSubjectPicture findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjSubjectPicture save(JskjSubjectPicture bean) {
		return dao.save(bean);
	}

	@Override
	public JskjSubjectPicture update(JskjSubjectPicture bean) {
		Updater<JskjSubjectPicture> updater = new Updater<JskjSubjectPicture>(bean);
		JskjSubjectPicture entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjSubjectPicture deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public JskjSubjectPicture[] deleteByIds(Integer[] ids) {
		JskjSubjectPicture[] beans = new JskjSubjectPicture[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	public List<JskjSubjectPicture> getList(Integer subjectid) {
		return dao.getList(subjectid);
	}
	
}
