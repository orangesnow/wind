
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.SfuDataCompanyDao;
import com.jskj.cms.entity.assist.SfuDataCompany;
import com.jskj.cms.manager.assist.SfuDataCompanyMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

@Service
@Transactional
public class SfuDataCompanyMngImpl implements SfuDataCompanyMng {

	@Override
	public SfuDataCompany findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public SfuDataCompany save(SfuDataCompany bean) {
		return dao.save(bean);
	}

	@Override
	public SfuDataCompany deleteById(Integer id) {
		return dao.deleteById(id);
	}

	private SfuDataCompanyDao dao;
	
	@Autowired
	public void setDao(SfuDataCompanyDao dao) {
		this.dao = dao;
	}

	@Override
	public SfuDataCompany[] deleteByIds(Integer[] ids) {
		SfuDataCompany[] beans = new SfuDataCompany[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}
	
	@Override
	public SfuDataCompany update(SfuDataCompany bean) {
		Updater<SfuDataCompany> updater = new Updater<SfuDataCompany>(bean);
		SfuDataCompany entity = dao.updateByUpdater(updater);
	
		return entity;
	}
	
	@Override
	public Pagination getPage(String city,int pageNo, int pageSize){
		return dao.getPage(city, pageNo, pageSize);
	}
	
}
