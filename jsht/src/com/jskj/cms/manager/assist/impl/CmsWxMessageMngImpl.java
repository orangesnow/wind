
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.CmsWxMessageDao;
import com.jskj.cms.entity.assist.CmsWxMessage;
import com.jskj.cms.manager.assist.CmsWxMessageMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信消息管理接口实现
 * @author VAIO
 * @sinc e 2014-06-03
 */
@Service
@Transactional
public class CmsWxMessageMngImpl implements CmsWxMessageMng {

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.CmsWxMessageMng#getPage(java.lang.Integer, java.lang.String, int, int, boolean)
	 */
	@Override
	public Pagination getPage(Integer status, String openid,Integer pubType,Integer orderBy, int pageNo,
			int pageSize) {
		Pagination pagination = dao.getPage(status, openid, pubType, orderBy,pageNo, pageSize);
		return pagination;
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.CmsWxMessageMng#findById(java.lang.Integer)
	 */
	@Override
	public CmsWxMessage findById(String id) {
		CmsWxMessage message = dao.findById(id);
		return message;
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.CmsWxMessageMng#save(com.jskj.cms.entity.assist.CmsWxMessage)
	 */
	@Override
	public CmsWxMessage save(CmsWxMessage bean) {
		CmsWxMessage message = dao.save(bean);
		return message;
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.CmsWxMessageMng#updateByUpdater(com.jskj.common.hibernate3.Updater)
	 */
	@Override
	public CmsWxMessage updateByUpdater(Updater<CmsWxMessage> updater) {
		
		return null;
	}

	public CmsWxMessage update(CmsWxMessage bean) {
		Updater<CmsWxMessage> updater = new Updater<CmsWxMessage>(bean);
		CmsWxMessage entity = dao.updateByUpdater(updater);
	
		return entity;
	}
	
	
	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.CmsWxMessageMng#deleteById(java.lang.Integer)
	 */
	@Override
	public CmsWxMessage deleteById(String id) {
		CmsWxMessage message = dao.deleteById(id);
		return message;
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.manager.assist.CmsWxMessageMng#countByCond(java.lang.Integer, java.lang.String)
	 */
	@Override
	public int countByCond(Integer status, String openid) {
		Integer count = dao.countByCond(status, openid);
		return count;
	}

	
	
	private CmsWxMessageDao dao;

	/**
	 * @param dao the dao to set
	 */
	@Autowired
	public void setDao(CmsWxMessageDao dao) {
		this.dao = dao;
	}

	@Override
	public CmsWxMessage[] deleteByIds(String[] ids){
		CmsWxMessage[] beans = new CmsWxMessage[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}
	
	
}
