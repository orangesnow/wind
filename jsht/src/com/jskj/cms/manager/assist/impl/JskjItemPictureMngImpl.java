
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjItemPictureDao;
import com.jskj.cms.entity.assist.JskjItemPicture;
import com.jskj.cms.manager.assist.JskjItemPictureMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjItemPictureMngImpl implements JskjItemPictureMng {

	@Autowired
	private JskjItemPictureDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjItemPictureDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjItemPicture findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjItemPicture save(JskjItemPicture bean) {
		return dao.save(bean);
	}

	@Override
	public JskjItemPicture update(JskjItemPicture bean) {
		Updater<JskjItemPicture> updater = new Updater<JskjItemPicture>(bean);
		JskjItemPicture entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjItemPicture deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public JskjItemPicture[] deleteByIds(Integer[] ids) {
		JskjItemPicture[] beans = new JskjItemPicture[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public Pagination getPage(Integer itemid, Integer ptype, int pageNo,
			int pageSize) {
		return dao.getList(itemid, ptype, pageNo, pageSize);
	}
	
}
