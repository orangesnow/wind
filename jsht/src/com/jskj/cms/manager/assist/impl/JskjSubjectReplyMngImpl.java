
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjSubjectReplyDao;
import com.jskj.cms.entity.assist.JskjSubjectReply;
import com.jskj.cms.manager.assist.JskjSubjectReplyMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjSubjectReplyMngImpl implements JskjSubjectReplyMng {

	@Autowired
	private JskjSubjectReplyDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjSubjectReplyDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjSubjectReply findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjSubjectReply save(JskjSubjectReply bean) {
		return dao.save(bean);
	}

	@Override
	public JskjSubjectReply update(JskjSubjectReply bean) {
		Updater<JskjSubjectReply> updater = new Updater<JskjSubjectReply>(bean);
		JskjSubjectReply entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjSubjectReply deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(Integer subjectid,String staDate,String endDate,int pageNo, int pageSize){
		return dao.getPage(subjectid,staDate,endDate,pageNo, pageSize);
	}
	
	@Override
	public JskjSubjectReply[] deleteByIds(Integer[] ids) {
		JskjSubjectReply[] beans = new JskjSubjectReply[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
