
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxUserGroupDao;
import com.jskj.cms.entity.assist.WxUserGroup;
import com.jskj.cms.manager.assist.WxUserGroupMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxUserGroupMngImpl implements WxUserGroupMng {

	@Override
	public List<WxUserGroup> getList(Integer serviceId) {
		return dao.getList(serviceId);
	}

	@Autowired
	private WxUserGroupDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(WxUserGroupDao dao) {
		this.dao = dao;
	}

	@Override
	public WxUserGroup findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public WxUserGroup save(WxUserGroup bean) {
		return dao.save(bean);
	}

	@Override
	public WxUserGroup update(WxUserGroup bean) {
		Updater<WxUserGroup> updater = new Updater<WxUserGroup>(bean);
		WxUserGroup entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public WxUserGroup deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(Integer accountId, int pageNo,
			int pageSize) {
		return dao.getPage(accountId, pageNo, pageSize, false);
	}
	
	@Override
	public WxUserGroup[] deleteByIds(Integer[] ids) {
		WxUserGroup[] beans = new WxUserGroup[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}
}
