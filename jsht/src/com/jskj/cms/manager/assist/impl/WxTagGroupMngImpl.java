
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxTagGroupDao;
import com.jskj.cms.entity.assist.WxTagGroup;
import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.cms.manager.assist.WxTagGroupMng;
import com.jskj.cms.manager.assist.WxTagListMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxTagGroupMngImpl implements WxTagGroupMng {

	private WxTagListMng tagMng;
	
	@Autowired
	public void setTagMng(WxTagListMng tagMng) {
		this.tagMng = tagMng;
	}
	
	@Override
	public List<WxTagGroup> getList(Integer serviceId) {
		return dao.getList(serviceId);
	}

	@Autowired
	private WxTagGroupDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(WxTagGroupDao dao) {
		this.dao = dao;
	}

	@Override
	public WxTagGroup findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public WxTagGroup save(WxTagGroup bean) {
		return dao.save(bean);
	}

	@Override
	public WxTagGroup update(WxTagGroup bean) {
		Updater<WxTagGroup> updater = new Updater<WxTagGroup>(bean);
		WxTagGroup entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public WxTagGroup deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(Integer accountId, int pageNo,
			int pageSize) {
		return dao.getPage(accountId, pageNo, pageSize, false);
	}
	
	@Override
	public WxTagGroup[] deleteByIds(Integer[] ids) {
		WxTagGroup[] beans = new WxTagGroup[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public WxTagGroup save(WxTagGroup bean, String[] tagNames) {
		
		bean = dao.save(bean);
		
		if(tagNames != null && tagNames.length >0){
			List<WxTagList> tags = tagMng.save(tagNames);
			bean.setTags(tags);
		}
		
		return bean;
	}

	@Override
	public WxTagGroup update(WxTagGroup bean, String[] tagNames) {
		
		bean = update(bean);
		List<WxTagList> tags = bean.getTags();
		tags.clear();
		
		if(tagNames != null && tagNames.length >0){
			tags = tagMng.save(tagNames);
			bean.setTags(tags);
		}
		
		return bean;
	}
}
