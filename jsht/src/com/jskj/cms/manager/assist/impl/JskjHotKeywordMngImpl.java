
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjHotKeywordDao;
import com.jskj.cms.entity.assist.JskjHotKeyword;
import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.cms.manager.assist.JskjHotKeywordMng;
import com.jskj.cms.manager.assist.WxTagListMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjHotKeywordMngImpl implements JskjHotKeywordMng {


	@Autowired
	private JskjHotKeywordDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjHotKeywordDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjHotKeyword findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjHotKeyword save(JskjHotKeyword bean) {
		return dao.save(bean);
	}

	@Override
	public JskjHotKeyword update(JskjHotKeyword bean) {
		Updater<JskjHotKeyword> updater = new Updater<JskjHotKeyword>(bean);
		JskjHotKeyword entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjHotKeyword deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,Integer htype, int pageNo,
			int pageSize) {
		return dao.getPage(name,htype, pageNo, pageSize);
	}
	
	@Override
	public JskjHotKeyword[] deleteByIds(Integer[] ids) {
		JskjHotKeyword[] beans = new JskjHotKeyword[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}


}
