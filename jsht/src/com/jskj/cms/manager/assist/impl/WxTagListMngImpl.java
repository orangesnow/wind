
package com.jskj.cms.manager.assist.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxTagCategoryDao;
import com.jskj.cms.dao.assist.WxTagListDao;
import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.cms.manager.assist.WxTagListMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class WxTagListMngImpl implements WxTagListMng {

	@Autowired
	private WxTagListDao dao;

	@Autowired
	private WxTagCategoryDao categoryDao;
		
	/**
	 * @param dao the dao to set
	 */
	public void setDao(WxTagListDao dao) {
		this.dao = dao;
	}

	@Override
	public WxTagList findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public WxTagList save(WxTagList bean) {
		return dao.save(bean);
	}

	@Override
	public WxTagList update(WxTagList bean) {
		Updater<WxTagList> updater = new Updater<WxTagList>(bean);
		WxTagList entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public WxTagList deleteById(Integer id) {
		dao.deleteGroupRef(id);
		dao.deleteUserRef(id);
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name, int pageNo,
			int pageSize) {
		return dao.getPage(name, pageNo, pageSize);
	}
	
	@Override
	public WxTagList[] deleteByIds(Integer[] ids) {
		WxTagList[] beans = new WxTagList[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public WxTagList findByName(String tagName) {
		return dao.findByName(tagName);
	}

	@Override
	public WxTagList findByName(String tagName,boolean single) {
		return dao.findByName(tagName,single);
	}
	
	@Override
	public List<WxTagList> save(String[] tagNames) {
		List<WxTagList> tags = new ArrayList<WxTagList>();
		for(int i=0;i<tagNames.length;i++){
			
			if(tagNames[i] != null && !tagNames[i].equals("")){
				WxTagList tagList = findByName(tagNames[i],true);
				if(tagList != null){
					tags.add(tagList);
				} else {
					WxTagList tag = new WxTagList();
					
					tag.setName(tagNames[i]);
					tag.setCategory(categoryDao.findById(1));
					
					tag = save(tag);
					tags.add(tag);
				}
			}
			
		}
		return tags;
	}

	@Override
	public List<WxTagList> getList() {
		return dao.getList();
	}

	@Override
	public Pagination getPage(String name, Integer cid, int pageNo, int pageSize) {
		return dao.getPage(name, cid, pageNo, pageSize);
	}

}
