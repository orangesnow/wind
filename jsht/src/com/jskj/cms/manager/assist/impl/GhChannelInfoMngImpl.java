
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhChannelInfoDao;
import com.jskj.cms.entity.assist.GhChannelInfo;
import com.jskj.cms.manager.assist.GhChannelInfoMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2015-05-13
 */
@Service
@Transactional
public class GhChannelInfoMngImpl implements GhChannelInfoMng {

	@Autowired
	private GhChannelInfoDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(GhChannelInfoDao dao) {
		this.dao = dao;
	}

	@Override
	public GhChannelInfo findById(Integer id) {
		if(id == null || id.equals("")) return null;
		return dao.findById(id);
	}

	@Override
	public GhChannelInfo save(GhChannelInfo bean) {
		return dao.save(bean);
	}

	@Override
	public GhChannelInfo update(GhChannelInfo bean) {
		Updater<GhChannelInfo> updater = new Updater<GhChannelInfo>(bean);
		GhChannelInfo entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhChannelInfo deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public List getList() {
		return dao.getList();
	}
	
	@Override
	public GhChannelInfo[] deleteByIds(Integer[] ids) {
		GhChannelInfo[] beans = new GhChannelInfo[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public Pagination getPage(String name, String pCode, int pageNo,
			int pageSize) {
		return dao.getPage(name, pCode, pageNo, pageSize);
	}
	
	
	
}
