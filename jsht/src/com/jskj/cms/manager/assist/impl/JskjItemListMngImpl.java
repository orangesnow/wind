
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjItemListDao;
import com.jskj.cms.entity.assist.JskjItemList;
import com.jskj.cms.manager.assist.JskjItemListMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjItemListMngImpl implements JskjItemListMng {

	@Autowired
	private JskjItemListDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjItemListDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjItemList findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjItemList save(JskjItemList bean) {
		return dao.save(bean);
	}

	@Override
	public JskjItemList update(JskjItemList bean) {
		Updater<JskjItemList> updater = new Updater<JskjItemList>(bean);
		JskjItemList entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjItemList deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,Integer status,Integer cid, Integer orderBy,int pageNo, int pageSize) {
		return dao.getPage(name,status,cid,orderBy,pageNo, pageSize);
	}
	
	@Override
	public JskjItemList[] deleteByIds(Integer[] ids) {
		JskjItemList[] beans = new JskjItemList[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
