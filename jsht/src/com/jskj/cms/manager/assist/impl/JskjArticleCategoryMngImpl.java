
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjArticleCategoryDao;
import com.jskj.cms.entity.assist.JskjArticleCategory;
import com.jskj.cms.manager.assist.JskjArticleCategoryMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjArticleCategoryMngImpl implements JskjArticleCategoryMng {

	@Autowired
	private JskjArticleCategoryDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjArticleCategoryDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjArticleCategory findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjArticleCategory save(JskjArticleCategory bean) {
		return dao.save(bean);
	}

	@Override
	public JskjArticleCategory update(JskjArticleCategory bean) {
		Updater<JskjArticleCategory> updater = new Updater<JskjArticleCategory>(bean);
		JskjArticleCategory entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjArticleCategory deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,Integer parentid,Integer status,int pageNo, int pageSize) {
		return dao.getPage(name, parentid,status, pageNo, pageSize);
	}
	
	@Override
	public JskjArticleCategory[] deleteByIds(Integer[] ids) {
		JskjArticleCategory[] beans = new JskjArticleCategory[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public List<JskjArticleCategory> getList(String name, Integer parentid,
			Integer status) {
		return dao.getList(name, parentid, status);
	}


}
