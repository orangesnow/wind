/**
 * 
 */
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhConfigCategoryDao;
import com.jskj.cms.entity.assist.GhConfigCategory;
import com.jskj.cms.manager.assist.GhConfigCategoryMng;
import com.jskj.common.hibernate3.Updater;

/**
 * 
 * @author VAIO
 * @since 2014-08-11
 */
@Service
@Transactional
class GhConfigCategoryMngImpl implements GhConfigCategoryMng {

	private GhConfigCategoryDao dao;
	
	@Autowired
	public void setDao(GhConfigCategoryDao dao) {
		this.dao = dao;
	}

	@Override
	public GhConfigCategory findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public GhConfigCategory save(GhConfigCategory bean) {
		return dao.save(bean);
	}

	@Override
	public GhConfigCategory update(GhConfigCategory bean) {
		Updater<GhConfigCategory> updater = new Updater<GhConfigCategory>(bean);
		GhConfigCategory entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhConfigCategory deleteById(Integer id) {
		return dao.deleteById(id);
	}

	@Override
	public GhConfigCategory[] deleteByIds(Integer[] ids) {

		GhConfigCategory[] beans = new GhConfigCategory[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;

	}

	@Override
	public List<GhConfigCategory> getList() {
		return dao.getList();
	}

}
