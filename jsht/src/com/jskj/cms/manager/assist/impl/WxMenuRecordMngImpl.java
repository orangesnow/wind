package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxMenuRecordDao;
import com.jskj.cms.entity.assist.WxMenuRecord;
import com.jskj.cms.manager.assist.WxMenuRecordMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

@Service
@Transactional
public class WxMenuRecordMngImpl implements WxMenuRecordMng {

	@Transactional(readOnly = true)
	public WxMenuRecord findById(Integer id) {
		WxMenuRecord entity = dao.findById(id);
		return entity;
	}

	public WxMenuRecord save(WxMenuRecord bean) {
		dao.save(bean);
		return bean;
	}

	public WxMenuRecord update(WxMenuRecord bean) {
		Updater<WxMenuRecord> updater = new Updater<WxMenuRecord>(bean);
		WxMenuRecord entity = dao.updateByUpdater(updater);
		return entity;
	}

	public WxMenuRecord deleteById(Integer id) {
		WxMenuRecord bean = dao.deleteById(id);
		return bean;
	}

	private WxMenuRecordDao dao;

	@Autowired
	public void setDao(WxMenuRecordDao dao) {
		this.dao = dao;
	}

	@Override
	public Pagination getPage(String fromUsername, Integer pubType, int pageNo,
			int pageSize) {
		
		return dao.getPage(fromUsername, pubType, pageNo, pageSize);
	}

	
}