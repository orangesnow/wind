
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhOrderOwnerDao;
import com.jskj.cms.entity.assist.GhOrderOwner;
import com.jskj.cms.manager.assist.GhOrderOwnerMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2015-05-13
 */
@Service
@Transactional
public class GhOrderOwnerMngImpl implements GhOrderOwnerMng {

	@Autowired
	private GhOrderOwnerDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(GhOrderOwnerDao dao) {
		this.dao = dao;
	}

	@Override
	public GhOrderOwner findById(Integer id) {
		if(id == null || id.equals("")) return null;
		return dao.findById(id);
	}

	@Override
	public GhOrderOwner save(GhOrderOwner bean) {
		return dao.save(bean);
	}

	@Override
	public GhOrderOwner update(GhOrderOwner bean) {
		Updater<GhOrderOwner> updater = new Updater<GhOrderOwner>(bean);
		GhOrderOwner entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhOrderOwner deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(Integer userid,String keyword,int pageNo,
			int pageSize) {
		return dao.getPage(userid,keyword, pageNo, pageSize);
	}
	
	@Override
	public GhOrderOwner[] deleteByIds(Integer[] ids) {
		GhOrderOwner[] beans = new GhOrderOwner[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	
}
