package com.jskj.cms.manager.assist.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxFollowUserDao;
import com.jskj.cms.entity.assist.GhCouponInfo;
import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.cms.manager.assist.GhCouponInfoMng;
import com.jskj.cms.manager.assist.WxFollowUserMng;
import com.jskj.cms.manager.assist.WxTagListMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

@Service
@Transactional
public class WxFollowUserMngImpl implements WxFollowUserMng {
	
	private WxTagListMng tagMng;
	private GhCouponInfoMng couponMng;
	
	@Autowired
	public void setCouponMng(GhCouponInfoMng couponMng) {
		this.couponMng = couponMng;
	}

	@Autowired
	public void setTagMng(WxTagListMng tagMng) {
		this.tagMng = tagMng;
	}

	@Transactional(readOnly = true)
	public WxFollowUser findById(Integer id) {
		WxFollowUser entity = dao.findById(id);
		return entity;
	}

	public WxFollowUser save(WxFollowUser bean) {
		dao.save(bean);
		return bean;
	}

	public WxFollowUser update(WxFollowUser bean) {
		Updater<WxFollowUser> updater = new Updater<WxFollowUser>(bean);
		WxFollowUser entity = dao.updateByUpdater(updater);
		return entity;
	}

	public WxFollowUser deleteById(Integer id) {
		WxFollowUser bean = dao.deleteById(id);
		return bean;
	}
	
	
	@Override
	public WxFollowUser[] deleteByIds(Integer[] ids) {
		WxFollowUser[] beans = new WxFollowUser[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public WxFollowUser[] signByIds(Integer[] ids) {
		WxFollowUser[] beans = new WxFollowUser[ids.length];
		
		for (int i = 0, len = ids.length; i < len; i++) {
			int tagId = 1;
			beans[i] = findById(ids[i]);
			if(beans[i].getIsSign() == null){
				beans[i].setIsSign(1);
			} else {
				tagId = (beans[i].getIsSign()+1)%2;
				beans[i].setIsSign(tagId);
			}
			
			//为1表示已经标记，0表示取消标记
			if(tagId == 1){
				GhCouponInfo couponInfo = new GhCouponInfo();
				
				couponInfo.setIsReceive(0);
				couponInfo.setCount(1);
				couponInfo.setCode("0");
				couponInfo.setMobile("0");
				couponInfo.setNums(1);
				couponInfo.setOprtime(new Date());
				couponInfo.setSource(1);
				couponInfo.setOpenid(beans[i].getOpenid());
				couponInfo.setUnionid(beans[i].getUnionid());
				
				couponMng.save(couponInfo);
			} else {
				String unionid = beans[i].getUnionid();
				GhCouponInfo coupon = couponMng.findByUnionId(unionid);
				if(coupon != null){
					couponMng.deleteById(coupon.getQid());
				}
			}
			
			update(beans[i]);
		}
		
		return beans;
	}
	
	@Override
	public WxFollowUser[] enterByIds(Integer[] ids) {
		WxFollowUser[] beans = new WxFollowUser[ids.length];
		
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = findById(ids[i]);
			if(beans[i].getIsClub() != null){
				beans[i].setIsClub((beans[i].getIsClub()+1)%2);
			} else {
				beans[i].setIsClub(1);
			}
			
			update(beans[i]);
		}
		
		return beans;
	}

	private WxFollowUserDao dao;

	
	@Autowired
	public void setDao(WxFollowUserDao dao) {
		this.dao = dao;
	}

	@Override
	public Pagination getPage(Integer status, Integer groupId,String staDate,String endDate, int pageNo,
			int pageSize) {
		return dao.getPage(status, groupId,staDate,endDate, pageNo, pageSize);
	}
	
	@Override
	public Pagination getPage(Integer status, Integer groupId,String staDate,String endDate,String province, int pageNo,
			int pageSize) {
		return dao.getPage(status, groupId,staDate,endDate,province, pageNo, pageSize);
	}
	
	@Override
	public Pagination getPage(Integer status, Integer groupId,Integer subType,String staDate,String endDate,String province, int pageNo,
			int pageSize) {
		return dao.getPage(status, groupId,subType,staDate,endDate,province, pageNo, pageSize);
	}
	
	@Override
	public Pagination getPage(Integer status, Integer groupId,Integer subType,String staDate,String endDate,String province,String nickname,String name,String openid, int pageNo,
			int pageSize){
		return dao.getPage(status, groupId,subType,staDate,endDate,province,nickname,name,openid, pageNo, pageSize);
	}

	@Override
	public WxFollowUser save(WxFollowUser bean, String[] tagNames) {
		dao.save(bean);
		
		if(tagNames != null && tagNames.length >0){
			List<WxTagList> tags = tagMng.save(tagNames);
			bean.setTags(tags);
		}
		
		return bean;
	}

	@Override
	public WxFollowUser update(WxFollowUser user, String[] tagNames) {
		user = update(user);
		List<WxTagList> tags = user.getTags();
		tags.clear();
		
		if(tagNames != null && tagNames.length >0){
			tags = tagMng.save(tagNames);
			user.setTags(tags);
		}
		
		return user;
	}

}