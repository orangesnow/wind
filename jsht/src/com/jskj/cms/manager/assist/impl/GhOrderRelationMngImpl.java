
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhOrderRelationDao;
import com.jskj.cms.entity.assist.GhOrderRelation;
import com.jskj.cms.manager.assist.GhOrderRelationMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2015-05-13
 */
@Service
@Transactional
public class GhOrderRelationMngImpl implements GhOrderRelationMng {

	@Autowired
	private GhOrderRelationDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(GhOrderRelationDao dao) {
		this.dao = dao;
	}

	@Override
	public GhOrderRelation findById(Integer id) {
		if(id == null || id.equals("")) return null;
		return dao.findById(id);
	}

	@Override
	public GhOrderRelation save(GhOrderRelation bean) {
		return dao.save(bean);
	}

	@Override
	public GhOrderRelation update(GhOrderRelation bean) {
		Updater<GhOrderRelation> updater = new Updater<GhOrderRelation>(bean);
		GhOrderRelation entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhOrderRelation deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	
	@Override
	public GhOrderRelation[] deleteByIds(Integer[] ids) {
		GhOrderRelation[] beans = new GhOrderRelation[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	
	@Override
	public Pagination getPage(Integer userId, String staDate, String endDate,
			int pageNo, int pageSize) {
		return dao.getPage(userId, staDate, endDate, pageNo, pageSize);
	}

	@Override
	public Pagination getPage(Integer userid, Integer visitFlag,
			String staDate, String endDate, int pageNo, int pageSize) {
		return dao.getPage(userid, visitFlag, staDate, endDate, pageNo, pageSize);
	}
	
}
