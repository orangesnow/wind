
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjArticleExtDao;
import com.jskj.cms.entity.assist.JskjArticleExt;
import com.jskj.cms.manager.assist.JskjArticleExtMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjArticleExtMngImpl implements JskjArticleExtMng {

	@Autowired
	private JskjArticleExtDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjArticleExtDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjArticleExt findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjArticleExt save(JskjArticleExt bean) {
		return dao.save(bean);
	}

	@Override
	public JskjArticleExt update(JskjArticleExt bean) {
		Updater<JskjArticleExt> updater = new Updater<JskjArticleExt>(bean);
		JskjArticleExt entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjArticleExt deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,Integer status,int pageNo, int pageSize) {
		return dao.getPage(name, status, pageNo, pageSize);
	}
	
	@Override
	public JskjArticleExt[] deleteByIds(Integer[] ids) {
		JskjArticleExt[] beans = new JskjArticleExt[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
