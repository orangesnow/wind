package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.CmsWxMenuDao;
import com.jskj.cms.entity.assist.WxMenuList;
import com.jskj.cms.manager.assist.CmsWxMenuMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

@Service
@Transactional
public class CmsWxMenuMngImpl implements CmsWxMenuMng {

	@Transactional(readOnly = true)
	public Pagination getPage(Integer isDisplay,  int pageNo,
			int pageSize,Integer pubType) {
		Pagination page = dao.getPage(isDisplay, pageNo, pageSize, true,pubType);
		return page;
	}

	@Transactional(readOnly = true)
	public WxMenuList findById(Integer id) {
		WxMenuList entity = dao.findById(id);
		return entity;
	}

	public WxMenuList save(WxMenuList bean) {
		dao.save(bean);
		return bean;
	}

	public WxMenuList update(WxMenuList bean) {
		Updater<WxMenuList> updater = new Updater<WxMenuList>(bean);
		WxMenuList entity = dao.updateByUpdater(updater);
		entity.blankToNull();
		return entity;
	}

	public WxMenuList deleteById(Integer id) {
		WxMenuList bean = dao.deleteById(id);
		return bean;
	}

	public WxMenuList[] deleteByIds(Integer[] ids) {
		WxMenuList[] beans = new WxMenuList[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	public WxMenuList[] updatePriority(Integer[] ids, Integer[] priority) {
		int len = ids.length;
		WxMenuList[] beans = new WxMenuList[len];
		for (int i = 0; i < len; i++) {
			beans[i] = findById(ids[i]);
			beans[i].setWeight(priority[i]);
		}
		return beans;
	}

	private CmsWxMenuDao dao;

	@Autowired
	public void setDao(CmsWxMenuDao dao) {
		this.dao = dao;
	}

	@Transactional(readOnly = true)
	public int countByIsDisplay(Integer isDisplay,Integer pubType) {
		return dao.countByIsDisplay(isDisplay,pubType);
	}

	@Transactional(readOnly = true)
	public List<WxMenuList> getList(int menuLevel,boolean flag,Integer pubType) {
		return dao.getList(menuLevel,flag,pubType);
	}

}