
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjItemTagDao;
import com.jskj.cms.entity.assist.JskjItemTag;
import com.jskj.cms.manager.assist.JskjItemTagMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjItemTagMngImpl implements JskjItemTagMng {

	@Autowired
	private JskjItemTagDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjItemTagDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjItemTag findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjItemTag save(JskjItemTag bean) {
		return dao.save(bean);
	}

	@Override
	public JskjItemTag update(JskjItemTag bean) {
		Updater<JskjItemTag> updater = new Updater<JskjItemTag>(bean);
		JskjItemTag entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjItemTag deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	
	@Override
	public JskjItemTag[] deleteByIds(Integer[] ids) {
		JskjItemTag[] beans = new JskjItemTag[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public List<JskjItemTag> getList(Integer itemid) {
		return dao.getList(itemid);
	}

}
