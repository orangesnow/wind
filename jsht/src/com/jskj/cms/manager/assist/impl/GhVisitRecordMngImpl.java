
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.GhVisitRecordDao;
import com.jskj.cms.entity.assist.GhVisitRecord;
import com.jskj.cms.manager.assist.GhVisitRecordMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2015-05-13
 */
@Service
@Transactional
public class GhVisitRecordMngImpl implements GhVisitRecordMng {

	@Autowired
	private GhVisitRecordDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(GhVisitRecordDao dao) {
		this.dao = dao;
	}

	@Override
	public GhVisitRecord findById(Integer id) {
		if(id == null || id.equals("")) return null;
		return dao.findById(id);
	}

	@Override
	public GhVisitRecord save(GhVisitRecord bean) {
		return dao.save(bean);
	}

	@Override
	public GhVisitRecord update(GhVisitRecord bean) {
		Updater<GhVisitRecord> updater = new Updater<GhVisitRecord>(bean);
		GhVisitRecord entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public GhVisitRecord deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	
	@Override
	public GhVisitRecord[] deleteByIds(Integer[] ids) {
		GhVisitRecord[] beans = new GhVisitRecord[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	
	@Override
	public Pagination getPage(Integer userId, String oid,
			int pageNo, int pageSize) {
		return dao.getPage(userId, oid, pageNo, pageSize);
	}

	@Override
	public Pagination getPage(Integer userId, String oid, String staDate,
			String endDate, int pageNo, int pageSize) {
		return dao.getPage(userId, oid, staDate, endDate, pageNo, pageSize);
	}
	
}
