
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjItemSkuDao;
import com.jskj.cms.entity.assist.JskjItemSku;
import com.jskj.cms.manager.assist.JskjItemSkuMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjItemSkuMngImpl implements JskjItemSkuMng {

	@Autowired
	private JskjItemSkuDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjItemSkuDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjItemSku findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjItemSku save(JskjItemSku bean) {
		return dao.save(bean);
	}

	@Override
	public JskjItemSku update(JskjItemSku bean) {
		Updater<JskjItemSku> updater = new Updater<JskjItemSku>(bean);
		JskjItemSku entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjItemSku deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(Integer itemid,int pageNo, int pageSize){
		return dao.getPage(itemid,pageNo, pageSize);
	}
	
	@Override
	public JskjItemSku[] deleteByIds(Integer[] ids) {
		JskjItemSku[] beans = new JskjItemSku[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public List<JskjItemSku> getList(Integer itemid) {
		return dao.getList(itemid);
	}

}
