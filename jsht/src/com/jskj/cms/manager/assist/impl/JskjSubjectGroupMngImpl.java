
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjSubjectGroupDao;
import com.jskj.cms.entity.assist.JskjSubjectGroup;
import com.jskj.cms.manager.assist.JskjSubjectGroupMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjSubjectGroupMngImpl implements JskjSubjectGroupMng {

	@Autowired
	private JskjSubjectGroupDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjSubjectGroupDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjSubjectGroup findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjSubjectGroup save(JskjSubjectGroup bean) {
		return dao.save(bean);
	}

	@Override
	public JskjSubjectGroup update(JskjSubjectGroup bean) {
		Updater<JskjSubjectGroup> updater = new Updater<JskjSubjectGroup>(bean);
		JskjSubjectGroup entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjSubjectGroup deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(String name,Integer status, int pageNo,
			int pageSize) {
		return dao.getPage(name, status,pageNo, pageSize);
	}
	
	@Override
	public JskjSubjectGroup[] deleteByIds(Integer[] ids) {
		JskjSubjectGroup[] beans = new JskjSubjectGroup[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

	@Override
	public List<JskjSubjectGroup> findAllGroups(Integer status) {
		return dao.findAllGroups(status);
	}

}
