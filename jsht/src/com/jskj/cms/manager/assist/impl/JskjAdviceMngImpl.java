
package com.jskj.cms.manager.assist.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.JskjAdviceDao;
import com.jskj.cms.entity.assist.JskjAdvice;
import com.jskj.cms.manager.assist.JskjAdviceMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 业务内容列表代理实现类
 * @author VAIO
 * @since 2014-05-13
 */
@Service
@Transactional
public class JskjAdviceMngImpl implements JskjAdviceMng {

	@Autowired
	private JskjAdviceDao dao;

	/**
	 * @param dao the dao to set
	 */
	public void setDao(JskjAdviceDao dao) {
		this.dao = dao;
	}

	@Override
	public JskjAdvice findById(Integer id) {
		if(id == null || id == -1) return null;
		return dao.findById(id);
	}

	@Override
	public JskjAdvice save(JskjAdvice bean) {
		return dao.save(bean);
	}

	@Override
	public JskjAdvice update(JskjAdvice bean) {
		Updater<JskjAdvice> updater = new Updater<JskjAdvice>(bean);
		JskjAdvice entity = dao.updateByUpdater(updater);
		return entity;
	}

	@Override
	public JskjAdvice deleteById(Integer id) {
		return dao.deleteById(id);
	}
	
	@Override
	public Pagination getPage(int pageNo,
			int pageSize) {
		return dao.getPage(pageNo, pageSize);
	}
	
	@Override
	public JskjAdvice[] deleteByIds(Integer[] ids) {
		JskjAdvice[] beans = new JskjAdvice[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}

}
