
package com.jskj.cms.manager.assist.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jskj.cms.dao.assist.WxAlertMessageDao;
import com.jskj.cms.entity.assist.WxAlertMessage;
import com.jskj.cms.manager.assist.WxAlertMessageMng;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

@Service
@Transactional
public class WxAlertMessageMngImpl implements WxAlertMessageMng {


	@Override
	public WxAlertMessage findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public WxAlertMessage updateByUpdater(Updater<WxAlertMessage> updater) {
		return dao.updateByUpdater(updater);
	}

	@Override
	public WxAlertMessage save(WxAlertMessage bean) {
		return dao.save(bean);
	}

	@Override
	public WxAlertMessage deleteById(Integer id) {
		return dao.deleteById(id);
	}

	@Override
	public List<WxAlertMessage> findByCtype(int ctype,int status) {
		return dao.findByCtype(ctype,status);
	}

	@Override
	public WxAlertMessage findByCKey(String ckey) {
		return dao.findByCKey(ckey);
	}

	private WxAlertMessageDao dao;
	
	@Autowired
	public void setDao(WxAlertMessageDao dao) {
		this.dao = dao;
	}

	@Override
	public WxAlertMessage[] deleteByIds(Integer[] ids) {
		WxAlertMessage[] beans = new WxAlertMessage[ids.length];
		for (int i = 0, len = ids.length; i < len; i++) {
			beans[i] = deleteById(ids[i]);
		}
		return beans;
	}
	
	@Override
	public WxAlertMessage update(WxAlertMessage bean) {
		Updater<WxAlertMessage> updater = new Updater<WxAlertMessage>(bean);
		WxAlertMessage entity = dao.updateByUpdater(updater);
	
		return entity;
	}
	
	@Override
	public Pagination getPage(String name,Integer categoryId,int pageNo, int pageSize){
		return dao.getPage(name, categoryId, pageNo, pageSize);
	}

	@Override
	public Pagination getPage(Integer sourceId, Integer ctype, Integer status,
			int pageNo, int pageSize) {
		return dao.getPage(sourceId, ctype, status, pageNo, pageSize);
	}
	
}
