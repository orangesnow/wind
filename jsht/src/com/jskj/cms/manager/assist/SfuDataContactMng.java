package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.SfuDataCompany;
import com.jskj.cms.entity.assist.SfuDataContact;
import com.jskj.common.page.Pagination;

public interface SfuDataContactMng {
	
	public SfuDataContact findById(Integer id);

	public SfuDataContact save(SfuDataContact bean);
	
	public SfuDataContact deleteById(Integer id);

	public SfuDataContact[] deleteByIds(Integer[] ids);
	
	public SfuDataContact update(SfuDataContact bean) ;
	
	public Pagination getPage(Integer companyId,int pageNo, int pageSize);
	
	public List<SfuDataContact> getList(Integer companyId);
	
}
