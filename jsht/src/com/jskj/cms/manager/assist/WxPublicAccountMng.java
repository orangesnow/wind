package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxPublicAccount;

public interface WxPublicAccountMng {

	public WxPublicAccount findById(Integer id);

	public WxPublicAccount save(WxPublicAccount bean);

	public WxPublicAccount update(WxPublicAccount bean);

	public WxPublicAccount deleteById(Integer id);
	
	public List<WxPublicAccount> getList(Integer status);
	
}