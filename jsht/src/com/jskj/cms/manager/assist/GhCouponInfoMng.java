
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.GhCouponInfo;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhCouponInfoMng{

	public Pagination getPage(String unionid,int pageNo, int pageSize);

	public GhCouponInfo findById(Integer id);
	
	public GhCouponInfo findByUnionId(String unionid);

	public GhCouponInfo save(GhCouponInfo bean);
	
	public GhCouponInfo update(GhCouponInfo bean);

	public GhCouponInfo deleteById(Integer id);

	public GhCouponInfo[] deleteByIds(Integer[] ids);
	
}
