
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjItemList;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjItemListMng {

	public Pagination getPage(String name,Integer status,Integer cid, Integer orderBy,int pageNo, int pageSize);
	
	public JskjItemList findById(Integer id);

	public JskjItemList save(JskjItemList bean);
	
	public JskjItemList update(JskjItemList bean);

	public JskjItemList deleteById(Integer id);
	
	public JskjItemList[] deleteByIds(Integer[] ids);

}
