
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjSubject;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjSubjectMng {

	public Pagination getPage(String staDate,String endDate,Integer groupid,Integer rtype,Integer status,int pageNo, int pageSize);
	
	public JskjSubject findById(Integer id);

	public JskjSubject save(JskjSubject bean);
	
	public JskjSubject update(JskjSubject bean);

	public JskjSubject deleteById(Integer id);
	
	public JskjSubject[] deleteByIds(Integer[] ids);

}
