
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjAd;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjAdMng {

	public Pagination getPage(Integer adtype, Integer status, int pageNo,
			int pageSize);
	
	public JskjAd findById(Integer id);

	public JskjAd save(JskjAd bean);
	
	public JskjAd update(JskjAd bean);

	public JskjAd deleteById(Integer id);
	
	public JskjAd[] deleteByIds(Integer[] ids);

}
