
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjItemSku;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjItemSkuMng {
	
	public Pagination getPage(Integer itemid,int pageNo, int pageSize);

	public JskjItemSku findById(Integer id);

	public JskjItemSku save(JskjItemSku bean);
	
	public JskjItemSku update(JskjItemSku bean);

	public JskjItemSku deleteById(Integer id);
	
	public JskjItemSku[] deleteByIds(Integer[] ids);

	public List<JskjItemSku> getList(Integer itemid) ;
}
