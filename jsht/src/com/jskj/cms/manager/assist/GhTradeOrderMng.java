/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhTradeOrder;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhTradeOrderMng {

	public GhTradeOrder findById(Integer id);

	public List<GhTradeOrder> findByGhId(String id);

	public GhTradeOrder save(GhTradeOrder bean);

	public GhTradeOrder update(GhTradeOrder bean);

	public GhTradeOrder deleteById(Integer id);
	
	public GhTradeOrder[] deleteByIds(Integer[] ids);
	
}
