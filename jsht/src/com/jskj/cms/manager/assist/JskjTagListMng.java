
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjTagList;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface  JskjTagListMng{

	public Pagination getPage(String name,Integer orderBy,int pageNo, int pageSize);
	
	public List<JskjTagList> getList(Integer status);
	
	public JskjTagList findById(Integer id);
	
	public JskjTagList save(JskjTagList bean);

	public JskjTagList update(JskjTagList bean);

	public JskjTagList deleteById(Integer id);
	
	public JskjTagList[] deleteByIds(Integer[] ids);

	public int deleteItemRef(Integer tagId) ;
	
	public int deleteTagRefFromItemId(Integer itemid);
	
}
