
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjCategory;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjCategoryMng {

	public Pagination getPage(String name, Integer status, int pageNo,
			int pageSize);
	
	public JskjCategory findById(Integer id);

	public JskjCategory save(JskjCategory bean);
	
	public JskjCategory update(JskjCategory bean);

	public JskjCategory deleteById(Integer id);
	
	public JskjCategory[] deleteByIds(Integer[] ids);

	public List<JskjCategory> findCategorys(Integer status);
}
