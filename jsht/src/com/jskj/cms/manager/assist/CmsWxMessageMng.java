
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.CmsWxMessage;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信消息管理接口
 * @author VAIO
 * @since 2014-06-03
 */
public interface CmsWxMessageMng {
	
	public Pagination getPage(Integer status, String openid,Integer pubType, Integer orderBy,int pageNo,
			int pageSize);

	public CmsWxMessage findById(String id);

	public CmsWxMessage save(CmsWxMessage bean);

	public CmsWxMessage updateByUpdater(Updater<CmsWxMessage> updater);
	
	public CmsWxMessage update(CmsWxMessage bean) ;
	
	public CmsWxMessage deleteById(String id);
	public CmsWxMessage[] deleteByIds(String[] ids);
	public int countByCond(Integer status, String openid);
	
}
