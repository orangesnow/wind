
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjUser;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjUserMng {

	public Pagination getPage(String staDate,String endDate,String nickname,int pageNo, int pageSize);
	
	public JskjUser findById(Integer id);

	public JskjUser save(JskjUser bean);
	
	public JskjUser update(JskjUser bean);

	public JskjUser deleteById(Integer id);
	
	public JskjUser[] deleteByIds(Integer[] ids);

}
