
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjShortMessage;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjShortMessageMng {

	public Pagination getPage(String name,String staDate,String endDate, Integer status, int pageNo, int pageSize);
	
	public JskjShortMessage findById(Integer id);

	public JskjShortMessage save(JskjShortMessage bean);
	
	public JskjShortMessage update(JskjShortMessage bean);

	public JskjShortMessage deleteById(Integer id);
	
	public JskjShortMessage[] deleteByIds(Integer[] ids);

}
