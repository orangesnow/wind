/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhConfigSource;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhConfigSourceMng {

	public Pagination getPage(String name,Integer category,int pageNo,
			int pageSize);
	
	public GhConfigSource findById(Integer id);

	public GhConfigSource save(GhConfigSource bean);

	public GhConfigSource update(GhConfigSource bean);

	public GhConfigSource deleteById(Integer id);
	
	public GhConfigSource[] deleteByIds(Integer[] ids);
	
	public List<GhConfigSource> findByCategory(Integer categoryId);
	
}
