
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxQrcodeType;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface WxQrcodeTypeMng {
	
	public List<WxQrcodeType> getList();
	
	public Pagination getPage(int pageNo,
			int pageSize);
	
	public WxQrcodeType findById(Integer id);

	public WxQrcodeType save(WxQrcodeType bean);

	public WxQrcodeType update(WxQrcodeType bean);

	public WxQrcodeType deleteById(Integer id);
	
	public WxQrcodeType[] deleteByIds(Integer[] ids);

}
