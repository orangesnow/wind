
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjArticle;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjArticleMng {

	public Pagination getPage(String name,Integer status,int pageNo, int pageSize);
	
	public JskjArticle findById(Integer id);

	public JskjArticle save(JskjArticle bean);
	
	public JskjArticle update(JskjArticle bean);

	public JskjArticle deleteById(Integer id);
	
	public JskjArticle[] deleteByIds(Integer[] ids);

}
