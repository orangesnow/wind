
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface WxUserStatDayMng {
	
	public Pagination getPage(Integer accountId,String staDate,String endDate, int pageNo,
			int pageSize);

}
