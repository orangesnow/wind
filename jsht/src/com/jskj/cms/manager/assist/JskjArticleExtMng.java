
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjArticleExt;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjArticleExtMng {

	public Pagination getPage(String name,Integer status,int pageNo, int pageSize);
	
	public JskjArticleExt findById(Integer id);

	public JskjArticleExt save(JskjArticleExt bean);
	
	public JskjArticleExt update(JskjArticleExt bean);

	public JskjArticleExt deleteById(Integer id);
	
	public JskjArticleExt[] deleteByIds(Integer[] ids);

}
