package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.common.page.Pagination;

public interface WxFollowUserMng {
	
	public WxFollowUser findById(Integer id);

	public WxFollowUser save(WxFollowUser bean);
	
	public WxFollowUser save(WxFollowUser bean,String[] tagNames);
	
	public WxFollowUser update(WxFollowUser user);
	
	public WxFollowUser update(WxFollowUser user,String[] tagNames);

	public WxFollowUser deleteById(Integer id);
	
	public WxFollowUser[] deleteByIds(Integer[] ids);

	public Pagination getPage(Integer accountId, Integer groupId,String staDate,String endDate,int pageNo, int pageSize);

	public Pagination getPage(Integer accountId, Integer groupId,String staDate,String endDate,String province, int pageNo,
			int pageSize) ;
	
	public Pagination getPage(Integer accountId, Integer groupId,Integer subType,String staDate,String endDate,String province, int pageNo,
			int pageSize) ;
	
	public Pagination getPage(Integer accountId, Integer groupId,Integer subType,String staDate,String endDate,String province, String nickname,String name,String openid,int pageNo,
			int pageSize) ;
	
	public WxFollowUser[] signByIds(Integer[] ids);
	
	public WxFollowUser[] enterByIds(Integer[] ids);
	
}