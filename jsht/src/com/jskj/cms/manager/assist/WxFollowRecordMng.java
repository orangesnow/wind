package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.WxFollowRecord;
import com.jskj.common.page.Pagination;

public interface WxFollowRecordMng {
	
	public WxFollowRecord findById(Integer id);

	public WxFollowRecord save(WxFollowRecord bean);

	public WxFollowRecord update(WxFollowRecord user);

	public WxFollowRecord deleteById(Integer id);
	
	public WxFollowRecord[] deleteByIds(Integer[] ids);

	public Pagination getPage(String openid,Integer pubType, int pageNo, int pageSize);

}