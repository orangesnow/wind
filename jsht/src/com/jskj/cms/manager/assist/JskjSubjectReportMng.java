
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjSubjectReport;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjSubjectReportMng {

	public Pagination getPage(String staDate,String endDate,Integer rtype,int pageNo, int pageSize);
	
	public JskjSubjectReport findById(Integer id);

	public JskjSubjectReport save(JskjSubjectReport bean);
	
	public JskjSubjectReport update(JskjSubjectReport bean);

	public JskjSubjectReport deleteById(Integer id);
	
	public JskjSubjectReport[] deleteByIds(Integer[] ids);

}
