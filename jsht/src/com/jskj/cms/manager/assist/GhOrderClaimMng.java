/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.GhOrderClaim;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhOrderClaimMng {

	public Pagination getPage(String orderId,Integer source,Integer claimFlag,Integer claimType,String staDate,String endDate,  int pageNo,
			int pageSize);
	
	public GhOrderClaim findById(int id);

	public GhOrderClaim save(GhOrderClaim bean);

	public GhOrderClaim update(GhOrderClaim bean);

	public GhOrderClaim deleteById(int id);
	
	public GhOrderClaim[] deleteByIds(int[] ids);
	
}
