/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.GhLeave;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhLeaveMng {

	public Pagination getPage(String userid,int pageNo,
			int pageSize);
	
	public GhLeave findById(Integer id);

	public GhLeave save(GhLeave bean);

	public GhLeave update(GhLeave bean);

	public GhLeave deleteById(Integer id);
	
	public GhLeave[] deleteByIds(Integer[] ids);
	
}
