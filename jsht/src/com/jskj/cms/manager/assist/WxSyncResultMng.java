
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.WxSyncResult;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface WxSyncResultMng {
	
	public Pagination getPage(Integer stype,int pageNo, int pageSize);
	
	public WxSyncResult findById(Integer id);

	public WxSyncResult save(WxSyncResult bean);

	public WxSyncResult update(WxSyncResult bean);

	public WxSyncResult deleteById(Integer id);
	
	public WxSyncResult[] deleteByIds(Integer[] ids);

}
