/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhChannelInfo;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhChannelInfoMng {

	public List getList();
	
	public GhChannelInfo findById(Integer id);

	public GhChannelInfo save(GhChannelInfo bean);

	public GhChannelInfo update(GhChannelInfo bean);

	public GhChannelInfo deleteById(Integer id);
	
	public GhChannelInfo[] deleteByIds(Integer[] ids);
	
	public Pagination getPage(String name,String pCode,int pageNo, int pageSize);
	
}
