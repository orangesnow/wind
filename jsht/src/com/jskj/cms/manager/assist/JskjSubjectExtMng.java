
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjSubjectExt;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjSubjectExtMng {

	public JskjSubjectExt findById(Integer id);

	public JskjSubjectExt save(JskjSubjectExt bean);
	
	public JskjSubjectExt update(JskjSubjectExt bean);

	public JskjSubjectExt deleteById(Integer id);
	
	public JskjSubjectExt[] deleteByIds(Integer[] ids);

}
