package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.CmsGuestbook;
import com.jskj.cms.entity.assist.CmsGuestbookExt;

public interface CmsGuestbookExtMng {
	public CmsGuestbookExt save(CmsGuestbookExt ext, CmsGuestbook guestbook);

	public CmsGuestbookExt update(CmsGuestbookExt ext);
}