
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjArea;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjAreaMng {

	public Pagination getPage(String name,Integer orderBy,Integer status,int pageNo, int pageSize);
	
	public JskjArea findById(Integer id);

	public JskjArea save(JskjArea bean);
	
	public JskjArea update(JskjArea bean);

	public JskjArea deleteById(Integer id);
	
	public JskjArea[] deleteByIds(Integer[] ids);

}
