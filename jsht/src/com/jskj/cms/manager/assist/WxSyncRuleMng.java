
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.WxSyncRule;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface WxSyncRuleMng {
	
	public Pagination getPage(Integer stype,int pageNo, int pageSize);
	
	public WxSyncRule findById(Integer id);

	public WxSyncRule save(WxSyncRule bean);

	public WxSyncRule update(WxSyncRule bean);

	public WxSyncRule deleteById(Integer id);
	
	public WxSyncRule[] deleteByIds(Integer[] ids);

}
