
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjItemTag;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjItemTagMng {

	public List<JskjItemTag> getList(Integer itemid);
	
	public JskjItemTag findById(Integer id);

	public JskjItemTag save(JskjItemTag bean);
	
	public JskjItemTag update(JskjItemTag bean);

	public JskjItemTag deleteById(Integer id);
	
	public JskjItemTag[] deleteByIds(Integer[] ids);

}
