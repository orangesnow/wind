
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxTagGroup;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface WxTagGroupMng {
	
	public List<WxTagGroup> getList(Integer accountId);
	
	public Pagination getPage(Integer accountId, int pageNo,
			int pageSize);
	
	public WxTagGroup findById(Integer id);

	public WxTagGroup save(WxTagGroup bean);
	
	public WxTagGroup save(WxTagGroup bean,String[] OrgTags);

	public WxTagGroup update(WxTagGroup bean);
	
	public WxTagGroup update(WxTagGroup bean,String[] OrgTags);

	public WxTagGroup deleteById(Integer id);
	
	public WxTagGroup[] deleteByIds(Integer[] ids);

}
