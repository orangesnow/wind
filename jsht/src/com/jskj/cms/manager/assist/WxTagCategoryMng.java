
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxTagCategory;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface WxTagCategoryMng {
	
	public List<WxTagCategory> getList(Integer rootId,Integer status);
	
	public Pagination getPage(Integer rootId,Integer status, int pageNo,
			int pageSize);
	
	public WxTagCategory findById(Integer id);

	public WxTagCategory save(WxTagCategory bean);
	
	public WxTagCategory update(WxTagCategory bean);

	public WxTagCategory deleteById(Integer id);
	
	public WxTagCategory[] deleteByIds(Integer[] ids);

}
