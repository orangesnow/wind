/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhConfigCategory;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhConfigCategoryMng {

	public List<GhConfigCategory> getList();
	
	public GhConfigCategory findById(Integer id);

	public GhConfigCategory save(GhConfigCategory bean);

	public GhConfigCategory update(GhConfigCategory bean);

	public GhConfigCategory deleteById(Integer id);
	
	public GhConfigCategory[] deleteByIds(Integer[] ids);
	
}
