
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjArticle;
import com.jskj.cms.entity.assist.JskjArticleCategory;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjArticleCategoryMng {

	public List<JskjArticleCategory> getList(String name,Integer parentid,Integer status);
	
	public Pagination getPage(String name,Integer parentid,Integer status,int pageNo, int pageSize);
	
	public JskjArticleCategory findById(Integer id);

	public JskjArticleCategory save(JskjArticleCategory bean);
	
	public JskjArticleCategory update(JskjArticleCategory bean);

	public JskjArticleCategory deleteById(Integer id);
	
	public JskjArticleCategory[] deleteByIds(Integer[] ids);

}
