
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjCoupon;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjCouponMng {

	public Pagination getPage(String name,String staDate, String endDate, Integer status,int pageNo, int pageSize);

	public JskjCoupon findById(Integer id);

	public JskjCoupon save(JskjCoupon bean);
	
	public JskjCoupon update(JskjCoupon bean);

	public JskjCoupon deleteById(Integer id);
	
	public JskjCoupon[] deleteByIds(Integer[] ids);

}
