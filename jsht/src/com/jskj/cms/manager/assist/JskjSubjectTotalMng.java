
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjSubjectTotal;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjSubjectTotalMng {

	public Pagination getPage(int pageNo, int pageSize);
	
	public JskjSubjectTotal findById(Integer id);

	public JskjSubjectTotal save(JskjSubjectTotal bean);
	
	public JskjSubjectTotal update(JskjSubjectTotal bean);

	public JskjSubjectTotal deleteById(Integer id);
	
	public JskjSubjectTotal[] deleteByIds(Integer[] ids);

}
