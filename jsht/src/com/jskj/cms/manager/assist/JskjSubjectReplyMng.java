
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjSubjectReply;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjSubjectReplyMng {
	
	public Pagination getPage(Integer subjectid,String staDate,String endDate,int pageNo, int pageSize);
		
	public JskjSubjectReply findById(Integer id);

	public JskjSubjectReply save(JskjSubjectReply bean);
	
	public JskjSubjectReply update(JskjSubjectReply bean);

	public JskjSubjectReply deleteById(Integer id);
	
	public JskjSubjectReply[] deleteByIds(Integer[] ids);

}
