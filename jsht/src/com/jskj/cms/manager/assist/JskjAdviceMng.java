
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjAdvice;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjAdviceMng {

	public Pagination getPage( int pageNo,
			int pageSize);
	
	public JskjAdvice findById(Integer id);

	public JskjAdvice save(JskjAdvice bean);
	
	public JskjAdvice update(JskjAdvice bean);

	public JskjAdvice deleteById(Integer id);
	
	public JskjAdvice[] deleteByIds(Integer[] ids);

}
