/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.GhOrderInfo;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhOrderInfoMng {

	
	public Pagination getPage(Integer userid,String staDate,String endDate, int pageNo,
			int pageSize);
	
	
	public Pagination getPage(String orderId,Integer source,String channelId,String staDate,String endDate,  int pageNo,
			int pageSize);
	
	public Pagination getPage(String orderId,Integer source,Integer claimFlag,Integer claimType,String staDate,String endDate,  int pageNo,
			int pageSize);
	
	public GhOrderInfo findById(String id);

	public GhOrderInfo save(GhOrderInfo bean);

	public GhOrderInfo update(GhOrderInfo bean);

	public GhOrderInfo deleteById(String id);
	
	public GhOrderInfo[] deleteByIds(String[] ids);
	
}
