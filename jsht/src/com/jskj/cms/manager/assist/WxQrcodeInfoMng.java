
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxQrcodeInfo;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface WxQrcodeInfoMng {
	
	public List<WxQrcodeInfo> getList(Integer accountId);
	
	public Pagination getPage(Integer accountId, int pageNo,
			int pageSize);
	
	public WxQrcodeInfo findById(Integer id);

	public WxQrcodeInfo save(WxQrcodeInfo bean);

	public WxQrcodeInfo update(WxQrcodeInfo bean);

	public WxQrcodeInfo deleteById(Integer id);
	
	public WxQrcodeInfo[] deleteByIds(Integer[] ids);

}
