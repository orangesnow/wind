
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjSubjectPicture;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjSubjectPictureMng {

	public List<JskjSubjectPicture> getList(Integer subjectid);
	
	public JskjSubjectPicture findById(Integer id);

	public JskjSubjectPicture save(JskjSubjectPicture bean);
	
	public JskjSubjectPicture update(JskjSubjectPicture bean);

	public JskjSubjectPicture deleteById(Integer id);
	
	public JskjSubjectPicture[] deleteByIds(Integer[] ids);

}
