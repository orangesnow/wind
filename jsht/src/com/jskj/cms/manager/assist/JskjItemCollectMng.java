
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjItemCollect;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjItemCollectMng {

	public Pagination getPage(String staDate, String endDate, Integer collecttype, int pageNo, int pageSize);
	
	public JskjItemCollect findById(Integer id);

	public JskjItemCollect save(JskjItemCollect bean);
	
	public JskjItemCollect update(JskjItemCollect bean);

	public JskjItemCollect deleteById(Integer id);
	
	public JskjItemCollect[] deleteByIds(Integer[] ids);

}
