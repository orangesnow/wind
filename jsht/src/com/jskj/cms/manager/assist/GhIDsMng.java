/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.GhIDs;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhIDsMng {
	public String findNextCardno();
	public GhIDs findNextCardno(Integer status);
	public GhIDs update(GhIDs bean);
}
