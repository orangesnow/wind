
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.GhVisitRecord;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhVisitRecordMng {
	
	public Pagination getPage(Integer userid,String oid, int pageNo,
			int pageSize);
	
	public Pagination getPage(Integer userId, String oid, String staDate,
			String endDate, int pageNo, int pageSize);
	
	public GhVisitRecord findById(Integer id);

	public GhVisitRecord save(GhVisitRecord bean);

	public GhVisitRecord update(GhVisitRecord bean);

	public GhVisitRecord deleteById(Integer id);
	
	public GhVisitRecord[] deleteByIds(Integer[] ids);
	
}
