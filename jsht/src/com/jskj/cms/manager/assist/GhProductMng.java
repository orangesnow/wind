/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhProduct;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhProductMng {

	public GhProduct findByOuterId(String id);
	
	public Pagination getPage(String name,int pageNo, int pageSize);

	public GhProduct findById(Integer id);

	public GhProduct save(GhProduct bean);

	public GhProduct update(GhProduct product);

	public GhProduct deleteById(Integer id);

	public GhProduct[] deleteByIds(Integer[] ids);
	
	public List<GhProduct> getList(Integer status);
}
