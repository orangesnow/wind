
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjSubjectAction;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjSubjectActionMng {

	public Pagination getPage(String staDate,String endDate, Integer atype, int pageNo,
			int pageSize);
	
	public JskjSubjectAction findById(Integer id);

	public JskjSubjectAction save(JskjSubjectAction bean);
	
	public JskjSubjectAction update(JskjSubjectAction bean);

	public JskjSubjectAction deleteById(Integer id);
	
	public JskjSubjectAction[] deleteByIds(Integer[] ids);

}
