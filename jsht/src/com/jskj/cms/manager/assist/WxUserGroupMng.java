
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxUserGroup;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface WxUserGroupMng {
	
	public List<WxUserGroup> getList(Integer accountId);
	
	public Pagination getPage(Integer accountId, int pageNo,
			int pageSize);
	
	public WxUserGroup findById(Integer id);

	public WxUserGroup save(WxUserGroup bean);

	public WxUserGroup update(WxUserGroup bean);

	public WxUserGroup deleteById(Integer id);
	
	public WxUserGroup[] deleteByIds(Integer[] ids);

}
