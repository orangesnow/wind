
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.WxShare;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface  WxShareMng{

	public Pagination getPage(int pageNo, int pageSize);

	public WxShare findById(Integer id);

	public WxShare save(WxShare bean);
	
	public WxShare update(WxShare bean);

	public WxShare deleteById(Integer id);

	public WxShare[] deleteByIds(Integer[] ids);
}
