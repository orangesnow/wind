/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.GhOrderOwner;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhOrderOwnerMng {

	public Pagination getPage(Integer source,String keyword,  int pageNo,
			int pageSize);
	
	public GhOrderOwner findById(Integer id);

	public GhOrderOwner save(GhOrderOwner bean);

	public GhOrderOwner update(GhOrderOwner bean);

	public GhOrderOwner deleteById(Integer id);
	
	public GhOrderOwner[] deleteByIds(Integer[] ids);
	
}
