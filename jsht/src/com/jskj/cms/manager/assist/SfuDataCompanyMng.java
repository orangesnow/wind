package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.SfuDataCompany;
import com.jskj.common.page.Pagination;

public interface SfuDataCompanyMng {
	
	public SfuDataCompany findById(Integer id);

	public SfuDataCompany save(SfuDataCompany bean);
	
	public SfuDataCompany deleteById(Integer id);

	public SfuDataCompany[] deleteByIds(Integer[] ids);
	
	public SfuDataCompany update(SfuDataCompany bean) ;
	
	public Pagination getPage(String city,int pageNo, int pageSize);


}
