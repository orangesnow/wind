
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.JskjVersion;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjVersionMng {

	public Pagination getPage(int pageNo,int pageSize);
	
	public JskjVersion findById(Integer id);

	public JskjVersion save(JskjVersion bean);
	
	public JskjVersion update(JskjVersion bean);

	public JskjVersion deleteById(Integer id);
	
	public JskjVersion[] deleteByIds(Integer[] ids);

}
