
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxSyncType;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface WxSyncTypeMng {
	
	public List<WxSyncType> getList();
	
	public Pagination getPage(int pageNo, int pageSize);
	
	public WxSyncType findById(Integer id);

	public WxSyncType save(WxSyncType bean);

	public WxSyncType update(WxSyncType bean);

	public WxSyncType deleteById(Integer id);
	
	public WxSyncType[] deleteByIds(Integer[] ids);

}
