/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhBindLeave;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhBindLeaveMng {

	public GhBindLeave findById(Integer id);

	public List<GhBindLeave> findByGhId(String id);

	public GhBindLeave save(GhBindLeave bean);

	public GhBindLeave update(GhBindLeave bean);

	public GhBindLeave deleteById(Integer id);
	
	public GhBindLeave[] deleteByIds(Integer[] ids);
	
	public Pagination getPage(String ghid,int pageNo, int pageSize) ;
	
}
