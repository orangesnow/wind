/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.GhGoodsInfo;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface GhGoodsInfoMng {

	public Pagination getPage(String name,String mobile,String staDate,String endDate,int pageNo, int pageSize);

	public GhGoodsInfo findById(Integer id);

	public GhGoodsInfo save(GhGoodsInfo bean);

	public GhGoodsInfo update(GhGoodsInfo goods);

	public GhGoodsInfo deleteById(Integer id);

	public GhGoodsInfo[] deleteByIds(Integer[] ids);
	
}
