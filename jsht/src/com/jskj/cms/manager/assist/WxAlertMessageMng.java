package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxAlertMessage;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

public interface WxAlertMessageMng {
	
	public WxAlertMessage findById(Integer id);

	public WxAlertMessage updateByUpdater(Updater<WxAlertMessage> updater);
	
	public WxAlertMessage save(WxAlertMessage bean);
	
	public WxAlertMessage deleteById(Integer id);

	public List<WxAlertMessage> findByCtype(int ctype,int status);
	
	public  WxAlertMessage findByCKey(String ckey);
	
	public WxAlertMessage[] deleteByIds(Integer[] ids);
	
	public WxAlertMessage update(WxAlertMessage bean) ;
	
	public Pagination getPage(String name,Integer categoryId,int pageNo, int pageSize);
	
	public Pagination getPage(Integer sourceId, Integer ctype, Integer status,
			int pageNo, int pageSize);

}
