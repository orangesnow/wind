
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface  WxTagListMng{
	
	public List<WxTagList> getList();
	
	public Pagination getPage(String name,int pageNo, int pageSize);
	
	public Pagination getPage(String name,Integer cid,int pageNo, int pageSize);
	
	public WxTagList findById(Integer id);
	
	public WxTagList findByName(String tagName);
	
	public WxTagList findByName(String tagName,boolean single);
	
	public WxTagList save(WxTagList bean);
	
	public List<WxTagList> save(String[] tagNames);

	public WxTagList update(WxTagList bean);

	public WxTagList deleteById(Integer id);
	
	public WxTagList[] deleteByIds(Integer[] ids);

}
