
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjSubjectGroup;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjSubjectGroupMng {

	public Pagination getPage(String name, Integer status, int pageNo,
			int pageSize);
	
	public JskjSubjectGroup findById(Integer id);

	public JskjSubjectGroup save(JskjSubjectGroup bean);
	
	public JskjSubjectGroup update(JskjSubjectGroup bean);

	public JskjSubjectGroup deleteById(Integer id);
	
	public JskjSubjectGroup[] deleteByIds(Integer[] ids);

	public List<JskjSubjectGroup> findAllGroups(Integer status);
}
