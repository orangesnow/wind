
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjItemPicture;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public interface JskjItemPictureMng {

	public Pagination getPage(Integer itemid,Integer ptype,int pageNo,int pageSize);
	
	public JskjItemPicture findById(Integer id);

	public JskjItemPicture save(JskjItemPicture bean);
	
	public JskjItemPicture update(JskjItemPicture bean);

	public JskjItemPicture deleteById(Integer id);
	
	public JskjItemPicture[] deleteByIds(Integer[] ids);

}
