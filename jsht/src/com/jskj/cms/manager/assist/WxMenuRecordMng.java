package com.jskj.cms.manager.assist;

import com.jskj.cms.entity.assist.WxMenuRecord;
import com.jskj.common.page.Pagination;

public interface WxMenuRecordMng {
	
	public WxMenuRecord findById(Integer id);

	public WxMenuRecord save(WxMenuRecord bean);

	public WxMenuRecord update(WxMenuRecord bean);

	public WxMenuRecord deleteById(Integer id);

	public Pagination getPage(String fromUsername, Integer pubType, int pageNo, int pageSize);

}