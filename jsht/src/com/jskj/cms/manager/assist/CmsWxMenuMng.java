package com.jskj.cms.manager.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxMenuList;
import com.jskj.common.page.Pagination;

public interface CmsWxMenuMng {

	public Pagination getPage(Integer isDisplay,  int pageNo,
			int pageSize,Integer pubType);

	public List<WxMenuList> getList(int menuLevel,boolean flag,Integer pubType);
	
	public WxMenuList findById(Integer id);

	public WxMenuList save(WxMenuList bean);

	public WxMenuList update(WxMenuList bean);

	public WxMenuList deleteById(Integer id);
	
	public WxMenuList[] deleteByIds(Integer[] ids);
	
	public int countByIsDisplay(Integer isDisplay, Integer pubType);
	
	public WxMenuList[] updatePriority(Integer[] ids, Integer[] priority);
}