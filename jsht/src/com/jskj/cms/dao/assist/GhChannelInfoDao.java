
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhChannelInfo;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 订单列表
 * @author VAIO
 * @since 2014-08-10
 */
public interface GhChannelInfoDao {

	public List getList();
	
	public GhChannelInfo findById(Integer id);

	public GhChannelInfo save(GhChannelInfo bean);

	public GhChannelInfo updateByUpdater(Updater<GhChannelInfo> updater);

	public GhChannelInfo deleteById(Integer id);
	
	public Pagination getPage(String name,String pCode,int pageNo, int pageSize);
	
}
