package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.GhGoodsInfo;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 物流用户信息列表
 * @author VAIO
 * @since 2014-08-10
 */
public interface GhGoodsInfoDao {
	
	public Pagination getPage(String name,String mobile,String staDate,String endDate,int pageNo, int pageSize);

	public GhGoodsInfo findById(Integer id);

	public GhGoodsInfo save(GhGoodsInfo bean);

	public GhGoodsInfo updateByUpdater(Updater<GhGoodsInfo> updater);

	public GhGoodsInfo deleteById(Integer id);
	
}
