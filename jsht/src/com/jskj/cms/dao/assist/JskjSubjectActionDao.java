
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjSubjectAction;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 * @since 2016-01-01
 */
public interface JskjSubjectActionDao {
	
	public Pagination getPage(String staDate,String endDate,Integer atype,int pageNo, int pageSize);
	
	public JskjSubjectAction findById(Integer id);

	public JskjSubjectAction save(JskjSubjectAction bean);

	public JskjSubjectAction updateByUpdater(Updater<JskjSubjectAction> updater);

	public JskjSubjectAction deleteById(Integer id);
	
}
