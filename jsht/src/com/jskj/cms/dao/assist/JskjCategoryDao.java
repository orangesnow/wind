
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjCategory;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjCategoryDao {
	
	public Pagination getPage(String name,Integer status,int pageNo, int pageSize);
	
	public JskjCategory findById(Integer id);

	public JskjCategory save(JskjCategory bean);

	public JskjCategory updateByUpdater(Updater<JskjCategory> updater);

	public JskjCategory deleteById(Integer id);
	
	public List<JskjCategory> findCategorys(Integer status);
	
}
