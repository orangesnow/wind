
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjVersion;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjVersionDao {
	
	public Pagination getPage(int pageNo, int pageSize);
	
	public JskjVersion findById(Integer id);

	public JskjVersion save(JskjVersion bean);

	public JskjVersion updateByUpdater(Updater<JskjVersion> updater);

	public JskjVersion deleteById(Integer id);
	
}
