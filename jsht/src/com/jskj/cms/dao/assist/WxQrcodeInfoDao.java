
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxQrcodeInfo;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxQrcodeInfoDao {
	
	public List<WxQrcodeInfo> getList(Integer accountId);
	
	public Pagination getPage(Integer accountId,int pageNo, int pageSize, boolean cacheable);
	
	public WxQrcodeInfo findById(Integer id);

	public WxQrcodeInfo save(WxQrcodeInfo bean);

	public WxQrcodeInfo updateByUpdater(Updater<WxQrcodeInfo> updater);

	public WxQrcodeInfo deleteById(Integer id);
	
}
