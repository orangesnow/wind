
package com.jskj.cms.dao.assist;

import com.jskj.common.page.Pagination;

/**
 * 抽奖公司信息
 * @author VAIO
 * @since 2014-07-14
 */
public interface WxLotteryStatTotalDao {

	public Pagination getPage(Integer productId,int pageNo, int pageSize);

}
