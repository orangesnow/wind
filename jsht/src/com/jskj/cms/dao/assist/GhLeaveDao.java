
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.GhLeave;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 留言内容列表
 * @author VAIO
 * @since 2014-08-11
 */
public interface GhLeaveDao {

	public Pagination getPage(String userId,int pageNo, int pageSize, boolean cacheable);
	
	public GhLeave findById(Integer id);

	public GhLeave save(GhLeave bean);

	public GhLeave updateByUpdater(Updater<GhLeave> updater);

	public GhLeave deleteById(Integer id);
	
}
