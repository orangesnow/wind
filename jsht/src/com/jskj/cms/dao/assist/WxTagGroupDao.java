
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxTagGroup;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxTagGroupDao {
	
	public List<WxTagGroup> getList(Integer accountId);
	
	public Pagination getPage(Integer accountId,int pageNo, int pageSize, boolean cacheable);
	
	public WxTagGroup findById(Integer id);

	public WxTagGroup save(WxTagGroup bean);

	public WxTagGroup updateByUpdater(Updater<WxTagGroup> updater);

	public WxTagGroup deleteById(Integer id);
	
}
