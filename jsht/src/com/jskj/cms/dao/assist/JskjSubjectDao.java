
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjSubject;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjSubjectDao {
	
	public Pagination getPage(String staDate,String endDate,Integer groupid,Integer rtype,Integer status,int pageNo, int pageSize);
	
	public JskjSubject findById(Integer id);

	public JskjSubject save(JskjSubject bean);

	public JskjSubject updateByUpdater(Updater<JskjSubject> updater);

	public JskjSubject deleteById(Integer id);
	
}
