
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjAd;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjAdDao {
	
	public Pagination getPage(Integer adtype,Integer status,int pageNo, int pageSize);
	
	public JskjAd findById(Integer id);

	public JskjAd save(JskjAd bean);

	public JskjAd updateByUpdater(Updater<JskjAd> updater);

	public JskjAd deleteById(Integer id);
	
}
