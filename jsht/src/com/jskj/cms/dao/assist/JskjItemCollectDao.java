
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjItemCollect;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjItemCollectDao {
	
	public Pagination getPage(String stadate, String enddate, Integer collecttype,int pageNo, int pageSize);
	
	public JskjItemCollect findById(Integer id);

	public JskjItemCollect save(JskjItemCollect bean);

	public JskjItemCollect updateByUpdater(Updater<JskjItemCollect> updater);

	public JskjItemCollect deleteById(Integer id);
	
}
