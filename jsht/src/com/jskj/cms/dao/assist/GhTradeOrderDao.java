package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhTradeOrder;
import com.jskj.common.hibernate3.Updater;

/**
 * 留言内容列表
 * @author VAIO
 * @since 2014-08-11
 */
public interface GhTradeOrderDao {

	public GhTradeOrder findById(Integer id);

	public GhTradeOrder save(GhTradeOrder bean);

	public GhTradeOrder updateByUpdater(Updater<GhTradeOrder> updater);

	public GhTradeOrder deleteById(Integer id);
	
	public List<GhTradeOrder> findByGhId(String ghid);
	
}
