
package com.jskj.cms.dao.assist;

import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxUserStatDayDao {
	
	public Pagination getPage(Integer accountId,String staDate,String endDate,int pageNo, int pageSize);
	
	
}
