
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.SfuDataCompany;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信消息接口
 * @author VAIO
 * @since 2014-06-03
 */
public interface SfuDataCompanyDao {

	public Pagination getPage(String city,int pageNo,int pageSize);

	public SfuDataCompany findById(Integer id);

	public SfuDataCompany save(SfuDataCompany bean);

	public SfuDataCompany updateByUpdater(Updater<SfuDataCompany> updater);

	public SfuDataCompany deleteById(Integer id);
	
}
