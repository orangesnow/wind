package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxMenuList;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

public interface CmsWxMenuDao {

	public Pagination getPage(Integer isDisplay,  int pageNo,
			int pageSize, boolean cacheable,Integer pubType);

	public List<WxMenuList> getList(int menuLevel,boolean flag,Integer pubType);
	
	public WxMenuList findById(Integer id);

	public WxMenuList save(WxMenuList bean);

	public WxMenuList updateByUpdater(Updater<WxMenuList> updater);

	public WxMenuList deleteById(Integer id);

	public int countByIsDisplay(Integer isDisplay,Integer pubType);

}