
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjSubjectTotal;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjSubjectTotalDao {
	
	public Pagination getPage(int pageNo, int pageSize);
	
	public JskjSubjectTotal findById(Integer id);

	public JskjSubjectTotal save(JskjSubjectTotal bean);

	public JskjSubjectTotal updateByUpdater(Updater<JskjSubjectTotal> updater);

	public JskjSubjectTotal deleteById(Integer id);
	
}
