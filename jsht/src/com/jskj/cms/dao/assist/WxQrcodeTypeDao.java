
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxQrcodeType;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxQrcodeTypeDao {
	
	public List<WxQrcodeType> getList();
	
	public Pagination getPage(int pageNo, int pageSize, boolean cacheable);
	
	public WxQrcodeType findById(Integer id);

	public WxQrcodeType save(WxQrcodeType bean);

	public WxQrcodeType updateByUpdater(Updater<WxQrcodeType> updater);

	public WxQrcodeType deleteById(Integer id);
	
}
