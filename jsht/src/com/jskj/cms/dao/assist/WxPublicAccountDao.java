package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.common.hibernate3.Updater;

public interface WxPublicAccountDao {

	public WxPublicAccount findById(Integer id);

	public WxPublicAccount save(WxPublicAccount bean);

	public WxPublicAccount updateByUpdater(Updater<WxPublicAccount> updater);

	public WxPublicAccount deleteById(Integer id);

	public List<WxPublicAccount> getList(Integer status);
	

}