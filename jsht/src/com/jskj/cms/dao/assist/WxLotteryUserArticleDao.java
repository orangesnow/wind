
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxLotteryUserArticle;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 抽奖公司信息
 * @author VAIO
 * @since 2014-07-14
 */
public interface WxLotteryUserArticleDao {

	public Pagination getPage(Integer productId, Integer isConfirm, int pageNo, int pageSize);

	public WxLotteryUserArticle findById(Integer id);

	public WxLotteryUserArticle save(WxLotteryUserArticle bean);

	public WxLotteryUserArticle updateByUpdater(Updater<WxLotteryUserArticle> updater);

	public WxLotteryUserArticle deleteById(Integer id);

	
}
