
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjSubjectGroup;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 * @since 2016-01-01
 */
public interface JskjSubjectGroupDao {
	
	public Pagination getPage(String name,Integer status,int pageNo, int pageSize);
	
	public JskjSubjectGroup findById(Integer id);

	public JskjSubjectGroup save(JskjSubjectGroup bean);

	public JskjSubjectGroup updateByUpdater(Updater<JskjSubjectGroup> updater);

	public JskjSubjectGroup deleteById(Integer id);
	
	public List<JskjSubjectGroup> findAllGroups(Integer status);
}
