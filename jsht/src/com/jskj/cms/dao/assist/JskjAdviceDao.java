
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjAdvice;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjAdviceDao {
	
	public Pagination getPage(int pageNo, int pageSize);
	
	public JskjAdvice findById(Integer id);

	public JskjAdvice save(JskjAdvice bean);

	public JskjAdvice updateByUpdater(Updater<JskjAdvice> updater);

	public JskjAdvice deleteById(Integer id);
	
}
