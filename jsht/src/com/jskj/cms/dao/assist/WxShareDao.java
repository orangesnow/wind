
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxShare;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxShareDao {
	
	public Pagination getPage(int pageNo, int pageSize);
	
	public WxShare findById(Integer id);
	
	public WxShare save(WxShare bean);

	public WxShare updateByUpdater(Updater<WxShare> updater);

	public WxShare deleteById(Integer id);

}
