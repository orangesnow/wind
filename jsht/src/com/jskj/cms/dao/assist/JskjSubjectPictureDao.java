
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjSubjectPicture;
import com.jskj.common.hibernate3.Updater;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjSubjectPictureDao {
	
	public List<JskjSubjectPicture> getList(Integer subjectid);
	
	public JskjSubjectPicture findById(Integer id);

	public JskjSubjectPicture save(JskjSubjectPicture bean);

	public JskjSubjectPicture updateByUpdater(Updater<JskjSubjectPicture> updater);

	public JskjSubjectPicture deleteById(Integer id);
	
}
