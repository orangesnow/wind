
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.GhCouponInfo;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 优惠券列表
 * @author VAIO
 * @since 2015-03-19
 */
public interface GhCouponInfoDao {
	
	public Pagination getPage(String unionid,int pageNo, int pageSize);
	
	public GhCouponInfo findById(Integer id);
	
	public GhCouponInfo save(GhCouponInfo bean);

	public GhCouponInfo updateByUpdater(Updater<GhCouponInfo> updater);

	public GhCouponInfo deleteById(Integer id);
	
	public GhCouponInfo findByUnionId(String unionid);

}
