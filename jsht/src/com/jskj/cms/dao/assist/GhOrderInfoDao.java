
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.GhOrderInfo;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 订单列表
 * @author VAIO
 * @since 2014-08-10
 */
public interface GhOrderInfoDao {
	
	public Pagination getPage(Integer userId,String staDate,String endDate,int pageNo, int pageSize);

	public Pagination getPage(String orderId,Integer source,String channelId,String staDate,String endDate,int pageNo, int pageSize, boolean cacheable);
	
	public Pagination getPage(String orderId,Integer source,Integer claimFlag,Integer claimType,String staDate,String endDate,int pageNo, int pageSize, boolean cacheable);
	
	public GhOrderInfo findById(String id);

	public GhOrderInfo save(GhOrderInfo bean);

	public GhOrderInfo updateByUpdater(Updater<GhOrderInfo> updater);

	public GhOrderInfo deleteById(String id);
	
}
