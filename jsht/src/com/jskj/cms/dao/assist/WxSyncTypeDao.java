
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxSyncType;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxSyncTypeDao {
	
	public List<WxSyncType> getList();
	
	public Pagination getPage(int pageNo, int pageSize);
	
	public WxSyncType findById(Integer id);

	public WxSyncType save(WxSyncType bean);

	public WxSyncType updateByUpdater(Updater<WxSyncType> updater);

	public WxSyncType deleteById(Integer id);
	
}
