
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.GhOrderRelation;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 订单归属信息
 * @author VAIO
 * @since 2014-08-10
 */
public interface GhOrderRelationDao {
	
	public Pagination getPage(Integer userId,String staDate,String endDate,int pageNo, int pageSize);
	
	public Pagination getPage(Integer userId,Integer visitFlag,String staDate,String endDate,int pageNo, int pageSize);
	
	public GhOrderRelation findById(Integer id);

	public GhOrderRelation save(GhOrderRelation bean);

	public GhOrderRelation updateByUpdater(Updater<GhOrderRelation> updater);

	public GhOrderRelation deleteById(Integer id);
	
}
