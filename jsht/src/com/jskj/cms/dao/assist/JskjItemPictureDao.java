
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjItemPicture;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjItemPictureDao {
	
	public Pagination getList(Integer itemid,Integer ptype,int pageNo, int pageSize);
	
	public JskjItemPicture findById(Integer id);

	public JskjItemPicture save(JskjItemPicture bean);

	public JskjItemPicture updateByUpdater(Updater<JskjItemPicture> updater);

	public JskjItemPicture deleteById(Integer id);
	
}
