
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxTagCategory;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxTagCategoryDao {
	
	public List<WxTagCategory> getList(Integer rootId,Integer status);
	
	public Pagination getPage(Integer rootId,Integer status,int pageNo, int pageSize);
	
	public WxTagCategory findById(Integer id);

	public WxTagCategory save(WxTagCategory bean);

	public WxTagCategory updateByUpdater(Updater<WxTagCategory> updater);

	public WxTagCategory deleteById(Integer id);
	
}
