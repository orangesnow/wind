package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxFollowRecord;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

public interface WxFollowRecordDao {

	public WxFollowRecord findById(Integer id);

	public WxFollowRecord save(WxFollowRecord bean);

	public WxFollowRecord updateByUpdater(Updater<WxFollowRecord> updater);

	public WxFollowRecord deleteById(Integer id);

	public Pagination getPage(String openid,Integer pubType, int pageNo, int pageSize);

}