
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjHotKeyword;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjHotKeywordDao {
	
	public Pagination getPage(String name,Integer htype,int pageNo, int pageSize);
	
	public JskjHotKeyword findById(Integer id);

	public JskjHotKeyword save(JskjHotKeyword bean);

	public JskjHotKeyword updateByUpdater(Updater<JskjHotKeyword> updater);

	public JskjHotKeyword deleteById(Integer id);
	
}
