package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.GhOrderOwner;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

public interface GhOrderOwnerDao {

	public GhOrderOwner findById(Integer id);

	public GhOrderOwner save(GhOrderOwner bean);

	public GhOrderOwner updateByUpdater(Updater<GhOrderOwner> updater);

	public GhOrderOwner deleteById(Integer id);

	public Pagination getPage(Integer userid,String keyword,int pageNo, int pageSize);


}