
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjTagList;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjTagListDao {

	public Pagination getPage(String name, Integer orderBy,int pageNo, int pageSize);
	
	public List<JskjTagList> getList(Integer status);
	
	public JskjTagList findById(Integer id);

	public JskjTagList save(JskjTagList bean);

	public JskjTagList updateByUpdater(Updater<JskjTagList> updater);

	public JskjTagList deleteById(Integer id);
	
	public int deleteItemRef(Integer tagId);
	
	public int deleteTagRefFromItemId(Integer itemid);
}
