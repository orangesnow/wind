package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

public interface WxFollowUserDao {

	public WxFollowUser findById(Integer id);

	public WxFollowUser save(WxFollowUser bean);

	public WxFollowUser updateByUpdater(Updater<WxFollowUser> updater);

	public WxFollowUser deleteById(Integer id);

	public Pagination getPage(Integer accountId, Integer groupId, int pageNo, int pageSize);

	public Pagination getPage(Integer accountId, Integer groupId,String staDate,String endDate, int pageNo, int pageSize);
	
	public Pagination getPage(Integer accountId, Integer groupId,String staDate,String endDate,String province, int pageNo, int pageSize);
	
	public Pagination getPage(Integer accountId, Integer groupId,Integer subType,String staDate,String endDate,String province, int pageNo, int pageSize);
	
	public Pagination getPage(Integer accountId, Integer groupId,Integer subType,String staDate,String endDate,String province,String nickname,String name,String openid, int pageNo, int pageSize);

}