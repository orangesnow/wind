
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjItemSku;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjItemSkuDao {
	
	public Pagination getPage(Integer itemid,int pageNo, int pageSize);
	
	public JskjItemSku findById(Integer id);

	public JskjItemSku save(JskjItemSku bean);

	public JskjItemSku updateByUpdater(Updater<JskjItemSku> updater);

	public JskjItemSku deleteById(Integer id);
	
	public List<JskjItemSku> getList(Integer itemid) ;
	
}
