
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.GhIDs;
import com.jskj.common.hibernate3.Updater;

/**
 * 留言内容列表
 * @author VAIO
 * @since 2014-08-11
 */
public interface GhIDsDao {

	public GhIDs findNextCardno(Integer status);

	public GhIDs updateByUpdater(Updater<GhIDs> updater);

}
