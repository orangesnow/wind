
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxLotteryAgency;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 抽奖公司信息
 * @author VAIO
 * @since 2014-07-14
 */
public interface WxLotteryAgencyDao {

	public Pagination getPage(int pageNo, int pageSize);

	public WxLotteryAgency findById(String weixin);

	public WxLotteryAgency save(WxLotteryAgency bean);

	public WxLotteryAgency updateByUpdater(Updater<WxLotteryAgency> updater);

	public WxLotteryAgency deleteById(String weixin);

	
}
