
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjArticleCategory;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjArticleCategoryDao {
	
	
	public List<JskjArticleCategory> getList(String name,Integer parentid,Integer status);
	
	public Pagination getPage(String name,Integer parentid,Integer status,int pageNo, int pageSize);
	
	public JskjArticleCategory findById(Integer id);

	public JskjArticleCategory save(JskjArticleCategory bean);

	public JskjArticleCategory updateByUpdater(Updater<JskjArticleCategory> updater);

	public JskjArticleCategory deleteById(Integer id);
	
}
