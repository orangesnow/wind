
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjSubjectReply;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjSubjectReplyDao {
	
	public Pagination getPage(Integer subjectid,String staDate,String endDate,int pageNo, int pageSize);
	
	public JskjSubjectReply findById(Integer id);

	public JskjSubjectReply save(JskjSubjectReply bean);

	public JskjSubjectReply updateByUpdater(Updater<JskjSubjectReply> updater);

	public JskjSubjectReply deleteById(Integer id);
	
}
