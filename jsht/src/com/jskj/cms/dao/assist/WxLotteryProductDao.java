
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxLotteryProduct;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 抽奖产品信息
 * @author VAIO
 * @since 2014-07-14
 */
public interface WxLotteryProductDao {

	public Pagination getPage(Integer status, Integer companyId,int pageNo, int pageSize);

	public WxLotteryProduct findById(Integer id);

	public WxLotteryProduct save(WxLotteryProduct bean);

	public WxLotteryProduct updateByUpdater(Updater<WxLotteryProduct> updater);

	public WxLotteryProduct deleteById(Integer id);

	
}
