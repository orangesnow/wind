
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.CmsWxMessage;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信消息接口
 * @author VAIO
 * @since 2014-06-03
 */
public interface CmsWxMessageDao {
	
	public static final int ORDERBY_OPRTIME = 1;
	public static final int ORDERBY_REPLYTIME = 2;
	
	public Pagination getPage(Integer status, String openid, Integer pubType,Integer orderBy,int pageNo,
			int pageSize);

	public CmsWxMessage findById(String id);

	public CmsWxMessage save(CmsWxMessage bean);

	public CmsWxMessage updateByUpdater(Updater<CmsWxMessage> updater);

	public CmsWxMessage deleteById(String id);

	public int countByCond(Integer status, String openid);
	
}
