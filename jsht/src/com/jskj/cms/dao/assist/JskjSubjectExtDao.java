
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjSubjectExt;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjSubjectExtDao {

	public JskjSubjectExt findById(Integer id);

	public JskjSubjectExt save(JskjSubjectExt bean);

	public JskjSubjectExt updateByUpdater(Updater<JskjSubjectExt> updater);

	public JskjSubjectExt deleteById(Integer id);
	
}
