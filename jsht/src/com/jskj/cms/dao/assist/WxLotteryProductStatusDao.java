
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxLotteryProductStatus;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 抽奖产品信息
 * @author VAIO
 * @since 2014-07-14
 */
public interface WxLotteryProductStatusDao {

	public Pagination getPage(Integer status, Integer productId,int pageNo, int pageSize);

	public WxLotteryProductStatus findById(Integer id);

	public WxLotteryProductStatus save(WxLotteryProductStatus bean);

	public WxLotteryProductStatus updateByUpdater(Updater<WxLotteryProductStatus> updater);

	public WxLotteryProductStatus deleteById(Integer id);
	
}
