
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxSyncResult;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxSyncResultDao {
	

	public Pagination getPage(Integer stype,int pageNo, int pageSize);
	
	public WxSyncResult findById(Integer id);

	public WxSyncResult save(WxSyncResult bean);

	public WxSyncResult updateByUpdater(Updater<WxSyncResult> updater);

	public WxSyncResult deleteById(Integer id);
	
}
