
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjCoupon;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjCouponDao {
	
	public Pagination getPage(String name,String staDate, String endDate, Integer status,int pageNo, int pageSize);
	
	public JskjCoupon findById(Integer id);

	public JskjCoupon save(JskjCoupon bean);

	public JskjCoupon updateByUpdater(Updater<JskjCoupon> updater);

	public JskjCoupon deleteById(Integer id);
	
}
