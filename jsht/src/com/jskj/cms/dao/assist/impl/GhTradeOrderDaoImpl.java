package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.GhTradeOrderDao;
import com.jskj.cms.entity.assist.GhTradeOrder;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;

public class GhTradeOrderDaoImpl extends
	HibernateBaseDao<GhTradeOrder, Integer> implements GhTradeOrderDao {

	
	@Override
	public GhTradeOrder findById(Integer id) {
		GhTradeOrder service = super.get(id);
		return service;
	}

	@Override
	public GhTradeOrder save(GhTradeOrder bean) {
		Integer nbean = (Integer)getSession().save(bean);
		System.out.println("nbean:"+nbean);
		return bean;
	}

	@Override
	public GhTradeOrder deleteById(Integer id) {
		GhTradeOrder entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhTradeOrder> getEntityClass() {
		return GhTradeOrder.class;
	}

	@Override
	public List<GhTradeOrder> findByGhId(String ghid) {
		Finder finder = Finder.create("from GhTradeOrder bean where 1=1");
		finder.append(" and bean.ghid =:ghid ");
		finder.setParam("ghid", ghid);
		return find(finder);
	}

}
