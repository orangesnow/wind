
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjItemTagDao;
import com.jskj.cms.entity.assist.JskjItemTag;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjItemTagDaoImpl extends
	HibernateBaseDao<JskjItemTag, Integer> implements JskjItemTagDao {

	@Override
	protected Class<JskjItemTag> getEntityClass() {
		return JskjItemTag.class;
	}

	@Override
	public JskjItemTag findById(Integer id) {
		JskjItemTag service = super.get(id);
		return service;
	}

	@Override
	public JskjItemTag save(JskjItemTag bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjItemTag deleteById(Integer id) {
		JskjItemTag entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public List<JskjItemTag> getList(Integer itemid) {
		
		Finder f = Finder.create("from JskjItemTag bean where 1=1");
		f.append(" and bean.item.id = :itemid");
		f.setParam("itemid",itemid);
		
		List<JskjItemTag> itemTags =  find(f);
		return itemTags;
	}

	

}
