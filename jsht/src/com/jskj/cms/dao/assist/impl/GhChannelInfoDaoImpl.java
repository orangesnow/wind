package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.GhChannelInfoDao;
import com.jskj.cms.entity.assist.GhChannelInfo;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhChannelInfoDaoImpl extends
	HibernateBaseDao<GhChannelInfo, Integer> implements GhChannelInfoDao {

	@Override
	public List getList() {
		
		Finder f = Finder.create("from GhChannelInfo bean where 1=1");
		f.append(" order by bean.pcode asc");
		
		return find(f);
	
	}

	@Override
	public GhChannelInfo findById(Integer id) {
		GhChannelInfo service = super.get(id);
		return service;
	}

	@Override
	public GhChannelInfo save(GhChannelInfo bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhChannelInfo deleteById(Integer id) {
		GhChannelInfo entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhChannelInfo> getEntityClass() {
		return GhChannelInfo.class;
	}

	@Override
	public Pagination getPage(String name, String pcode, int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from GhChannelInfo bean where 1=1");
		
		if(name != null && !name.equals("")){
			f.append(" and instr(bean.name,:name)>0");
			f.setParam("name",name);
		}
		
		if(pcode != null && !pcode.equals("")){
			f.append(" and instr(bean.pcode,:pcode)>0");
			f.setParam("pcode",pcode);
		}
		
		f.append(" order by bean.pcode asc");
		
		return find(f,pageNo, pageSize);
		
	}

}
