package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.CmsWxMenuDao;
import com.jskj.cms.entity.assist.WxMenuList;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

@Repository
public class CmsWxMenuDaoImpl extends HibernateBaseDao<WxMenuList, Integer>
		implements CmsWxMenuDao {

	public Pagination getPage(Integer isdisplay, int pageNo,
			int pageSize, boolean cacheable,Integer pubType) {
		Finder f = Finder.create("from WxMenuList bean where 1=1");

		if(isdisplay != null ){
			f.append(" and bean.isDisplay =:isdisplay ");
			f.setParam("isdisplay", isdisplay);
		} 
		
		if(pubType != null){
			f.append(" and bean.pub.id=:pubType");
			f.setParam("pubType", pubType);
		}
		
		f.append(" order by bean.groupid asc,bean.weight asc");
		return find(f, pageNo, pageSize);
	}

	public List<WxMenuList> getList(int menuLevel,boolean flag,Integer pubType){
		StringBuffer hql = new StringBuffer("from WxMenuList as bean where bean.isDisplay = 1 and bean.status=1 ");
		
		if(menuLevel > 0){
			
			if(flag){
				hql.append(" and bean.level <= ").append(menuLevel);
			} else {
				hql.append(" and bean.level = ").append(menuLevel);
			}
			
		}
		
		if(pubType !=null){
			hql.append(" and bean.pub.id=").append(pubType);
		}
		
		hql.append("  order by bean.groupid asc,bean.weight asc");
		
		Finder finder = Finder.create(hql.toString());
		return find(finder);
	}
	
	
	public WxMenuList findById(Integer id) {
		WxMenuList entity = get(id);
		return entity;
	}

	public WxMenuList save(WxMenuList bean) {
		getSession().save(bean);
		return bean;
	}

	public WxMenuList deleteById(Integer id) {
		WxMenuList entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	public int countByIsDisplay(Integer isDisplay,Integer pubType) {
		
		StringBuffer sql = new StringBuffer();
		sql.append("select count(*) from WxMenuList bean where 1=1 ");
		
		if(isDisplay != null){
			sql.append(" and bean.isDisplay=").append(isDisplay);
		}
		
		if(pubType != null){
			sql.append(" and bean.pub.id=").append(pubType);
		}
		
		Query query = getSession().createQuery(sql.toString());
	
		return ((Number) query.iterate().next()).hashCode();
	}

	@Override
	protected Class<WxMenuList> getEntityClass() {
		return WxMenuList.class;
	}


}