
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxSyncResultDao;
import com.jskj.cms.entity.assist.WxSyncResult;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class WxSyncResultDaoImpl extends
	HibernateBaseDao<WxSyncResult, Integer> implements WxSyncResultDao {

	
	@Override
	protected Class<WxSyncResult> getEntityClass() {
		return WxSyncResult.class;
	}

	@Override
	public WxSyncResult findById(Integer id) {
		WxSyncResult service = super.get(id);
		return service;
	}

	@Override
	public WxSyncResult save(WxSyncResult bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxSyncResult deleteById(Integer id) {
		WxSyncResult entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(Integer stype,int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from WxSyncResult bean where 1=1");
		
		if(stype != null && stype>-1){
			f.append(" and bean.rule.stype.id =:stype");
			f.setParam("stype", stype);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
	}
	
}
