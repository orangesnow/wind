
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.WxQrcodeInfoDao;
import com.jskj.cms.entity.assist.WxQrcodeInfo;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public class WxQrcodeInfoDaoImpl extends
	HibernateBaseDao<WxQrcodeInfo, Integer> implements WxQrcodeInfoDao {

	@Override
	public List<WxQrcodeInfo> getList(Integer typeid) {
		Finder f = Finder.create(" from WxQrcodeInfo bean where 1=1 ");
		if(typeid != null && typeid>0){
			f.append(" and bean.qrcodeType.id =:typeid ");
			f.setParam("typeid", typeid);
		}
		
		f.append(" order by bean.id desc ");
		return find(f);
	}
	
	@Override
	protected Class<WxQrcodeInfo> getEntityClass() {
		return WxQrcodeInfo.class;
	}

	@Override
	public WxQrcodeInfo findById(Integer id) {
		WxQrcodeInfo service = super.get(id);
		return service;
	}

	@Override
	public WxQrcodeInfo save(WxQrcodeInfo bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxQrcodeInfo deleteById(Integer id) {
		WxQrcodeInfo entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(Integer typeId, int pageNo,
			int pageSize, boolean cacheable) {
		
		Finder f = Finder.create("from WxQrcodeInfo bean where 1=1");
		
		
		if(typeId != null && typeId>0){
			f.append(" and bean.qrcodeType.id =:typeId ");
			f.setParam("typeId", typeId);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
	}
	
}
