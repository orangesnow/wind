
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjSubjectGroupDao;
import com.jskj.cms.entity.assist.JskjSubjectGroup;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjSubjectGroupDaoImpl extends
	HibernateBaseDao<JskjSubjectGroup, Integer> implements JskjSubjectGroupDao {

	@Override
	protected Class<JskjSubjectGroup> getEntityClass() {
		return JskjSubjectGroup.class;
	}

	@Override
	public JskjSubjectGroup findById(Integer id) {
		JskjSubjectGroup service = super.get(id);
		return service;
	}

	@Override
	public JskjSubjectGroup save(JskjSubjectGroup bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjSubjectGroup deleteById(Integer id) {
		JskjSubjectGroup entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name, Integer status, int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjSubjectGroup bean where 1=1");
		
		if( name != null && !name.equals("") ){
			f.append(" and instr(bean.name,:name) >0 ");
			f.setParam("name", name);
		}
		
		if( status != null && status > -1 ){
			f.append(" and bean.status =:status");
			f.setParam("status", status);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
		
	}

	@Override
	public List<JskjSubjectGroup> findAllGroups(Integer status) {

		Finder f = Finder.create("from JskjSubjectGroup bean where 1=1");
		if(status != null && status >-1) {
			f.append(" and bean.status=:status");
			f.setParam("status", status);
		}
		
		return find(f);
	}

}
