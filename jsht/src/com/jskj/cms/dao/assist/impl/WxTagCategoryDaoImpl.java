
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxTagCategoryDao;
import com.jskj.cms.entity.assist.WxTagCategory;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class WxTagCategoryDaoImpl extends
	HibernateBaseDao<WxTagCategory, Integer> implements WxTagCategoryDao {

	@Override
	public List<WxTagCategory> getList(Integer rootId, Integer status) {
		Finder f = Finder.create(" from WxTagCategory bean where 1=1 ");
		
		if( rootId != null && rootId>0 ){
			f.append(" and bean.rootId =:rootId ");
			f.setParam("rootId", rootId);
		}
		
		if( status != null && status>0 ){
			f.append(" and bean.status =:status ");
			f.setParam("status", status);
		}
		
		f.append(" order by bean.id asc ");
		
		return find(f);
	}
	
	@Override
	protected Class<WxTagCategory> getEntityClass() {
		return WxTagCategory.class;
	}

	@Override
	public WxTagCategory findById(Integer id) {
		WxTagCategory service = super.get(id);
		return service;
	}

	@Override
	public WxTagCategory save(WxTagCategory bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxTagCategory deleteById(Integer id) {
		WxTagCategory entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(Integer rootId, Integer status,int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from WxTagCategory bean where 1=1");
		
		
		if(rootId != null && rootId>0){
			f.append(" and bean.rootId =:rootId ");
			f.setParam("rootId", rootId);
		}
		
		if(status != null && status>0){
			f.append(" and bean.status =:status ");
			f.setParam("status", status);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
	}
	
}
