
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjShortMessageDao;
import com.jskj.cms.entity.assist.JskjShortMessage;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjShortMessageDaoImpl extends
	HibernateBaseDao<JskjShortMessage, Integer> implements JskjShortMessageDao {

	@Override
	protected Class<JskjShortMessage> getEntityClass() {
		return JskjShortMessage.class;
	}

	@Override
	public JskjShortMessage findById(Integer id) {
		JskjShortMessage service = super.get(id);
		return service;
	}

	@Override
	public JskjShortMessage save(JskjShortMessage bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjShortMessage deleteById(Integer id) {
		JskjShortMessage entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name,String staDate,String endDate, Integer status, int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjShortMessage bean where 1=1");

		if(name != null && !name.equals("")){
			f.append(" and instr(bean.content,:name)>0");
			f.setParam("name", name);
		}
		
		if( status != null && status > -1 ){
			f.append(" and bean.status=:status");
			f.setParam("status", status);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.oprtime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.oprtime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
		
	}

}
