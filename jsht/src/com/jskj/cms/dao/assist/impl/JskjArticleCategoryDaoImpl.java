
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjArticleCategoryDao;
import com.jskj.cms.entity.assist.JskjArticleCategory;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjArticleCategoryDaoImpl extends
	HibernateBaseDao<JskjArticleCategory, Integer> implements JskjArticleCategoryDao {

	@Override
	protected Class<JskjArticleCategory> getEntityClass() {
		return JskjArticleCategory.class;
	}

	@Override
	public JskjArticleCategory findById(Integer id) {
		JskjArticleCategory service = super.get(id);
		return service;
	}

	@Override
	public JskjArticleCategory save(JskjArticleCategory bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjArticleCategory deleteById(Integer id) {
		JskjArticleCategory entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name,Integer parentid,Integer status,int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjArticleCategory bean where 1=1");
		
		if( name != null && !name.equals("") ){
			f.append(" and instr(bean.title,:title) >0 ");
			f.setParam("title", name);
		}
		
		if(parentid != null && parentid >0){
			f.append(" and bean.parent.id=:parentid");
			f.setParam("parentid", parentid);
		}
	
		if(status != null && status >-1){
			f.append(" and bean.status=:status");
			f.setParam("status", status);
		}
		
		f.append("order by bean.id desc");
		
		return find(f,pageNo,pageSize);
		
	}

	@Override
	public List<JskjArticleCategory> getList(String name, Integer parentid,
			Integer status) {
		
		Finder f = Finder.create("from JskjArticleCategory bean where 1=1");
		
		if( name != null && !name.equals("") ){
			f.append(" and instr(bean.name,:name) >0 ");
			f.setParam("name", name);
		}
		
		if(parentid != null && parentid >0){
			f.append(" and bean.parent.id=:parentid");
			f.setParam("parentid", parentid);
		} else {
			f.append(" and bean.clevel=:clevel");
			f.setParam("clevel",1);
		}
	
		if(status != null && status >-1){
			f.append(" and bean.status=:status");
			f.setParam("status", status);
		}
		
		f.append(" order by bean.id desc");

		return find(f);
	}


}
