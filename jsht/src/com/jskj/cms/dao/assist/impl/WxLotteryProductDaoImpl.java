
package com.jskj.cms.dao.assist.impl;

import com.jskj.cms.dao.assist.WxLotteryProductDao;
import com.jskj.cms.entity.assist.WxLotteryProduct;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class WxLotteryProductDaoImpl extends HibernateBaseDao<WxLotteryProduct, Integer> 
	implements WxLotteryProductDao {

	@Override
	public Pagination getPage(Integer status, Integer companyId, int pageNo, int pageSize) {
		Finder finder = Finder.create(" from  WxLotteryProduct bean where 1=1");
		if(status != null){
			finder.append(" and bean.status=:status");
			finder.setParam("status", status);
		} else {
			finder.append(" and bean.status<>-1");
		}
		
		if(companyId != null){
			finder.append(" and bean.company.id=:id");
			finder.setParam("id",companyId);
		}
		
		finder.append(" order by bean.id desc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public WxLotteryProduct findById(Integer id) {
		WxLotteryProduct company =  super.get(id);
		return company;
	}

	@Override
	public WxLotteryProduct save(WxLotteryProduct bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxLotteryProduct deleteById(Integer id) {
		WxLotteryProduct entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<WxLotteryProduct> getEntityClass() {
		return WxLotteryProduct.class;
	}

}
