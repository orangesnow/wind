package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.GhBindLeaveDao;
import com.jskj.cms.entity.assist.GhBindLeave;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhBindLeaveDaoImpl extends
	HibernateBaseDao<GhBindLeave, Integer> implements GhBindLeaveDao {

	@Override
	public GhBindLeave findById(Integer id) {
		GhBindLeave service = super.get(id);
		return service;
	}

	@Override
	public GhBindLeave save(GhBindLeave bean) {
		Integer nbean = (Integer)getSession().save(bean);
		System.out.println("nbean:"+nbean);
		return bean;
	}

	@Override
	public GhBindLeave deleteById(Integer id) {
		GhBindLeave entity = super.get(id);
		System.out.println("+++++++++++++0++++++++++++++++++");
		System.out.println(entity != null);
		if (entity != null) {
			System.out.println("++++++++1+++++++++++++++++++++++");
			getSession().delete(entity);
			System.out.println("+++++++++2++++++++++++++++++++++");
		}
		return entity;
	}

	@Override
	protected Class<GhBindLeave> getEntityClass() {
		return GhBindLeave.class;
	}

	@Override
	public List<GhBindLeave> findByGhId(String ghid) {
		Finder finder = Finder.create("from GhBindLeave bean where 1=1");
		finder.append(" and bean.ghid =:ghid ");
		finder.setParam("ghid", ghid);
		
		@SuppressWarnings("unchecked")
		List<GhBindLeave> bindLeaves = find(finder);
		
		if(bindLeaves != null && bindLeaves.size() > 0){
			return bindLeaves;
		}
		
		return null;
	}
	
	@Override
	public Pagination getPage(String ghid,int pageNo, int pageSize)  {
		Finder finder = Finder.create("from GhBindLeave bean where 1=1");
		finder.append(" and bean.ghid =:ghid ");
		finder.setParam("ghid", ghid);
		
		return find(finder,pageNo,pageSize);

	}

	@Override
	public GhBindLeave findByLeaveId(Integer leaveId) {
		Finder finder = Finder.create("from GhBindLeave bean where 1=1");
		finder.append(" and bean.leave.id =:leaveId ");
		finder.setParam("leaveId", leaveId);
		
		@SuppressWarnings("unchecked")
		List<GhBindLeave> bindLeaves = find(finder);
		
		if(bindLeaves != null && bindLeaves.size() > 0){
			return bindLeaves.get(0);
		}
		
		return null;
	}

}
