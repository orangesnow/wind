
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjSubjectReplyDao;
import com.jskj.cms.entity.assist.JskjSubjectReply;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjSubjectReplyDaoImpl extends
	HibernateBaseDao<JskjSubjectReply, Integer> implements JskjSubjectReplyDao {

	@Override
	protected Class<JskjSubjectReply> getEntityClass() {
		return JskjSubjectReply.class;
	}

	@Override
	public JskjSubjectReply findById(Integer id) {
		JskjSubjectReply service = super.get(id);
		return service;
	}

	@Override
	public JskjSubjectReply save(JskjSubjectReply bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjSubjectReply deleteById(Integer id) {
		JskjSubjectReply entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(Integer subjectid,String staDate,String endDate,int pageNo, int pageSize){
	
		Finder f = Finder.create("from JskjSubjectReply bean where 1=1");
	
		if(subjectid != null && subjectid >-1) {
			f.append(" and bean.subject.id=:subjectid");
			f.setParam("subjectid", subjectid);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.oprtime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.oprtime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		f.append(" order by bean.id desc");
		
		return find(f,pageNo,pageSize);
		
	}

}
