
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxTagListDao;
import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class WxTagListDaoImpl extends
	HibernateBaseDao<WxTagList, Integer> implements WxTagListDao {

	
	@Override
	protected Class<WxTagList> getEntityClass() {
		return WxTagList.class;
	}

	@Override
	public WxTagList findById(Integer id) {
		WxTagList service = super.get(id);
		return service;
	}

	@Override
	public WxTagList save(WxTagList bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxTagList deleteById(Integer id) {
		WxTagList entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name,int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from WxTagList bean where 1=1");
		
		if(name != null && !name.equals("")){
			f.append(" and instr(bean.name,:name)>0");
			f.setParam("name", name);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
	}
	
	@Override
	public Pagination getPage(String name,Integer cid,int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from WxTagList bean where 1=1");
		
		if(name != null && !name.equals("")){
			f.append(" and instr(bean.name,:name)>0");
			f.setParam("name", name);
		}
		
		if(cid != null){
			f.append(" and bean.category.id=:cid");
			f.setParam("cid",cid);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
	}

	@Override
	public WxTagList findByName(String tagName) {
		Finder f = Finder.create("from WxTagList bean where 1=1");
		
		if(tagName != null && !tagName.equals("")){
			f.append(" and instr(bean.name,:name)>0");
			f.setParam("name", tagName);
		}
		
		
		List<WxTagList> tags =  find(f);
		if(tags != null && tags.size() >0){
			return tags.get(0);
		}
		return null;
	}

	@Override
	public List<WxTagList> getList(){
		Finder f = Finder.create("from WxTagList bean where 1=1");
		List<WxTagList> tags =  find(f);
		return tags;
		
	}

	@Override
	public WxTagList findByName(String tagName, boolean single) {
		Finder f = Finder.create("from WxTagList bean where 1=1");
		
		if(single){
			if(tagName != null && !tagName.equals("")){
				f.append(" and bean.name=:name");
				f.setParam("name", tagName);
			}
		}
		
		List<WxTagList> tags =  find(f);
		if(tags != null && tags.size() >0){
			return tags.get(0);
		}
		return null;
	}

	@Override
	public int deleteUserRef(Integer tagId) {
		Query query = getSession().getNamedQuery("WxTagList.deleteUserRef");
		return query.setParameter(0, tagId).executeUpdate();
	}
	
	@Override
	public int deleteGroupRef(Integer tagId) {
		Query query = getSession().getNamedQuery("WxTagList.deleteGroupRef");
		return query.setParameter(0, tagId).executeUpdate();
	}
	
}