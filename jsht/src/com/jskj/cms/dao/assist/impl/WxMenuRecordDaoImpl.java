package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxMenuRecordDao;
import com.jskj.cms.entity.assist.WxMenuRecord;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

@Repository
public class WxMenuRecordDaoImpl extends HibernateBaseDao<WxMenuRecord, Integer>
		implements WxMenuRecordDao {

	

	@Override
	protected Class<WxMenuRecord> getEntityClass() {
		return WxMenuRecord.class;
	}

	@Override
	public WxMenuRecord findById(Integer id) {
		WxMenuRecord entity = super.get(id);
		return entity;
	}

	@Override
	public WxMenuRecord save(WxMenuRecord bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxMenuRecord deleteById(Integer id) {
		WxMenuRecord entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String fromUsername, Integer pubType, int pageNo,
			int pageSize) {
		
		Finder finder = Finder.create();
		finder.append(" from WxMenuRecord bean where 1=1");
		
		if(fromUsername != null && !fromUsername.equals("")){
			finder.append(" and bean.fromUsername=:fromUsername");
			finder.setParam("fromUsername", fromUsername);
		}
		
		if(pubType != null && pubType>-1){
			finder.append(" and bean.account.id=:pubType");
			finder.setParam("pubType", pubType);
		}
		
		finder.append(" order by bean.oprtime desc");
		
		return find(finder, pageNo, pageSize);
	}

}