
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.WxQrcodeTypeDao;
import com.jskj.cms.entity.assist.WxQrcodeType;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
public class WxQrcodeTypeDaoImpl extends
	HibernateBaseDao<WxQrcodeType, Integer> implements WxQrcodeTypeDao {

	@Override
	public List<WxQrcodeType> getList() {
		Finder f = Finder.create(" from WxQrcodeType bean where 1=1 ");
		f.append(" order by bean.id asc ");
		return find(f);
	}
	
	@Override
	protected Class<WxQrcodeType> getEntityClass() {
		return WxQrcodeType.class;
	}

	@Override
	public WxQrcodeType findById(Integer id) {
		WxQrcodeType service = super.get(id);
		return service;
	}

	@Override
	public WxQrcodeType save(WxQrcodeType bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxQrcodeType deleteById(Integer id) {
		WxQrcodeType entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(int pageNo,
			int pageSize, boolean cacheable) {
		
		Finder f = Finder.create("from WxQrcodeType bean where 1=1");
		f.append(" order by bean.id asc");
		return find(f,pageNo,pageSize);
	}
	
}
