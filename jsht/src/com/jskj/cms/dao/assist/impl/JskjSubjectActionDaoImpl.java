
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjSubjectActionDao;
import com.jskj.cms.entity.assist.JskjSubjectAction;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjSubjectActionDaoImpl extends
	HibernateBaseDao<JskjSubjectAction, Integer> implements JskjSubjectActionDao {

	@Override
	protected Class<JskjSubjectAction> getEntityClass() {
		return JskjSubjectAction.class;
	}

	@Override
	public JskjSubjectAction findById(Integer id) {
		JskjSubjectAction service = super.get(id);
		return service;
	}

	@Override
	public JskjSubjectAction save(JskjSubjectAction bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjSubjectAction deleteById(Integer id) {
		JskjSubjectAction entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String staDate,String endDate, Integer atype, int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjSubjectAction bean where 1=1");
	
		if( atype != null && atype > -1 ){
			f.append(" and bean.atype =:atype");
			f.setParam("atype", atype);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.oprtime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.oprtime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
		
	}

}
