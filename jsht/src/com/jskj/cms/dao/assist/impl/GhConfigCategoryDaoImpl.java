package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.GhConfigCategoryDao;
import com.jskj.cms.entity.assist.GhConfigCategory;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;

public class GhConfigCategoryDaoImpl extends
	HibernateBaseDao<GhConfigCategory, Integer> implements GhConfigCategoryDao {

	@Override
	public List<GhConfigCategory> getList() {
		Finder f = Finder.create("from GhConfigCategory bean where 1=1");
		return find(f);
	}

	@Override
	public GhConfigCategory findById(Integer id) {
		GhConfigCategory service = super.get(id);
		return service;
	}

	@Override
	public GhConfigCategory save(GhConfigCategory bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhConfigCategory deleteById(Integer id) {
		GhConfigCategory entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhConfigCategory> getEntityClass() {
		return GhConfigCategory.class;
	}

}
