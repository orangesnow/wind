
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjAreaDao;
import com.jskj.cms.entity.assist.JskjArea;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjAreaDaoImpl extends
	HibernateBaseDao<JskjArea, Integer> implements JskjAreaDao {

	@Override
	protected Class<JskjArea> getEntityClass() {
		return JskjArea.class;
	}

	@Override
	public JskjArea findById(Integer id) {
		JskjArea service = super.get(id);
		return service;
	}

	@Override
	public JskjArea save(JskjArea bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjArea deleteById(Integer id) {
		JskjArea entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name,Integer orderBy,Integer status,int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjArea bean where 1=1");
		
		if( name != null && !name.equals("") ){
			f.append(" and instr(bean.name,:name) >0 ");
			f.setParam("name", name);
		}
		
		if( status != null  && status >-1){
			f.append(" and bean.status =:status");
			f.setParam("status", status);
		}
		
		if(orderBy == null){
			f.append(" order by bean.id desc");
		} else if(orderBy == 1){
			f.append(" order by bean.istop desc");
		} else if(orderBy == 2){
			f.append(" order by bean.istop asc");
		} else if(orderBy == 3){
			f.append(" order by bean.isrecomm desc");
		} else if(orderBy == 4){
			f.append(" order by bean.isrecomm asc");
		}
		
	
		
		return find(f,pageNo,pageSize);
		
	}

}
