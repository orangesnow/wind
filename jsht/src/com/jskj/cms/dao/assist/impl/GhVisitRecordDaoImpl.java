package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import com.jskj.cms.dao.assist.GhVisitRecordDao;
import com.jskj.cms.entity.assist.GhVisitRecord;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhVisitRecordDaoImpl extends
	HibernateBaseDao<GhVisitRecord, Integer> implements GhVisitRecordDao {

	@Override
	public Pagination getPage(Integer userId,String oid, int pageNo, int pageSize) {
		
		Finder f = Finder.create("from GhVisitRecord bean where 1=1");

		if(userId != null && userId > 0){
			f.append(" and bean.userid=:userId");
			f.setParam("userId", userId);
		} 
		
		if(oid != null && !oid.equals("")){
			f.append(" and bean.oid=:oid");
			f.setParam("oid", oid);
		}
		
		f.append(" order by bean.visitTime desc");
		return find(f, pageNo, pageSize);
	
	}


	@Override
	public GhVisitRecord findById(Integer id) {
		GhVisitRecord service = super.get(id);
		return service;
	}

	@Override
	public GhVisitRecord save(GhVisitRecord bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhVisitRecord deleteById(Integer id) {
		GhVisitRecord entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhVisitRecord> getEntityClass() {
		return GhVisitRecord.class;
	}


	@Override
	public Pagination getPage(Integer userId, String oid, String staDate,
			String endDate, int pageNo, int pageSize) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Finder f = Finder.create("from GhVisitRecord bean where 1=1");

		if(userId != null && userId > 0){
			f.append(" and bean.userid=:userId");
			f.setParam("userId", userId);
		} 
		
		if(oid != null && !oid.equals("")){
			f.append(" and bean.oid=:oid");
			f.setParam("oid", oid);
		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.visitTime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.visitTime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		f.append(" order by bean.visitTime desc");
		return find(f, pageNo, pageSize);
	}


}
