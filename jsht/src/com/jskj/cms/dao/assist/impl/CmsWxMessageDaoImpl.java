/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.hibernate.Query;

import com.jskj.cms.dao.assist.CmsWxMessageDao;
import com.jskj.cms.entity.assist.CmsWxMessage;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * 微信消息接口实现
 * @author VAIO
 * @since 2014-06-03
 */
public class CmsWxMessageDaoImpl extends
HibernateBaseDao<CmsWxMessage, String> implements CmsWxMessageDao {

	
	/* (non-Javadoc)
	 * @see com.jskj.cms.dao.assist.CmsWxMessageDao#getPage(java.lang.Integer, java.lang.String, int, int, boolean)
	 */
	@Override
	public Pagination getPage(Integer status, String openid, Integer pubType,Integer orderBy,int pageNo,
			int pageSize) {
		
		System.out.println("status:"+status+",openid:"+openid+",orderBy:"+orderBy+",pageNo:"+pageNo+",pageSize:"+pageSize);
		
		Finder f = Finder.create("from  CmsWxMessage bean where 1=1");

		if(status != null && status >-1){
			f.append(" and bean.status =:status ");
			f.setParam("status", status);
		} 
		
		if(openid != null && !openid.trim().equals("")){
			f.append(" and bean.openid=:openid");
			f.setParam("openid", openid);
		}
		
		if(pubType != null && pubType >-1){
			f.append(" and bean.account.id=:pubType");
			f.setParam("pubType", pubType);
		}
		
		if(orderBy != null && orderBy == CmsWxMessageDao.ORDERBY_REPLYTIME) {
			f.append(" order by bean.replyTime desc");
		} else {
			f.append(" order by bean.oprtime desc");
		}
	
		System.out.println("Hql================="+f.getOrigHql());
		return find(f, pageNo, pageSize);
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.dao.assist.CmsWxMessageDao#findById(java.lang.Integer)
	 */
	@Override
	public CmsWxMessage findById(String id) {
	
		CmsWxMessage message = get(id);
		return message;
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.dao.assist.CmsWxMessageDao#save(com.jskj.cms.entity.assist.CmsWxMessage)
	 */
	@Override
	public CmsWxMessage save(CmsWxMessage bean) {
		getSession().save(bean);
		return bean;
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.dao.assist.CmsWxMessageDao#deleteById(java.lang.Integer)
	 */
	@Override
	public CmsWxMessage deleteById(String id) {
		CmsWxMessage entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	/* (non-Javadoc)
	 * @see com.jskj.cms.dao.assist.CmsWxMessageDao#countByCond(java.lang.Integer, java.lang.String)
	 */
	@Override
	public int countByCond(Integer status, String openid) {
		StringBuffer hql = new StringBuffer("select count(*) from CmsWxMessage bean  where 1=1");
		
		if(status != null && status >-1){
			hql.append(" and bean.status =:status ");
		} 
		
		if(openid != null && !openid.trim().equals("")){
			hql.append(" and bean.openid=:openid");
		}

		Query query = getSession().createQuery(hql.toString());
		if(status >-1){
			query.setParameter("status", status);
		}
		if(openid != null && !openid.trim().equals("")){
			query.setParameter("openid", openid);
		}

		return ((Number) query.iterate().next()).hashCode();

	}

	@Override
	protected Class<CmsWxMessage> getEntityClass() {
	
		return CmsWxMessage.class;
	}

}
