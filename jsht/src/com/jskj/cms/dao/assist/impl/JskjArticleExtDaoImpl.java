
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjArticleExtDao;
import com.jskj.cms.entity.assist.JskjArticleExt;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjArticleExtDaoImpl extends
	HibernateBaseDao<JskjArticleExt, Integer> implements JskjArticleExtDao {

	@Override
	protected Class<JskjArticleExt> getEntityClass() {
		return JskjArticleExt.class;
	}

	@Override
	public JskjArticleExt findById(Integer id) {
		JskjArticleExt service = super.get(id);
		return service;
	}

	@Override
	public JskjArticleExt save(JskjArticleExt bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjArticleExt deleteById(Integer id) {
		JskjArticleExt entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name,Integer status,int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjArticleExt bean where 1=1");

		return find(f,pageNo,pageSize);
		
	}

}
