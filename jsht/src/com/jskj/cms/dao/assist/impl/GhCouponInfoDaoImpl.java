
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.GhCouponInfoDao;
import com.jskj.cms.entity.assist.GhCouponInfo;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * 优惠券接口实现
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class GhCouponInfoDaoImpl extends
	HibernateBaseDao<GhCouponInfo, Integer> implements GhCouponInfoDao {

	@Override
	protected Class<GhCouponInfo> getEntityClass() {
		return GhCouponInfo.class;
	}

	@Override
	public GhCouponInfo findById(Integer id) {
		GhCouponInfo service = super.get(id);
		return service;
	}

	@Override
	public GhCouponInfo save(GhCouponInfo bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhCouponInfo deleteById(Integer id) {
		GhCouponInfo entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String unionid,int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from GhCouponInfo bean where 1=1");
		
		if(unionid != null && !unionid.equals("")){
			f.append("and bean.unionid=:unionid");
			f.setParam("unionid", unionid);
		} else {
			f.append(" and 1 > 1");
		}
	
		return find(f,pageNo,pageSize);
	}

	@Override
	public GhCouponInfo findByUnionId(String unionid) {

		Finder f = Finder.create("from GhCouponInfo bean where 1=1");
		
		if(unionid != null && !unionid.equals("")){
			f.append("and bean.unionid=:unionid");
			f.setParam("unionid", unionid);
		} else {
			f.append(" and 1 > 1");
		}
		
		List<GhCouponInfo> coupons = find(f);
		if(coupons != null && coupons.size() > 0){
			return coupons.get(0);
		}
		
		return null;
	}
	
}