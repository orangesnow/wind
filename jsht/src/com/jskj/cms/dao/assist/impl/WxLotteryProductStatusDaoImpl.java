
package com.jskj.cms.dao.assist.impl;

import com.jskj.cms.dao.assist.WxLotteryProductStatusDao;
import com.jskj.cms.entity.assist.WxLotteryProductStatus;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class WxLotteryProductStatusDaoImpl extends HibernateBaseDao<WxLotteryProductStatus, Integer> 
	implements WxLotteryProductStatusDao {

	@Override
	public Pagination getPage(Integer status, Integer productId, int pageNo, int pageSize) {
		Finder finder = Finder.create(" from  WxLotteryProductStatus bean where 1=1");
		if(status != null){
			finder.append(" and bean.status=:status");
			finder.setParam("status", status);
		} else {
			finder.append(" and bean.status<>-1");
		}
		
		if(productId != null){
			finder.append(" and bean.product.id=:id");
			finder.setParam("id",productId);
		}
		
		finder.append(" order by bean.id desc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public WxLotteryProductStatus findById(Integer id) {
		WxLotteryProductStatus company =  super.get(id);
		return company;
	}

	@Override
	public WxLotteryProductStatus save(WxLotteryProductStatus bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxLotteryProductStatus deleteById(Integer id) {
		WxLotteryProductStatus entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<WxLotteryProductStatus> getEntityClass() {
		return WxLotteryProductStatus.class;
	}

}
