package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.GhConfigSourceDao;
import com.jskj.cms.entity.assist.GhConfigSource;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhConfigSourceDaoImpl extends
	HibernateBaseDao<GhConfigSource, Integer> implements GhConfigSourceDao {

	@Override
	public Pagination getPage(String name, Integer category,int pageNo, int pageSize,
			boolean cacheable) {
		Finder f = Finder.create("from GhConfigSource bean where 1=1");
		if(name != null && !name.equals("")){
			f.append(" and instr(bean.name,:name)>0 ");
			f.setParam("name", name);
		}
		
		if(category != null && category >-1){
			f.append(" and bean.category.id=:category");
			f.setParam("category", category);
		}
		
		f.append(" order by bean.id desc");
		return find(f, pageNo, pageSize);
	
	}

	@Override
	public GhConfigSource findById(Integer id) {
		GhConfigSource service = super.get(id);
		return service;
	}

	@Override
	public GhConfigSource save(GhConfigSource bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhConfigSource deleteById(Integer id) {
		GhConfigSource entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhConfigSource> getEntityClass() {
		return GhConfigSource.class;
	}

	@Override
	public List<GhConfigSource> findByCategory(Integer categoryId) {
		
		Finder f = Finder.create("from GhConfigSource bean where 1=1");

		if(categoryId != null && categoryId >-1){
			f.append(" and bean.category.id=:categoryId");
			f.setParam("categoryId", categoryId);
		}
		
		f.append(" order by bean.id desc");
		return find(f);
	}

}
