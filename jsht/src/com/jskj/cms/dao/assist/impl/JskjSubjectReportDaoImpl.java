
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjSubjectReportDao;
import com.jskj.cms.entity.assist.JskjSubjectReport;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjSubjectReportDaoImpl extends
	HibernateBaseDao<JskjSubjectReport, Integer> implements JskjSubjectReportDao {

	@Override
	protected Class<JskjSubjectReport> getEntityClass() {
		return JskjSubjectReport.class;
	}

	@Override
	public JskjSubjectReport findById(Integer id) {
		JskjSubjectReport service = super.get(id);
		return service;
	}

	@Override
	public JskjSubjectReport save(JskjSubjectReport bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjSubjectReport deleteById(Integer id) {
		JskjSubjectReport entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String staDate,String endDate,Integer rtype,int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjSubjectReport bean where 1=1");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		if(rtype != null && rtype > -1){
			f.append(" and bean.rtype=:rtype");
			f.setParam("rtype", rtype);
		}
	
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.oprtime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.oprtime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		f.append(" order by bean.oprtime desc");
		return find(f, pageNo, pageSize);

	}

}
