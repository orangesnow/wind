package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxFollowRecordDao;
import com.jskj.cms.entity.assist.WxFollowRecord;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

@Repository
public class WxFollowRecordDaoImpl extends HibernateBaseDao<WxFollowRecord, Integer>
		implements WxFollowRecordDao {


	public WxFollowRecord findById(Integer id) {
		WxFollowRecord entity = get(id);
		return entity;
	}

	public WxFollowRecord save(WxFollowRecord bean) {
		getSession().save(bean);
		return bean;
	}

	public WxFollowRecord deleteById(Integer id) {
		WxFollowRecord entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<WxFollowRecord> getEntityClass() {
		return WxFollowRecord.class;
	}

	@Override
	public Pagination getPage(String openid, Integer pubType,int pageNo,
			int pageSize) {
		Finder finder = Finder.create(" from WxFollowRecord bean where 1=1");
		
		if(openid != null ){
			finder.append(" and bean.fromUsername=:openid");
			finder.setParam("openid", openid);
		}

		if(pubType != null){
			finder.append(" and bean.account.id=:ptype");
			finder.setParam("ptype", pubType);
		}
		
		finder.append(" order by bean.oprtime desc");
		
		return find(finder, pageNo, pageSize);
	}


}