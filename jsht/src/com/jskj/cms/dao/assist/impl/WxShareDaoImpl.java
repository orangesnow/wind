
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxShareDao;
import com.jskj.cms.entity.assist.WxShare;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class WxShareDaoImpl extends
	HibernateBaseDao<WxShare, Integer> implements WxShareDao {

	
	@Override
	protected Class<WxShare> getEntityClass() {
		return WxShare.class;
	}

	@Override
	public WxShare findById(Integer id) {
		WxShare service = super.get(id);
		return service;
	}

	@Override
	public WxShare save(WxShare bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxShare deleteById(Integer id) {
		WxShare entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from WxShare bean where 1=1");
		f.append(" order by bean.id desc");
		
		return find(f,pageNo,pageSize);
	}
	
}