package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.GhProductDao;
import com.jskj.cms.entity.assist.GhProduct;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhProductDaoImpl extends
	HibernateBaseDao<GhProduct, Integer> implements GhProductDao {

	@Override
	public GhProduct findByOuterId(String outerid) {
		
		Finder f = Finder.create("from GhProduct bean where 1=1");
		if(outerid != null && !outerid.equals("")){
			f.append(" and bean.outerid=:outerid");
			f.setParam("outerid", outerid);
		}

		List<GhProduct> products = super.find(f);
		
		if(products != null && products.size() >0){
			return products.get(0);
		} else {
			return null;
		}
		
	}

	@Override
	public Pagination getPage(String name,int pageNo, int pageSize) {
		Finder f = Finder.create("from GhProduct bean where 1=1");
		
		if(name != null && !name.equals("")){
			f.append(" and instr(bean.title,:name)>0");
			f.setParam("name", name);
		}
		
		f.append(" order by bean.soldNum desc");
		return find(f,pageNo,pageSize);
	}

	@Override
	public GhProduct findById(Integer id) {
		GhProduct service = super.get(id);
		return service;
	}

	@Override
	public GhProduct save(GhProduct bean) {
		getSession().save(bean);
		return bean;
	}

	
	@Override
	protected Class<GhProduct> getEntityClass() {
		return GhProduct.class;
	}

	@Override
	public GhProduct deleteById(Integer id) {
		GhProduct entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public List<GhProduct> getList(Integer status) {
		
		Finder f = Finder.create("from GhProduct bean where 1=1");
		if(status != null && status>-1){
			f.append(" and bean.status=:status");
			f.setParam("status", status);
		}

		return super.find(f);
	}
}
