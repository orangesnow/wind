
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjItemCollectDao;
import com.jskj.cms.entity.assist.JskjItemCollect;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjItemCollectDaoImpl extends
	HibernateBaseDao<JskjItemCollect, Integer> implements JskjItemCollectDao {

	@Override
	protected Class<JskjItemCollect> getEntityClass() {
		return JskjItemCollect.class;
	}

	@Override
	public JskjItemCollect findById(Integer id) {
		JskjItemCollect service = super.get(id);
		return service;
	}

	@Override
	public JskjItemCollect save(JskjItemCollect bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjItemCollect deleteById(Integer id) {
		JskjItemCollect entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String staDate, String endDate, Integer collecttype, int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjItemCollect bean where 1=1");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.odTime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.odTime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		if( collecttype > -1 ){
			f.append(" and bean.collecttype =:collecttype");
			f.setParam("collecttype", collecttype);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
		
	}

}
