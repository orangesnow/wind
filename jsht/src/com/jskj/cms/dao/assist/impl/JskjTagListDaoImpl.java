
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjTagListDao;
import com.jskj.cms.entity.assist.JskjTagList;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjTagListDaoImpl extends
	HibernateBaseDao<JskjTagList, Integer> implements JskjTagListDao {

	
	@Override
	protected Class<JskjTagList> getEntityClass() {
		return JskjTagList.class;
	}

	@Override
	public JskjTagList findById(Integer id) {
		JskjTagList service = super.get(id);
		return service;
	}

	@Override
	public JskjTagList save(JskjTagList bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjTagList deleteById(Integer id) {
		JskjTagList entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	
	@Override
	public Pagination getPage(String name,Integer orderBy,int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from JskjTagList bean where 1=1");
		
		if(name != null && !name.equals("")){
			f.append(" and instr(bean.name,:name)>0");
			f.setParam("name", name);
		}
		
		if(orderBy != null ) {
			if(orderBy == 1) {
				f.append(" order by bean.hotlevel desc");
			} else if(orderBy == 2){
				f.append(" order by bean.hotlevel asc");
			} else if(orderBy == 3){
				f.append(" order by bean.recommlevel desc");
			} else if(orderBy == 4){
				f.append(" order by bean.recommlevel asc");
			} else {
				f.append(" order by bean.id desc");
			}
		} else {
			f.append(" order by bean.id desc");
		}
		
		return find(f,pageNo,pageSize);
	}

	@Override
	public int deleteItemRef(Integer tagId) {
		Query query = getSession().getNamedQuery("JskjTagList.deleteItemRef");
		return query.setParameter(0, tagId).executeUpdate();
	}
	
	@Override
	public int deleteTagRefFromItemId(Integer itemid) {
		Query query = getSession().getNamedQuery("JskjTagList.deleteTagRef");
		return query.setParameter(0, itemid).executeUpdate();
	}

	@Override
	public List<JskjTagList> getList(Integer status) {
		
		Finder f = Finder.create("from JskjTagList bean where 1=1");
		
		if(status != null &&status >-1){
			f.append(" and bean.status =:status");
			f.setParam("status", status);
		}
		
		List<JskjTagList> tags = find(f);
		
		return tags;
	}

}