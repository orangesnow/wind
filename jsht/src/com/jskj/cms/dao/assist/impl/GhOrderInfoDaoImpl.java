package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import com.jskj.cms.dao.assist.GhOrderInfoDao;
import com.jskj.cms.entity.assist.GhOrderInfo;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhOrderInfoDaoImpl extends
	HibernateBaseDao<GhOrderInfo, String> implements GhOrderInfoDao {

	@Override
	public Pagination getPage(String orderId,Integer source,String channelId,String staDate,String endDate, int pageNo, int pageSize,
			boolean cacheable) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Finder f = Finder.create("from GhOrderInfo bean where 1=1");
		if(orderId != null && !orderId.equals("")){
			f.append(" and instr(bean.oid,:oid)>0");
			f.setParam("oid", orderId);
		}
		
		if(source != null && source > 0){
			f.append(" and bean.source=:source");
			f.setParam("source", source);
		}
		
		if(channelId != null && !channelId.equals("")){
			f.append(" and bean.pcode=:pcode");
			f.setParam("pcode", channelId);
		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.odTime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.odTime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		f.append(" order by bean.odTime desc");
		return find(f, pageNo, pageSize);
	
	}
	
	@Override
	public Pagination getPage(Integer userId,String staDate,String endDate, int pageNo, int pageSize) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Finder f = Finder.create("from GhOrderInfo bean where 1=1");
		
		
		if(userId != null && userId > 0){
			f.append(" and bean.ownerId=:userId");
			f.setParam("userId", userId);
		} else {
			f.append(" and bean.ownerId=-1");
		}

		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.odTime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.odTime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		f.append(" order by bean.odTime desc");
		return find(f, pageNo, pageSize);
	
	}


	@Override
	public GhOrderInfo findById(String id) {
		GhOrderInfo service = super.get(id);
		return service;
	}

	@Override
	public GhOrderInfo save(GhOrderInfo bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhOrderInfo deleteById(String id) {
		GhOrderInfo entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhOrderInfo> getEntityClass() {
		return GhOrderInfo.class;
	}

	@Override
	public Pagination getPage(String orderId, Integer source,
			Integer claimFlag, Integer claimType, String staDate,
			String endDate, int pageNo, int pageSize, boolean cacheable) {
		
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Finder f = Finder.create("from GhOrderInfo bean where 1=1");
		if(orderId != null && !orderId.equals("")){
			f.append(" and instr(bean.oid,:oid)>0");
			f.setParam("oid", orderId);
		}
		
		if(source != null && source > 0){
			f.append(" and bean.source=:source");
			f.setParam("source", source);
		}
		
		if(claimFlag != null && claimFlag >0){
			f.append(" and bean.claimFlag=1");
		}else {
			f.append(" and bean.claimFlag=0");
		}
		
		if(claimType != null && claimType >-1){
			f.append(" and bean.claimType=:claimType");
			f.setParam("claimType", claimType);
		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.odTime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.odTime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		f.append(" order by bean.odTime desc");
		return find(f, pageNo, pageSize);
	}

}
