
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.SfuDataCompanyDao;
import com.jskj.cms.entity.assist.SfuDataCompany;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

@Repository
public class SfuDataCompanyDaoImpl extends HibernateBaseDao<SfuDataCompany, Integer>  
implements SfuDataCompanyDao {
	
	public SfuDataCompany findById(Integer id) {
		SfuDataCompany config = get(id);
		return config;
	}

	public SfuDataCompany save(SfuDataCompany bean){
		getSession().save(bean);
		return bean;
	}
	
	public SfuDataCompany deleteById(Integer id){
		SfuDataCompany entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<SfuDataCompany> getEntityClass() {
		return SfuDataCompany.class;
	}


	@Override
	public Pagination getPage(String city,
			int pageNo, int pageSize) {
		
		Finder f = Finder.create(" from SfuDataCompany bean where 1=1 ");

		if(city != null && !city.equals("")){
			f.append(" and instr(bean.city,:city)>0");
			f.setParam("city", city);
		}
		
		f.append(" order by bean.companyId desc");
		
		return find(f,pageNo,pageSize);
	}


}
