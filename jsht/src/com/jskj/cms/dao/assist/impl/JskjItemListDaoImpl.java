
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjItemListDao;
import com.jskj.cms.entity.assist.JskjItemList;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjItemListDaoImpl extends
	HibernateBaseDao<JskjItemList, Integer> implements JskjItemListDao {

	@Override
	protected Class<JskjItemList> getEntityClass() {
		return JskjItemList.class;
	}

	@Override
	public JskjItemList findById(Integer id) {
		JskjItemList service = super.get(id);
		return service;
	}

	@Override
	public JskjItemList save(JskjItemList bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjItemList deleteById(Integer id) {
		JskjItemList entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name,Integer status,Integer cid, Integer rtype,int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjItemList bean where 1=1");

		if( name != null && !name.equals("") ) {
			f.append(" and  ( instr(bean.title,:name)>0");
			f.append("  or  instr(bean.remark,:name)>0 )");
			f.setParam("name", name);
		}
		
		if(status != null && status >-1){
			f.append(" and bean.status = :status");
			f.setParam("status", status);
		}
		
		if(cid != null && cid >-1){
			f.append(" and bean.category.id = :cid");
			f.setParam("cid", cid);
		}
		
		if(rtype == null) {
			f.append(" order by created desc");
		} else if(rtype == 1){
			f.append(" order by created asc ");
		} else if(rtype == 2){
			f.append(" order by created desc");
		} else if(rtype == 3){
			f.append(" order by soldweight desc,soldnum desc");
		}else if(rtype == 4){
			f.append(" order by soldweight desc,soldnum asc");
		} else if(rtype == 5){
			f.append(" order by price desc");
		} else if(rtype == 6){
			f.append(" order by price asc");
		} else if(rtype == 7){
			f.append(" order by recommweight desc");
		}else if(rtype == 8){
			f.append(" order by recommweight asc");
		}

		return find(f,pageNo,pageSize);
		
	}

}
