
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjUserDao;
import com.jskj.cms.entity.assist.JskjUser;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjUserDaoImpl extends
	HibernateBaseDao<JskjUser, Integer> implements JskjUserDao {

	@Override
	protected Class<JskjUser> getEntityClass() {
		return JskjUser.class;
	}

	@Override
	public JskjUser findById(Integer id) {
		JskjUser service = super.get(id);
		return service;
	}

	@Override
	public JskjUser save(JskjUser bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjUser deleteById(Integer id) {
		JskjUser entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String staDate,String endDate,String nickname, int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjUser bean where 1=1");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.logintime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.logintime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if( nickname != null && !nickname.equals("") ){
			f.append(" and instr(bean.nickname,:nickname)>0");
			f.setParam("nickname", nickname);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
		
	}

}
