
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjAdviceDao;
import com.jskj.cms.entity.assist.JskjAdvice;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjAdviceDaoImpl extends
	HibernateBaseDao<JskjAdvice, Integer> implements JskjAdviceDao {

	@Override
	protected Class<JskjAdvice> getEntityClass() {
		return JskjAdvice.class;
	}

	@Override
	public JskjAdvice findById(Integer id) {
		JskjAdvice service = super.get(id);
		return service;
	}

	@Override
	public JskjAdvice save(JskjAdvice bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjAdvice deleteById(Integer id) {
		JskjAdvice entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjAdvice bean where 1=1");

		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
		
	}

}
