
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjVersionDao;
import com.jskj.cms.entity.assist.JskjVersion;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjVersionDaoImpl extends
	HibernateBaseDao<JskjVersion, Integer> implements JskjVersionDao {

	@Override
	protected Class<JskjVersion> getEntityClass() {
		return JskjVersion.class;
	}

	@Override
	public JskjVersion findById(Integer id) {
		JskjVersion service = super.get(id);
		return service;
	}

	@Override
	public JskjVersion save(JskjVersion bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjVersion deleteById(Integer id) {
		JskjVersion entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjVersion bean where 1=1");
		f.append(" order by bean.appid desc");
		return find(f,pageNo,pageSize);
		
	}

}
