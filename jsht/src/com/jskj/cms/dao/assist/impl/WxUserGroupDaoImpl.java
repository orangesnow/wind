
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxUserGroupDao;
import com.jskj.cms.entity.assist.WxUserGroup;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class WxUserGroupDaoImpl extends
	HibernateBaseDao<WxUserGroup, Integer> implements WxUserGroupDao {

	@Override
	public List<WxUserGroup> getList(Integer accountId) {
		Finder f = Finder.create(" from WxUserGroup bean where 1=1 ");
		if(accountId != null && accountId>0){
			f.append(" and bean.account.id =:accountId ");
			f.setParam("accountId", accountId);
		}
		
		f.append(" order by bean.groupid asc ");
		return find(f);
	}
	
	@Override
	protected Class<WxUserGroup> getEntityClass() {
		return WxUserGroup.class;
	}

	@Override
	public WxUserGroup findById(Integer id) {
		WxUserGroup service = super.get(id);
		return service;
	}

	@Override
	public WxUserGroup save(WxUserGroup bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxUserGroup deleteById(Integer id) {
		WxUserGroup entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(Integer accountId, int pageNo,
			int pageSize, boolean cacheable) {
		
		Finder f = Finder.create("from WxUserGroup bean where 1=1");
		
		
		if(accountId != null && accountId>0){
			f.append(" and bean.account.id =:accountId ");
			f.setParam("accountId", accountId);
		}
		
		f.append(" order by bean.groupid asc");
		return find(f,pageNo,pageSize);
	}
	
}
