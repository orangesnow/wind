
package com.jskj.cms.dao.assist.impl;

import com.jskj.cms.dao.assist.WxLotteryUserArticleDao;
import com.jskj.cms.entity.assist.WxLotteryUserArticle;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class WxLotteryUserArticleDaoImpl extends HibernateBaseDao<WxLotteryUserArticle, Integer> 
	implements WxLotteryUserArticleDao {

	@Override
	public Pagination getPage(Integer productId, Integer isConfirm, int pageNo, int pageSize) {
		Finder finder = Finder.create(" from  WxLotteryUserArticle bean where 1=1");
		if(productId != null){
			finder.append(" and bean.article.product.id=:productId");
			finder.setParam("productId", productId);
		}
		
		if(isConfirm != null){
			finder.append(" and bean.isConfirm=:isConfirm");
			finder.setParam("isConfirm",isConfirm);
		}
		
		finder.append(" order by  bean.oprtime desc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public WxLotteryUserArticle findById(Integer id) {
		WxLotteryUserArticle company =  super.get(id);
		return company;
	}

	@Override
	public WxLotteryUserArticle save(WxLotteryUserArticle bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxLotteryUserArticle deleteById(Integer id) {
		WxLotteryUserArticle entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<WxLotteryUserArticle> getEntityClass() {
		return WxLotteryUserArticle.class;
	}

}
