package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.GhIDsDao;
import com.jskj.cms.entity.assist.GhIDs;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;

public class GhIDsDaoImpl extends
	HibernateBaseDao<GhIDs, String> implements GhIDsDao {

	@Override
	protected Class<GhIDs> getEntityClass() {
		return GhIDs.class;
	}

	@Override
	public GhIDs findNextCardno(Integer status) {

		Finder f = Finder.create(" from GhIDs bean where 1=1 ");
		if(status >-1){
			f.append(" and bean.status=:status");
			f.setParam("status", status);
		}
		
		List list = find(f);
		if(list != null && list.size() >0){
			GhIDs ghIDs = (GhIDs)list.get(0);
			return ghIDs;
		}
		return null;
	}

}
