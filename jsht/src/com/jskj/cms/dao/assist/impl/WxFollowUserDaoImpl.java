package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxFollowUserDao;
import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

@Repository
public class WxFollowUserDaoImpl extends HibernateBaseDao<WxFollowUser, Integer>
		implements WxFollowUserDao {


	public WxFollowUser findById(Integer id) {
		WxFollowUser entity = get(id);
		return entity;
	}

	public WxFollowUser save(WxFollowUser bean) {
		getSession().save(bean);
		return bean;
	}

	public WxFollowUser deleteById(Integer id) {
		WxFollowUser entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<WxFollowUser> getEntityClass() {
		return WxFollowUser.class;
	}

	@Override
	public Pagination getPage(Integer accountId, Integer groupId, int pageNo,
			int pageSize) {
		Finder finder = Finder.create(" from WxFollowUser bean where 1=1");
		
		if(accountId != null && accountId >-1){
			finder.append(" and bean.account.id=:accountId");
			finder.setParam("accountId", accountId);
		}
		
		if(groupId != null && groupId >-1){
			
			if(groupId == 0){
				finder.append(" and bean.group is null");
			} else {
				finder.append(" and bean.group.groupid=:groupId");
				finder.setParam("groupId", groupId);
			}

		}
		
		finder.append(" order by bean.subDate desc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public Pagination getPage(Integer accountId, Integer groupId, String staDate,
			String endDate, int pageNo, int pageSize) {
		Finder finder = Finder.create(" from WxFollowUser bean where 1=1");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if(accountId != null && accountId >-1){
			finder.append(" and bean.account.id=:accountId");
			finder.setParam("accountId", accountId);
		}
		
		if(groupId != null && groupId >-1){
			
			if(groupId == 0){
				finder.append(" and bean.group is null");
			} else {
				finder.append(" and bean.group.groupid=:groupId");
				finder.setParam("groupId", groupId);
			}

		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				finder.append(" and bean.subDate>=:staDate");
				finder.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				finder.append(" and bean.subDate<:endDate");
				finder.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		finder.append(" order by bean.subDate desc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public Pagination getPage(Integer accountId, Integer groupId,
			String staDate, String endDate, String province, int pageNo,
			int pageSize) {
		Finder finder = Finder.create(" from WxFollowUser bean where 1=1");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if(accountId != null && accountId >-1){
			finder.append(" and bean.account.id=:accountId");
			finder.setParam("accountId", accountId);
		}
		
		if(groupId != null && groupId >-1){
			
			if(groupId == 0){
				finder.append(" and bean.group is null");
			} else {
				finder.append(" and bean.group.groupid=:groupId");
				finder.setParam("groupId", groupId);
			}

		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				finder.append(" and bean.subDate>=:staDate");
				finder.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				finder.append(" and bean.subDate<:endDate");
				finder.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(province != null && !province.equals("")){
			finder.append(" and instr(bean.province,:province)>0");
			finder.setParam("province",province);
		}
		
		finder.append(" order by bean.subDate desc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public Pagination getPage(Integer accountId, Integer groupId,Integer subType,
			String staDate, String endDate, String province, int pageNo,
			int pageSize) {
		Finder finder = Finder.create(" from WxFollowUser bean where 1=1");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if(accountId != null && accountId >-1){
			finder.append(" and bean.account.id=:accountId");
			finder.setParam("accountId", accountId);
		}
		
		if(subType != null && subType>-1){
			finder.append(" and bean.subType =:subType");
			if(subType == 1){
				finder.setParam("subType", "subscribe");
			} else {
				finder.setParam("subType", "unsubscribe");
			}
			
		}
		
		if(groupId != null && groupId >-1){
			
			if(groupId == 0){
				finder.append(" and bean.group is null");
			} else {
				finder.append(" and bean.group.groupid=:groupId");
				finder.setParam("groupId", groupId);
			}

		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				finder.append(" and bean.subDate>=:staDate");
				finder.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				finder.append(" and bean.subDate<:endDate");
				finder.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(province != null && !province.equals("")){
			finder.append(" and instr(bean.province,:province)>0");
			finder.setParam("province",province);
		}
		
		finder.append(" order by bean.lastOprtime desc,bean.userid desc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public Pagination getPage(Integer accountId, Integer groupId,
			Integer subType, String staDate, String endDate, String province,
			String nickname, String name, String openid, int pageNo,
			int pageSize) {
		Finder finder = Finder.create(" from WxFollowUser bean where 1=1");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if(accountId != null && accountId >-1){
			finder.append(" and bean.account.id=:accountId");
			finder.setParam("accountId", accountId);
		}
		
		if(subType != null && subType>-1){
			finder.append(" and bean.subType =:subType");
			if(subType == 1){
				finder.setParam("subType", "subscribe");
			} else {
				finder.setParam("subType", "unsubscribe");
			}
			
		}
		
		if(groupId != null && groupId >-1){
			
			if(groupId == 0){
				finder.append(" and bean.group is null");
			} else {
				finder.append(" and bean.group.groupid=:groupId");
				finder.setParam("groupId", groupId);
			}

		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				finder.append(" and bean.subDate>=:staDate");
				finder.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				finder.append(" and bean.subDate<:endDate");
				finder.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(province != null && !province.equals("")){
			finder.append(" and instr(bean.province,:province)>0");
			finder.setParam("province",province);
		}
		
		if(nickname != null && !nickname.equals("")){
			finder.append(" and instr(bean.nickname,:nickname)>0");
			finder.setParam("nickname", nickname);
		}
		
		if(name != null && !name.equals("")){
			finder.append(" and instr(bean.name,:name)>0");
			finder.setParam("name", name);
		}
		
		if(openid != null && !openid.equals("")){
			finder.append(" and instr(bean.openid,:openid)>0");
			finder.setParam("openid", openid);
		}
		
		finder.append(" order by bean.lastOprtime desc,bean.userid desc");
		
		return find(finder, pageNo, pageSize);
	}

}