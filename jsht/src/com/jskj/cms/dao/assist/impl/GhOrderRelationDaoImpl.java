package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import com.jskj.cms.dao.assist.GhOrderRelationDao;
import com.jskj.cms.entity.assist.GhOrderRelation;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhOrderRelationDaoImpl extends
	HibernateBaseDao<GhOrderRelation, Integer> implements GhOrderRelationDao {

	@Override
	public Pagination getPage(Integer userId,String staDate,String endDate, int pageNo, int pageSize) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Finder f = Finder.create("from GhOrderRelation bean where 1=1");
		
		
		if(userId != null && userId > 0){
			f.append(" and bean.owner.userid=:userId");
			f.setParam("userId", userId);
		} 
		
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.ghOrder.odTime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.ghOrder.odTime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		f.append(" order by bean.ghOrder.odTime desc");
		return find(f, pageNo, pageSize);
	
	}


	@Override
	public GhOrderRelation findById(Integer id) {
		GhOrderRelation service = super.get(id);
		return service;
	}

	@Override
	public GhOrderRelation save(GhOrderRelation bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhOrderRelation deleteById(Integer id) {
		GhOrderRelation entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhOrderRelation> getEntityClass() {
		return GhOrderRelation.class;
	}


	@Override
	public Pagination getPage(Integer userId, Integer visitFlag,
			String staDate, String endDate, int pageNo, int pageSize) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Finder f = Finder.create("from GhOrderRelation bean where 1=1");
		
		
		if(userId != null && userId > 0){
			f.append(" and bean.owner.userid=:userId");
			f.setParam("userId", userId);
		} 
		
		if(visitFlag != null && visitFlag > -1){
			f.append(" and bean.visitFlag=:visitFlag");
			f.setParam("visitFlag", visitFlag);
		} 
		
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.ghOrder.odTime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.ghOrder.odTime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		f.append(" order by bean.ghOrder.odTime desc");
		return find(f, pageNo, pageSize);
	}


}
