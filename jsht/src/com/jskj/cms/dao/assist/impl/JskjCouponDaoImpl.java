
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjCouponDao;
import com.jskj.cms.entity.assist.JskjCoupon;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjCouponDaoImpl extends
	HibernateBaseDao<JskjCoupon, Integer> implements JskjCouponDao {

	@Override
	protected Class<JskjCoupon> getEntityClass() {
		return JskjCoupon.class;
	}

	@Override
	public JskjCoupon findById(Integer id) {
		JskjCoupon service = super.get(id);
		return service;
	}

	@Override
	public JskjCoupon save(JskjCoupon bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjCoupon deleteById(Integer id) {
		JskjCoupon entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name,String staDate, String endDate, Integer status, int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjCoupon bean where 1=1");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if(name != null && !name.equals("")){
			f.append(" and instr(bean.name,:name)>0");
			f.setParam("name", name);
		}
		
		if(status != null && status >-1){
			f.append(" and bean.status=:status");
			f.setParam("status", status);
		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.oprtime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.oprtime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
		
	}

}
