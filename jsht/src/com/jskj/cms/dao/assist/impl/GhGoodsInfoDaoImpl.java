package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import com.jskj.cms.dao.assist.GhGoodsInfoDao;
import com.jskj.cms.entity.assist.GhGoodsInfo;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhGoodsInfoDaoImpl extends
	HibernateBaseDao<GhGoodsInfo, Integer> implements GhGoodsInfoDao {

	@Override
	public GhGoodsInfo findById(Integer id) {
		GhGoodsInfo service = super.get(id);
		return service;
	}

	@Override
	public GhGoodsInfo save(GhGoodsInfo bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhGoodsInfo deleteById(Integer id) {
		GhGoodsInfo entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhGoodsInfo> getEntityClass() {
		return GhGoodsInfo.class;
	}

	@Override
	public Pagination getPage(String name,String mobile,String staDate,String endDate,int pageNo, int pageSize) {
		
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Finder f = Finder.create("from GhGoodsInfo bean where 1=1");
		if(name != null && !name.equals("")){
			f.append(" and instr(bean.name,:name)>0");
			f.setParam("name", name);
		}
		
		if(mobile != null && !mobile.equals("")){
			f.append(" and instr(bean.mobile,:mobile)>0");
			f.setParam("mobile", mobile);
		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.oprtime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.oprtime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		f.append(" order by bean.oprtime desc");
		return find(f, pageNo, pageSize);
	}

}
