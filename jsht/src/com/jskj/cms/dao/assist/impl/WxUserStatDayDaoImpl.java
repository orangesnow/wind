
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxUserStatDayDao;
import com.jskj.cms.entity.assist.WxUserGroup;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-10-06
 */
@Repository
public class WxUserStatDayDaoImpl extends
	HibernateBaseDao<WxUserGroup, Integer> implements WxUserStatDayDao {

	@Override
	protected Class<WxUserGroup> getEntityClass() {
		return WxUserGroup.class;
	}


	@Override
	public Pagination getPage(Integer accountId, String staDate,String endDate,int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from WxUserStatDay bean where 1=1");
		
		if(staDate != null && !staDate.equals("")){
			f.append(" and bean.totalTime>=:staDate");
			f.setParam("staDate", staDate);
		}
		
		if(endDate != null && !endDate.equals("")){
			f.append(" and bean.totalTime<=:endDate");
			f.setParam("endDate", endDate);
		}
		
		if(accountId != null && accountId>0){
			f.append(" and bean.account.id =:accountId ");
			f.setParam("accountId", accountId);
		}
		
		f.append(" order by bean.totalTime desc");
		return find(f,pageNo,pageSize);
	}
	
}
