
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjAdDao;
import com.jskj.cms.entity.assist.JskjAd;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjAdDaoImpl extends
	HibernateBaseDao<JskjAd, Integer> implements JskjAdDao {

	@Override
	protected Class<JskjAd> getEntityClass() {
		return JskjAd.class;
	}

	@Override
	public JskjAd findById(Integer id) {
		JskjAd service = super.get(id);
		return service;
	}

	@Override
	public JskjAd save(JskjAd bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjAd deleteById(Integer id) {
		JskjAd entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(Integer adtype, Integer status, int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjAd bean where 1=1");
		
		if( adtype != null && adtype >-1 ){
			f.append(" and bean.adtype=:adtype");
			f.setParam("adtype", adtype);
		}
		
		if( status != null && status > -1 ){
			f.append(" and bean.status=:status");
			f.setParam("status", status);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
		
	}

}
