
package com.jskj.cms.dao.assist.impl;
       
import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjHotKeywordDao;
import com.jskj.cms.entity.assist.JskjHotKeyword;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjHotKeywordDaoImpl extends
	HibernateBaseDao<JskjHotKeyword, Integer> implements JskjHotKeywordDao {

	@Override
	protected Class<JskjHotKeyword> getEntityClass() {
		return JskjHotKeyword.class;
	}

	@Override
	public JskjHotKeyword findById(Integer id) {
		JskjHotKeyword service = super.get(id);
		return service;
	}

	@Override
	public JskjHotKeyword save(JskjHotKeyword bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjHotKeyword deleteById(Integer id) {
		JskjHotKeyword entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name,Integer htype,int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from JskjHotKeyword bean where 1=1");
		
		if( name != null && !name.equals("") ){
			f.append(" and instr(bean.content,:content)>0");
			f.setParam("content", name);
		}
		
		if( htype != null && htype >-1 ){
			f.append(" and bean.htype =:htype");
			f.setParam("htype", htype);
		}
		
		f.append(" order by bean.id desc");
		
		return find(f,pageNo,pageSize);
	}
	
}
