package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxPublicAccountDao;
import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;

@Repository
public class WxPublicAccountDaoImpl extends HibernateBaseDao<WxPublicAccount, Integer>
		implements WxPublicAccountDao {


	public WxPublicAccount findById(Integer id) {
		WxPublicAccount entity = get(id);
		return entity;
	}

	public WxPublicAccount save(WxPublicAccount bean) {
		getSession().save(bean);
		return bean;
	}

	public WxPublicAccount deleteById(Integer id) {
		WxPublicAccount entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<WxPublicAccount> getEntityClass() {
		return WxPublicAccount.class;
	}

	@Override
	public List<WxPublicAccount> getList(Integer status) {
		Finder f = Finder.create(" from WxPublicAccount bean where 1=1 ");
		
		if(status != null && status >-1){
			f.append(" and bean.status =:status");
			f.setParam("status", status);
		}
		
		return find(f);
	}


}