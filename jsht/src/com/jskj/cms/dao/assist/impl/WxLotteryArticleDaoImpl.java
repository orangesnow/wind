
package com.jskj.cms.dao.assist.impl;

import com.jskj.cms.dao.assist.WxLotteryArticleDao;
import com.jskj.cms.entity.assist.WxLotteryArticle;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class WxLotteryArticleDaoImpl extends HibernateBaseDao<WxLotteryArticle, String> 
	implements WxLotteryArticleDao {

	@Override
	public Pagination getPage(Integer status,Integer productId, int pageNo, int pageSize) {
		Finder finder = Finder.create(" from  WxLotteryArticle bean where 1=1");
		if(status != null){
			finder.append(" and bean.status=:status");
			finder.setParam("status", status);
		} else {
			finder.append(" and bean.status<>-1");
		}
		
		if(productId != null){
			finder.append(" and bean.product.id=:id");
			finder.setParam("id", productId);
		}
		finder.append(" order by bean.weight asc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public WxLotteryArticle findById(String id) {
		WxLotteryArticle company =  super.get(id);
		return company;
	}

	@Override
	public WxLotteryArticle save(WxLotteryArticle bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxLotteryArticle deleteById(String id) {
		WxLotteryArticle entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<WxLotteryArticle> getEntityClass() {
		return WxLotteryArticle.class;
	}

}
