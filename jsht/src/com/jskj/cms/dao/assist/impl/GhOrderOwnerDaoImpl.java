package com.jskj.cms.dao.assist.impl;

import com.jskj.cms.dao.assist.GhOrderOwnerDao;
import com.jskj.cms.entity.assist.GhOrderOwner;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhOrderOwnerDaoImpl extends
	HibernateBaseDao<GhOrderOwner, Integer> implements GhOrderOwnerDao {

	@Override
	public Pagination getPage(Integer userid,String keyword, int pageNo, int pageSize) {
		
		
		Finder f = Finder.create("from GhOrderOwner bean where 1=1");

		if(userid != null && userid > 0){
			f.append(" and bean.userid=:userid");
			f.setParam("userid", userid);
		}
		
		if(keyword != null && !keyword.equals("")){
			f.append(" and instr(bean.keyword,:keyword) >0");
			f.setParam("keyword", keyword);
		}

		f.append(" order by bean.odTime desc");
		return find(f, pageNo, pageSize);
	
	}

	@Override
	public GhOrderOwner findById(Integer id) {
		GhOrderOwner service = super.get(id);
		return service;
	}

	@Override
	public GhOrderOwner save(GhOrderOwner bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhOrderOwner deleteById(Integer id) {
		GhOrderOwner entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhOrderOwner> getEntityClass() {
		return GhOrderOwner.class;
	}

	
}
