
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxTagGroupDao;
import com.jskj.cms.entity.assist.WxTagGroup;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class WxTagGroupDaoImpl extends
	HibernateBaseDao<WxTagGroup, Integer> implements WxTagGroupDao {

	@Override
	public List<WxTagGroup> getList(Integer accountId) {
		Finder f = Finder.create(" from WxTagGroup bean where 1=1 ");
		if(accountId != null && accountId>0){
			f.append(" and bean.account.id =:accountId ");
			f.setParam("accountId", accountId);
		}
		
		f.append(" order by bean.id desc ");
		return find(f);
	}
	
	@Override
	protected Class<WxTagGroup> getEntityClass() {
		return WxTagGroup.class;
	}

	@Override
	public WxTagGroup findById(Integer id) {
		WxTagGroup service = super.get(id);
		return service;
	}

	@Override
	public WxTagGroup save(WxTagGroup bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxTagGroup deleteById(Integer id) {
		WxTagGroup entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(Integer accountId, int pageNo,
			int pageSize, boolean cacheable) {
		
		Finder f = Finder.create("from WxTagGroup bean where 1=1");
		
		
		if(accountId != null && accountId>0){
			f.append(" and bean.account.id =:accountId ");
			f.setParam("accountId", accountId);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
	}
	
}
