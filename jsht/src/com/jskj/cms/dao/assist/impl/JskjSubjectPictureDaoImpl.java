
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjSubjectPictureDao;
import com.jskj.cms.entity.assist.JskjSubjectPicture;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjSubjectPictureDaoImpl extends
	HibernateBaseDao<JskjSubjectPicture, Integer> implements JskjSubjectPictureDao {

	@Override
	protected Class<JskjSubjectPicture> getEntityClass() {
		return JskjSubjectPicture.class;
	}

	@Override
	public JskjSubjectPicture findById(Integer id) {
		JskjSubjectPicture service = super.get(id);
		return service;
	}

	@Override
	public JskjSubjectPicture save(JskjSubjectPicture bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjSubjectPicture deleteById(Integer id) {
		JskjSubjectPicture entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public List<JskjSubjectPicture> getList(Integer subjectid) {
		Finder f = Finder.create("from JskjSubjectPicture bean where 1=1");

		if( subjectid != null && subjectid > -1 ) {
			f.append(" and bean.subjectid=:subjectid");
			f.setParam("subjectid", subjectid);
		}
	
		List<JskjSubjectPicture> pictures = find(f);
		return pictures;
		
	}

}
