
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjSubjectTotalDao;
import com.jskj.cms.entity.assist.JskjSubjectTotal;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjSubjectTotalDaoImpl extends
	HibernateBaseDao<JskjSubjectTotal, Integer> implements JskjSubjectTotalDao {

	@Override
	protected Class<JskjSubjectTotal> getEntityClass() {
		return JskjSubjectTotal.class;
	}

	@Override
	public JskjSubjectTotal findById(Integer id) {
		JskjSubjectTotal service = super.get(id);
		return service;
	}

	@Override
	public JskjSubjectTotal save(JskjSubjectTotal bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjSubjectTotal deleteById(Integer id) {
		JskjSubjectTotal entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjSubjectTotal bean where 1=1");
	
		f.append(" order by bean.id desc");
		
		return find(f,pageNo,pageSize);
		
	}

}
