package com.jskj.cms.dao.assist.impl;

import com.jskj.cms.dao.assist.GhLeaveDao;
import com.jskj.cms.entity.assist.GhLeave;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhLeaveDaoImpl extends
	HibernateBaseDao<GhLeave, Integer> implements GhLeaveDao {

	@Override
	public Pagination getPage(String userid, int pageNo, int pageSize,
			boolean cacheable) {
		Finder f = Finder.create("from GhLeave bean where 1=1");
		if(userid != null && !userid.equals("")){
			f.append(" and bean.userid=:userid ");
			f.setParam("userid", userid);
		}
		
		f.append(" order by bean.oprtime desc");
		return find(f, pageNo, pageSize);
	
	}

	@Override
	public GhLeave findById(Integer id) {
		GhLeave service = super.get(id);
		return service;
	}

	@Override
	public GhLeave save(GhLeave bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhLeave deleteById(Integer id) {
		GhLeave entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhLeave> getEntityClass() {
		return GhLeave.class;
	}

}
