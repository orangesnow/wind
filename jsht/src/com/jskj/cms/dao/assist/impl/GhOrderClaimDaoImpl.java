package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import com.jskj.cms.dao.assist.GhOrderClaimDao;
import com.jskj.cms.entity.assist.GhOrderClaim;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class GhOrderClaimDaoImpl extends
	HibernateBaseDao<GhOrderClaim, Integer> implements GhOrderClaimDao {

	@Override
	public GhOrderClaim findById(int id) {
		GhOrderClaim service = super.get(id);
		return service;
	}

	@Override
	public GhOrderClaim save(GhOrderClaim bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public GhOrderClaim deleteById(int id) {
		GhOrderClaim entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<GhOrderClaim> getEntityClass() {
		return GhOrderClaim.class;
	}

	@Override
	public Pagination getPage(String orderId, Integer source,
			Integer claimFlag, Integer claimType, String staDate,
			String endDate, int pageNo, int pageSize, boolean cacheable) {
		
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Finder f = Finder.create("from GhOrderClaim bean where 1=1");
		if(orderId != null && !orderId.equals("")){
			f.append(" and bean.ghOrder.oid=:oid");
			f.setParam("oid", orderId);
		}
		
		if(source != null && source > 0){
			f.append(" and bean.source=:source");
			f.setParam("source", source);
		}
		
		if(claimFlag != null && claimFlag >-1){
			f.append(" and bean.claimFlag=:claimFlag");
			f.setParam("claimFlag", claimFlag);
		}
		
		if(claimType != null && claimType >-1){
			f.append(" and bean.claimType=:claimType");
			f.setParam("claimType", claimType);
		}
		
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.claimOprtime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.claimOprtime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		f.append(" order by bean.claimOprtime desc");
		return find(f, pageNo, pageSize);
	}

}
