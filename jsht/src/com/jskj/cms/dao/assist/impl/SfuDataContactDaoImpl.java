
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.SfuDataContactDao;
import com.jskj.cms.entity.assist.SfuDataContact;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

@Repository
public class SfuDataContactDaoImpl extends HibernateBaseDao<SfuDataContact, Integer>  
implements SfuDataContactDao {
	
	public SfuDataContact findById(Integer id) {
		SfuDataContact config = get(id);
		return config;
	}

	public SfuDataContact save(SfuDataContact bean){
		getSession().save(bean);
		return bean;
	}
	
	public SfuDataContact deleteById(Integer id){
		SfuDataContact entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<SfuDataContact> getEntityClass() {
		return SfuDataContact.class;
	}

	@Override
	public Pagination getPage(Integer companyId,
			int pageNo, int pageSize) {
		
		Finder f = Finder.create(" from SfuDataContact bean where 1=1 ");

		if(companyId != null && companyId >0){
			f.append(" and bean.companyId=:companyId");
			f.setParam("companyId", companyId);
		}
		
		f.append(" order by bean.contactId desc");
		
		return find(f,pageNo,pageSize);
	}

	@Override
	public List<SfuDataContact> getList(Integer companyId) {
		Finder f = Finder.create(" from SfuDataContact bean where 1=1 ");

		if(companyId != null && companyId >0){
			f.append(" and bean.companyId=:companyId");
			f.setParam("companyId", companyId);
		}
		
		f.append(" order by bean.contactId desc");
		
		return find(f);
	}


}
