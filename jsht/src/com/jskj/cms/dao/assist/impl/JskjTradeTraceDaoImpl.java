package com.jskj.cms.dao.assist.impl;

import com.jskj.cms.dao.assist.JskjTradeTraceDao;
import com.jskj.cms.entity.assist.JskjTradeTrace;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class JskjTradeTraceDaoImpl extends
	HibernateBaseDao<JskjTradeTrace, Integer> implements JskjTradeTraceDao {

	@Override
	public JskjTradeTrace findById(Integer id) {
		JskjTradeTrace service = super.get(id);
		return service;
	}

	@Override
	public JskjTradeTrace save(JskjTradeTrace bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjTradeTrace deleteById(Integer id) {
		JskjTradeTrace entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<JskjTradeTrace> getEntityClass() {
		return JskjTradeTrace.class;
	}

	@Override
	public Pagination getPage(String tradeid, String sid,	 int pageNo, int pageSize) {

		Finder f = Finder.create("from JskjTradeTrace bean where 1=1");
		if(tradeid != null && !tradeid.equals("")){
			f.append(" and bean.tradeid=:tradeid");
			f.setParam("tradeid", tradeid);
		}
		
		if(sid != null &&  !sid.equals("")){
			f.append(" and bean.sid=:sid");
			f.setParam("sid", sid);
		}

		f.append(" order by bean.id desc");
		
		return find(f, pageNo, pageSize);
	}

}
