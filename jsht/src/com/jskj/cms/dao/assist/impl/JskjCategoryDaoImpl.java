
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjCategoryDao;
import com.jskj.cms.entity.assist.JskjCategory;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjCategoryDaoImpl extends
	HibernateBaseDao<JskjCategory, Integer> implements JskjCategoryDao {

	@Override
	protected Class<JskjCategory> getEntityClass() {
		return JskjCategory.class;
	}

	@Override
	public JskjCategory findById(Integer id) {
		JskjCategory service = super.get(id);
		return service;
	}

	@Override
	public JskjCategory save(JskjCategory bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjCategory deleteById(Integer id) {
		JskjCategory entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name, Integer status, int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjCategory bean where 1=1");
		
		if( name != null && !name.equals("") ){
			f.append(" and instr(bean.name,:name) >0 ");
			f.setParam("name", name);
		}
		
		if(status != null &&  status > -1 ){
			f.append(" and bean.status =:status");
			f.setParam("status", status);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
		
	}

	@Override
	public List<JskjCategory> findCategorys(Integer status) {
		Finder f = Finder.create("from JskjCategory bean where 1=1");
	
		if(status != null &&  status > -1 ){
			f.append(" and bean.status =:status");
			f.setParam("status", status);
		}
		
		f.append(" order by bean.id desc");
		return find(f);
		
	}

}
