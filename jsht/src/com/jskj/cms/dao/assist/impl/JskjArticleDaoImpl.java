
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjArticleDao;
import com.jskj.cms.entity.assist.JskjArticle;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjArticleDaoImpl extends
	HibernateBaseDao<JskjArticle, Integer> implements JskjArticleDao {

	@Override
	protected Class<JskjArticle> getEntityClass() {
		return JskjArticle.class;
	}

	@Override
	public JskjArticle findById(Integer id) {
		JskjArticle service = super.get(id);
		return service;
	}

	@Override
	public JskjArticle save(JskjArticle bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjArticle deleteById(Integer id) {
		JskjArticle entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String name,Integer status,int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjArticle bean where 1=1");
		
		if( name != null && !name.equals("") ){
			f.append(" and instr(bean.title,:title) >0 ");
			f.setParam("title", name);
		}
	
		f.append("order by bean.id desc");
		
		return find(f,pageNo,pageSize);
		
	}

}
