
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjSubjectDao;
import com.jskj.cms.entity.assist.JskjSubject;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjSubjectDaoImpl extends
	HibernateBaseDao<JskjSubject, Integer> implements JskjSubjectDao {

	@Override
	protected Class<JskjSubject> getEntityClass() {
		return JskjSubject.class;
	}

	@Override
	public JskjSubject findById(Integer id) {
		JskjSubject service = super.get(id);
		return service;
	}

	@Override
	public JskjSubject save(JskjSubject bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjSubject deleteById(Integer id) {
		JskjSubject entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(String staDate,String endDate,Integer groupid,Integer rtype,Integer status,int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjSubject bean where 1=1");
		
		if(groupid != null && groupid >-1){
			f.append(" and bean.group.id = :groupid");
			f.setParam("groupid", groupid);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			if(staDate != null && !staDate.equals("")){
				f.append(" and bean.oprtime>=:staDate");
				f.setParam("staDate", sdf.parse(staDate));
			}
			
			if(endDate != null && !endDate.equals("")){
				f.append(" and bean.oprtime<:endDate");
				f.setParam("endDate", sdf.parse(endDate));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(status != null && status >-1) {
			f.append(" and bean.status=:status");
			f.setParam("status", status);
		}
		
		if(rtype == null) rtype = 2;
		
		if(rtype == 0) {
			f.append(" order by bean.isrecomm desc");
		} else if(rtype == 1) {
			f.append(" order by bean.istop desc");
		} else if(rtype == 2) {
			f.append(" order by bean.oprtime desc");
		} else if(rtype == 3) {
			f.append(" order by bean.istag desc");
		}

		return find(f,pageNo,pageSize);
		
	}

}
