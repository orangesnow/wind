
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjItemSkuDao;
import com.jskj.cms.entity.assist.JskjItemSku;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjItemSkuDaoImpl extends
	HibernateBaseDao<JskjItemSku, Integer> implements JskjItemSkuDao {

	@Override
	protected Class<JskjItemSku> getEntityClass() {
		return JskjItemSku.class;
	}

	@Override
	public JskjItemSku findById(Integer id) {
		JskjItemSku service = super.get(id);
		return service;
	}

	@Override
	public JskjItemSku save(JskjItemSku bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjItemSku deleteById(Integer id) {
		JskjItemSku entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(Integer itemid,int pageNo, int pageSize) {
		
		Finder f = Finder.create("from JskjItemSku bean where 1=1");

		if(itemid != null && itemid >-1){
			f.append(" and bean.itemid = :itemid");
			f.setParam("itemid", itemid);
		}
	
		f.append(" order by bean.id desc");

		return find(f,pageNo,pageSize);
		
	}

	@Override
	public List<JskjItemSku> getList(Integer itemid) {
		
		Finder f = Finder.create("from JskjItemSku bean where 1=1");
		
		if(itemid != null && itemid > -1){
			f.append(" and bean.itemid=:itemid");
			f.setParam("itemid", itemid);
		}
		
		f.append(" and bean.num >0");
		
		f.append(" order by bean.id desc");

		return find(f);
	}
	
	
	

}
