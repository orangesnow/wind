
package com.jskj.cms.dao.assist.impl;
       
import java.util.List;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxSyncTypeDao;
import com.jskj.cms.entity.assist.WxSyncType;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class WxSyncTypeDaoImpl extends
	HibernateBaseDao<WxSyncType, Integer> implements WxSyncTypeDao {

	@Override
	public List<WxSyncType> getList() {
		Finder f = Finder.create(" from WxSyncType bean where 1=1 ");
		f.append(" order by bean.id desc ");
		return find(f);
	}
	
	@Override
	protected Class<WxSyncType> getEntityClass() {
		return WxSyncType.class;
	}

	@Override
	public WxSyncType findById(Integer id) {
		WxSyncType service = super.get(id);
		return service;
	}

	@Override
	public WxSyncType save(WxSyncType bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxSyncType deleteById(Integer id) {
		WxSyncType entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from WxSyncType bean where 1=1");
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
	}
	
}
