
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjSubjectExtDao;
import com.jskj.cms.entity.assist.JskjSubject;
import com.jskj.cms.entity.assist.JskjSubjectExt;
import com.jskj.common.hibernate3.HibernateBaseDao;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjSubjectExtDaoImpl extends
	HibernateBaseDao<JskjSubjectExt, Integer> implements JskjSubjectExtDao {

	@Override
	protected Class<JskjSubjectExt> getEntityClass() {
		return JskjSubjectExt.class;
	}

	@Override
	public JskjSubjectExt findById(Integer id) {
		JskjSubjectExt service = super.get(id);
		return service;
	}

	@Override
	public JskjSubjectExt save(JskjSubjectExt bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjSubjectExt deleteById(Integer id) {
		JskjSubjectExt entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	
}
