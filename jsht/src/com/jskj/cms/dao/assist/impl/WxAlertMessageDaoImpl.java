
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxAlertMessageDao;
import com.jskj.cms.entity.assist.WxAlertMessage;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

@Repository
public class WxAlertMessageDaoImpl extends HibernateBaseDao<WxAlertMessage, Integer>  
implements WxAlertMessageDao {
	
	public WxAlertMessage findById(Integer id) {
		WxAlertMessage config = get(id);
		return config;
	}

	public WxAlertMessage save(WxAlertMessage bean){
		getSession().save(bean);
		return bean;
	}
	
	public WxAlertMessage deleteById(Integer id){
		WxAlertMessage entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public List<WxAlertMessage> findByCtype(int ctype,int status) {
		
		StringBuffer hql = new StringBuffer(" from WxAlertMessage as bean where 1=1 ");

		if(ctype >0){
			 hql.append(" and bean.ctype=:ctype");
		} 
		
		if(status >0) {
			 hql.append(" and bean.status=:status");
		}
		
		Query query = getSession().createQuery(hql.toString());
		if(ctype >0){
			query.setInteger("ctype", ctype);
		}
		
		if(status >0) {
			query.setInteger("status", status);
		}
		
		return query.list();
		
	}

	@Override
	public WxAlertMessage findByCKey(String ckey) {
		StringBuffer hql = new StringBuffer(" from WxAlertMessage as bean where bean.status=1 and ckey=:ckey");
		Query query = getSession().createQuery(hql.toString()).setString("ckey", ckey);
		
		List<WxAlertMessage> list = query.list();
		if(list != null && list.size() >0){
			return  list.get(0);
		}
	
		return null;
	}
	
	@Override
	protected Class<WxAlertMessage> getEntityClass() {
		return WxAlertMessage.class;
	}

	@Override
	public Pagination getPage(String name, Integer categoryId,
			int pageNo, int pageSize) {
		Finder f = Finder.create(" from WxAlertMessage bean where 1=1 ");
		
		if(categoryId != null ){
			f.append(" and bean.source.category.id=:id");
			f.setParam("id", categoryId);
		}
		
		if(name != null && name.equals("")){
			f.append(" and instr(bean.source.name,:name)>0");
			f.setParam("name", name);
		}
		
		f.append(" order by bean.id desc");
		
		return find(f,pageNo,pageSize);
	}

	@Override
	public Pagination getPage(Integer sourceId, Integer ctype, Integer status,
			int pageNo, int pageSize) {
		
		Finder f = Finder.create(" from WxAlertMessage bean where 1=1 ");
		
		if(sourceId != null && sourceId > -1){
			f.append(" and bean.source.category.id=:id");
			f.setParam("id", sourceId);
		}
		
		if(ctype != null && ctype>-1){
			f.append(" and instr(bean.source.id,:sid)>0");
			f.setParam("sid", ctype);
		}
		
		f.append(" order by bean.id desc");
		
		return find(f,pageNo,pageSize);
	}
	
}
