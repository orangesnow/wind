
package com.jskj.cms.dao.assist.impl;

import com.jskj.cms.dao.assist.WxLotteryAgencyDao;
import com.jskj.cms.entity.assist.WxLotteryAgency;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class WxLotteryAgencyDaoImpl extends HibernateBaseDao<WxLotteryAgency, String> 
	implements WxLotteryAgencyDao {

	@Override
	public Pagination getPage(int pageNo, int pageSize) {
		Finder finder = Finder.create(" from  WxLotteryAgency bean where 1=1");
	
		finder.append(" order by bean.oprtime desc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public WxLotteryAgency findById(String id) {
		WxLotteryAgency bean =  super.get(id);
		return bean;
	}

	@Override
	public WxLotteryAgency save(WxLotteryAgency bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxLotteryAgency deleteById(String id) {
		WxLotteryAgency entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<WxLotteryAgency> getEntityClass() {
		return WxLotteryAgency.class;
	}

}
