
package com.jskj.cms.dao.assist.impl;

import java.util.List;

import com.jskj.cms.dao.assist.WxLotteryCompanyDao;
import com.jskj.cms.entity.assist.WxLotteryCompany;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

public class WxLotteryCompanyDaoImpl extends HibernateBaseDao<WxLotteryCompany, Integer> 
	implements WxLotteryCompanyDao {

	@Override
	public Pagination getPage(Integer status, int pageNo, int pageSize) {
		Finder finder = Finder.create(" from  WxLotteryCompany bean where 1=1");
		if(status != null){
			finder.append(" and bean.status=:status");
			finder.setParam("status", status);
		} else {
			finder.append(" and bean.status<>-1");
		}
		
		finder.append(" order by bean.id desc");
		
		return find(finder, pageNo, pageSize);
	}

	@Override
	public WxLotteryCompany findById(Integer id) {
		WxLotteryCompany company =  super.get(id);
		return company;
	}

	@Override
	public WxLotteryCompany save(WxLotteryCompany bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxLotteryCompany deleteById(Integer id) {
		WxLotteryCompany entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	protected Class<WxLotteryCompany> getEntityClass() {
		return WxLotteryCompany.class;
	}

	@Override
	public List<WxLotteryCompany> findLotteryCompanyList(Integer status,
			String name) {
		Finder finder = Finder.create(" from  WxLotteryCompany bean where 1=1");
		if(status != null){
			finder.append(" and bean.status=:status");
			finder.setParam("status", status);
		}
		
		if(name != null){
			finder.append(" and INSTR(bean.name,:name)>0");
			finder.setParam("name", name);
		}
		
		finder.append(" order by bean.id desc");
		
		return find(finder);
	}

}
