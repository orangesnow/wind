
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.WxSyncRuleDao;
import com.jskj.cms.entity.assist.WxSyncRule;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class WxSyncRuleDaoImpl extends
	HibernateBaseDao<WxSyncRule, Integer> implements WxSyncRuleDao {

	
	@Override
	protected Class<WxSyncRule> getEntityClass() {
		return WxSyncRule.class;
	}

	@Override
	public WxSyncRule findById(Integer id) {
		WxSyncRule service = super.get(id);
		return service;
	}

	@Override
	public WxSyncRule save(WxSyncRule bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public WxSyncRule deleteById(Integer id) {
		WxSyncRule entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getPage(Integer stype,int pageNo,
			int pageSize) {
		
		Finder f = Finder.create("from WxSyncRule bean where 1=1");
		
		if(stype != null && stype>-1){
			f.append(" and bean.stype.id =:stype");
			f.setParam("stype", stype);
		}
		
		f.append(" order by bean.id desc");
		return find(f,pageNo,pageSize);
	}
	
}
