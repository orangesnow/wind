
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.assist.JskjItemPictureDao;
import com.jskj.cms.entity.assist.JskjItemPicture;
import com.jskj.common.hibernate3.Finder;
import com.jskj.common.hibernate3.HibernateBaseDao;
import com.jskj.common.page.Pagination;

/**
 * @author VAIO
 * @since 2014-08-01
 */
@Repository
public class JskjItemPictureDaoImpl extends
	HibernateBaseDao<JskjItemPicture, Integer> implements JskjItemPictureDao {

	@Override
	protected Class<JskjItemPicture> getEntityClass() {
		return JskjItemPicture.class;
	}

	@Override
	public JskjItemPicture findById(Integer id) {
		JskjItemPicture service = super.get(id);
		return service;
	}

	@Override
	public JskjItemPicture save(JskjItemPicture bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	public JskjItemPicture deleteById(Integer id) {
		JskjItemPicture entity = super.get(id);
		if (entity != null) {
			getSession().delete(entity);
		}
		return entity;
	}

	@Override
	public Pagination getList(Integer itemid,Integer ptype,int pageNo, int pageSize) {
		Finder f = Finder.create("from JskjItemPicture bean where 1=1");

		if( itemid != null && itemid > -1 ) {
			f.append(" and bean.itemid=:itemid");
			f.setParam("itemid", itemid);
		}
		
		if(ptype != null && ptype >-1){
			f.append(" and bean.ptype = :ptype");
			f.setParam("ptype", ptype);
		}
		
		f.append(" order by bean.weight asc");
		
		return  find(f,pageNo,pageSize);

	}

}
