
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhConfigCategory;
import com.jskj.common.hibernate3.Updater;

/**
 * 留言内容列表
 * @author VAIO
 * @since 2014-08-11
 */
public interface GhConfigCategoryDao {

	public List<GhConfigCategory> getList();
	
	public GhConfigCategory findById(Integer id);

	public GhConfigCategory save(GhConfigCategory bean);

	public GhConfigCategory updateByUpdater(Updater<GhConfigCategory> updater);

	public GhConfigCategory deleteById(Integer id);
	
}
