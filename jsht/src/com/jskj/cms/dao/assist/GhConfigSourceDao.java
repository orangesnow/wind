
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhConfigSource;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 留言内容列表
 * @author VAIO
 * @since 2014-08-11
 */
public interface GhConfigSourceDao {

	public Pagination getPage(String name,Integer category,int pageNo, int pageSize, boolean cacheable);
	
	public GhConfigSource findById(Integer id);

	public GhConfigSource save(GhConfigSource bean);

	public GhConfigSource updateByUpdater(Updater<GhConfigSource> updater);

	public GhConfigSource deleteById(Integer id);
	
	public List<GhConfigSource> findByCategory(Integer categoryId);
	
}
