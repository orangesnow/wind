
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhBindLeave;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 留言内容列表
 * @author VAIO
 * @since 2014-08-11
 */
public interface GhBindLeaveDao {

	public GhBindLeave findById(Integer id);

	public GhBindLeave save(GhBindLeave bean);

	public GhBindLeave updateByUpdater(Updater<GhBindLeave> updater);

	public GhBindLeave deleteById(Integer id);
	
	public List<GhBindLeave> findByGhId(String ghid);
	
	public Pagination getPage(String ghid,int pageNo, int pageSize);
	
	public GhBindLeave findByLeaveId(Integer leaveId);
}
