
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxTagListDao {
	
	public Pagination getPage(String name,int pageNo, int pageSize);
	
	public Pagination getPage(String name,Integer cid,int pageNo, int pageSize);
	
	public WxTagList findById(Integer id);
	
	public WxTagList findByName(String tagName);
	
	public WxTagList findByName(String tagName,boolean single);

	public WxTagList save(WxTagList bean);

	public WxTagList updateByUpdater(Updater<WxTagList> updater);

	public WxTagList deleteById(Integer id);
	
	public List<WxTagList> getList();
	
	public int deleteUserRef(Integer tagId);
	
	public int deleteGroupRef(Integer tagId);
}
