
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.GhVisitRecord;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 订单归属信息
 * @author VAIO
 * @since 2014-08-10
 */
public interface GhVisitRecordDao {
	
	public Pagination getPage(Integer userId,String oid,int pageNo, int pageSize);
	
	public Pagination getPage(Integer userId,String oid,String staDate,String endDate,int pageNo, int pageSize);

	public GhVisitRecord findById(Integer id);

	public GhVisitRecord save(GhVisitRecord bean);

	public GhVisitRecord updateByUpdater(Updater<GhVisitRecord> updater);

	public GhVisitRecord deleteById(Integer id);
	
}
