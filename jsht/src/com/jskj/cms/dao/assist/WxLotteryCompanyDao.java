
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxLotteryCompany;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 抽奖公司信息
 * @author VAIO
 * @since 2014-07-14
 */
public interface WxLotteryCompanyDao {

	public Pagination getPage(Integer status, int pageNo, int pageSize);

	public WxLotteryCompany findById(Integer id);

	public WxLotteryCompany save(WxLotteryCompany bean);

	public WxLotteryCompany updateByUpdater(Updater<WxLotteryCompany> updater);

	public WxLotteryCompany deleteById(Integer id);

	public List<WxLotteryCompany> findLotteryCompanyList(Integer status,String name);
	
}
