
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjShortMessage;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjShortMessageDao {
	
	public Pagination getPage(String name,String staDate,String endDate,Integer status,int pageNo, int pageSize);
	
	public JskjShortMessage findById(Integer id);

	public JskjShortMessage save(JskjShortMessage bean);

	public JskjShortMessage updateByUpdater(Updater<JskjShortMessage> updater);

	public JskjShortMessage deleteById(Integer id);
	
}
