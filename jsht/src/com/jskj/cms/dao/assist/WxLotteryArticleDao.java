
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxLotteryArticle;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 抽奖公司信息
 * @author VAIO
 * @since 2014-07-14
 */
public interface WxLotteryArticleDao {

	public Pagination getPage(Integer status, Integer productId, int pageNo, int pageSize);

	public WxLotteryArticle findById(String id);

	public WxLotteryArticle save(WxLotteryArticle bean);

	public WxLotteryArticle updateByUpdater(Updater<WxLotteryArticle> updater);

	public WxLotteryArticle deleteById(String id);

	
}
