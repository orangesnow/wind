

package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.GhProduct;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 订单列表
 * @author VAIO
 * @since 2014-08-10
 */
public interface GhProductDao {

	public GhProduct findByOuterId(String id);
	
	public Pagination getPage(String name,int pageNo, int pageSize);
	
	public GhProduct findById(Integer id);

	public GhProduct save(GhProduct bean);

	public GhProduct updateByUpdater(Updater<GhProduct> updater);

	public GhProduct deleteById(Integer id);
	
	public List<GhProduct> getList(Integer status);
	
}
