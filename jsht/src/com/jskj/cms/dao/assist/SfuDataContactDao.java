
/**
 * 酒学坊版权所有（2014-2020）
 */
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.SfuDataCompany;
import com.jskj.cms.entity.assist.SfuDataContact;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信消息接口
 * @author VAIO
 * @since 2014-06-03
 */
public interface SfuDataContactDao {

	public List<SfuDataContact> getList(Integer companyId);
	
	public Pagination getPage(Integer companyId,int pageNo,int pageSize);

	public SfuDataContact findById(Integer id);

	public SfuDataContact save(SfuDataContact bean);

	public SfuDataContact updateByUpdater(Updater<SfuDataContact> updater);

	public SfuDataContact deleteById(Integer id);
	
}
