package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.CmsVoteItem;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

public interface CmsVoteItemDao {
	public Pagination getPage(int pageNo, int pageSize);

	public CmsVoteItem findById(Integer id);

	public CmsVoteItem save(CmsVoteItem bean);

	public CmsVoteItem updateByUpdater(Updater<CmsVoteItem> updater);

	public CmsVoteItem deleteById(Integer id);
}