
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjArea;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjAreaDao {
	
	public Pagination getPage(String name,Integer orderBy,Integer status,int pageNo, int pageSize);
	
	public JskjArea findById(Integer id);

	public JskjArea save(JskjArea bean);

	public JskjArea updateByUpdater(Updater<JskjArea> updater);

	public JskjArea deleteById(Integer id);
	
}
