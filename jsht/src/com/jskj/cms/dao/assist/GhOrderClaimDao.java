package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.GhOrderClaim;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 订单列表
 * @author VAIO
 * @since 2014-08-10
 */
public interface GhOrderClaimDao {
	
	public Pagination getPage(String orderId,Integer source,Integer claimFlag,Integer claimType,String staDate,String endDate,int pageNo, int pageSize, boolean cacheable);
	
	public GhOrderClaim findById(int id);

	public GhOrderClaim save(GhOrderClaim bean);

	public GhOrderClaim updateByUpdater(Updater<GhOrderClaim> updater);

	public GhOrderClaim deleteById(int id);
	
}
