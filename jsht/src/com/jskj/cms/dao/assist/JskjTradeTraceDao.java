package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjTradeTrace;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 留言内容列表
 * @author VAIO
 * @since 2014-08-11
 */
public interface JskjTradeTraceDao {

	public JskjTradeTrace findById(Integer id);

	public JskjTradeTrace save(JskjTradeTrace bean);

	public JskjTradeTrace updateByUpdater(Updater<JskjTradeTrace> updater);

	public JskjTradeTrace deleteById(Integer id);

	public Pagination getPage(String tradeid, String sid,
			 int pageNo, int pageSize);
}
