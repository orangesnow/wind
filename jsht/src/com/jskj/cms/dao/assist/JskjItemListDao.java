
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjItemList;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjItemListDao {
	
	public Pagination getPage(String name,Integer status,Integer cid, Integer orderBy,int pageNo, int pageSize);
	
	public JskjItemList findById(Integer id);

	public JskjItemList save(JskjItemList bean);

	public JskjItemList updateByUpdater(Updater<JskjItemList> updater);

	public JskjItemList deleteById(Integer id);
	
}
