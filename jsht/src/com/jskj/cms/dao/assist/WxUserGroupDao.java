
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxUserGroup;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxUserGroupDao {
	
	public List<WxUserGroup> getList(Integer accountId);
	
	public Pagination getPage(Integer accountId,int pageNo, int pageSize, boolean cacheable);
	
	public WxUserGroup findById(Integer id);

	public WxUserGroup save(WxUserGroup bean);

	public WxUserGroup updateByUpdater(Updater<WxUserGroup> updater);

	public WxUserGroup deleteById(Integer id);
	
}
