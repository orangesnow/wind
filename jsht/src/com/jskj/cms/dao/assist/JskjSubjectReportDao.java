
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjSubjectReport;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjSubjectReportDao {
	
	public Pagination getPage(String staDate,String endDate,Integer rtype,int pageNo, int pageSize);
	
	public JskjSubjectReport findById(Integer id);

	public JskjSubjectReport save(JskjSubjectReport bean);

	public JskjSubjectReport updateByUpdater(Updater<JskjSubjectReport> updater);

	public JskjSubjectReport deleteById(Integer id);
	
}
