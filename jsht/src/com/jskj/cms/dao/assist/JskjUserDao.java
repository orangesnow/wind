
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjUser;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjUserDao {
	
	public Pagination getPage(String staDate,String endDate,String nickname,int pageNo, int pageSize);
	
	public JskjUser findById(Integer id);

	public JskjUser save(JskjUser bean);

	public JskjUser updateByUpdater(Updater<JskjUser> updater);

	public JskjUser deleteById(Integer id);
	
}
