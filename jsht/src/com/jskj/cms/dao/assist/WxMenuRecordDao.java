package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxMenuRecord;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

public interface WxMenuRecordDao {

	public WxMenuRecord findById(Integer id);

	public WxMenuRecord save(WxMenuRecord bean);

	public WxMenuRecord updateByUpdater(Updater<WxMenuRecord> updater);

	public WxMenuRecord deleteById(Integer id);

	public Pagination getPage(String fromUsername, Integer pubType, int pageNo, int pageSize);

}