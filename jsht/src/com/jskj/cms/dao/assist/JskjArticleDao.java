
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjArticle;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjArticleDao {
	
	public Pagination getPage(String name,Integer status,int pageNo, int pageSize);
	
	public JskjArticle findById(Integer id);

	public JskjArticle save(JskjArticle bean);

	public JskjArticle updateByUpdater(Updater<JskjArticle> updater);

	public JskjArticle deleteById(Integer id);
	
}
