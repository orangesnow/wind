
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.WxSyncRule;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface WxSyncRuleDao {
	
	public Pagination getPage(Integer stype,int pageNo, int pageSize);
	
	public WxSyncRule findById(Integer id);

	public WxSyncRule save(WxSyncRule bean);

	public WxSyncRule updateByUpdater(Updater<WxSyncRule> updater);

	public WxSyncRule deleteById(Integer id);
	
}
