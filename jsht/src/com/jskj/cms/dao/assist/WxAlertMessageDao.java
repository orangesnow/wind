package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.WxAlertMessage;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

public interface WxAlertMessageDao {
	
	public WxAlertMessage findById(Integer id);

	public WxAlertMessage updateByUpdater(Updater<WxAlertMessage> updater);
	
	public WxAlertMessage save(WxAlertMessage bean);
	
	public WxAlertMessage deleteById(Integer id);
	
	public List<WxAlertMessage> findByCtype(int ctype,int status);
	
	public Pagination getPage(String name,Integer categoryId,int pageNo, int pageSize);
	
	public WxAlertMessage findByCKey(String ckey);
	
	public Pagination getPage(Integer source,Integer ctype,Integer status,int pageNo, int pageSize);

}