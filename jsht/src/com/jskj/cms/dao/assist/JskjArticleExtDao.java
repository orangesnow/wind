
package com.jskj.cms.dao.assist;

import com.jskj.cms.entity.assist.JskjArticleExt;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjArticleExtDao {
	
	public Pagination getPage(String name,Integer status,int pageNo, int pageSize);
	
	public JskjArticleExt findById(Integer id);

	public JskjArticleExt save(JskjArticleExt bean);

	public JskjArticleExt updateByUpdater(Updater<JskjArticleExt> updater);

	public JskjArticleExt deleteById(Integer id);
	
}
