
package com.jskj.cms.dao.assist;

import java.util.List;

import com.jskj.cms.entity.assist.JskjItemTag;
import com.jskj.common.hibernate3.Updater;

/**
 * 微信业务内容列表
 * @author VAIO
 *
 */
public interface JskjItemTagDao {
	
	public List<JskjItemTag> getList(Integer itemid);
	
	public JskjItemTag findById(Integer id);

	public JskjItemTag save(JskjItemTag bean);

	public JskjItemTag updateByUpdater(Updater<JskjItemTag> updater);

	public JskjItemTag deleteById(Integer id);
	
}
