package com.jskj.cms.dao.main;

import java.util.List;

import com.jskj.cms.entity.main.Channel;
import com.jskj.common.hibernate3.Updater;
import com.jskj.common.page.Pagination;

public interface ChannelDao {
	public List<Channel> getTopList(Integer siteId, boolean hasContentOnly,
			boolean displayOnly,boolean belong, boolean cacheable);

	public Pagination getTopPage(Integer siteId, boolean hasContentOnly,
			boolean displayOnly,boolean belong, boolean cacheable, int pageNo, int pageSize);

	public List<Channel> getTopListByRigth(Integer userId, Integer siteId,
			boolean hasContentOnly,boolean belong);

	public List<Channel> getChildList(Integer parentId, boolean hasContentOnly,
			boolean displayOnly, boolean belong,boolean cacheable);

	public Pagination getChildPage(Integer parentId, boolean hasContentOnly,
			boolean displayOnly,boolean belong, boolean cacheable, int pageNo, int pageSize);

	public List<Channel> getChildListByRight(Integer userId, Integer parentId,
			boolean hasContentOnly,boolean belong);

	public Channel findByPath(String path, Integer siteId, boolean cacheable);

	public Channel findById(Integer id);

	public Channel save(Channel bean);

	public Channel updateByUpdater(Updater<Channel> updater);

	public Channel deleteById(Integer id);
}