package com.jskj.cms.dao.main;

import com.jskj.cms.entity.main.ContentTxt;
import com.jskj.common.hibernate3.Updater;

public interface ContentTxtDao {
	public ContentTxt findById(Integer id);

	public ContentTxt save(ContentTxt bean);

	public ContentTxt updateByUpdater(Updater<ContentTxt> updater);
}