package com.jskj.cms.dao.main;

import com.jskj.cms.entity.main.CmsUserExt;
import com.jskj.common.hibernate3.Updater;

public interface CmsUserExtDao {
	public CmsUserExt findById(Integer id);

	public CmsUserExt save(CmsUserExt bean);

	public CmsUserExt updateByUpdater(Updater<CmsUserExt> updater);
}