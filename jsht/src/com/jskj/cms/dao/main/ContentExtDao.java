package com.jskj.cms.dao.main;

import com.jskj.cms.entity.main.ContentExt;
import com.jskj.common.hibernate3.Updater;

public interface ContentExtDao {
	public ContentExt findById(Integer id);

	public ContentExt save(ContentExt bean);

	public ContentExt updateByUpdater(Updater<ContentExt> updater);
}