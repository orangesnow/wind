package com.jskj.cms.dao.main;

import com.jskj.cms.entity.main.CmsThirdAccount;

public interface CmsThirdAccountDao {
	public CmsThirdAccount findById(Integer id);

	public CmsThirdAccount findByThirdUid(String uid);
	
	public CmsThirdAccount save(CmsThirdAccount bean);


}