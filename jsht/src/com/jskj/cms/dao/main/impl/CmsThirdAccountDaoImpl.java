package com.jskj.cms.dao.main.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.main.CmsThirdAccountDao;
import com.jskj.cms.entity.main.CmsThirdAccount;
import com.jskj.common.hibernate3.HibernateBaseDao;

@Repository
public class CmsThirdAccountDaoImpl extends HibernateBaseDao<CmsThirdAccount, Integer> implements CmsThirdAccountDao {
	public CmsThirdAccount findById(Integer id) {
		CmsThirdAccount entity = get(id);
		return entity;
	}

	public CmsThirdAccount save(CmsThirdAccount bean) {
		getSession().save(bean);
		return bean;
	}
	
	@Override
	protected Class<CmsThirdAccount> getEntityClass() {
		return CmsThirdAccount.class;
	}

	@Override
	public CmsThirdAccount findByThirdUid(String thirdUid) {
		String hql = "from CmsThirdAccount bean where bean.thirdUid=:thirdUid";
		Query query = getSession().createQuery(hql).setString("thirdUid", thirdUid);
		return (CmsThirdAccount) query.uniqueResult();
	}
	
	
	

}