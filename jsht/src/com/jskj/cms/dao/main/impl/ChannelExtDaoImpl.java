package com.jskj.cms.dao.main.impl;

import org.springframework.stereotype.Repository;

import com.jskj.cms.dao.main.ChannelExtDao;
import com.jskj.cms.entity.main.ChannelExt;
import com.jskj.common.hibernate3.HibernateBaseDao;

@Repository
public class ChannelExtDaoImpl extends HibernateBaseDao<ChannelExt, Integer>
		implements ChannelExtDao {
	public ChannelExt save(ChannelExt bean) {
		getSession().save(bean);
		return bean;
	}

	@Override
	protected Class<ChannelExt> getEntityClass() {
		return ChannelExt.class;
	}
}