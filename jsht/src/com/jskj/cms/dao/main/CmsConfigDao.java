package com.jskj.cms.dao.main;

import com.jskj.cms.entity.main.CmsConfig;
import com.jskj.common.hibernate3.Updater;

public interface CmsConfigDao {
	public CmsConfig findById(Integer id);

	public CmsConfig updateByUpdater(Updater<CmsConfig> updater);
}