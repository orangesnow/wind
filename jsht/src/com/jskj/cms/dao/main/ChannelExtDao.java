package com.jskj.cms.dao.main;

import com.jskj.cms.entity.main.ChannelExt;
import com.jskj.common.hibernate3.Updater;

public interface ChannelExtDao {
	public ChannelExt save(ChannelExt bean);

	public ChannelExt updateByUpdater(Updater<ChannelExt> updater);
}