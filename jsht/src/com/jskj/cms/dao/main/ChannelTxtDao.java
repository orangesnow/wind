package com.jskj.cms.dao.main;

import com.jskj.cms.entity.main.ChannelTxt;
import com.jskj.common.hibernate3.Updater;

public interface ChannelTxtDao {
	public ChannelTxt findById(Integer id);

	public ChannelTxt save(ChannelTxt bean);

	public ChannelTxt updateByUpdater(Updater<ChannelTxt> updater);
}