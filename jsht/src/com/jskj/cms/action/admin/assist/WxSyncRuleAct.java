package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.daguu.lib.httpsqs4j.Httpsqs4j;
import com.daguu.lib.httpsqs4j.HttpsqsClient;
import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.entity.assist.WxSyncResult;
import com.jskj.cms.entity.assist.WxSyncRule;
import com.jskj.cms.entity.assist.WxSyncType;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.cms.manager.assist.WxSyncResultMng;
import com.jskj.cms.manager.assist.WxSyncRuleMng;
import com.jskj.cms.manager.assist.WxSyncTypeMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.util.StoreServerManager;
import com.jskj.common.util.SysConstants;
import com.jskj.common.web.CookieUtils;

@Controller
public class WxSyncRuleAct {

	private static final Logger logger = LoggerFactory.getLogger(WxSyncRuleAct.class);

	@RequestMapping("/wx_sync_rule/v_list.do")
	public String list(Integer stype,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		List<WxSyncType> stypes = syncTypeMng.getList();
		model.addAttribute("stypes", stypes);
		
		Pagination pagination = syncRuleMng.getPage(stype, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(stype != null ){
			model.addAttribute("stype", stype);
		}

		return "wx_sync_rule/list";
	}

	@RequestMapping("/wx_sync_rule/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		
		List<WxSyncType> stypes = syncTypeMng.getList();
		model.addAttribute("stypes", stypes);
		
		List<WxPublicAccount> accounts = publicAccountMng.getList(1);
		model.addAttribute("accounts", accounts);

		return "wx_sync_rule/add";
	}

	@RequestMapping("/wx_sync_rule/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		List<WxSyncType> stypes = syncTypeMng.getList();
		model.addAttribute("stypes", stypes);

		List<WxPublicAccount> accounts = publicAccountMng.getList(1);
		model.addAttribute("accounts", accounts);
		
		WxSyncRule rule = syncRuleMng.findById(id);
		model.addAttribute("rule", rule);

		return "wx_sync_rule/edit";
	}

	@RequestMapping("/wx_sync_rule/v_sync.do")
	public String sync(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		WxSyncRule rule = syncRuleMng.findById(id);
		
		if(rule == null || rule.getIsHistory() == 1 || rule.getStatus() == 0){
			model.addAttribute("message", "global.failure");
		}else {
			//生成待执行记录
			WxSyncResult bean = new WxSyncResult();
			bean.setOprtime(new Date());
			bean.setResult("【未完成】");
			
			bean.setRule(rule);
			bean.setStatus(0);
			syncResultMng.save(bean);
			
			System.out.println("sync:"+bean.getId());
			//发送信息到消息队列
			StoreServerManager.initQueue();
			HttpsqsClient httpSqsClient = Httpsqs4j.createNewClient();
			try {
				httpSqsClient.putString(SysConstants.SYNC_QUEUE, ""+bean.getId());
				model.addAttribute("message", "global.success");
			} catch (Exception e) {
				model.addAttribute("message", "global.failure");
			}
		}

		return list(-1,1,request,model);
	}
	
	
	@RequestMapping("/wx_sync_rule/o_save.do")
	public String save(WxSyncRule bean, Integer st,Integer pubtype,
			HttpServletRequest request, ModelMap model) {

		if(st == null || pubtype == null) return null;
		
		WxSyncType syncType = syncTypeMng.findById(st);
		bean.setStype(syncType);

		WxPublicAccount account = publicAccountMng.findById(pubtype);
		bean.setAccount(account);
		
		WxSyncRule nbean = syncRuleMng.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/wx_sync_rule/o_update.do")
	public String update(WxSyncRule bean,Integer st,Integer pubtype,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		if(st == null || pubtype == null) return null;
		
		WxSyncType syncType = syncTypeMng.findById(st);
		bean.setStype(syncType);
	
		WxPublicAccount account = publicAccountMng.findById(pubtype);
		bean.setAccount(account);
		
		syncRuleMng.update(bean);

		return list(-1,pageNo, request, model);
	}

	@RequestMapping("/wx_sync_rule/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		syncRuleMng.deleteByIds(ids);
		
		return list(null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		WxSyncRule entity = syncRuleMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private WxSyncRuleMng syncRuleMng;
	
	@Autowired
	private WxSyncTypeMng syncTypeMng;
	
	@Autowired
	private WxSyncResultMng syncResultMng;
	
	@Autowired
	private WxPublicAccountMng publicAccountMng;
	
}