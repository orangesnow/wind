package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.jskj.cms.entity.assist.JskjSubject;
import com.jskj.cms.entity.assist.JskjSubjectExt;
import com.jskj.cms.entity.assist.JskjSubjectGroup;
import com.jskj.cms.entity.assist.JskjSubjectPicture;
import com.jskj.cms.entity.assist.JskjUser;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjSubjectExtMng;
import com.jskj.cms.manager.assist.JskjSubjectGroupMng;
import com.jskj.cms.manager.assist.JskjSubjectMng;
import com.jskj.cms.manager.assist.JskjSubjectPictureMng;
import com.jskj.cms.manager.assist.JskjUserMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.upload.FileRepository;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjSubjectAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjSubjectAct.class);

	@RequestMapping("/jskj_subject/v_list.do")
	public String list(String staDate,String endDate, Integer groupid,Integer rtype,Integer status,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		List<JskjSubjectGroup> groups = subjectGroupMng.findAllGroups(1);
		model.addAttribute("groups", groups);
		
		Pagination pagination = subjectMng.getPage(staDate,endDate,groupid,rtype,status, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(staDate != null && !staDate.equals("")){
			model.addAttribute("staDate", staDate);
		}
		
		if(endDate != null && !endDate.equals("")){
			model.addAttribute("endDate", endDate);
		}
		
		if(groupid != null && groupid >-1 ){
			model.addAttribute("groupid", groupid);
		}
		
		if(rtype != null && rtype >-1 ){
			model.addAttribute("rtype", rtype);
		}

		if(status != null && status >-1){
			model.addAttribute("status", status);
		}

		return "jskj_subject/list";
	}

	@RequestMapping("/jskj_subject/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		
		List<JskjSubjectGroup> groups = subjectGroupMng.findAllGroups(1);
		model.addAttribute("groups", groups);
		
		return "jskj_subject/add";
	}
	
	@RequestMapping("/jskj_subject/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		List<JskjSubjectGroup> groups = subjectGroupMng.findAllGroups(1);
		model.addAttribute("groups", groups);
		
		JskjSubject subject = subjectMng.findById(id);
		model.addAttribute("subject", subject);

		JskjSubjectExt ext = subjectExtMng.findById(id);
		model.addAttribute("ext", ext);

		List<JskjSubjectPicture> pictures = subjectPictureMng.getList(id);
		model.addAttribute("pictures", pictures);
		
		return "jskj_subject/edit";
	}

	@RequestMapping("/jskj_subject/v_reply.do")
	public String reply(Integer id, HttpServletRequest request, ModelMap model) {
			return "redirect:../jskj_subject_reply/v_list.do?subjectid="+id;
	}
	
	@RequestMapping("/jskj_subject/o_save.do")
	public String save(JskjSubject bean, String[] attachmentPaths, String[] attachmentNames,
			String[] attachmentFilenames, Integer groupid,String body,HttpServletRequest request, ModelMap model) {
			
			System.out.println("=================");
			if(attachmentPaths != null && attachmentPaths.length >0){
				String picurl = attachmentPaths[0];
				bean.setPicurl(picurl);
			}
			
			if(groupid != null && groupid>-1){
				JskjSubjectGroup group = subjectGroupMng.findById(groupid);
				bean.setGroup(group);
			}
			
			JskjUser user = userMng.findById(1);
			bean.setOprtime(new Date());
			bean.setUser(user);
			JskjSubject nbean = subjectMng.save(bean);
			
			Integer subjectid = nbean.getId();
			JskjSubjectExt ext = new JskjSubjectExt();
			ext.setId(subjectid);
			ext.setRemark(body);
			subjectExtMng.save(ext);

			if(attachmentPaths != null && attachmentPaths.length >0){
				for(int i=0;i<attachmentPaths.length;i++){
					String picurl = attachmentPaths[i];
					String filename =  attachmentFilenames[i];
					String attchName = attachmentNames[i];
					
					JskjSubjectPicture picture = new JskjSubjectPicture();
					
					picture.setPicurl(picurl);
					picture.setSubjectid(subjectid);
					picture.setWeight(i);
					picture.setNewName(attchName);
					picture.setOriginName(filename);
					subjectPictureMng.save(picture);
				}
			}

			return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_subject/o_upload_attachment.do")
	public String uploadAttachment(
			@RequestParam(value = "attachmentFile", required = false) MultipartFile file,
			String attachmentNum, HttpServletRequest request, ModelMap model) {
		WebErrors errors = validateUpload(file, request);
		if (errors.hasErrors()) {
			model.addAttribute("error", errors.getErrors().get(0));
			return "jskj_subject/attachment_iframe";
		}
		CmsSite site = CmsUtils.getSite(request);
		String origName = file.getOriginalFilename();
		String ext = FilenameUtils.getExtension(origName).toLowerCase(
				Locale.ENGLISH);
		// TODO 检查允许上传的后缀
		try {
			String fileUrl;
		
			String ctx = request.getContextPath();
			fileUrl = fileRepository.storeByExt(site.getUploadPath(), ext,
						file);
				// 加上部署路径
			fileUrl = ctx + fileUrl;

			model.addAttribute("attachmentPath", fileUrl);
			model.addAttribute("attachmentName", origName);
			model.addAttribute("attachmentNum", attachmentNum);
		}catch (Exception e) {
			model.addAttribute("error", e.getMessage());
			logger.error("upload file error!", e);
		}
		return "jskj_subject/attachment_iframe";
	}
	
	private WebErrors validateUpload(MultipartFile file,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (errors.ifNull(file, "file")) {
			return errors;
		}
		return errors;
	}
	
	@RequestMapping("/jskj_subject/o_update.do")
	public String update(JskjSubject bean,Integer groupid,Integer userid,
			String[] attachmentPaths, String[] attachmentNames,
			String[] attachmentFilenames,String body,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		Integer subjectid = bean.getId();
		
		JskjSubjectGroup group = subjectGroupMng.findById(groupid);
		if(group != null){
			bean.setGroup(group);
		}
		
		JskjUser user = userMng.findById(userid);
		if(user != null){
			bean.setUser(user);
		}

		if(attachmentPaths != null && attachmentPaths.length >0){
			String picurl = attachmentPaths[0];
			bean.setPicurl(picurl);
		}
		
		subjectMng.update(bean);

		JskjSubjectExt ext = subjectExtMng.findById(subjectid);
		ext.setRemark(body);
		subjectExtMng.update(ext);
		
		List<JskjSubjectPicture> pictures = subjectPictureMng.getList(subjectid);
		if(pictures != null && pictures.size()>0){
			for(int i=0;i<pictures.size();i++){
				subjectPictureMng.deleteById(pictures.get(i).getId());
			}
		}
		
		if(attachmentPaths != null && attachmentPaths.length >0){
			for(int i=0;i<attachmentPaths.length;i++){
				String picurl = attachmentPaths[i];
				String filename =  attachmentFilenames[i];
				String attchName = attachmentNames[i];
				
				JskjSubjectPicture picture = new JskjSubjectPicture();
				
				picture.setPicurl(picurl);
				picture.setSubjectid(bean.getId());
				picture.setWeight(i);
				picture.setNewName(attchName);
				picture.setOriginName(filename);
				
				subjectPictureMng.save(picture);
			}
		}
		return list(null,null,null,null,-1,pageNo, request, model);
	}

	@RequestMapping("/jskj_subject/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		subjectMng.deleteByIds(ids);
		
		return list(null,null,null,null,-1,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjSubject entity = subjectMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}


	
	@Autowired
	private JskjSubjectMng subjectMng;
	
	@Autowired
	private JskjSubjectExtMng subjectExtMng;
	
	@Autowired
	private JskjSubjectPictureMng subjectPictureMng;
	
	@Autowired
	private JskjSubjectGroupMng subjectGroupMng;
	
	@Autowired
	private JskjUserMng userMng;
	
	@Autowired
	private FileRepository fileRepository;
}