package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.GhGoodsInfo;
import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.GhGoodsInfoMng;
import com.jskj.cms.manager.assist.WxFollowUserMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class GhGoodsInfoAct {

	private static final Logger logger = LoggerFactory.getLogger(GhGoodsInfoAct.class);

	@RequestMapping("/gh_goods_info/v_list.do")
	public String list(String name,String mobile,String staDate,String endDate, Integer pageNo, 
				HttpServletRequest request, ModelMap model) {

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = manager.getPage(name,mobile,staDate,endDate,cpn(pageNo),
			CookieUtils.getPageSize(request));

		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(name != null ){
			model.addAttribute("name", name);
		}
		
		if(mobile != null){
			model.addAttribute("mobile", mobile);
		}
		
		if(staDate != null){
			model.addAttribute("staDate", staDate);
		}
		
		if(endDate != null){
			model.addAttribute("endDate", endDate);
		}

		return "gh_goods_info/list";
	}

	@RequestMapping("/gh_goods_info/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "gh_goods_info/add";
	}
	
	@RequestMapping("/gh_goods_info/o_save.do")
	public String save(GhGoodsInfo bean, Integer userid,
			HttpServletRequest request, ModelMap model) throws IOException {

		manager.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/gh_goods_info/v_edit.do")
	public String edit(Integer id,HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		GhGoodsInfo goodsInfo = manager.findById(id);
		model.addAttribute("goodsInfo", goodsInfo);
		
		return "gh_goods_info/edit";
	}

	@RequestMapping("/gh_goods_info/o_update.do")
	public String update(GhGoodsInfo bean,Integer userid,Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		//保存记录
		manager.update(bean);
		return list(null,null,null,null,pageNo, request, model);
	}
	
	@RequestMapping("/gh_goods_info/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		manager.deleteByIds(ids);
		
		return list(null,null,null,null,pageNo, request, model);
	}
	
	
	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}


	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		GhGoodsInfo entity = manager.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}


	@Autowired
	private GhGoodsInfoMng manager;

}