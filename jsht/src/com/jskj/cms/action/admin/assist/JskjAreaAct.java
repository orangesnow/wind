package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjArea;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjAreaMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjAreaAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjAreaAct.class);

	@RequestMapping("/jskj_area/v_list.do")
	public String list(String name,Integer orderBy,Integer status,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		logger.info("name:"+name);
		logger.info("orderBy:"+orderBy);
		logger.info("status:"+status);
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = areaMng.getPage(name, orderBy,status, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(name != null && !name.equals("")){
			model.addAttribute("name", name);
		}

		if(orderBy != null && orderBy > -1){
			model.addAttribute("orderBy", orderBy);
		}

		if(status != null && status >-1){
			model.addAttribute("status", status);
		}

		return "jskj_area/list";
	}

	@RequestMapping("/jskj_area/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "jskj_area/add";
	}

	@RequestMapping("/jskj_area/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjArea  area = areaMng.findById(id);
		model.addAttribute("area", area);

		return "jskj_area/edit";
	}

	@RequestMapping("/jskj_area/o_save.do")
	public String save(JskjArea bean,Integer cid, 
			HttpServletRequest request, ModelMap model) {

		JskjArea nbean = areaMng.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_area/o_update.do")
	public String update(JskjArea bean,Integer cid,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		areaMng.update(bean);

		return list(null,null,null,pageNo, request, model);
	}

	@RequestMapping("/jskj_area/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		areaMng.deleteByIds(ids);
		
		return list(null,null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjArea entity = areaMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjAreaMng areaMng;
	
}