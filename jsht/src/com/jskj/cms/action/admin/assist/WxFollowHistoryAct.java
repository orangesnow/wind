package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.entity.assist.WxUserGroup;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.CmsWxMessageMng;
import com.jskj.cms.manager.assist.WxFollowRecordMng;
import com.jskj.cms.manager.assist.WxFollowUserMng;
import com.jskj.cms.manager.assist.WxMenuRecordMng;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.cms.manager.assist.WxUserGroupMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;
import com.jskj.common.web.ResponseUtils;

@Controller
public class WxFollowHistoryAct {

	private static final Logger logger = LoggerFactory.getLogger(WxFollowHistoryAct.class);

	@RequestMapping("/wx_follow_history/v_list.do")
	public String follow_list(Integer id,String openid, Integer ptype,Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = recordMng.getPage(openid,ptype,cpn(pageNo),
					CookieUtils.getPageSize(request));

		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());

		model.addAttribute("openid", openid);
		model.addAttribute("id", id);
		model.addAttribute("ptype", ptype);
		
		return "wx_follow_history/list";
	}
	
	@Autowired
	private WxFollowRecordMng recordMng;

	
}