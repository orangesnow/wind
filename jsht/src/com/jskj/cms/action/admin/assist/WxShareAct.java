package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.WxShare;
import com.jskj.cms.entity.assist.WxSyncResult;
import com.jskj.cms.entity.assist.WxSyncRule;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxShareMng;
import com.jskj.cms.manager.assist.WxSyncResultMng;
import com.jskj.cms.manager.assist.WxSyncRuleMng;
import com.jskj.cms.manager.assist.WxSyncTypeMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class WxShareAct {

	private static final Logger logger = LoggerFactory.getLogger(WxShareAct.class);

	@RequestMapping("/wx_share/v_list.do")
	public String list(String queryName,Integer cid,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = shareMng.getPage(cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());

		return "wx_share/list";
	}

	@RequestMapping("/wx_share/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "wx_share/add";
	}

	@RequestMapping("/wx_share/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		WxShare rule = shareMng.findById(id);
		model.addAttribute("tag", rule);
		
		return "wx_share/edit";
	}
	
	@RequestMapping("/wx_share/v_detail.do")
	public String detail(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		WxShare rule = shareMng.findById(id);
		model.addAttribute("tag", rule);
		
		return "wx_share/detail";
	}
	
	@RequestMapping("/wx_share/o_save.do")
	public String save(WxShare bean, 
			HttpServletRequest request, ModelMap model) {
		
		WxShare nbean = shareMng.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/wx_share/v_share.do")
	public String share(Integer id, HttpServletRequest request, ModelMap model) {
		
		if(id != null && id >0){
			WxShare rule = shareMng.findById(id);
			model.addAttribute("tag", rule);
			
			String path = request.getContextPath(); 
			String basePath = "http://"+request.getServerName()+path; 
			
			if(request.getServerPort()!= 80){
				basePath = "http://"+request.getServerName()+":"+request.getServerPort()+path; 
			}
			
			if(rule.getPicUrl().indexOf("/")>2){
				model.addAttribute("url", rule.getPicUrl());
			} else {
				model.addAttribute("url", basePath+rule.getPicUrl());
			}
			

			return "wx_share/share";
		}
		
		return null;

	}
	
	@RequestMapping("/wx_share/o_send.do")
	public String send(WxShare bean,String[] users,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		String path = request.getContextPath(); 
		String basePath = "http://"+request.getServerName()+path+"/"; 
		
		if(request.getServerPort()!= 80){
			basePath = "http://"+request.getServerName()+":"+request.getServerPort()+path+"/"; 
		}

		StringBuffer userlist = new StringBuffer();
		if( users != null && users.length >0 ){
			for(int i=0;i<users.length;i++){
				userlist.append(users[i]);
				if(i<users.length-1){
					userlist.append(",");
				}
			}
		}
		
		WxSyncRule rule = syncRuleMng.findById(8);
		
		WxSyncResult result = new WxSyncResult();
		result.setExetime(null);
		result.setOprtime(new Date());
		result.setResult("【未完成】");
		result.setStatus(0);
		result.setRule(rule);
	
		try {
			JSONObject json = new JSONObject();
			json.put("userlist", userlist);
			json.put("basePath", basePath);
			json.put("id", bean.getId());
			result.setParams(json.toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		syncResultMng.save(result);
		
		WxShare share = shareMng.findById(bean.getId());
		model.addAttribute("tag", share);
		
		return "wx_share/detail";
	
	}
	
	@RequestMapping("/wx_share/o_update.do")
	public String update(WxShare bean,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		shareMng.update(bean);

		return list(null,null,pageNo, request, model);
	}

	@RequestMapping("/wx_share/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		shareMng.deleteByIds(ids);
		
		return list(null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		WxShare entity = shareMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private WxSyncRuleMng syncRuleMng;
	
	@Autowired
	private WxShareMng shareMng;

	@Autowired
	private WxSyncResultMng syncResultMng;
	
	@Autowired
	private WxSyncTypeMng syncTypeMng;

}