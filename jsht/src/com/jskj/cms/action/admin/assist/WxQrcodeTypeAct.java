package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.WxQrcodeType;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxQrcodeTypeMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class WxQrcodeTypeAct {

	private static final Logger logger = LoggerFactory.getLogger(WxQrcodeTypeAct.class);

	@RequestMapping("/wx_qrcode_type/v_list.do")
	public String list(Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = typeMng.getPage(cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		return "wx_qrcode_type/list";
	}

	@RequestMapping("/wx_qrcode_type/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
	
		return "wx_qrcode_type/add";
	}

	
	@RequestMapping("/wx_qrcode_type/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		WxQrcodeType qrcodeType = typeMng.findById(id);
		model.addAttribute("group", qrcodeType);

		return "wx_qrcode_type/edit";
	}

	@RequestMapping("/wx_qrcode_type/o_save.do")
	public String save(WxQrcodeType bean, 
			HttpServletRequest request, ModelMap model) {

		WxQrcodeType nbean = typeMng.save(bean);
		System.out.println("nbean.getId():"+nbean.getId());
		
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/wx_qrcode_type/o_update.do")
	public String update(WxQrcodeType bean,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		typeMng.update(bean);

		return list(pageNo, request, model);
	}

	@RequestMapping("/wx_qrcode_type/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		typeMng.deleteByIds(ids);
		
		return list(pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		WxQrcodeType entity = typeMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	
	@Autowired
	private WxQrcodeTypeMng typeMng;
	
}