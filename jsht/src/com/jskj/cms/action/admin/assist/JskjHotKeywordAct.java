package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjHotKeyword;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjHotKeywordMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjHotKeywordAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjHotKeywordAct.class);

	@RequestMapping("/jskj_hotkeyword/v_list.do")
	public String list(String name,Integer htype,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = hotKeywordMng.getPage(name,htype, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(name != null && !name.equals("")){
			model.addAttribute("name", name);
		}

		if(htype != null && htype >-1){
			model.addAttribute("htype", htype);
		}
		
		return "jskj_hotkeyword/list";
	}

	@RequestMapping("/jskj_hotkeyword/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "jskj_hotkeyword/add";
	}

	@RequestMapping("/jskj_hotkeyword/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjHotKeyword keywords = hotKeywordMng.findById(id);
		model.addAttribute("hotkeyword", keywords);

		return "jskj_hotkeyword/edit";
	}

	@RequestMapping("/jskj_hotkeyword/o_save.do")
	public String save(JskjHotKeyword bean,
			HttpServletRequest request, ModelMap model) {

		JskjHotKeyword nbean = hotKeywordMng.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_hotkeyword/o_update.do")
	public String update(JskjHotKeyword bean,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		hotKeywordMng.update(bean);

		return list(null,null,pageNo, request, model);
	}

	@RequestMapping("/jskj_hotkeyword/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		hotKeywordMng.deleteByIds(ids);
		
		return list(null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjHotKeyword entity = hotKeywordMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjHotKeywordMng hotKeywordMng;

}