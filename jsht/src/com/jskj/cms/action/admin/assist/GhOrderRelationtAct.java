package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.GhOrderRelation;
import com.jskj.cms.entity.assist.GhVisitRecord;
import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.GhOrderRelationMng;
import com.jskj.cms.manager.assist.GhVisitRecordMng;
import com.jskj.cms.manager.assist.WxFollowUserMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class GhOrderRelationtAct {

	private static final Logger logger = LoggerFactory.getLogger(GhOrderRelationtAct.class);

	@RequestMapping("/gh_ret_visit/v_list.do")
	public String list(Integer visitFlag,Integer userId,String staDate,String endDate, Integer pageNo, 
				HttpServletRequest request, ModelMap model) {

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = manager.getPage(userId,visitFlag,staDate,endDate,cpn(pageNo),
			CookieUtils.getPageSize(request));

		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(staDate != null){
			model.addAttribute("staDate", staDate);
		}
		
		if(endDate != null){
			model.addAttribute("endDate", endDate);
		}
		
		if(userId != null){
			model.addAttribute("userId", userId);
		}
		
		if(visitFlag != null){
			model.addAttribute("visitFlag", visitFlag);
		} else {
			model.addAttribute("visitFlag", -1);
		}

		
		return "gh_ret_visit/list";
	}

	@RequestMapping("/gh_ret_visit/v_edit.do")
	public String edit(Integer id, Integer ownerId,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		GhOrderRelation orderRelation = manager.findById(id);
		model.addAttribute("orderRelation", orderRelation);
		
		return "gh_ret_visit/edit";
	}

	@RequestMapping("/gh_ret_visit/o_update.do")
	public String update(GhVisitRecord record,Integer rid,Integer ownerId,Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		GhVisitRecord bean = null;
		if(record.getId() != null){
			bean = visitRecordMng.update(record);
		} else {
			bean = visitRecordMng.save(record);
		}
		
		GhOrderRelation relation = manager.findById(rid);
		if(ownerId != null &&  ownerId >0){
			WxFollowUser owner = followUserMng.findById(ownerId);
			relation.setOwner(owner);
			relation.setGhVisit(bean);
			relation.setVisitFlag(1);
			relation.setOprtime(new Date());
			//保存记录
			manager.update(relation);
		}
		
		return list(null,null,null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		GhOrderRelation entity = manager.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private GhOrderRelationMng manager;
	
	@Autowired
	private WxFollowUserMng followUserMng;
	
	@Autowired
	private GhVisitRecordMng visitRecordMng;
	
	
}