package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.GhChannelInfo;
import com.jskj.cms.entity.assist.GhOrderInfo;
import com.jskj.cms.entity.assist.GhProduct;
import com.jskj.cms.entity.assist.GhTradeOrder;
import com.jskj.cms.entity.assist.JskjItemList;
import com.jskj.cms.entity.assist.JskjItemSku;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.GhChannelInfoMng;
import com.jskj.cms.manager.assist.GhIDsMng;
import com.jskj.cms.manager.assist.GhOrderInfoMng;
import com.jskj.cms.manager.assist.GhProductMng;
import com.jskj.cms.manager.assist.GhTradeOrderMng;
import com.jskj.cms.manager.assist.JskjItemListMng;
import com.jskj.cms.manager.assist.JskjItemSkuMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.util.ExcelUtil;
import com.jskj.common.util.Md5Util;
import com.jskj.common.web.CookieUtils;

@Controller
public class GhOrderInfoAct {

	private static final Logger logger = LoggerFactory.getLogger(GhOrderInfoAct.class);
	private static final int MAX_DOWN_SIZE = 1000;
	
	@RequestMapping("/gh_order/v_down.do")
	public String down(String orderId,Integer source,String channelId,String staDate,String endDate, Integer pageNo, 
				HttpServletRequest request, HttpServletResponse response,ModelMap model) {

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = manager.getPage(orderId,source,channelId,staDate,endDate,cpn(pageNo),
				MAX_DOWN_SIZE);

		String fileName="excel文件";
       
		//填充orders数据
        List orders=pagination.getList();
        List<Map<String,Object>> list=createExcelRecord(orders);

        String columnNames[]={"订单编号","数量","订单金额","下单时间","下单用户","收货人","收货地址","收货人联系方式","支付方式","商品编号"};//列名
        String keys[] = {"oid","nums","price","oprtime","username","revUserName","revAddress","revMobile","payType","pcode"};//map中的key
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        
        try {
            ExcelUtil.createWorkBook(list,keys,columnNames).write(os);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        byte[] content = os.toByteArray();
        InputStream is = new ByteArrayInputStream(content);
        // 设置response参数，可以打开下载页面
        response.reset();
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        try {
        	 response.setHeader("Content-Disposition", "attachment;filename="+ new String((fileName + ".xls").getBytes(), "iso-8859-1"));
             ServletOutputStream out = response.getOutputStream();
             BufferedInputStream bis = null;
             BufferedOutputStream bos = null;
             try {
                 bis = new BufferedInputStream(is);
                 bos = new BufferedOutputStream(out);
                 byte[] buff = new byte[2048];
                 int bytesRead;
                 // Simple read/write loop.
                 while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                     bos.write(buff, 0, bytesRead);
                 }
             } catch (final IOException e) {
                 throw e;
             } finally {
                 if (bis != null)
                     bis.close();
                 if (bos != null)
                     bos.close();
             }
		} catch (Exception e) {
			e.printStackTrace();
		}
       
        return null;
	}
	
    private List<Map<String, Object>> createExcelRecord(List<GhOrderInfo> orders) {
        List<Map<String, Object>> listmap = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("sheetName", "订单数据");
        listmap.add(map);
        GhOrderInfo order=null;
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (int j = 0; j < orders.size(); j++) {
            order=orders.get(j);
            Map<String, Object> mapValue = new HashMap<String, Object>();
            mapValue.put("oid", order.getOid());
            mapValue.put("nums", order.getNums());
            mapValue.put("price", order.getPrice());
            mapValue.put("oprtime", sdf.format(order.getOdTime()));
            mapValue.put("username", order.getSendName());
            mapValue.put("revUserName", order.getRecevName());
            mapValue.put("revAddress", order.getRecevAddress());
            mapValue.put("revMobile", order.getRecevMobile());
            mapValue.put("payType", order.getPayType());
            
            String pcode = (order.getPcode() == null)?"33-01":order.getPcode();
            mapValue.put("pcode", pcode);
            
            listmap.add(mapValue);
        }
        return listmap;
    }
	
	
	@RequestMapping("/gh_order/v_list.do")
	public String list(String orderId,Integer source,String channelId,String staDate,String endDate, Integer pageNo, 
				HttpServletRequest request, ModelMap model) {

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = manager.getPage(orderId,source,channelId,staDate,endDate,cpn(pageNo),
			CookieUtils.getPageSize(request));

		List channels = channelManager.getList();
		model.addAttribute("channels", channels);
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(orderId != null ){
			model.addAttribute("orderId", orderId);
		}else {
			model.addAttribute("orderId", null);
		}
		
		if(staDate != null){
			model.addAttribute("staDate", staDate);
		}
		
		if(endDate != null){
			model.addAttribute("endDate", endDate);
		}
		
		if(source != null){
			model.addAttribute("source", source);
		}
		
		if(channelId != null){
			model.addAttribute("channelId", channelId);
		}
		
		return "gh_order/list";
	}

	@RequestMapping("/gh_order/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		
		List<GhChannelInfo> channels =  channelInfoMng.getList();
		model.addAttribute("channels", channels);
		
		return "gh_order/add";
	}
	
	@RequestMapping("/gh_order/v_item_add.do")
	public String add_item(String id,String ids,HttpServletRequest request,ModelMap model) {
		
		if(id == null || id.equals("")){
			return null;
		}
		
		Integer itemid = 0;
		if(ids != null && ids.length() >0) {
			String[] itemids = ids.split(",");
			if(itemids != null && itemids.length >0){
				itemid = Integer.parseInt(itemids[0]);
			}
			
		} else {			
			String url = "../gh_order/v_item_add.do?id="+id;
			String from = "";
			try {
				from = URLEncoder.encode(url, "utf-8");
			} catch (Exception e) {
				logger.error("add_item Exception");
			}
			return "redirect:../jskj_item_list/v_list.do?from="+from;
		}
		
		model.addAttribute("id",id);
		
		JskjItemList item = itemListMng.findById(itemid);
		model.addAttribute("item", item);
		
		List<JskjItemSku> skus = itemSkuMng.getList(itemid);
		model.addAttribute("skus",skus);
		
		return "gh_order/item_add";
	}

	@RequestMapping("/gh_order/o_save.do")
	public String save(GhOrderInfo bean, 
			HttpServletRequest request, ModelMap model) throws IOException {

		bean.setGoodsStatus(0);
		manager.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/gh_order/v_edit.do")
	public String edit(String id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		GhOrderInfo orderInfo = manager.findById(id);
		model.addAttribute("orderInfo", orderInfo);
		
		List<GhChannelInfo> channels =  channelInfoMng.getList();
		model.addAttribute("channels", channels);

		return "gh_order/edit";
	}

	@RequestMapping("/gh_order/v_express.do")
	public String express(String id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		GhOrderInfo orderInfo = manager.findById(id);
		model.addAttribute("orderInfo", orderInfo);
		
		String addr = orderInfo.getRecevAddress();
		if(orderInfo.getRecevProv() == null ){
			model.addAttribute("prov", addr.substring(0, 2));
		} else {
			
			if(addr.startsWith(orderInfo.getRecevProv())){
				model.addAttribute("prov", orderInfo.getRecevProv());
			} else {
				model.addAttribute("prov", addr.substring(0, 2));
			}
			
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String currTime = sdf.format(new Date());
		model.addAttribute("year", currTime.substring(0, 4));
		model.addAttribute("month", currTime.substring(4, 6));
		model.addAttribute("day", currTime.substring(6, 8));
		
		manager.update(orderInfo);
		
		String outerid = orderInfo.getPcode();
		GhProduct product = productMng.findByOuterId(outerid);
		model.addAttribute("product", product);
		
		
		StringBuffer name = new StringBuffer("");
		List<GhTradeOrder> orders = tradeOrderMng.findByGhId(id);
		
		if(orders != null && orders.size() >0){
			HashMap<Integer,String> productMap = new HashMap<Integer,String>();
			
			List<GhProduct> products = productMng.getList(1);
			for(int i=0;i<products.size();i++){
				GhProduct pr = products.get(i);
				Integer numIid = pr.getPid();
				String title = pr.getTitle();
				String shortName = pr.getNickname();
				String mapTitle = (shortName== null)?getShortName(title):shortName;
				productMap.put(numIid, mapTitle);
			}
			
			if(orders.size() >3){
				name.append("橘色春雪系列产品");
			} else {
				for(int i=0;i<orders.size();i++){
					GhTradeOrder order = orders.get(i);
					Integer numIid = order.getItemid();
//					String title =	order.getTitle();
					//name.append(getShortName(title)).append("<br/>");
					name.append(productMap.get(numIid)).append("<br/>");
				}
			}
			
		} else {
			if(product == null) {
				name.append("标准款");
			} else {
				String title = product.getTitle();
				String shortName = product.getNickname();
				String mapTitle = (shortName== null)?getShortName(title):shortName;
				name.append(mapTitle);
			}
		}
		
		model.addAttribute("name", name);
		
		return "gh_order/express";
	}

	
	private String getShortName(String title){
		String[] ts = title.split("[,|:|-|，|-]");
		if(ts.length >0){
			return ts[0];
		}
		return "";
	}
	
	@RequestMapping("/gh_order/v_detail.do")
	public String detail(String id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		GhOrderInfo orderInfo = manager.findById(id);
		model.addAttribute("orderInfo", orderInfo);
		
		String outerid = orderInfo.getPcode();
		GhProduct product = productMng.findByOuterId(outerid);
		model.addAttribute("product", product);
		
		//设置已打印标志
		orderInfo.setGoodsStatus(2);
		manager.update(orderInfo);
		
		List<GhTradeOrder> orders = tradeOrderMng.findByGhId(id);
		model.addAttribute("orders", orders);
		
		return "gh_order/detail";
	}
	

	@RequestMapping("/gh_order/v_item.do")
	public String item(String id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		GhOrderInfo orderInfo = manager.findById(id);
		model.addAttribute("orderInfo", orderInfo);

		List<GhTradeOrder> orders =  tradeOrderMng.findByGhId(id);
		model.addAttribute("orders", orders);

		return "gh_order/item";
	}
	

	@RequestMapping("/gh_order/o_item_delete.do")
	public String item_delete(Integer id, HttpServletRequest request, ModelMap model) {

		GhTradeOrder tradeOnder = tradeOrderMng.deleteById(id);
		
		Integer orderNums = 0;
		Double orderPrice = 0.0;
		Double itemFee = 0.0;
		
		String ghid = tradeOnder.getGhid();
		GhOrderInfo orderInfo = manager.findById(ghid);
		List<GhTradeOrder> orders = tradeOrderMng.findByGhId(ghid);
		
		if(orders != null && orders.size() >0){
			for(int i=0;i<orders.size();i++){
				
				GhTradeOrder tradeOrder = orders.get(i);
				Integer num = tradeOrder.getNum();
				Double discountFee = tradeOrder.getDiscountFee();
				Double price = tradeOrder.getPrice();
				
				orderNums += num;
				orderPrice +=num*price;
				itemFee +=discountFee;
				
			}
		}
		
		orderInfo.setItemFee(itemFee);
		orderInfo.setPrice(orderPrice);
		orderInfo.setNums(orderNums);
		
		manager.update(orderInfo);
		
		return "redirect:v_list.do";
	}

	@RequestMapping("/gh_order/o_item_update.do")
	public String item_update(GhTradeOrder bean,
			HttpServletRequest request, ModelMap model) {
		
		GhTradeOrder tradeItem = tradeOrderMng.update(bean);
		String  ghid = tradeItem.getGhid();
		
		Integer orderNums = 0;
		Double orderPrice = 0.0;
		Double itemFee = 0.0;
		
		GhOrderInfo orderInfo = manager.findById(ghid);
		List<GhTradeOrder> orders = tradeOrderMng.findByGhId(ghid);
		
		if(orders != null && orders.size() >0){
			for(int i=0;i<orders.size();i++){
				
				GhTradeOrder tradeOrder = orders.get(i);
				Integer num = tradeOrder.getNum();
				Double discountFee = tradeOrder.getDiscountFee();
				Double price = tradeOrder.getPrice();
				
				orderNums += num;
				orderPrice +=num*price;
				itemFee +=discountFee;
				
			}
		}
		
		orderInfo.setItemFee(itemFee);
		orderInfo.setPrice(orderPrice);
		orderInfo.setNums(orderNums);
		
		manager.update(orderInfo);
		
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/gh_order/o_item_save.do")
	public String item_save(GhTradeOrder bean,
			HttpServletRequest request, ModelMap model) {

		GhTradeOrder trade = tradeOrderMng.save(bean);
		
		String  ghid = trade.getGhid();
		
		Integer orderNums = 0;
		Double orderPrice = 0.0;
		Double itemFee = 0.0;
		
		GhOrderInfo orderInfo = manager.findById(ghid);
		List<GhTradeOrder> orders = tradeOrderMng.findByGhId(ghid);
		
		if(orders != null && orders.size() >0){
			for(int i=0;i<orders.size();i++){
				
				GhTradeOrder tradeOrder = orders.get(i);
				Integer num = tradeOrder.getNum();
				Double discountFee = tradeOrder.getDiscountFee();
				Double price = tradeOrder.getPrice();
				
				orderNums += num;
				orderPrice +=num*price;
				itemFee +=discountFee;
				
			}
		}
		
		orderInfo.setItemFee(itemFee);
		orderInfo.setPrice(orderPrice);
		orderInfo.setNums(orderNums);
		
		manager.update(orderInfo);
		
		
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/gh_order/o_update.do")
	public String update(GhOrderInfo bean,Integer lastCardCode,Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		GhOrderInfo orderInfo = manager.findById(bean.getId());
		
		String buyerMessage = orderInfo.getBuyerMessage();
		String recevAddress = orderInfo.getRecevAddress();
		String recevMobile = orderInfo.getRecevMobile();
		String recevName = orderInfo.getRecevName();
		
		String demoMd5Str = Md5Util.encode(buyerMessage);
		String addressMd5Str = Md5Util.encode(recevAddress+recevMobile+recevName);

		String newBuyerMessage = bean.getBuyerMessage();
		String newRecevAddress = bean.getRecevAddress();
		String newRecevMobile = bean.getRecevMobile();
		String newRecevName = bean.getRecevName();
		
		String newDemoMd5Str = Md5Util.encode(newBuyerMessage);
		String newAddressMd5Str = Md5Util.encode(newRecevAddress+newRecevMobile+newRecevName);
		
		if(!demoMd5Str.equals(newDemoMd5Str)){
			bean.setDemoFlag(1);
		}
		
		if(!addressMd5Str.equals(newAddressMd5Str)){
			bean.setAddressFlag(1);
		}
	
		//保存记录
		manager.update(bean);

		return list(null,-1,null,null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(String id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}
	
	@RequestMapping("/gh_order/o_delete.do")
	public String delete(String[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		manager.deleteByIds(ids);
		return list(null,-1,null,null,null,pageNo, request, model);
	}
	
	@RequestMapping("/gh_order/o_sync.do")
	public String sync(String id, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
	
		GhOrderInfo orderInfo = manager.findById(id);
		orderInfo.setDutyStatus(2);

		return list(null,-1,null,null,null,pageNo, request, model);
	}
	

	private WebErrors validateUpdate(String id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(String[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (String id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(String id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		GhOrderInfo entity = manager.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}
	
	private boolean vldItemExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		GhTradeOrder entity = tradeOrderMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private GhTradeOrderMng tradeOrderMng;
	
	@Autowired
	private GhOrderInfoMng manager;

	@Autowired
	private GhIDsMng idsManager;
	
	@Autowired
	private GhChannelInfoMng channelManager;
	
	@Autowired
	private GhProductMng productMng;
	
	@Autowired
	private GhChannelInfoMng channelInfoMng;

	@Autowired
	private JskjItemListMng itemListMng;
	
	@Autowired
	private JskjItemSkuMng itemSkuMng;
}