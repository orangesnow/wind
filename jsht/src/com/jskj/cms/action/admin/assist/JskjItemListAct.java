package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjCategory;
import com.jskj.cms.entity.assist.JskjItemList;
import com.jskj.cms.entity.assist.JskjItemTag;
import com.jskj.cms.entity.assist.JskjTagList;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjCategoryMng;
import com.jskj.cms.manager.assist.JskjItemListMng;
import com.jskj.cms.manager.assist.JskjItemTagMng;
import com.jskj.cms.manager.assist.JskjTagListMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjItemListAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjItemListAct.class);

	@RequestMapping("/jskj_item_list/v_list.do")
	public String list(String name,Integer status,Integer cid, Integer rtype,String from,Integer pageNo,
				HttpServletRequest request, ModelMap model){

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = itemMng.getPage(name, status,cid,rtype, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(name != null && !name.equals("")){
			model.addAttribute("name", name);
		}

		if(rtype != null && rtype > -1){
			model.addAttribute("rtype", rtype);
		}

		if(status != null && status >-1){
			model.addAttribute("status", status);
		}
		
		if(from != null && !from.equals("")){
			model.addAttribute("from", from);
		}
		
		List<JskjCategory> categorys =  categoryMng.findCategorys(1);
		model.addAttribute("categorys", categorys);
		
		return "jskj_item_list/list";
	}

	@RequestMapping("/jskj_item_list/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		
		List<JskjCategory> categorys =  categoryMng.findCategorys(1);
		model.addAttribute("categorys", categorys);

		List<JskjTagList> tags = tagMng.getList(1);
		model.addAttribute("tags", tags);

		return "jskj_item_list/add";
	}

	@RequestMapping("/jskj_item_list/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjItemList  item = itemMng.findById(id);
		model.addAttribute("item", item);

		List<JskjCategory> categorys =  categoryMng.findCategorys(1);
		model.addAttribute("categorys", categorys);
		
		List<JskjItemTag> itemTags = itemTagMng.getList(id);
		model.addAttribute("itemTags", itemTags);
		
		List<JskjTagList> tags = tagMng.getList(1);
		model.addAttribute("tags", tags);
		
		return "jskj_item_list/edit";
		
	}

	@RequestMapping("/jskj_item_list/o_save.do")
	public String save(JskjItemList bean,Integer[] tagstr,Integer cid, 
			HttpServletRequest request, ModelMap model) {

		if(cid != null){
			JskjCategory  category = categoryMng.findById(cid);
			if(category != null){
				bean.setCategory(category);
			}
		}
		
		if(tagstr != null && tagstr.length >0){
			JskjTagList tag = tagMng.findById(tagstr[0]);
			bean.setTagstr(tag.getName());
		} else {
			bean.setTagstr("");
		}
		
		JskjItemList item = itemMng.save(bean);
		if(tagstr != null && tagstr.length >0){
			for(int i=0;i<tagstr.length;i++){
				JskjTagList tag = tagMng.findById(tagstr[i]);
				JskjItemTag itemTag = new JskjItemTag();
				itemTag.setItem(item);
				itemTag.setTag(tag);
				itemTag.setWeight(0);
				itemTag.setHotflag(0);
				itemTagMng.save(itemTag);
			}
		}
		
		return "redirect:v_list.do";
		
	}
	
	@RequestMapping("/jskj_item_list/o_update.do")
	public String update(JskjItemList bean,Integer[] tagstr,Integer cid,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		Integer itemid = bean.getId();
		
		if(cid != null){
			JskjCategory  category = categoryMng.findById(cid);
			if(category != null){
				bean.setCategory(category);
			}
		}
		
		if(tagstr != null && tagstr.length >0){
			JskjTagList tag = tagMng.findById(tagstr[0]);
			bean.setTagstr(tag.getName());
		} else {
			bean.setTagstr("");
		}
		
		JskjItemList item =itemMng.update(bean);

		tagMng.deleteTagRefFromItemId(itemid);
		if(tagstr != null && tagstr.length >0){
			for(int i=0;i<tagstr.length;i++){
				JskjTagList tag = tagMng.findById(tagstr[i]);
				JskjItemTag itemTag = new JskjItemTag();
				itemTag.setItem(item);
				itemTag.setTag(tag);
				itemTag.setWeight(0);
				itemTag.setHotflag(0);
				itemTagMng.save(itemTag);
			}
		}
		
		return list(null,null,null,null,null,pageNo, request, model);
	}

	@RequestMapping("/jskj_item_list/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		itemMng.deleteByIds(ids);
		
		return list(null,null,null,null,null,pageNo, request, model);
	}
	
	@RequestMapping("/jskj_item_list/o_filter.do")
	public String filter(Integer[] ids,String from, Integer pageNo,
			HttpServletRequest request, HttpServletResponse response,ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		StringBuffer query = new StringBuffer();
		if(ids != null && ids.length >0){
			for(int i=0;i<ids.length;i++){
				query.append(ids[i]).append(",");
			}
			query.deleteCharAt(query.length()-1);
		}
		
		String jump = "";
		if(from.indexOf("?") >0){
			jump =from+"&ids="+query.toString();
		} else {
			jump = from+"?ids="+query.toString();
		}
		
		try {
			response.sendRedirect(jump);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjItemList entity = itemMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjItemListMng itemMng;
	
	@Autowired
	private JskjCategoryMng categoryMng;
	
	@Autowired
	private JskjItemTagMng itemTagMng;
	
	@Autowired
	private JskjTagListMng tagMng;
	
}