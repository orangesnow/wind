package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.cms.entity.assist.WxUserGroup;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxFollowRecordMng;
import com.jskj.cms.manager.assist.WxFollowUserMng;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.cms.manager.assist.WxTagListMng;
import com.jskj.cms.manager.assist.WxUserGroupMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;
import com.jskj.common.web.ResponseUtils;

@Controller
public class WxFollowUserAct {

	private static final Logger logger = LoggerFactory.getLogger(WxFollowUserAct.class);

	@RequestMapping("/wx_follow_user/v_list.do")
	public String list(Integer accountId,Integer groupId,Integer subType,String staDate,String endDate,
			String province,String nickname,String name,String openid, Integer pageNo,
			String source,String sourceid,
			HttpServletRequest request, ModelMap model) {
		
		accountId = 4;
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);
		
		Pagination pagination = manager.getPage(accountId, groupId,subType,staDate,endDate,province,nickname,name,openid,cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		model.addAttribute("accountId", accountId);	
		List<WxUserGroup> groups = groupMng.getList(accountId);
		if(groups != null && groups.size()>0){
			model.addAttribute("groups", groups);
		}
			
		if(nickname != null){
			model.addAttribute("nickname",nickname);
		}
		
		if(name != null){
			model.addAttribute("name",name);
		}
		
		if(openid != null){
			model.addAttribute("openid",openid);
		}
		
		if(groupId != null ){
			model.addAttribute("groupId", groupId);
		}
		
		if(staDate != null ){
			model.addAttribute("staDate", staDate);
		}
		
		if(endDate != null ){
			model.addAttribute("endDate", endDate);
		}
		
		if(province != null ){
			model.addAttribute("province", province);
		}
		
		if(subType != null){
			model.addAttribute("subType", subType);
		}
		
		if(source != null ){
			model.addAttribute("source", source);
		}
		
		if(sourceid != null){
			model.addAttribute("sourceid", sourceid);
		}
		
		return "wx_follow_user/list";
	}
	
	@RequestMapping("/wx_follow_user/v_follow_list.do")
	public String follow_list(String openid, Integer ptype,Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = recordMng.getPage(openid,ptype,cpn(pageNo),
					CookieUtils.getPageSize(request));

		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());

		model.addAttribute("openid", openid);
		model.addAttribute("ptype", ptype);
		
		return "wx_follow_user/follow_list";
	}
	
	
	@RequestMapping("/wx_follow_user/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);
		
		List<WxTagList> tagLists = tagListMng.getList(); 
		model.addAttribute("tagLists", tagLists);
		
		return "wx_follow_user/add";
	}
	
	@RequestMapping("/wx_follow_user/v_group.do")
	public String group(HttpServletRequest request,ModelMap model) {
		return "wx_follow_user/group_list";
	}
	
	@RequestMapping("/wx_follow_user/by_group.do")
	public void group(Integer accountId,HttpServletRequest request,HttpServletResponse response,ModelMap model) {
		JSONArray arr = new JSONArray();
		if (accountId != null) {
			List<WxUserGroup> list = groupMng.getList(accountId);
			try {
				for (WxUserGroup t : list) {
					JSONObject o= new JSONObject();
					o.put("id", t.getGroupid());
					o.put("name", t.getName());
					arr.put(o);
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		ResponseUtils.renderJson(response, arr.toString());
	}

	@RequestMapping("/wx_follow_user/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);

		
		WxFollowUser followUser = manager.findById(id);
		model.addAttribute("followUser", followUser);
		
		List<WxTagList> tagLists = tagListMng.getList(); 
		tagLists.removeAll(followUser.getTags());
		model.addAttribute("tagLists", tagLists);

		List<WxUserGroup> groups = groupMng.getList(followUser.getAccount().getId());
		if(groups != null && groups.size()>0){
			model.addAttribute("groups", groups);
		}

		return "wx_follow_user/edit";
	}
	
	@RequestMapping("/wx_follow_user/v_detail.do")
	public String detail(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);

		
		WxFollowUser followUser = manager.findById(id);
		model.addAttribute("followUser", followUser);
		
		List<WxTagList> tagLists = tagListMng.getList(); 
		tagLists.removeAll(followUser.getTags());
		model.addAttribute("tagLists", tagLists);

		List<WxUserGroup> groups = groupMng.getList(followUser.getAccount().getId());
		if(groups != null && groups.size()>0){
			model.addAttribute("groups", groups);
		}

		return "wx_follow_user/detail";
	}


	@RequestMapping("/wx_follow_user/o_save.do")
	public String save(WxFollowUser bean,String tagAttr, Integer groupId,Integer accountId,
			HttpServletRequest request, ModelMap model) {

		if(accountId == null ) return null;
		
		WxPublicAccount account = accountMng.findById(accountId);
		bean.setAccount(account);
		
		if(groupId != null){
			WxUserGroup group = groupMng.findById(groupId);
			bean.setGroup(group);
		}
		bean.setStatus(1);
		
		if(tagAttr != null && !tagAttr.equals("")){
			String[] tags = tagAttr.split(",");
			WxFollowUser nbean = manager.save(bean,tags);
		} else {
			WxFollowUser nbean = manager.save(bean,null);
		}
		
		
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/wx_follow_user/o_update.do")
	public String update(WxFollowUser bean,String tagAttr,Integer accountId,Integer groupId,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getUserid(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		if(accountId == null ) return null;
		
		WxPublicAccount account = accountMng.findById(accountId);
		bean.setAccount(account);
		
		if(groupId != null){
			WxUserGroup group = groupMng.findById(groupId);
			bean.setGroup(group);
		}
		if(tagAttr != null && !tagAttr.equals("")){
			String[] tags = tagAttr.split(",");
			manager.update(bean,tags);
		} else {
			manager.update(bean,null);
		}
		

		return list(accountId,null,null,null,null,null,null,null,null,pageNo,null,null,request, model);

	}

	@RequestMapping("/wx_follow_user/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		manager.deleteByIds(ids);
		
		return list(null,null,null,null,null,null,null,null,null,pageNo,null,null, request, model);
	}
	
	@RequestMapping("/wx_follow_user/o_sign.do")
	public String sign(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateSign(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		manager.signByIds(ids);
		
		if(ids.length==0){
			return list(null,null,null,null,null,null,null,null,null,pageNo,null,null, request, model);
		}
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);

		
		WxFollowUser followUser = manager.findById(ids[0]);
		model.addAttribute("followUser", followUser);
		
		List<WxTagList> tagLists = tagListMng.getList(); 
		tagLists.removeAll(followUser.getTags());
		model.addAttribute("tagLists", tagLists);

		List<WxUserGroup> groups = groupMng.getList(followUser.getAccount().getId());
		if(groups != null && groups.size()>0){
			model.addAttribute("groups", groups);
		}

		return "wx_follow_user/detail";
	}
	
	@RequestMapping("/wx_follow_user/o_enter.do")
	public String enter(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEnter(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		manager.enterByIds(ids);
		
		if(ids.length==0){
			return list(null,null,null,null,null,null,null,null,null,pageNo,null,null, request, model);
		}
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);

		WxFollowUser followUser = manager.findById(ids[0]);
		model.addAttribute("followUser", followUser);
		
		List<WxTagList> tagLists = tagListMng.getList(); 
		tagLists.removeAll(followUser.getTags());
		model.addAttribute("tagLists", tagLists);

		List<WxUserGroup> groups = groupMng.getList(followUser.getAccount().getId());
		if(groups != null && groups.size()>0){
			model.addAttribute("groups", groups);
		}

		return "wx_follow_user/detail";
	}


	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}
	
	private WebErrors validateSign(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}
	
	private WebErrors validateEnter(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		WxFollowUser entity = manager.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private WxFollowUserMng manager;
	
	@Autowired
	private WxPublicAccountMng accountMng;
	
	@Autowired
	private WxUserGroupMng groupMng;
	
	@Autowired
	private WxFollowRecordMng recordMng;
	
	@Autowired
	private WxTagListMng tagListMng;
	
}