package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjCategory;
import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjCategoryMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjCategoryAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjCategoryAct.class);

	@RequestMapping("/jskj_category/v_list.do")
	public String list(String name, Integer status,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
	
		Pagination pagination = categoryMng.getPage(name,status, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(name != null && !name.equals("")){
			model.addAttribute("name", name);
		}
		
		if(status != null && status >-1){
			model.addAttribute("status", status);
		}

		return "jskj_category/list";
	}

	@RequestMapping("/jskj_category/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "jskj_category/add";
	}
	
	@RequestMapping("/jskj_category/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjCategory category = categoryMng.findById(id);
		model.addAttribute("category", category);

		return "jskj_category/edit";
	}

	@RequestMapping("/jskj_category/o_save.do")
	public String save(JskjCategory bean, 
			HttpServletRequest request, ModelMap model) {
		
			JskjCategory nbean = categoryMng.save(bean);

			return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_category/o_update.do")
	public String update(JskjCategory bean,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		categoryMng.update(bean);

		return list(null,-1,pageNo, request, model);
	}

	@RequestMapping("/jskj_category/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		categoryMng.deleteByIds(ids);
		
		return list(null,-1,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjCategory entity = categoryMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}


	@Autowired
	private JskjCategoryMng categoryMng;
	
}