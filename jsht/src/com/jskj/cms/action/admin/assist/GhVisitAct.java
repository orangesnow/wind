package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.GhOrderRelation;
import com.jskj.cms.entity.assist.GhVisitRecord;
import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.GhOrderRelationMng;
import com.jskj.cms.manager.assist.GhVisitRecordMng;
import com.jskj.cms.manager.assist.WxFollowUserMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class GhVisitAct {

	private static final Logger logger = LoggerFactory.getLogger(GhVisitAct.class);

	@RequestMapping("/gh_visit/v_list.do")
	public String list(Integer userId,String staDate,String endDate, Integer pageNo, 
				HttpServletRequest request, ModelMap model) {

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = manager.getPage(userId,null,staDate,endDate,cpn(pageNo),
			CookieUtils.getPageSize(request));

		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(staDate != null){
			model.addAttribute("staDate", staDate);
		}
		
		if(endDate != null){
			model.addAttribute("endDate", endDate);
		}
		
		if(userId != null){
			model.addAttribute("userId", userId);
		}

		return "gh_visit/list";
	}

	@Autowired
	private GhVisitRecordMng manager;
	
	@Autowired
	private WxFollowUserMng followUserMng;

}