package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjUser;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjUserMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjUserAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjUserAct.class);

	@RequestMapping("/jskj_user/v_list.do")
	public String list(String staDate,String endDate,String nickname,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = userMng.getPage(staDate, endDate, nickname,cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(staDate != null && !staDate.equals("")){
			model.addAttribute("staDate", staDate);
		}

		if(endDate != null && !endDate.equals("")){
			model.addAttribute("endDate", endDate);
		}
		
		if(nickname != null && !nickname.equals("")){
			model.addAttribute("nickname", nickname);
		}

		return "jskj_user/list";
	}

	@RequestMapping("/jskj_user/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "jskj_user/add";
	}

	@RequestMapping("/jskj_user/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjUser user = userMng.findById(id);
		model.addAttribute("user", user);

		return "jskj_user/edit";
	}

	@RequestMapping("/jskj_user/o_save.do")
	public String save(JskjUser bean,
			HttpServletRequest request, ModelMap model) {
		
		bean.setLogintime(new Date());
		JskjUser nbean = userMng.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_user/o_update.do")
	public String update(JskjUser bean,Integer cid,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		userMng.update(bean);

		return list(null,null,null,pageNo, request, model);
	}

	@RequestMapping("/jskj_user/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		userMng.deleteByIds(ids);
		
		return list(null,null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjUser entity = userMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjUserMng userMng;
	
}