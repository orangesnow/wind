package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.entity.assist.WxUserGroup;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.cms.manager.assist.WxUserGroupMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class WxUserGroupAct {

	private static final Logger logger = LoggerFactory.getLogger(WxUserGroupAct.class);

	@RequestMapping("/wx_user_group/v_list.do")
	public String list(Integer accountId,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);
		
		Pagination pagination = groupMng.getPage(accountId, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(accountId != null ){
			model.addAttribute("accountId", accountId);
		}

		return "wx_user_group/list";
	}

	@RequestMapping("/wx_user_group/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);
		
		return "wx_user_group/add";
	}

	
	@RequestMapping("/wx_user_group/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);

		WxUserGroup group = groupMng.findById(id);
		model.addAttribute("group", group);

		return "wx_user_group/edit";
	}

	@RequestMapping("/wx_user_group/o_save.do")
	public String save(WxUserGroup bean, Integer accountId,
			HttpServletRequest request, ModelMap model) {

		if(accountId == null ) return null;
		
		WxPublicAccount account = accountMng.findById(accountId);
		bean.setAccount(account);
		
		
		WxUserGroup nbean = groupMng.save(bean);
		System.out.println("nbean.getId():"+nbean.getGroupid());
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/wx_user_group/o_update.do")
	public String update(WxUserGroup bean,Integer accountId,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getGroupid(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		if(accountId == null ) return null;
		
		WxPublicAccount account = accountMng.findById(accountId);
		bean.setAccount(account);
	
		groupMng.update(bean);

		return list(accountId,pageNo, request, model);
	}

	@RequestMapping("/wx_user_group/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		groupMng.deleteByIds(ids);
		
		return list(null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		WxUserGroup entity = groupMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private WxPublicAccountMng accountMng;
	
	@Autowired
	private WxUserGroupMng groupMng;
	
}