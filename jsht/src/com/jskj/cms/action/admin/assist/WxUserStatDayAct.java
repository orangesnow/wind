package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxUserStatDayMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class WxUserStatDayAct {

	private static final Logger log = LoggerFactory.getLogger(WxUserStatDayAct.class);
	
	@RequestMapping("/wx_user_stat/v_list.do")
	public String list(Integer reportType, String staDate,String endDate, Integer pageNo, 
			HttpServletRequest request,ModelMap model) {
		
		if(reportType == null) reportType = 1;
		
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = manager.getPage(4,staDate,endDate,cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		model.addAttribute("reportType", reportType);
		if(staDate != null){
			model.addAttribute("staDate", staDate);
		}
		
		if(endDate != null){
			model.addAttribute("endDate", endDate);
		}
		
		
		return "wx_user_stat/list";
	}

	@Autowired
	private WxUserStatDayMng manager;

}