package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.danga.MemCached.MemCachedClient;
import com.jskj.cms.entity.assist.GhConfigSource;
import com.jskj.cms.entity.assist.WxAlertMessage;
import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.GhConfigSourceMng;
import com.jskj.cms.manager.assist.WxAlertMessageMng;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.common.page.Pagination;
import com.jskj.common.security.encoder.Md5PwdEncoder;
import com.jskj.common.util.Num62;
import com.jskj.common.util.StoreServerManager;
import com.jskj.common.web.CookieUtils;
import com.jskj.core.web.WebErrors;

@Controller
public class PortalAlertMessageAct {

	private static final Logger log = LoggerFactory.getLogger(PortalAlertMessageAct.class);

	public static final String WX_ALERT_MESSAGE_FOLLOW = "wx_alert_message_follow";
	public static final String WX_ALERT_MESSAGE_SEARCH = "wx_alert_message_search";
	public static final String WX_ALERT_MESSAGE_LEAVEWORD = "wx_alert_message_leaveword";
	public static final String  WX_ALERT_MESSAGE_MORE= "wx_alert_message_more";
	public static final String WX_MEMCACHE_NAME = "wx_memcache_name";
	
	public static final int WX_ALERT_FOLLOWMESSAGE_KEY = 1;
	public static final int WX_ALERT_SEARCHMESSAGE_KEY = 2;
	public static final int WX_ALERT_LEAVEWORDMESSAGE_KEY = 3;
	public static final int WX_ALERT_MOREMESSAGE_KEY = 4;
	
	public static final int ALERT_MESSAGE_ID = 4;
	
	
	@RequestMapping("/alert_portal/v_sync.do")
	public String syncToWeixin( HttpServletRequest request,
			ModelMap model) {
		
		StoreServerManager.initMemcache(WX_MEMCACHE_NAME);
		MemCachedClient memcacheClient = new MemCachedClient(WX_MEMCACHE_NAME,true,false);

		List<WxAlertMessage> follows = wxAlertMessageMng.findByCtype(WX_ALERT_FOLLOWMESSAGE_KEY,1);
		List<WxAlertMessage>searchs = wxAlertMessageMng.findByCtype(WX_ALERT_SEARCHMESSAGE_KEY,1);
		List<WxAlertMessage>leavewords = wxAlertMessageMng.findByCtype(WX_ALERT_LEAVEWORDMESSAGE_KEY,1);
		List<WxAlertMessage>others = wxAlertMessageMng.findByCtype(WX_ALERT_MOREMESSAGE_KEY,1);
		
		JSONArray followArray=JSONArray.fromObject(follows);
		JSONArray searchArray=JSONArray.fromObject(searchs);
		JSONArray leavewordArray=JSONArray.fromObject(leavewords);
		JSONArray otherArray=JSONArray.fromObject(others);
		
		boolean followFlag = memcacheClient.set(WX_ALERT_MESSAGE_FOLLOW, followArray.toString());
		boolean searchFlag = memcacheClient.set(WX_ALERT_MESSAGE_SEARCH, searchArray.toString());
		boolean leavewordFlag = memcacheClient.set(WX_ALERT_MESSAGE_LEAVEWORD, leavewordArray.toString());
		boolean otherFlag = memcacheClient.set(WX_ALERT_MESSAGE_MORE, otherArray.toString());
		
		if(followFlag && searchFlag && leavewordFlag && otherFlag ) {
			model.addAttribute("message", "global.success");
		} else {
			model.addAttribute("message", "global.failure");
		}
	
		return list(-1, 1, request, model);
	}
	

	@RequestMapping("/alert_portal/v_list.do")
	public String list(Integer ctype, Integer pageNo, 
				HttpServletRequest request, ModelMap model) {
		
		if(ctype == null) ctype = -1;
		
		Pagination pagination = wxAlertMessageMng.getPage(ALERT_MESSAGE_ID, ctype, 1, cpn(pageNo),
				CookieUtils.getPageSize(request));
		model.addAttribute("pagination", pagination);
		
		List<GhConfigSource> sources = configSourceMng.findByCategory(ALERT_MESSAGE_ID);
		model.addAttribute("sources", sources);
		
		model.addAttribute("ctype", ctype);
		

		return "alert_portal/list";
	}

	@RequestMapping("/alert_portal/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		
		Md5PwdEncoder encoder = new Md5PwdEncoder();
		String md5Ckey = Num62.longToN62(System.currentTimeMillis());
		model.addAttribute("ckey", encoder.encodePassword(md5Ckey));
		
		List<WxPublicAccount> accounts = publicAccountMng.getList(1);
		model.addAttribute("accounts",accounts);
		
		List<GhConfigSource> sources = configSourceMng.findByCategory(ALERT_MESSAGE_ID);
		model.addAttribute("sources", sources);
		
		return "alert_portal/add";
	}

	@RequestMapping("/alert_portal/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		List<GhConfigSource> sources = configSourceMng.findByCategory(ALERT_MESSAGE_ID);
		model.addAttribute("sources", sources);

		WxAlertMessage wxAlertMessage = wxAlertMessageMng.findById(id);
		model.addAttribute("wxAlertMessage", wxAlertMessage);
		
		List<WxPublicAccount> accounts = publicAccountMng.getList(1);
		model.addAttribute("accounts",accounts);
		
		
		return "alert_portal/edit";
	}

	@RequestMapping("/alert_portal/o_save.do")
	public String save(WxAlertMessage bean,Integer pubType, Integer ctype,
			HttpServletRequest request, ModelMap model) throws IOException {

		//WxPublicAccount account = publicAccountMng.findById(pubType);
		//bean.setAccount(account);
		
		GhConfigSource source = configSourceMng.findById(ctype);
		bean.setSource(source);
		
		wxAlertMessageMng.save(bean);
		
		return "redirect:v_list.do";
	}

	@RequestMapping("/alert_portal/o_update.do")
	public String update(WxAlertMessage bean,Integer pubType, Integer ctype,
			Integer pageNo,HttpServletRequest request, ModelMap model) {
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		//WxPublicAccount account = publicAccountMng.findById(pubType);
		//bean.setAccount(account);
		
		GhConfigSource source = configSourceMng.findById(ctype);
		bean.setSource(source);
		
		wxAlertMessageMng.update(bean);

		return list(bean.getSource().getId(),pageNo, request, model);
	}

	@RequestMapping("/alert_portal/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		wxAlertMessageMng.deleteByIds(ids);
		return list(-1,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	
	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		WxAlertMessage entity = wxAlertMessageMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}


	@Autowired
	private WxAlertMessageMng wxAlertMessageMng;
	
	@Autowired
	private GhConfigSourceMng configSourceMng;
	
	@Autowired
	private WxPublicAccountMng publicAccountMng;
	
}