package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjItemList;
import com.jskj.cms.entity.assist.JskjItemPicture;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjCategoryMng;
import com.jskj.cms.manager.assist.JskjItemListMng;
import com.jskj.cms.manager.assist.JskjItemPictureMng;
import com.jskj.cms.manager.assist.JskjItemTagMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjItemPictureAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjItemPictureAct.class);

	@RequestMapping("/jskj_item_picture/v_list.do")
	public String list(Integer itemid, Integer ptype,Integer pageNo,
				HttpServletRequest request, ModelMap model){

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = itemPictureMng.getPage(itemid,ptype, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		JskjItemList item = itemMng.findById(itemid);
		model.addAttribute("item", item);
	
		if(itemid != null && itemid > -1){
			model.addAttribute("itemid", itemid);
		}

		if(ptype != null && ptype >-1){
			model.addAttribute("status", ptype);
		}
		

		
		return "jskj_item_picture/list";
	}

	@RequestMapping("/jskj_item_picture/v_add.do")
	public String add(Integer itemid,HttpServletRequest request,ModelMap model) {
	
		JskjItemList item = itemMng.findById(itemid);
		model.addAttribute("item", item);
		
		return "jskj_item_picture/add";
	}

	@RequestMapping("/jskj_item_picture/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjItemPicture  picture = itemPictureMng.findById(id);
		model.addAttribute("picture", picture);

		Integer itemid = picture.getItemid();
		JskjItemList item = itemMng.findById(itemid);
		model.addAttribute("item", item);
		
		return "jskj_item_picture/edit";
		
	}

	@RequestMapping("/jskj_item_picture/o_save.do")
	public String save(JskjItemPicture bean,
			HttpServletRequest request, ModelMap model) {

		JskjItemPicture item = itemPictureMng.save(bean);
	
		return "redirect:v_list.do?itemid="+item.getItemid();
		
	}
	
	@RequestMapping("/jskj_item_picture/o_update.do")
	public String update(JskjItemPicture bean,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		itemPictureMng.update(bean);

		return list(bean.getItemid(),null,pageNo, request, model);
	}

	@RequestMapping("/jskj_item_picture/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		JskjItemPicture[] pictures = itemPictureMng.deleteByIds(ids);
		
		return list(pictures[0].getItemid(),null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjItemPicture entity = itemPictureMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjItemPictureMng itemPictureMng;
	
	@Autowired
	private JskjCategoryMng categoryMng;
	
	@Autowired
	private JskjItemTagMng itemTagMng;
	
	@Autowired
	private JskjItemListMng itemMng;
	
}