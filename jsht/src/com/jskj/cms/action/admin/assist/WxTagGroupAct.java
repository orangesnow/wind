package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.entity.assist.WxTagGroup;
import com.jskj.cms.entity.assist.WxTagList;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.cms.manager.assist.WxTagGroupMng;
import com.jskj.cms.manager.assist.WxTagListMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class WxTagGroupAct {

	private static final Logger logger = LoggerFactory.getLogger(WxTagGroupAct.class);

	@RequestMapping("/wx_tag_group/v_list.do")
	public String list(Integer accountId,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);
		
		Pagination pagination = groupMng.getPage(accountId, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(accountId != null ){
			model.addAttribute("accountId", accountId);
		}

		return "wx_tag_group/list";
	}

	@RequestMapping("/wx_tag_group/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);
		
		List<WxTagList> tagLists = tagListMng.getList(); 
		model.addAttribute("tagLists", tagLists);
		
		return "wx_tag_group/add";
	}

	
	@RequestMapping("/wx_tag_group/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);


		WxTagGroup group = groupMng.findById(id);
		model.addAttribute("group", group);

		List<WxTagList> tagLists = tagListMng.getList(); 
		tagLists.removeAll(group.getTags());
		model.addAttribute("tagLists", tagLists);

		return "wx_tag_group/edit";
	}

	@RequestMapping("/wx_tag_group/o_save.do")
	public String save(WxTagGroup bean, Integer accountId,String tagAttr,
			HttpServletRequest request, ModelMap model) {

		if(accountId == null ) return null;
		
		WxPublicAccount account = accountMng.findById(accountId);
		bean.setAccount(account);
		
		if(tagAttr != null && !tagAttr.equals("")){
			String[] tags = tagAttr.split(",");
			WxTagGroup nbean = groupMng.save(bean,tags);
		} else {
			WxTagGroup nbean = groupMng.save(bean,null);
		}

		return "redirect:v_list.do";
	}
	
	@RequestMapping("/wx_tag_group/o_update.do")
	public String update(WxTagGroup bean,Integer accountId,String tagAttr,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		if(accountId == null ) return null;
		
		WxPublicAccount account = accountMng.findById(accountId);
		bean.setAccount(account);
	
		if(tagAttr != null && !tagAttr.equals("")){
			String[] tags = tagAttr.split(",");
			groupMng.update(bean,tags);
		} else {
			groupMng.update(bean,null);
		}

		return list(accountId,pageNo, request, model);
	}

	@RequestMapping("/wx_tag_group/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		groupMng.deleteByIds(ids);
		
		return list(null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		WxTagGroup entity = groupMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private WxPublicAccountMng accountMng;
	
	@Autowired
	private WxTagGroupMng groupMng;
	
	@Autowired
	private WxTagListMng tagListMng;
	
}