package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.SfuDataCompany;
import com.jskj.cms.entity.assist.SfuDataContact;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.SfuDataCompanyMng;
import com.jskj.cms.manager.assist.SfuDataContactMng;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class SfuDataCompanyAct {

	private static final Logger log = LoggerFactory.getLogger(SfuDataCompanyAct.class);
	public static final String CUSTOM_MT_QUEUE = "weixin_custom_queue";
	
	@RequestMapping("/sfu_data/v_list.do")
	public String list(String city,Integer pageNo, HttpServletRequest request,
			ModelMap model) {
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = manager.getPage(city, cpn(pageNo),CookieUtils.getPageSize(request));

		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(city != null ){
			model.addAttribute("city", city);
		}

		return "sfu_data/list";
	}

	@RequestMapping("/sfu_data/v_edit.do")
	public String edit(Integer id, Integer pageNo, HttpServletRequest request,
			ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		SfuDataCompany dataCompany = manager.findById(id);
		
		model.addAttribute("company", dataCompany);
		model.addAttribute("pageNo", pageNo);
		
		List<SfuDataContact> contacts = contactMng.getList(id);
		model.addAttribute("contacts", contacts);
		
		return "sfu_data/edit";
	}

	@RequestMapping("/sfu_data/o_update.do")
	public String update(SfuDataCompany bean,Integer pubType, Integer pageNo, HttpServletRequest request,
			ModelMap model) {
		WebErrors errors = validateUpdate(bean.getCompanyId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		
	
		return 	list(null,1,  request, model) ;
		
	}

	@RequestMapping("/sfu_data/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		SfuDataCompany[] beans = manager.deleteByIds(ids);
		
		return 	list(null,1,request,model) ;
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		CmsSite site = CmsUtils.getSite(request);
		if (vldExist(id, site.getId(), errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		CmsSite site = CmsUtils.getSite(request);
		if (vldExist(id, site.getId(), errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		CmsSite site = CmsUtils.getSite(request);
		if (errors.ifEmpty(ids, "ids")) {
			return errors;
		}
		for (Integer id : ids) {
			vldExist(id, site.getId(), errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, Integer siteId, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		SfuDataCompany entity = manager.findById(id);
		if (errors.ifNotExist(entity, SfuDataCompany.class, id)) {
			return true;
		}
		
		return false;
	}

	@Autowired
	private SfuDataCompanyMng manager;
	
	@Autowired
	private SfuDataContactMng contactMng;
	
	@Autowired
	private WxPublicAccountMng accountMng;
}