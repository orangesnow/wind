package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjShortMessage;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjShortMessageMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjShortMessageAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjShortMessageAct.class);

	@RequestMapping("/jskj_shortmessage/v_list.do")
	public String list(String name,String staDate,String endDate, Integer status,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		if(name != null ){
			name = name.replaceAll("'", "逗号");
			model.addAttribute("name", name);
		}
		
		Pagination pagination = messageMng.getPage(name,staDate, endDate,status, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(staDate != null && !staDate.equals("")){
			model.addAttribute("staDate", staDate);
		}
		
		if(endDate != null && !endDate.equals("")){
			model.addAttribute("endDate", endDate);
		}

		if(status != null && status >-1){
			model.addAttribute("status", status);
		}

		return "jskj_shortmessage/list";
	}

	@RequestMapping("/jskj_shortmessage/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "jskj_shortmessage/add";
	}

	@RequestMapping("/jskj_shortmessage/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjShortMessage message = messageMng.findById(id);
		model.addAttribute("message", message);

		return "jskj_shortmessage/edit";
	}

	@RequestMapping("/jskj_shortmessage/o_save.do")
	public String save(JskjShortMessage bean,
			HttpServletRequest request, ModelMap model) {

		bean.setLasttime(new Date());
		if(bean.getOprtime() == null ){
			bean.setOprtime(new Date());
		}
		
		JskjShortMessage nbean = messageMng.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_shortmessage/o_update.do")
	public String update(JskjShortMessage bean,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		Integer status = bean.getStatus();
		if(status == 0){
			bean.setReadtime(null);
		}
		bean.setLasttime(new Date());
		messageMng.update(bean);

		return list(null,null,null,-1,pageNo, request, model);
	}

	@RequestMapping("/jskj_shortmessage/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		messageMng.deleteByIds(ids);
		
		return list(null,null,null,-1,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjShortMessage entity = messageMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjShortMessageMng messageMng;
	
}