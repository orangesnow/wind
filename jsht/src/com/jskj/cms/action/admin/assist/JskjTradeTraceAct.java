package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.GhOrderInfo;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjTradeTraceMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjTradeTraceAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjTradeTraceAct.class);
	private static final int MAX_DOWN_SIZE = 1000;

	@RequestMapping("/trade_trace/v_list.do")
	public String list(String tradeid,String sid, Integer pageNo, 
				HttpServletRequest request, ModelMap model) {

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = manager.getPage(tradeid,sid,cpn(pageNo),
			CookieUtils.getPageSize(request));

		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(tradeid != null ){
			model.addAttribute("tradeid", tradeid);
		}else {
			model.addAttribute("tradeid", null);
		}
		
		if(sid != null ){
			model.addAttribute("sid", sid);
		}else {
			model.addAttribute("sid", null);
		}

		return "trade_trace/list";
	}

	
	@Autowired
	private JskjTradeTraceMng manager;
	
}