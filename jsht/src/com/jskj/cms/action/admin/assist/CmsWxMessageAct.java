package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.daguu.lib.httpsqs4j.Httpsqs4j;
import com.daguu.lib.httpsqs4j.HttpsqsClient;
import com.jskj.cms.entity.assist.CmsWxMessage;
import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.CmsWxMessageMng;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.util.DateJsonValueProcessor;
import com.jskj.common.util.StoreServerManager;
import com.jskj.common.web.CookieUtils;

@Controller
public class CmsWxMessageAct {

	private static final Logger log = LoggerFactory.getLogger(CmsWxMessageAct.class);
	public static final String CUSTOM_MT_QUEUE = "weixin_custom_queue";
	
	@RequestMapping("/wx_message/v_list.do")
	public String list(Integer id,Integer status,String openid,Integer pubType,
			Integer orderBy,Integer pageNo, HttpServletRequest request,
			ModelMap model) {
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		Pagination pagination = manager.getPage(status,openid,pubType,orderBy, cpn(pageNo),
				CookieUtils.getPageSize(request));

		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);

		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(status != null ){
			model.addAttribute("status", status);
		}
		
		if(openid !=null && !openid.equals("")){
			model.addAttribute("openid", openid);
		}
		
		if(id !=null && !id.equals("")){
			model.addAttribute("id", id);
		}
		
		if(pubType != null){
			model.addAttribute("pubType", pubType);
		}
		
		return "wx_message/list";
	}

	@RequestMapping("/wx_message/v_edit.do")
	public String edit(String id, Integer pageNo, HttpServletRequest request,
			ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		CmsWxMessage cmsWxMessage = manager.findById(id);
		model.addAttribute("cmsWxMessage", cmsWxMessage);
		model.addAttribute("pageNo", pageNo);

		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);

		return "wx_message/edit";
	}

	@RequestMapping("/wx_message/o_update.do")
	public String update(CmsWxMessage bean,Integer pubType, Integer pageNo, HttpServletRequest request,
			ModelMap model) {
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		Date now=new Date();
		bean.setStatus(CmsWxMessage.STATUS_RELPY_NOSEND);
		bean.setReplyTime(now);
		
		
		WxPublicAccount account = accountMng.findById(pubType);
		bean.setAccount(account);
		
		bean = manager.update(bean);
		
		StoreServerManager.initQueue();
		HttpsqsClient httpSqsClient = Httpsqs4j.createNewClient();
		
		JsonConfig config = new JsonConfig();
		config.registerJsonValueProcessor(java.util.Date.class,new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss")); 
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 
		JSONObject json = JSONObject.fromObject(bean,config );
		
		try {
			httpSqsClient.putString(CUSTOM_MT_QUEUE,  json.toString());
		} catch (Exception e) {
			log.error("sync failure!!");
		}
	
		return 	list(null,null, bean.getOpenid(),bean.getAccount().getId(), 2, 1,  request, model) ;
		
	}

	@RequestMapping("/wx_message/o_delete.do")
	public String delete(String[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		CmsWxMessage[] beans = manager.deleteByIds(ids);
		for (CmsWxMessage bean : beans) {
			log.info("delete CmsGuestbook id={}", bean.getId());
		}
		
		return 	list(null, null, null,null,1, 1,  request,
				 model) ;
	}

	private WebErrors validateEdit(String id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		CmsSite site = CmsUtils.getSite(request);
		if (vldExist(id, site.getId(), errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(String id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		CmsSite site = CmsUtils.getSite(request);
		if (vldExist(id, site.getId(), errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateDelete(String[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		CmsSite site = CmsUtils.getSite(request);
		if (errors.ifEmpty(ids, "ids")) {
			return errors;
		}
		for (String id : ids) {
			vldExist(id, site.getId(), errors);
		}
		return errors;
	}

	private boolean vldExist(String id, Integer siteId, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		CmsWxMessage entity = manager.findById(id);
		if (errors.ifNotExist(entity, CmsWxMessage.class, id)) {
			return true;
		}
		
		return false;
	}

	@Autowired
	private CmsWxMessageMng manager;
	
	
	@Autowired
	private WxPublicAccountMng accountMng;
}