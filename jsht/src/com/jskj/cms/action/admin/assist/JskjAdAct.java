package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjAd;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjAdMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjAdAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjAdAct.class);

	@RequestMapping("/jskj_ad/v_list.do")
	public String list(Integer adtype,Integer status,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = adMng.getPage(adtype, status, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(adtype != null && adtype > -1){
			model.addAttribute("adtype", adtype);
		}

		if(status != null && status >-1){
			model.addAttribute("status", status);
		}

		return "jskj_ad/list";
	}

	@RequestMapping("/jskj_ad/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "jskj_ad/add";
	}

	@RequestMapping("/jskj_ad/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjAd ad = adMng.findById(id);
		model.addAttribute("ad", ad);

		return "jskj_ad/edit";
	}

	@RequestMapping("/jskj_ad/o_save.do")
	public String save(JskjAd bean,Integer cid, 
			HttpServletRequest request, ModelMap model) {

		JskjAd nbean = adMng.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_ad/o_update.do")
	public String update(JskjAd bean,Integer cid,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		adMng.update(bean);

		return list(null,null,pageNo, request, model);
	}

	@RequestMapping("/jskj_ad/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		adMng.deleteByIds(ids);
		
		return list(null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjAd entity = adMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjAdMng adMng;
	
}