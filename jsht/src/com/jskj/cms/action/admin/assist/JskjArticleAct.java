package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjArticle;
import com.jskj.cms.entity.assist.JskjArticleCategory;
import com.jskj.cms.entity.assist.JskjArticleExt;
import com.jskj.cms.entity.assist.base.SimpleCate;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjArticleCategoryMng;
import com.jskj.cms.manager.assist.JskjArticleExtMng;
import com.jskj.cms.manager.assist.JskjArticleMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.upload.FileRepository;
import com.jskj.common.web.CookieUtils;
import com.jskj.common.web.ResponseUtils;

@Controller
public class JskjArticleAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjArticleAct.class);

	@RequestMapping("/jskj_article/v_list.do")
	public String list(String name,Integer status,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = articleMng.getPage(name,status, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(name != null && !name.equals("")){
			model.addAttribute("name", name);
		}
	
		if(status != null && status >-1){
			model.addAttribute("status", status);
		}

		return "jskj_article/list";
	}

	@RequestMapping("/jskj_article/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {

		List categorys = categoryMng.getList(null, null, 1);
		model.addAttribute("categorys", categorys);
		
		return "jskj_article/add";
	}
	
	@RequestMapping("/jskj_article/by_category.do")
	public void by_category(Integer rid,HttpServletRequest request,HttpServletResponse response,ModelMap model) {
		
		List<JskjArticleCategory> categorys =  categoryMng.getList("", rid, 1);
		
		ArrayList<SimpleCate> cates = new ArrayList<SimpleCate>();
		if(categorys != null && categorys.size() >0){
			for(int i=0;i<categorys.size();i++){
				JskjArticleCategory cate = categorys.get(i);
				SimpleCate sc = new SimpleCate();
				sc.setId(cate.getId());
				sc.setName(cate.getName());
				cates.add(sc);
			}
		}
		
		JsonConfig config = new JsonConfig();

		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT); 
		
		JSONArray jsonObj = JSONArray.fromObject(cates,config);
		ResponseUtils.renderJson(response, jsonObj.toString());

	}
	
	@RequestMapping("/jskj_article/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		JskjArticle article = articleMng.findById(id);
		model.addAttribute("article", article);

		JskjArticleExt ext =  articleExtMng.findById(id);
		model.addAttribute("ext", ext);
	
		List categorys = categoryMng.getList(null, null, 1);
		model.addAttribute("categorys", categorys);
		
		JskjArticleCategory category = article.getCategory();
		if(category != null){
			Integer parentid = category.getParent().getId();
			List<JskjArticleCategory> subs = categoryMng.getList("", parentid, 1);
			model.addAttribute("subs", subs);
		}
		
		return "jskj_article/edit";
	}

	@RequestMapping("/jskj_article/o_save.do")
	public String save(JskjArticle bean, Integer categoryid,String body,HttpServletRequest request, ModelMap model) {
	
		if(categoryid != null && categoryid >0){
			JskjArticleCategory jsc = categoryMng.findById(categoryid);
			bean.setCategory(jsc);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		bean.setCreated(sdf.format(new Date()));
		bean.setReads(0);
		JskjArticle nbean = articleMng.save(bean);
				
		Integer articleid = nbean.getId();
		JskjArticleExt ext = new JskjArticleExt();
		ext.setId(articleid);
		ext.setBody(body);
		articleExtMng.save(ext);

		return "redirect:v_list.do";
		
	}

	@RequestMapping("/jskj_article/o_update.do")
	public String update(JskjArticle bean,Integer categoryid,Integer userid,
			String[] attachmentPaths, String[] attachmentNames,
			String[] attachmentFilenames,String body,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		Integer articleid = bean.getId();

		if(attachmentPaths != null && attachmentPaths.length >0){
			String picurl = attachmentPaths[0];
			bean.setPicurl(picurl);
		}
		

		if(categoryid != null && categoryid >0){
			JskjArticleCategory jsc = categoryMng.findById(categoryid);
			bean.setCategory(jsc);
		}
		
		
		articleMng.update(bean);

		JskjArticleExt ext = articleExtMng.findById(articleid);
		ext.setBody(body);
		articleExtMng.update(ext);

		return list(null,-1,pageNo, request, model);
	}

	@RequestMapping("/jskj_article/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		articleExtMng.deleteByIds(ids);
		articleMng.deleteByIds(ids);
		
		return list(null,-1,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjArticle entity = articleMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}
	
	@Autowired
	private JskjArticleMng articleMng;
	
	@Autowired
	private JskjArticleExtMng articleExtMng;

	@Autowired
	private FileRepository fileRepository;
	
	@Autowired
	private JskjArticleCategoryMng categoryMng;
	
}