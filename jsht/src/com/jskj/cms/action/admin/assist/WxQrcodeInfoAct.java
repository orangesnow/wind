package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.WxQrcodeInfo;
import com.jskj.cms.entity.assist.WxQrcodeType;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxQrcodeInfoMng;
import com.jskj.cms.manager.assist.WxQrcodeTypeMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.util.Num62;
import com.jskj.common.web.CookieUtils;

@Controller
public class WxQrcodeInfoAct {

	private static final Logger logger = LoggerFactory.getLogger(WxQrcodeInfoAct.class);

	@RequestMapping("/wx_qrcode_info/v_list.do")
	public String list(Integer typeId,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		List<WxQrcodeType> types = typeMng.getList();
		model.addAttribute("types", types);
		
		Pagination pagination = qrcodeMng.getPage(typeId, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(typeId != null ){
			model.addAttribute("typeId", typeId);
		}

		return "wx_qrcode_info/list";
	}

	@RequestMapping("/wx_qrcode_info/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		
		List<WxQrcodeType> types = typeMng.getList();
		model.addAttribute("types", types);
		
		return "wx_qrcode_info/add";
	}

	
	@RequestMapping("/wx_qrcode_info/v_create.do")
	public String create(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		WxQrcodeInfo qrcodeInfo = qrcodeMng.findById(id);
		model.addAttribute("qrcodeInfo", qrcodeInfo);

		return "wx_qrcode_info/create";
	}
	
	@RequestMapping("/wx_qrcode_info/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		List<WxQrcodeType> types = typeMng.getList();
		model.addAttribute("types", types);

		WxQrcodeInfo qrcodeInfo = qrcodeMng.findById(id);
		model.addAttribute("qrcodeInfo", qrcodeInfo);

		return "wx_qrcode_info/edit";
	}

	@RequestMapping("/wx_qrcode_info/o_save.do")
	public String save(WxQrcodeInfo bean,Integer isauto, Integer typeId,
			HttpServletRequest request, ModelMap model) {

		if(typeId == null ) return null;

		if( isauto != null || bean.getCode() == null || bean.getCode().equals("") ){
			SimpleDateFormat sdf = new SimpleDateFormat("ssHHmmyyyyMMdd");
			String curr = sdf.format(new Date());

			String code = Num62.longToN62(Long.valueOf(curr));
			bean.setCode(code);
		}
		
		WxQrcodeType qrcodeType = typeMng.findById(typeId);
		bean.setQrcodeType(qrcodeType);
	
		WxQrcodeInfo nbean = qrcodeMng.save(bean);
		
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/wx_qrcode_info/o_update.do")
	public String update(WxQrcodeInfo bean,Integer typeId,Integer isauto,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		if(typeId == null ) return null;
		
		WxQrcodeType qrcodeType = typeMng.findById(typeId);
		bean.setQrcodeType(qrcodeType);
	
		if( isauto != null || bean.getCode() == null || bean.getCode().equals("") ){
			SimpleDateFormat sdf = new SimpleDateFormat("ssHHmmyyyyMMdd");
			String curr = sdf.format(new Date());

			String code = Num62.longToN62(Long.valueOf(curr));
			bean.setCode(code);
		}

		qrcodeMng.update(bean);

		return list(typeId,pageNo, request, model);
	}
	
	@RequestMapping("/wx_qrcode_info/o_create.do")
	public String create(WxQrcodeInfo bean,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		WxQrcodeInfo qrcodeInfo = qrcodeMng.findById(bean.getId());
		bean.setBackground(qrcodeInfo.getBackground());
		bean.setCode(qrcodeInfo.getCode());
		bean.setDisplay(qrcodeInfo.getDisplay());
		bean.setFlag(qrcodeInfo.getFlag());
		bean.setPicUrl(qrcodeInfo.getPicUrl());
		bean.setQrcodeType(qrcodeInfo.getQrcodeType());
		bean.setUrl(qrcodeInfo.getUrl());
		bean.setTitle(qrcodeInfo.getTitle());
		bean.setStatus(qrcodeInfo.getStatus());
		bean.setRemark(bean.getRemark());

		qrcodeMng.update(bean);

		return list(bean.getQrcodeType().getId(),pageNo, request, model);
	}

	@RequestMapping("/wx_qrcode_info/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		qrcodeMng.deleteByIds(ids);
		
		return list(null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		WxQrcodeInfo entity = qrcodeMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private WxQrcodeInfoMng qrcodeMng;
	
	@Autowired
	private WxQrcodeTypeMng typeMng;
	
}