package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.GhConfigCategory;
import com.jskj.cms.entity.assist.GhConfigSource;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.GhConfigCategoryMng;
import com.jskj.cms.manager.assist.GhConfigSourceMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.security.encoder.Md5PwdEncoder;
import com.jskj.common.util.Num62;
import com.jskj.common.web.CookieUtils;

@Controller
public class GhConfigSourceAct {

	private static final Logger logger = LoggerFactory.getLogger(GhConfigSourceAct.class);
	
	@RequestMapping("/gh_config/v_list.do")
	public String list(String name,Integer categoryId,Integer pageNo, 
				HttpServletRequest request, ModelMap model) {

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = manager.getPage(name,categoryId,cpn(pageNo),
			CookieUtils.getPageSize(request));

		List<GhConfigCategory> categorys =  categoryManager.getList();
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("categorys",  categorys);
		model.addAttribute("pageNo", pagination.getPageNo());

		logger.info("name:"+name);
		logger.info("categoryId:"+categoryId);
		
		if(name!= null){
			model.addAttribute("name", name);
		}else {
			model.addAttribute("name", null);
		}
		
		if(categoryId!= null){
			model.addAttribute("categoryId", categoryId);
		}else {
			model.addAttribute("categoryId", null);
		}
		
		return "gh_config/list";
	}

	@RequestMapping("/gh_config/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		List<GhConfigCategory> categorys =  categoryManager.getList();
		model.addAttribute("categorys",  categorys);
		return "gh_config/add";
	}

	@RequestMapping("/gh_config/v_edit.do")
	public String edit(Integer id,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		GhConfigSource configSource = manager.findById(id);
		model.addAttribute("configSource", configSource);
		
		List<GhConfigCategory> categorys =  categoryManager.getList();
		model.addAttribute("categorys",  categorys);
		
		return "gh_config/edit";
	}

	@RequestMapping("/gh_config/o_save.do")
	public String save(GhConfigSource bean,Integer categoryId,
			HttpServletRequest request, ModelMap model) throws IOException {
		
		Md5PwdEncoder encoder = new Md5PwdEncoder();
		String md5Ckey = Num62.longToN62(System.currentTimeMillis());
		bean.setMd5Key(encoder.encodePassword(md5Ckey));

		bean.setCategory(categoryManager.findById(categoryId));
		
		GhConfigSource nbean = manager.save(bean);
		System.out.println("nbean.getId():"+nbean.getId());
		
		return "redirect:v_list.do";
	}

	@RequestMapping("/gh_config/o_update.do")
	public String update(GhConfigSource bean, Integer categoryId,Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		GhConfigCategory category = categoryManager.findById(categoryId);
		bean.setCategory(category);
		
		manager.update(bean);

		return list(bean.getName(),bean.getCategory().getId(),pageNo, request, model);
	}

	@RequestMapping("/gh_config/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		manager.deleteByIds(ids);
		return list(null,-1,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		GhConfigSource entity = manager.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}


	@Autowired
	private GhConfigSourceMng manager;
	
	@Autowired
	private GhConfigCategoryMng categoryManager;
}