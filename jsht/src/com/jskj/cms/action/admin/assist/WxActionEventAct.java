package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.WxPublicAccount;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxMenuRecordMng;
import com.jskj.cms.manager.assist.WxPublicAccountMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class WxActionEventAct {

	private static final Logger logger = LoggerFactory.getLogger(WxActionEventAct.class);

	@RequestMapping("/wx_user_event/v_list.do")
	public String list(Integer id,String openid, Integer ptype,Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		List<WxPublicAccount> accounts = accountMng.getList(1);
		model.addAttribute("accounts", accounts);
		
		Pagination pagination = menuRecordMng.getPage(openid,ptype,cpn(pageNo),
					CookieUtils.getPageSize(request));

		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());

		model.addAttribute("openid", openid);
		model.addAttribute("ptype", ptype);
		
		if(id !=null && !id.equals("")){
			model.addAttribute("id", id);
		}
		
		return "wx_user_event/event_list";
	}

	@Autowired
	private WxPublicAccountMng accountMng;

	@Autowired
	private WxMenuRecordMng menuRecordMng;

	
}