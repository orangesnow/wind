package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjSubject;
import com.jskj.cms.entity.assist.JskjSubjectReply;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjSubjectReplyMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjSubjectReplyAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjSubjectReplyAct.class);

	@RequestMapping("/jskj_subject_reply/v_list.do")
	public String list(Integer subjectid,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
	
		Pagination pagination = subjectReplyMng.getPage(subjectid,null,null, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());

		if(subjectid != null && subjectid >-1){
			model.addAttribute("subjectid", subjectid);
		}

		return "jskj_subject_reply/list";
		
	}

	@RequestMapping("/jskj_subject_reply/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "jskj_subject_reply/add";
	}
	
	@RequestMapping("/jskj_subject_reply/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjSubjectReply category = subjectReplyMng.findById(id);
		model.addAttribute("category", category);

		return "jskj_subject_reply/edit";
		
	}

	@RequestMapping("/jskj_subject_reply/o_save.do")
	public String save(JskjSubjectReply bean, Integer subjectid,
			HttpServletRequest request, ModelMap model) {
		
			JskjSubjectReply nbean = subjectReplyMng.save(bean);

			return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_subject_reply/o_update.do")
	public String update(JskjSubjectReply bean,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		subjectReplyMng.update(bean);
		JskjSubject subject = bean.getSubject();
		
		return list(subject.getId(),pageNo, request, model);
		
	}

	@RequestMapping("/jskj_subject_reply/o_delete.do")
	public String delete(Integer subjectid,Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		subjectReplyMng.deleteByIds(ids);
		
		return list(subjectid,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjSubjectReply entity = subjectReplyMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjSubjectReplyMng subjectReplyMng;

}