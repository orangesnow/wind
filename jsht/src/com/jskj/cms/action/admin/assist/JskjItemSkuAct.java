package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjItemList;
import com.jskj.cms.entity.assist.JskjItemSku;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjItemListMng;
import com.jskj.cms.manager.assist.JskjItemSkuMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjItemSkuAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjItemSkuAct.class);

	@RequestMapping("/jskj_item_sku/v_list.do")
	public String list(Integer itemid, Integer pageNo,
				HttpServletRequest request, ModelMap model){

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = itemSkuMng.getPage(itemid,cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		JskjItemList item = itemMng.findById(itemid);
		model.addAttribute("item", item);
	
		if(itemid != null && itemid > -1){
			model.addAttribute("itemid", itemid);
		}

		return "jskj_item_sku/list";
	}

	@RequestMapping("/jskj_item_sku/v_add.do")
	public String add(Integer itemid,HttpServletRequest request,ModelMap model) {
	
		JskjItemList item = itemMng.findById(itemid);
		model.addAttribute("item", item);
		
		return "jskj_item_sku/add";
	}

	@RequestMapping("/jskj_item_sku/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjItemSku  sku = itemSkuMng.findById(id);
		model.addAttribute("sku", sku);

		Integer itemid = sku.getItemid();
		JskjItemList item = itemMng.findById(itemid);
		model.addAttribute("item", item);
		
		return "jskj_item_sku/edit";
		
	}

	@RequestMapping("/jskj_item_sku/o_save.do")
	public String save(JskjItemSku bean,
			HttpServletRequest request, ModelMap model) {

		JskjItemSku item = itemSkuMng.save(bean);
	
		return "redirect:v_list.do?itemid="+item.getItemid();
		
	}
	
	@RequestMapping("/jskj_item_sku/o_update.do")
	public String update(JskjItemSku bean,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		itemSkuMng.update(bean);

		return list(bean.getItemid(),pageNo, request, model);
	}

	@RequestMapping("/jskj_item_sku/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		JskjItemSku[] skus = itemSkuMng.deleteByIds(ids);
		
		return list(skus[0].getItemid(),pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjItemSku entity = itemSkuMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjItemSkuMng itemSkuMng;
	
	@Autowired
	private JskjItemListMng itemMng;
	
}