package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjTagList;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjTagListMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjTagListAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjTagListAct.class);

	@RequestMapping("/jskj_tag_list/v_list.do")
	public String list(String name,Integer orderBy,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = tagListMng.getPage(name, orderBy, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(name != null ){
			model.addAttribute("name", name);
		}

		if(orderBy != null && orderBy >-1){
			model.addAttribute("orderBy", orderBy);
		}

		return "jskj_tag_list/list";
	}

	@RequestMapping("/jskj_tag_list/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "jskj_tag_list/add";
	}

	@RequestMapping("/jskj_tag_list/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjTagList rule = tagListMng.findById(id);
		model.addAttribute("tag", rule);

		return "jskj_tag_list/edit";
	}

	@RequestMapping("/jskj_tag_list/o_save.do")
	public String save(JskjTagList bean,Integer cid, 
			HttpServletRequest request, ModelMap model) {

		JskjTagList nbean = tagListMng.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_tag_list/o_update.do")
	public String update(JskjTagList bean,Integer cid,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		tagListMng.update(bean);

		return list(null,null,pageNo, request, model);
	}

	@RequestMapping("/jskj_tag_list/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		tagListMng.deleteByIds(ids);
		
		return list(null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjTagList entity = tagListMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjTagListMng tagListMng;
	
}