package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.WxSyncType;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.WxSyncResultMng;
import com.jskj.cms.manager.assist.WxSyncTypeMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class WxSyncResultAct {

	private static final Logger logger = LoggerFactory.getLogger(WxSyncResultAct.class);

	@RequestMapping("/wx_sync_result/v_list.do")
	public String result(Integer stype,Integer id,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		List<WxSyncType> stypes = syncTypeMng.getList();
		model.addAttribute("stypes", stypes);
		
		Pagination pagination = syncResultMng.getPage(stype, cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(stype != null ){
			model.addAttribute("stype", stype);
		}
		
		if(id != null){
			model.addAttribute("id", id);
		}

		return "wx_sync_result/list";
	}

	
	@Autowired
	private WxSyncTypeMng syncTypeMng;
	
	@Autowired
	private WxSyncResultMng syncResultMng;
	

	
}