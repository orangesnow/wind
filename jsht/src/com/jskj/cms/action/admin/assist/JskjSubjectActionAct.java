package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.JskjSubjectAction;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.JskjSubjectActionMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class JskjSubjectActionAct {

	private static final Logger logger = LoggerFactory.getLogger(JskjSubjectActionAct.class);

	@RequestMapping("/jskj_subject_action/v_list.do")
	public String list(String staDate,String endDate,Integer atype,Integer pageNo,
				HttpServletRequest request, ModelMap model){
		
		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);

		Pagination pagination = actionMng.getPage(staDate, endDate, atype,cpn(pageNo),
				CookieUtils.getPageSize(request));
		
		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(staDate != null && !staDate.equals("")){
			model.addAttribute("staDate", staDate);
		}

		if(endDate != null && !endDate.equals("")){
			model.addAttribute("endDate", endDate);
		}
		
		if(atype != null && atype >-1){
			model.addAttribute("atype", atype);
		}

		return "jskj_subject_action/list";
	}

	@RequestMapping("/jskj_subject_action/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "jskj_subject_action/add";
	}

	@RequestMapping("/jskj_subject_action/v_edit.do")
	public String edit(Integer id, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		JskjSubjectAction action = actionMng.findById(id);
		model.addAttribute("action", action);

		return "jskj_subject_action/edit";
	}

	@RequestMapping("/jskj_subject_action/o_save.do")
	public String save(JskjSubjectAction bean,
			HttpServletRequest request, ModelMap model) {

		JskjSubjectAction nbean = actionMng.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/jskj_subject_action/o_update.do")
	public String update(JskjSubjectAction bean,Integer cid,
			Integer pageNo, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		actionMng.update(bean);

		return list(null,null,null,pageNo, request, model);
	}

	@RequestMapping("/jskj_subject_action/o_delete.do")
	public String delete(Integer[] ids, Integer pageNo,
			HttpServletRequest request, ModelMap model) {

		WebErrors errors = validateDelete(ids, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
	
		actionMng.deleteByIds(ids);
		
		return list(null,null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(Integer id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}

	private WebErrors validateUpdate(Integer id,
			HttpServletRequest request) {
		
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private WebErrors validateDelete(Integer[] ids, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		errors.ifEmpty(ids, "ids");
		for (Integer id : ids) {
			vldExist(id, errors);
		}
		return errors;
	}

	private boolean vldExist(Integer id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		JskjSubjectAction entity = actionMng.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}

	@Autowired
	private JskjSubjectActionMng actionMng;
	
}