package com.jskj.cms.action.admin.assist;

import static com.jskj.common.page.SimplePage.cpn;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jskj.cms.entity.assist.GhOrderClaim;
import com.jskj.cms.entity.assist.GhOrderInfo;
import com.jskj.cms.entity.assist.WxFollowUser;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.manager.assist.GhOrderClaimMng;
import com.jskj.cms.manager.assist.GhOrderInfoMng;
import com.jskj.cms.manager.assist.WxFollowUserMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.page.Pagination;
import com.jskj.common.web.CookieUtils;

@Controller
public class GhOrderClaimAct {

	private static final Logger logger = LoggerFactory.getLogger(GhOrderClaimAct.class);

	@RequestMapping("/gh_claim/v_list.do")
	public String list(String orderId,Integer source,Integer claimFlag,Integer claimType,
			String staDate,String endDate, Integer pageNo, 
				HttpServletRequest request, ModelMap model) {

		CmsSite site = CmsUtils.getSite(request);
		FrontUtils.frontData(request, model, site);
		
		Pagination pagination = manager.getPage(orderId,source,claimFlag,claimType,staDate,endDate,cpn(pageNo),
			CookieUtils.getPageSize(request));

		model.addAttribute("pagination", pagination);
		model.addAttribute("pageNo", pagination.getPageNo());
		
		if(orderId != null ){
			model.addAttribute("orderId", orderId);
		}else {
			model.addAttribute("orderId", null);
		}
		
		if(staDate != null){
			model.addAttribute("staDate", staDate);
		}
		
		if(endDate != null){
			model.addAttribute("endDate", endDate);
		}
		
		if(source != null){
			model.addAttribute("source", source);
		}
		
		if(claimFlag != null){
			model.addAttribute("claimFlag", claimFlag);
		}
		
		if(claimType != null){
			model.addAttribute("claimType", claimType);
		}
		
		return "gh_claim/list";
	}

	@RequestMapping("/gh_claim/v_add.do")
	public String add(HttpServletRequest request,ModelMap model) {
		return "gh_claim/add";
	}
	
	@RequestMapping("/gh_claim/o_save.do")
	public String save(GhOrderClaim bean, String ghid,
			HttpServletRequest request, ModelMap model) throws IOException {

		if(ghid != null && !ghid.equals("")){
			GhOrderInfo ghOrder = orderManager.findById(ghid);
			bean.setGhOrder(ghOrder);
		}

		manager.save(bean);
	
		return "redirect:v_list.do";
	}
	
	@RequestMapping("/gh_claim/v_edit.do")
	public String edit(int id,Integer[] ids, HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateEdit(id, request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}

		GhOrderClaim orderClaim = manager.findById(id);
		
		if(ids != null && ids.length >0){
			WxFollowUser user = followUserMng.findById(ids[0]);
			model.addAttribute("user", user);
		}
		
		model.addAttribute("orderClaim", orderClaim);
		
		return "gh_claim/edit";
	}

	@RequestMapping("/gh_claim/o_update.do")
	public String update(GhOrderClaim bean,String ghid,Integer claim_user_id,Integer pageNo,
			HttpServletRequest request, ModelMap model) {
		
		WebErrors errors = validateUpdate(bean.getId(), request);
		if (errors.hasErrors()) {
			return errors.showErrorPage(model);
		}
		
		if(ghid != null && !ghid.equals("")){
			GhOrderInfo ghOrder = orderManager.findById(ghid);
			bean.setGhOrder(ghOrder);
		}
		
		WxFollowUser user = followUserMng.findById(claim_user_id);
		if(user != null){
			bean.setUser(user);
			bean.setClaimFlag(1);
			bean.setClaimType(3);
		}
		
		//保存记录
		manager.update(bean);

		return list(null,-1,-1,-1,null,null,pageNo, request, model);
	}

	private WebErrors validateEdit(int id, HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		return errors;
	}


	private WebErrors validateUpdate(int id,
			HttpServletRequest request) {
		WebErrors errors = WebErrors.create(request);
		if (vldExist(id, errors)) {
			return errors;
		}
		
		return errors;
	}

	private boolean vldExist(int id, WebErrors errors) {
		if (errors.ifNull(id, "id")) {
			return true;
		}
		GhOrderClaim entity = manager.findById(id);
		if (errors.ifNotExist(entity, CmsSite.class, id)) {
			return true;
		}
		return false;
	}


	@Autowired
	private GhOrderClaimMng manager;

	@Autowired
	private GhOrderInfoMng orderManager;

	@Autowired
	private WxFollowUserMng followUserMng;
	
}