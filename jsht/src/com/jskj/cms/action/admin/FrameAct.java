package com.jskj.cms.action.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FrameAct {
	
	@RequestMapping("/frame/item_main.do")
	public String itemMain(ModelMap model) {
		return "frame/item_main";
	}

	@RequestMapping("/frame/item_left.do")
	public String itemLeft(ModelMap model) {
		return "frame/item_left";
	}

	@RequestMapping("/frame/item_right.do")
	public String itemRight(ModelMap model) {
		return "frame/item_right";
	}

	@RequestMapping("/frame/config_main.do")
	public String configMain(ModelMap model) {
		return "frame/config_main";
	}

	@RequestMapping("/frame/config_left.do")
	public String configLeft(ModelMap model) {
		return "frame/config_left";
	}

	@RequestMapping("/frame/config_right.do")
	public String configRight(ModelMap model) {
		return "frame/config_right";
	}
	
	
	@RequestMapping("/frame/action_main.do")
	public String actionMain(ModelMap model) {
		return "frame/action_main";
	}

	@RequestMapping("/frame/action_left.do")
	public String actionLeft(ModelMap model) {
		return "frame/action_left";
	}

	@RequestMapping("/frame/action_right.do")
	public String actionRight(ModelMap model) {
		return "frame/action_right";
	}

	@RequestMapping("/frame/trade_main.do")
	public String generateMain(ModelMap model) {
		return "frame/trade_main";
	}

	@RequestMapping("/frame/trade_left.do")
	public String generateLeft(ModelMap model) {
		return "frame/trade_left";
	}

	@RequestMapping("/frame/trade_right.do")
	public String generateRight(ModelMap model) {
		return "frame/trade_right";
	}

	@RequestMapping("/frame/weixin_main.do")
	public String maintainMain(ModelMap model) {
		return "frame/weixin_main";
	}

	@RequestMapping("/frame/weixin_left.do")
	public String maintainLeft(ModelMap model) {
		return "frame/weixin_left";
	}

	@RequestMapping("/frame/weixin_right.do")
	public String maintainRight(ModelMap model) {
		return "frame/weixin_right";
	}

	@RequestMapping("/frame/club_main.do")
	public String assistantMain(ModelMap model) {
		return "frame/club_main";
	}

	@RequestMapping("/frame/club_left.do")
	public String assistantLeft(ModelMap model) {
		return "frame/club_left";
	}

	@RequestMapping("/frame/club_right.do")
	public String assistantRight(ModelMap model) {
		return "frame/club_right";
	}

	@RequestMapping("/frame/data_main.do")
	public String dataMain(ModelMap model) {
		return "frame/data_main";
	}

	@RequestMapping("/frame/data_left.do")
	public String dataLeft(ModelMap model) {
		return "frame/data_left";
	}

	@RequestMapping("/frame/data_right.do")
	public String dataRight(ModelMap model) {
		return "frame/data_right";
	}
	
	@RequestMapping("/frame/control_main.do")
	public String controlMain(ModelMap model) {
		return "frame/control_main";
	}

	@RequestMapping("/frame/control_left.do")
	public String controlLeft(ModelMap model) {
		return "frame/control_left";
	}

	@RequestMapping("/frame/control_right.do")
	public String controlRight(ModelMap model) {
		return "frame/control_right";
	}
	
//	@RequestMapping("/frame/template_main.do")
//	public String templateMain(ModelMap model) {
//		return "frame/template_main";
//	}
//
//	@RequestMapping("/frame/resource_main.do")
//	public String resourceMain(ModelMap model) {
//		return "frame/resource_main";
//	}
//
//	@RequestMapping("/frame/product_channel_main.do")
//	public String productChannelMain(ModelMap model) {
//		return "frame/product_channel_main";
//	}
//
//	@RequestMapping("/frame/channel_main.do")
//	public String channelMain(ModelMap model) {
//		return "frame/channel_main";
//	}
//
//	@RequestMapping("/frame/content_main.do")
//	public String contentMain(ModelMap model) {
//		return "frame/content_main";
//	}
//	
//	@RequestMapping("/frame/product_content_main.do")
//	public String productContentMain(ModelMap model) {
//		return "frame/product_content_main";
//	}
//	
//	@RequestMapping("/frame/statistic_main.do")
//	public String statisticMain(ModelMap model) {
//		return "frame/statistic_main";
//	}
//	
//	@RequestMapping("/frame/statistic_left.do")
//	public String statisticLeft(){
//		return "frame/statistic_left";
//	}
//	
//	@RequestMapping("/frame/statistic_right.do")
//	public String statisticRight(){
//		return "frame/statistic_right";
//	}
}
