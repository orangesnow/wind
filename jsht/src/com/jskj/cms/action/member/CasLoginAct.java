package com.jskj.cms.action.member;

import static com.jskj.cms.Constants.TPLDIR_MEMBER;
import static com.jskj.core.action.front.LoginAct.MESSAGE;
import static com.jskj.core.action.front.LoginAct.PROCESS_URL;
import static com.jskj.core.action.front.LoginAct.RETURN_URL;
import static com.jskj.core.manager.AuthenticationMng.AUTH_KEY;

import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.entity.main.CmsThirdAccount;
import com.jskj.cms.entity.main.CmsUser;
import com.jskj.cms.entity.main.CmsUserExt;
import com.jskj.cms.manager.main.CmsThirdAccountMng;
import com.jskj.cms.manager.main.CmsUserMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.common.email.EmailSender;
import com.jskj.common.email.MessageTemplate;
import com.jskj.common.security.BadCredentialsException;
import com.jskj.common.security.DisabledException;
import com.jskj.common.security.UsernameNotFoundException;
import com.jskj.common.web.CookieUtils;
import com.jskj.common.web.RequestUtils;
import com.jskj.common.web.session.SessionProvider;
import com.jskj.core.entity.Authentication;
import com.jskj.core.entity.Config.ConfigLogin;
import com.jskj.core.manager.AuthenticationMng;
import com.jskj.core.manager.ConfigMng;
import com.jskj.core.manager.UnifiedUserMng;
import com.jskj.core.web.WebErrors;
import com.octo.captcha.service.image.ImageCaptchaService;
import com.qq.connect.api.OpenID;
import com.qq.connect.api.qzone.UserInfo;
import com.qq.connect.javabeans.qzone.UserInfoBean;

@Controller
public class CasLoginAct {
	private static final Logger log = LoggerFactory.getLogger(CasLoginAct.class);

	public static final String COOKIE_ERROR_REMAINING = "_error_remaining";
	public static final String LOGIN_INPUT = "tpl.loginInput";
	public static final String LOGIN_STATUS = "tpl.loginStatus";
	public static final String ENCODE_RETURN_URL = "encodeReturnURL";
	public static final int THID_ACCOUNT_TYPE_SINA = 1;
	public static final int THID_ACCOUNT_TYPE_QQ = 2;
	
	@RequestMapping(value = "/login.jspx", method = RequestMethod.GET)
	public String input(HttpServletRequest request, ModelMap model) {
		CmsSite site = CmsUtils.getSite(request);
		String sol = site.getSolutionPath();
		String processUrl = RequestUtils.getQueryParam(request, PROCESS_URL);
		String returnUrl = RequestUtils.getQueryParam(request, RETURN_URL);
		String message = RequestUtils.getQueryParam(request, MESSAGE);
		String authId = (String) session.getAttribute(request, AUTH_KEY);
		if (authId != null) {
			// 存在认证ID
			Authentication auth = authMng.retrieve(authId);
			// 存在认证信息，且未过期
			if (auth != null) {
				String view = getView(processUrl, returnUrl, auth.getId());
				if (view != null) {
					return view;
				} else {
					FrontUtils.frontData(request, model, site);
					model.addAttribute("auth", auth);
					return FrontUtils.getTplPath(request, sol, TPLDIR_MEMBER,
							LOGIN_STATUS);
				}
			}
		}
		FrontUtils.frontData(request, model, site);
		if (!StringUtils.isBlank(processUrl)) {
			model.addAttribute(PROCESS_URL, processUrl);
		}
		if (!StringUtils.isBlank(returnUrl)) {
			model.addAttribute(RETURN_URL, returnUrl);
		}
		if (!StringUtils.isBlank(message)) {
			model.addAttribute(MESSAGE, message);
		}
		return FrontUtils.getTplPath(request, sol, TPLDIR_MEMBER, LOGIN_INPUT);
	}

	@RequestMapping(value = "/login.jspx", method = RequestMethod.POST)
	public String submit(String username, String password, String captcha,String rem,
			String processUrl, String returnUrl, String message,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {
		Integer errorRemaining = unifiedUserMng.errorRemaining(username);
		CmsSite site = CmsUtils.getSite(request);
		String sol = site.getSolutionPath();
		WebErrors errors = validateSubmit(username, password, captcha,
				errorRemaining, request, response);
		if (!errors.hasErrors()) {
			try {
				String ip = RequestUtils.getIpAddr(request);
				Authentication auth = authMng.login(username, password, ip,
						request, response, session);
				// 是否需要在这里加上登录次数的更新？按正常的方式，应该在process里面处理的，不过这里处理也没大问题。
				cmsUserMng.updateLoginInfo(auth.getUid(), ip);
				CmsUser user = cmsUserMng.findById(auth.getUid());
				if (user.getDisabled()) {
					// 如果已经禁用，则推出登录。
					authMng.deleteById(auth.getId());
					session.logout(request, response);
					throw new DisabledException("user disabled");
				}
				removeCookieErrorRemaining(request, response);
				String view = getView(processUrl, returnUrl, auth.getId());
				if (view != null) {
					return view;
				} else {
					FrontUtils.frontData(request, model, site);
					return "redirect:login.jspx";
				}
			} catch (UsernameNotFoundException e) {
				//errors.addErrorString(e.getMessage());
				errors.addErrorCode("error.badCredentials");
			} catch (BadCredentialsException e) {
				//errors.addErrorString(e.getMessage());
				errors.addErrorCode("error.badCredentials");
			} catch (DisabledException e) {
				//errors.addErrorString(e.getMessage());
				errors.addErrorCode("error.userDisabled");
			}
		} 
		// 登录失败
		writeCookieErrorRemaining(errorRemaining, request, response, model);
		if(errors.hasErrors()){
			WebErrors error = WebErrors.create(request);
			error.addErrorCode("error.badCredentials");
			error.toModel(model);
		}

		FrontUtils.frontData(request, model, site);
		if (!StringUtils.isBlank(processUrl)) {
			model.addAttribute(PROCESS_URL, processUrl);
		}
		if (!StringUtils.isBlank(returnUrl)) {
			model.addAttribute(RETURN_URL, returnUrl);
		}
		if (!StringUtils.isBlank(message)) {
			model.addAttribute(MESSAGE, message);
		}
		if (!StringUtils.isBlank(username)) {
			model.addAttribute("username", username);
		}
		if(!StringUtils.isBlank(password)){
			model.addAttribute("password", password);
		}

		return FrontUtils.getTplPath(request, sol, TPLDIR_MEMBER, LOGIN_INPUT);
	}

	@RequestMapping(value = "/qqzone/login.jspx", method = RequestMethod.GET)
	public String qqzoneLogin(HttpServletRequest request, ModelMap model, HttpServletResponse response){
		
		 response.setContentType("text/html;charset=utf-8");
		 String code = RequestUtils.getQueryParam(request, "code");
		
	       try {
	    	   if( code == null || code.equals("") ){
	    		
	    		   	String returnUrl = RequestUtils.getQueryParam(request, RETURN_URL);
	    		   	if( returnUrl == null ) returnUrl="/";
	    		   	String url = new com.qq.connect.oauth.Oauth().getAuthorizeURL(request);
	    		   	return "redirect:"+url;

	    	   } else {
	    		    com.qq.connect.javabeans.AccessToken accessTokenObj = (new com.qq.connect.oauth.Oauth()).getAccessTokenByRequest(request);

		            String accessToken   = null;
		            String openID        = null;
		            long tokenExpireIn = 0L;

		            if (accessTokenObj.getAccessToken().equals("")) {
		            	System.out.println("没有获取到响应参数");
		            } else {
		            	System.out.println("获取到响应参数");
		                accessToken = accessTokenObj.getAccessToken();
		                tokenExpireIn = accessTokenObj.getExpireIn();
		                System.out.println("accessToken:"+accessToken);
		                
		                // 利用获取到的accessToken 去获取当前用的openid -------- start
		                OpenID openIDObj =  new OpenID(accessToken);
		                openID = openIDObj.getUserOpenID();
		                CmsThirdAccount thirdAccount = cmsThirdAccountMng.findByThirdUid(openID);
		                if(thirdAccount == null){
		                	 UserInfo qzoneUserInfo = new UserInfo(accessToken, openID);
				             UserInfoBean userInfoBean = qzoneUserInfo.getUserInfo();
	
							String username = userInfoBean.getNickname()+"(QQ空间)";
							String nickname = userInfoBean.getNickname();
							System.out.println("username:"+username);
							
							String ip = RequestUtils.getIpAddr(request);
							EmailSender sender = configMng.getEmailSender();
							MessageTemplate msgTpl = configMng.getRegisterMessageTemplate();
							CmsUserExt userExt = new CmsUserExt();
							userExt.setUserImg( userInfoBean.getAvatar().getAvatarURL100());
							userExt.setUserSignature(userInfoBean.getMsg());
							userExt.setRealname(nickname);
							userExt.setWeibo("");
							try {
									CmsUser cmsUser = cmsUserMng.registerMember(username, "@","0", openID, ip, null, userExt, true, sender, msgTpl);
									
									CmsThirdAccount cmsThirdAccount = new CmsThirdAccount();
									cmsThirdAccount.setOprtime(new Date());
									cmsThirdAccount.setId(cmsUser.getId());
									cmsThirdAccount.setStatus(1);
									cmsThirdAccount.setThirdType(THID_ACCOUNT_TYPE_QQ);
									cmsThirdAccount.setThirdUid(openID);
									
									thirdAccount = cmsThirdAccountMng.save(cmsThirdAccount);
							}catch (Exception e) {
								log.error("send inner_register.jspx exception.", e);
							}
						}
		          
		            	Integer currId = thirdAccount.getId();
		    			
						CmsUser curr = cmsUserMng.findById(currId);
						if( curr == null ) return  "redirect:/";
				
						String ip = RequestUtils.getIpAddr(request);
						Authentication auth = new Authentication();
						auth.setUid(curr.getId());
						auth.setUsername(curr.getUsername());
						auth.setEmail(curr.getEmail());
						auth.setLoginIp(ip);
						authMng.save(auth);
						
						session.setAttribute(request, response, AUTH_KEY, auth.getId());
						// 是否需要在这里加上登录次数的更新？按正常的方式，应该在process里面处理的，不过这里处理也没大问题。
						cmsUserMng.updateLoginInfo(auth.getUid(), ip);
						CmsUser user = cmsUserMng.findById(auth.getUid());
						if (user.getDisabled()) {
							// 如果已经禁用，则推出登录。
							authMng.deleteById(auth.getId());
							session.logout(request, response);
							throw new DisabledException("user disabled");
						}
						removeCookieErrorRemaining(request, response);
						
						String returnUrl = RequestUtils.getQueryParam(request, RETURN_URL);
						if(returnUrl == null) returnUrl = "/";
						return "redirect:"+returnUrl;

		            }
	    	   }
	            
	        } catch (Exception e) {
	        }
	        
	        
		return null;
	}
	
	
	
	
	@RequestMapping(value = "/logout.jspx")
	public String logout(HttpServletRequest request,
			HttpServletResponse response) {
		String authId = (String) session.getAttribute(request, AUTH_KEY);
		if (authId != null) {
			authMng.deleteById(authId);
			session.logout(request, response);
		}
		String processUrl = RequestUtils.getQueryParam(request, PROCESS_URL);
		String returnUrl = RequestUtils.getQueryParam(request, RETURN_URL);
		String view = getView(processUrl, returnUrl, authId);
		if (view != null) {
			return view;
		} else {
			return "redirect:login.jspx";
		}
	}

	private WebErrors validateSubmit(String username, String password,
			String captcha, Integer errorRemaining, HttpServletRequest request,
			HttpServletResponse response) {
		WebErrors errors = WebErrors.create(request);
		if (errors.ifOutOfLength(username, "会员名", 4, 100)) {
			return errors;
		}
		if (errors.ifOutOfLength(password, "密码", 4, 20)) {
			return errors;
		}
		
//		if(errors.ifBlank(captcha, "验证码", 4)){
//			return errors;
//		}
		
		// 如果输入了验证码，那么必须验证；如果没有输入验证码，则根据当前用户判断是否需要验证码。
//		if (!StringUtils.isBlank(captcha)
//				|| (errorRemaining != null && errorRemaining < 0)) {
//			if (errors.ifBlank(captcha, "captcha", 4)) {
//				return errors;
//			}
//			try {
//				if (!imageCaptchaService.validateResponseForID(session
//						.getSessionId(request, response), captcha)) {
//					errors.addErrorCode("error.invalidCaptcha");
//					return errors;
//				}
//			} catch (CaptchaServiceException e) {
//				errors.addErrorCode("error.exceptionCaptcha");
//				log.warn("", e);
//				return errors;
//			}
//		}
		return errors;
	}

	/**
	 * 获得地址
	 * 
	 * @param processUrl
	 * @param returnUrl
	 * @param authId
	 * @param defaultUrl
	 * @return
	 */
	private String getView(String processUrl, String returnUrl, String authId) {
		if (!StringUtils.isBlank(processUrl)) {
			StringBuilder sb = new StringBuilder("redirect:");
			sb.append(processUrl).append("?").append(AUTH_KEY).append("=")
					.append(authId);
			if (!StringUtils.isBlank(returnUrl)) {
				sb.append("&").append(RETURN_URL).append("=").append(returnUrl);
			}
			return sb.toString();
		} else if (!StringUtils.isBlank(returnUrl)) {
			StringBuilder sb = new StringBuilder("redirect:");
			sb.append(returnUrl);
			return sb.toString();
		} else {
			return null;
		}
	}

	private void writeCookieErrorRemaining(Integer userErrorRemaining,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) {
		// 所有访问的页面都需要写一个cookie，这样可以判断已经登录了几次。
		Integer errorRemaining = getCookieErrorRemaining(request, response);
		ConfigLogin configLogin = configMng.getConfigLogin();
		Integer errorInterval = configLogin.getErrorInterval();
		if (userErrorRemaining != null
				&& (errorRemaining == null || userErrorRemaining < errorRemaining)) {
			errorRemaining = userErrorRemaining;
		}
		int maxErrorTimes = configLogin.getErrorTimes();
		if (errorRemaining == null || errorRemaining > maxErrorTimes) {
			errorRemaining = maxErrorTimes;
		} else if (errorRemaining <= 0) {
			errorRemaining = 0;
		} else {
			errorRemaining--;
		}
		model.addAttribute("errorRemaining", errorRemaining);
		CookieUtils.addCookie(request, response, COOKIE_ERROR_REMAINING,
				errorRemaining.toString(), errorInterval * 60, null);
	}

	private Integer getCookieErrorRemaining(HttpServletRequest request,
			HttpServletResponse response) {
		Cookie cookie = CookieUtils.getCookie(request, COOKIE_ERROR_REMAINING);
		if (cookie != null) {
			String value = cookie.getValue();
			if (NumberUtils.isDigits(value)) {
				return Integer.parseInt(value);
			}
		}
		return null;
	}
	
	
	private Integer getCookieErrorRemaining1(HttpServletRequest request,
			HttpServletResponse response) {
		Cookie cookie = CookieUtils.getCookie(request, COOKIE_ERROR_REMAINING);
		if (cookie != null) {
			String value = cookie.getValue();
			if (NumberUtils.isDigits(value)) {
				return Integer.parseInt(value);
			}
		}
		return null;
	}

	private void removeCookieErrorRemaining(HttpServletRequest request,
			HttpServletResponse response) {
		CookieUtils.cancleCookie(request, response, COOKIE_ERROR_REMAINING,
				null);
	}

	@Autowired
	private CmsUserMng cmsUserMng;
	@Autowired
	private ConfigMng configMng;
	@Autowired
	private AuthenticationMng authMng;
	@Autowired
	private UnifiedUserMng unifiedUserMng;
	@Autowired
	private ImageCaptchaService imageCaptchaService;
	@Autowired
	private SessionProvider session;
	@Autowired
	private CmsThirdAccountMng cmsThirdAccountMng;
	
}
