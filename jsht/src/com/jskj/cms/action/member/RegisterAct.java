package com.jskj.cms.action.member;

import static com.jskj.cms.Constants.TPLDIR_MEMBER;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jskj.cms.entity.main.CmsConfig;
import com.jskj.cms.entity.main.CmsSite;
import com.jskj.cms.entity.main.CmsUser;
import com.jskj.cms.entity.main.CmsUserExt;
import com.jskj.cms.entity.main.MemberConfig;
import com.jskj.cms.manager.main.CmsUserMng;
import com.jskj.cms.web.CmsUtils;
import com.jskj.cms.web.FrontUtils;
import com.jskj.cms.web.WebErrors;
import com.jskj.common.email.EmailSender;
import com.jskj.common.email.MessageTemplate;
import com.jskj.common.util.SendSMS;
import com.jskj.common.web.RequestUtils;
import com.jskj.common.web.ResponseUtils;
import com.jskj.common.web.session.SessionProvider;
import com.jskj.core.entity.UnifiedUser;
import com.jskj.core.manager.AuthenticationMng;
import com.jskj.core.manager.ConfigMng;
import com.jskj.core.manager.UnifiedUserMng;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.ImageCaptchaService;

/**
 * 前台会员注册Action
 * 
 * @author liufang
 * 
 */
@Controller
public class RegisterAct {
	private static final Logger log = LoggerFactory
			.getLogger(RegisterAct.class);

	public static final String REGISTER_EMAIL = "tpl.register";
	public static final String REGISTER_MOBILE = "tpl.register_mobile";
	public static final String REGISTER_RESULT = "tpl.registerResult";
	public static final String REGISTER_ACTIVE_SUCCESS = "tpl.registerActiveSuccess";
	
	@RequestMapping(value = "/register.jspx", method = RequestMethod.GET)
	public String input(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		CmsSite site = CmsUtils.getSite(request);
		MemberConfig mcfg = site.getConfig().getMemberConfig();
		// 没有开启会员功能
		if (!mcfg.isMemberOn()) {
			return FrontUtils.showMessage(request, model, "member.memberClose");
		}
		// 没有开启会员注册
		if (!mcfg.isRegisterOn()) {
			return FrontUtils.showMessage(request, model,
					"member.registerClose");
		}
		FrontUtils.frontData(request, model, site);
		model.addAttribute("mcfg", mcfg);
		return FrontUtils.getTplPath(request, site.getSolutionPath(),
				TPLDIR_MEMBER, REGISTER_EMAIL);
	}

	@RequestMapping(value = "/register_mobile.jspx", method = RequestMethod.GET)
	public String input_mobile(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		CmsSite site = CmsUtils.getSite(request);
		MemberConfig mcfg = site.getConfig().getMemberConfig();
		// 没有开启会员功能
		if (!mcfg.isMemberOn()) {
			return FrontUtils.showMessage(request, model, "member.memberClose");
		}
		// 没有开启会员注册
		if (!mcfg.isRegisterOn()) {
			return FrontUtils.showMessage(request, model,
					"member.registerClose");
		}
		FrontUtils.frontData(request, model, site);
		model.addAttribute("mcfg", mcfg);
		return FrontUtils.getTplPath(request, site.getSolutionPath(),
				TPLDIR_MEMBER, REGISTER_MOBILE);
	}
	
	
	@RequestMapping(value = "/register.jspx", method = RequestMethod.POST)
	public String submit(String username, String email,String mobile, String password,String aggr,
			CmsUserExt userExt, String captcha, String nextUrl,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap model) throws IOException {
		CmsSite site = CmsUtils.getSite(request);
		WebErrors errors = validateSubmit(username, email, password, aggr,captcha,
				site, request, response);
		if (errors.hasErrors()) {
			return FrontUtils.showError(request, response, model, errors);
		}
		CmsUser user = null;
		String ip = RequestUtils.getIpAddr(request);
		EmailSender sender = configMng.getEmailSender();
		MessageTemplate msgTpl = configMng.getRegisterMessageTemplate();
//		if (sender == null) {
//			// 邮件服务器没有设置好
//			model.addAttribute("status", 4);
//		} else if (msgTpl == null) {
//			// 邮件模板没有设置好
//			model.addAttribute("status", 5);
//		} else {
			try {
				if(email != null && !email.equals("") && !email.equals("null")){
					user = cmsUserMng.registerMember(username, email,"0", password, ip, null, userExt,
							false, sender, msgTpl);
				} else {
					user = cmsUserMng.registerMember(username, "@",mobile, password, ip, null, userExt,
							true, sender, msgTpl);
					FrontUtils.frontData(request, model, site);
					return FrontUtils.getTplPath(request, site.getSolutionPath(),
							TPLDIR_MEMBER, REGISTER_ACTIVE_SUCCESS);
				}

				model.addAttribute("status", 0);
				model.addAttribute("email", user.getEmail());

			} catch (Exception e) {
				// 发送邮件异常
				model.addAttribute("status", 100);
				model.addAttribute("message", e.getMessage());
				log.error("send email exception.", e);
			}
//		}
		log.info("member register success. username={}", username);
		FrontUtils.frontData(request, model, site);
		if (!StringUtils.isBlank(nextUrl)) {
			response.sendRedirect(nextUrl);
			return null;
		} else {
			return FrontUtils.getTplPath(request, site.getSolutionPath(),
					TPLDIR_MEMBER, REGISTER_RESULT);
		}
	}

	@RequestMapping(value = "/inner_register.jspx", method = RequestMethod.GET)
	public String submit(String username, String email,String mobile, String password,
			CmsUserExt userExt, String captcha,HttpServletRequest request, HttpServletResponse response,
			ModelMap model) throws IOException {
	
		String ip = RequestUtils.getIpAddr(request);
		EmailSender sender = configMng.getEmailSender();
		MessageTemplate msgTpl = configMng.getRegisterMessageTemplate();

		try {
				cmsUserMng.registerMember(username, email,mobile, password, ip, null, userExt,
						true, sender, msgTpl);
		} catch (Exception e) {
				log.error("send inner_register.jspx exception.", e);
		}
		log.info("member register success. username={}", username);
		return null;
	}
	
	
	
	@RequestMapping(value = "/active.jspx", method = RequestMethod.GET)
	public String active(HttpServletRequest request, 
			HttpServletResponse response,ModelMap model) throws IOException {

		String username = RequestUtils.getQueryParam(request, "username");
		String key = RequestUtils.getQueryParam(request, "key");
		
		CmsSite site = CmsUtils.getSite(request);
		WebErrors errors = validateActive(username, key, request, response);
		if (errors.hasErrors()) {
			return FrontUtils.showError(request, response, model, errors);
		}
		UnifiedUser user = unifiedUserMng.active(username, key);
		String ip = RequestUtils.getIpAddr(request);
		authMng.activeLogin(user, ip, request, response, session);
		FrontUtils.frontData(request, model, site);
		return FrontUtils.getTplPath(request, site.getSolutionPath(),
				TPLDIR_MEMBER, REGISTER_ACTIVE_SUCCESS);
	}

	@RequestMapping(value = "/username_unique.jspx")
	public void usernameUnique(HttpServletRequest request,
			HttpServletResponse response) {
		String username = RequestUtils.getQueryParam(request, "username");
		System.out.println("username:"+username);
		
		// 用户名为空，返回false。
		if (StringUtils.isBlank(username)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		CmsSite site = CmsUtils.getSite(request);
		CmsConfig config = site.getConfig();
		// 保留字检查不通过，返回false。
		if (!config.getMemberConfig().checkUsernameReserved(username)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		// 用户名存在，返回false。
		if (unifiedUserMng.usernameExist(username)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		ResponseUtils.renderJson(response, "true");
	}

	@RequestMapping(value = "/email_unique.jspx")
	public void emailUnique(HttpServletRequest request,
			HttpServletResponse response) {
		String email = RequestUtils.getQueryParam(request, "email");
		// email为空，返回false。
		if (StringUtils.isBlank(email)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		// email存在，返回false。
		if (unifiedUserMng.emailExist(email)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		ResponseUtils.renderJson(response, "true");
	}

	@RequestMapping(value = "/mobile_unique.jspx")
	public void mobileUnique(HttpServletRequest request,
			HttpServletResponse response) {
		String mobile = RequestUtils.getQueryParam(request, "mobile");
		
		// mobile为空，返回false。
		if (StringUtils.isBlank(mobile)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		// mobile存在，返回false。
		if (unifiedUserMng.mobileExist(mobile)) {
			ResponseUtils.renderJson(response, "false");
			return;
		}
		ResponseUtils.renderJson(response, "true");
	}
	
	@RequestMapping(value = "/sendRegisterEmail.jspx")
	public void sendRegisterEmail(HttpServletRequest request,
			HttpServletResponse response)   throws JSONException{
		String email = RequestUtils.getQueryParam(request, "email");
		
		// mobile为空，返回false。
		JSONObject object = new JSONObject();
		if (StringUtils.isBlank(email)) {
			object.put("result", false);
			ResponseUtils.renderJson(response, object.toString());
			return;
		}

		List<UnifiedUser> users = unifiedUserMng.getByEmail(email);
		if(users != null && users.size() == 1){
			UnifiedUser user = users.get(0);
			
			if(user.getActivation()){
				object.put("result", false);
				ResponseUtils.renderJson(response, object.toString());
				return;
			} else {
				EmailSender sender = configMng.getEmailSender();
				MessageTemplate msgTpl = configMng.getRegisterMessageTemplate();
				senderEmail(user.getUsername(), email, user.getActivationCode(), sender, msgTpl) ;
				object.put("result", true);
				ResponseUtils.renderJson(response, object.toString());
			}
			
		} else {
			
			object.put("result", false);
			ResponseUtils.renderJson(response, object.toString());
			
		}
		
	}
	
	
	@RequestMapping(value = "/obtain_code.jspx")
	public void obtainCode(HttpServletRequest request,
			HttpServletResponse response) {
		
		String mobile = RequestUtils.getQueryParam(request, "mobile");
		System.out.println("mobile:"+mobile);
		System.out.println("captchaId:"+session.getSessionId(request, response));
		String captchaId = getNumFromStr(session.getSessionId(request, response));
		System.out.println("captchaId:"+captchaId);
		System.out.println("captchaId(0,4):"+captchaId.substring(0, 4));
		StringBuilder message = new StringBuilder().append("酒学坊验证码:").append(captchaId.substring(0, 4)).append(",请在30分钟内使用,用后即失效,本条消息免费。如未访问过酒学坊,请忽略本消息【酒学坊jiuxf.cn】");
		String result = SendSMS.send(message.toString(), mobile);
    	System.out.println(result);
    	ResponseUtils.renderJson(response, "true");
	}
	
	@RequestMapping(value = "/checkCaptcha.jspx")
	public void checkCaptcha(String captcha, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("captcha:"+captcha);
		if(captcha != null &&  captcha.trim().length() == 4){
			
			if (!imageCaptchaService.validateResponseForID(session.getSessionId(request, response), captcha)) {
				ResponseUtils.renderJson(response, "false");
			} else {
				ResponseUtils.renderJson(response, "true");
			}
		} else {
			ResponseUtils.renderJson(response, "false");
		}
		
	}
	
	
	private WebErrors validateSubmit(String username, String email,
			String password,String aggr, String captcha, CmsSite site,
			HttpServletRequest request, HttpServletResponse response) {
		MemberConfig mcfg = site.getConfig().getMemberConfig();
		WebErrors errors = WebErrors.create(request);
		try {
			if(email != null){
				if (!imageCaptchaService.validateResponseForID(session.getSessionId(request, response), captcha)) {
					errors.addErrorCode("error.invalidCaptcha");
					return errors;
				}
			} else {
				if (!checkCaptcha(getNumFromStr(session.getSessionId(request, response)), captcha)) {
					errors.addErrorCode("error.invalidCaptcha");
					return errors;
				}
			}
			
		} catch (CaptchaServiceException e) {
			errors.addErrorCode("error.exceptionCaptcha");
			log.warn("", e);
			return errors;
		}
		if (errors.ifOutOfLength(username, "username",
				mcfg.getUsernameMinLen(), 100)) {
			return errors;
		}
		if (errors.ifOutOfLength(password, "password",
				mcfg.getPasswordMinLen(), 100)) {
			return errors;
		}
		if (errors.ifMaxLength(email, "email", 100)) {
			return errors;
		}

		if(aggr == null || !aggr.equals("1")){
			errors.addErrorCode("error.aggrExist");
			return errors;
		}
		// 保留字检查不通过，返回false。
		if (!mcfg.checkUsernameReserved(username)) {
			errors.addErrorCode("error.usernameReserved");
			return errors;
		}
		
		// 用户名存在，返回false。
		if (unifiedUserMng.usernameExist(username)) {
			errors.addErrorCode("error.usernameExist");
			return errors;
		}
		return errors;
	}

	private WebErrors validateActive(String username, String activationCode,
			HttpServletRequest request, HttpServletResponse response) {
		WebErrors errors = WebErrors.create(request);
		
		if (StringUtils.isBlank(username)
				|| StringUtils.isBlank(activationCode)) {
			errors.addErrorCode("error.exceptionParams");
			return errors;
		}
		
		UnifiedUser user = unifiedUserMng.getByUsername(username);
		if (user == null) {
			errors.addErrorCode("error.usernameNotExist");
			return errors;
		}
		if (user.getActivation()
				|| StringUtils.isBlank(user.getActivationCode())) {
			errors.addErrorCode("error.usernameActivated");
			return errors;
		}
		if (!user.getActivationCode().equals(activationCode)) {
			errors.addErrorCode("error.exceptionActivationCode");
			return errors;
		}
		return errors;
	}

	@Autowired
	private UnifiedUserMng unifiedUserMng;
	@Autowired
	private CmsUserMng cmsUserMng;
	@Autowired
	private SessionProvider session;
	@Autowired
	private ImageCaptchaService imageCaptchaService;
	@Autowired
	private ConfigMng configMng;
	@Autowired
	private AuthenticationMng authMng;
	
	
	private boolean checkCaptcha(String sessionId,String captcha) {
		String real = sessionId.substring(0, 4);
		if(captcha != null && real.equals(captcha)){
			return true;
		}
		return false;
	}
	
	private void senderEmail(final String username, final String to,
			final String activationCode, final EmailSender email,
			final MessageTemplate tpl) {
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		Properties props = System.getProperties();
//		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.auth", "true");

		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setJavaMailProperties(props);
		sender.setHost(email.getHost());
		sender.setUsername(email.getUsername());
		sender.setPassword(email.getPassword());
		
		sender.send(new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage)
					throws MessagingException, UnsupportedEncodingException {
				MimeMessageHelper msg = new MimeMessageHelper(mimeMessage,
						false, email.getEncoding());
				msg.setSubject(tpl.getRegisterSubject());
				msg.setTo(to);
				msg.setFrom(email.getUsername(), email.getPersonal());
				String text = tpl.getRegisterText();
				text = StringUtils.replace(text, "${nickname}", username);
				String name = URLEncoder.encode(username, "utf-8");
				text = StringUtils.replace(text, "${username}", name);
				text = StringUtils.replace(text, "${activationCode}",activationCode);
				msg.setText(text);
			}
		});
	}
	
	private static boolean isEmailValid(String email) {  
		boolean isMatched = false;  
		try {  
			String check = "^([a-z0-9A-Z]+[//_|-|//.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?//.)+[a-zA-Z]{2,6}$";  
			Pattern regex = Pattern.compile(check);  
			Matcher matcher = regex.matcher(email);  
			isMatched = matcher.matches();  
		}   catch(Exception e){
		}
		return isMatched;  
	}

	
	public String getNumFromStr(String str){
		str = str.toUpperCase();
		HashMap mapper = new HashMap();
		
		mapper.put("0", 0);
		mapper.put("1", 1);
		mapper.put("2", 2);
		mapper.put("3", 3);
		mapper.put("4", 4);
		mapper.put("5", 5);
		
		mapper.put("6", 6);
		mapper.put("7", 7);
		mapper.put("8", 8);
		mapper.put("9", 9);

		mapper.put("A", 1);
		mapper.put("B", 2);
		mapper.put("C", 3);
		mapper.put("D", 4);
		mapper.put("E", 5);
		mapper.put("F", 6);
		
		mapper.put("G", 1);
		mapper.put("H", 2);
		mapper.put("I", 3);
		mapper.put("J", 4);
		mapper.put("K", 5);
		mapper.put("L", 6);
		
		mapper.put("M", 1);
		mapper.put("N", 2);
		mapper.put("O", 3);
		mapper.put("P", 4);
		mapper.put("Q", 5);
		mapper.put("R", 6);
		
		mapper.put("S", 1);
		mapper.put("T", 2);
		mapper.put("U", 3);
		mapper.put("V", 4);
		mapper.put("W", 5);
		mapper.put("X", 6);
		mapper.put("Y", 6);
		mapper.put("Z", 6);
		
		StringBuffer chapta = new StringBuffer();
		for(int i=0;i<str.length();i++){
			char ch = str.charAt(i);
			chapta.append(mapper.get(""+ch));
		}
		
		return chapta.toString();
	}
		
	
}
