package com.jskj.common.util;

import org.apache.log4j.Logger;

import com.daguu.lib.httpsqs4j.Httpsqs4j;
import com.daguu.lib.httpsqs4j.HttpsqsException;
import com.danga.MemCached.SockIOPool;

/**
 * 存储服务器配置信息管理
 * @author jordon wang
 * @since 2014-05-18
 */
public class StoreServerManager {
	
	private static boolean initMemcache = false;
	private static boolean initQueue = false;
	
	private static final Logger logger = Logger.getLogger(StoreServerManager.class);
	
	/**
	 * 初始化memcache内存数据
	 */
	public static void initMemcache(String name){
		
		if(initMemcache == false) {
			String[] servers = { "127.0.0.1:12190" };//127.0.0.1
			SockIOPool pool = SockIOPool.getInstance(name);
			pool.setServers(servers);
			pool.setFailover(true);
			pool.setInitConn(10);
			pool.setMinConn(5);
			pool.setMaxConn(250);
			pool.setMaintSleep(30);
			pool.setNagle(false);
			pool.setSocketTO(3000);
			pool.setAliveCheck(true);
			pool.initialize();
		       
			initMemcache = true;
			
		}
		
		logger.info("initMemcache success!!");
		
	}
	
	/**
	 * 初始化消息队列
	 */
	public static void initQueue(){
		
		if(initQueue == false){
			try {
				String server = "127.0.0.1";
				String charset = SysConstants.UTF8;
				int port = SysConstants.HTTP_QUEUE_SERVER_PORT;
				String pass = SysConstants.HTTP_QUEUE_PASSWORD;
				Httpsqs4j.setConnectionInfo(server, port, charset,pass);
			} catch (HttpsqsException e) {
				e.printStackTrace();
			}
			initQueue = true;
		}
		
		logger.info("initQueue success!!");

	}
	
	
	

}
