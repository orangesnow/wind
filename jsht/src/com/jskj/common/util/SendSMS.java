package com.jskj.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 发送短信功能
 * @author Administrator
 * @since 2012-04-13
 */
public class SendSMS {

	private static Logger logger = LoggerFactory.getLogger(SendSMS.class);
	/**
	 * 短信平台帐号
	 */
	private static String SMS_USER = "dlahmyyx";
	/**
	 * 短信平台密码
	 */
	private static String SMS_PASSWORD = "87654321";
	/**
	 * 短信平台产品编码
	 */
	private static String SMS_PID = "1012818";
	/**
	 * 电信通道地址
	 */
	private static String SMS_TELCOM_URL = "http://dx.lmobile.cn:6003/submitdata/Service.asmx/g_Submit";
	/**
	 * 联通移动地址
	 */
	private static String SMS_UNI_URL = "http://wt.lmobile.cn:6003/submitdata/Service.asmx/g_Submit";
	private static String SMS_URL="http://chufa.lmobile.cn/submitdata/service.asmx/g_Submit";
	/**
	 * 电信号段前缀
	 */
	private static String[] telcoms = new String[] { "133", "153", "189", "180" };

	/**
	 * 发送短信功能
	 * 
	 * @param msg
	 *            短信内容
	 * @param mobile
	 *            手机号
	 * @return
	 */
	public static String send(String msg, String mobile) {

		logger.info("mobile:" + mobile);
		logger.info("msg:" + msg);

		if (msg != null && mobile != null) {
			
			String wsUrl = getUrlByMobile(mobile.trim());
			if (wsUrl == null) return null;
			
			StringBuffer query = new StringBuffer();
			query.append("sname=").append(SMS_USER).append("&");
			query.append("spwd=").append(SMS_PASSWORD).append("&");
			query.append("scorpid=").append("").append("&");
			query.append("sprdid=").append(SMS_PID).append("&");
			query.append("sdst=").append(mobile.trim()).append("&");
			query.append("smsg=").append(msg.trim());
			logger.info("query:" + query.toString());
			
			try {
				// 发送POST请求
				URL url = new URL(wsUrl);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setDoOutput(true);

				OutputStreamWriter out = new OutputStreamWriter(conn
						.getOutputStream(), "UTF-8");
				out.write(query.toString());
				out.flush();
				out.close();

				// 获取响应状态
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					logger.error(new StringBuilder("ResponseCode:").append(conn.getResponseCode()).toString());
					return "";
				}
				// 获取响应内容体
				String line, result = "";
				BufferedReader in = new BufferedReader(new InputStreamReader(
						conn.getInputStream(), "utf-8"));
				while ((line = in.readLine()) != null) {
					result += line + "\n";
				}
				in.close();
				
				return result;
			} catch (IOException e) {
				logger.error("sms interface exception");
				sendForYulong(msg, mobile);
			}
		}

		return "";

	}

	
	public static String sendForYulong(String msg, String mobile) {
		
		try {
			// 发送POST请求
			StringBuffer query = new StringBuffer();
			query.append("ua=yulong&pw=916119&mobile=").append(mobile).append(
					"&msg=").append(msg).append("&ylchar=");

			URL url = new URL("http://202.85.214.45:20009/shtp.recmt?"
					+ query.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);

			OutputStreamWriter out = new OutputStreamWriter(conn
					.getOutputStream(), "UTF-8");
			out.write(query.toString());
			out.flush();
			out.close();

			// 获取响应状态

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				logger.error("connect failed!");
				return "";
			}
			// 获取响应内容体
			String line, result = "";
			BufferedReader in = new BufferedReader(new InputStreamReader(conn
					.getInputStream(), "utf-8"));
			while ((line = in.readLine()) != null) {
				result += line + "\n";
			}
			in.close();
			return result;
		} catch (IOException e) {
			logger.error("sms interface exception");
		}
		return "";
	}

	/**
	 * 获取手机号段类型
	 * 
	 * @param mobile
	 * @return
	 */
	public static String getUrlByMobile(String mobile) {
		return SMS_URL;
//		if (mobile != null && mobile.length() == 11) {
//			String mobileNo = mobile.substring(0, 3);
//			for (int i = 0; i < telcoms.length; i++) {
//				if (mobileNo.equals(telcoms[i])) {
//					return SendSMS.SMS_TELCOM_URL;
//				}
//			}
//			return SendSMS.SMS_UNI_URL;
//		} else {
//			return null;
//		}
	}

	/**
	 * 获取手机类型
	 * 
	 * @param mobile
	 *            手机号
	 * @return 0 默认 1电信
	 */
	private int getMobileType(String mobile) {
		int result = 0;
		if (mobile != null && mobile.trim().length() == 11) {
			String prifex = mobile.trim().substring(0, 3);

			try {
				int haoduan = Integer.parseInt(prifex);
			} catch (Exception e) {
				result = -1;
				return -1;
			}

			for (int i = 0; i < telcoms.length; i++) {
				if (prifex.equals(telcoms[i])) {
					return 1;
				}
			}

		}
		return result;
	}

	public static void main(String[] args) {
		String mobile = "13581848108";
		StringBuilder message = new StringBuilder().append("注册验证码:").append(
				"1C893CD57A992DFC91881209AD700ABB".substring(0, 4)).append(
				"。验证码5分钟后失效,本条消息免费。【光合原品】");
		String result = SendSMS.send(message.toString(), mobile);
		System.out.println(result);
	}

}
