package com.jskj.common.util;

/**
 * 系统常量定义
 * @author jordon
 * @since 2014-06-06
 */
public  interface SysConstants {

	public static final String SPT = "/"; //路径分隔符
	public static final String INDEX = "index"; //索引页
	public static final String DEFAULT = "default"; // 默认模板
	public static final String UTF8 = "UTF-8"; //UTF-8编码
	public static final String MESSAGE = "message"; // 提示信息

	public static final String JSESSION_COOKIE = "JSESSIONID";//cookie中的JSESSIONID名称
	public static final String JSESSION_URL = "jsessionid"; // url中的jsessionid名称
	public static final String POST = "POST";//HTTP POST请求
	public static final String GET = "GET"; // HTTP GET请求
	
	public static final String MD5_PASSWORD="1qaz2wsx!@#";
	public static final String AUTO_FOLLOW_MESSAGE="欢迎你的关注，请问有什么可以帮到你的";
	public static final String AUTO_TIPS = "欢迎访问<a href=\"http://www.jiuxf.com\">酒学坊</a>!";
	public static final String AOTU_REPLY_MESSAGE = "辛苦了，感谢您无私的分享！ 小编一定会在后台仔细拜读并给您回复。我们还会邀请行业专家解答您的问题，并在【坊间说】栏目中和大家一起交流。";

	public static  String NO_SEARCH_MESSAGE = "没有搜到与“${1}”相关的信息咧。小编有些脸红了，要不换个词查查试试？也欢迎在菜单中点“微网站”，随便逛逛吧。";
	public static final String SEARCH_URL = "http://www.jiuxf.com/api/search.json?q=${q}";
	public static final String MINI_SEARCH_URL = "http://m.jiuxf.com/search.jspx";
	public static final String MIN_DETAIL_URL = "m.jiuxf.com";
	public static final String MINI_MORE_PICURL = "http://m.jiuxf.com/images/more.png";
	public static final String INFO_BASE_URL = "http://m.jiuxf.com";
	public static final String INFO_MAIN_DOMAIN = "www.jiuxf.com";
	public static final int SYNC_FAILURE = 1;
	public static final int REPEAT_COUNT = 3;
	public static final int INTERVAL_CYCLE = 5000;
	
	public static final int STATUS_SEND_OK = 1;
	public static final int STATUS_SEND_FAILURE = 9;
	
	public static final String SYNC_QUEUE = "weixin_sync_queue";
	public static final String MENU_QUEUE = "weixin_menu_queue";
	public static final String CUSTOM_MT_QUEUE = "weixin_custom_queue";
	public static final String MT_QUEUE = "weixin_mt_queue";
	public static final String MO_QUEUE = "weixin_mo_queue";
	
	public static String MT_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
	public static String MT_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
	
	public static int THREAD_SLEEP_TIME = 50;

	public static final String HTTP_QUEUE_CHARSET = "UTF-8";
	public static final int HTTP_QUEUE_SERVER_PORT = 12180;
	public static final boolean HTTP_QUEUE_CHECKPASSWORD = true;
	public static final String HTTP_QUEUE_SERVER_IP = "112.124.68.28";
	public static final String HTTP_QUEUE_PASSWORD = "jiuxf_weixin_123";

//	public static final String HTTP_QUEUE_SERVER_IP = "116.255.147.42";
//	public static final String HTTP_QUEUE_PASSWORD = "wap_sms_123";
	
	public static final String MORE_RESULT_HEAD = "【更多】酒学坊提供更多的“ ";
	public static final String MORE_RESULT_END = "”相关信息。点击查看……";
	
	public static final String SEARCH_FEILD_TOTALCOUNT = "totalCount";
	public static final String SEARCH_FEILD_LIST = "list"; 
	public static final String SEARCH_FIELD_TITLE = "title";
	public static final String SEARCH_FIELD_CTGNAME= "ctgName";
	public static final String SEARCH_FIELD_URL = "url";
	public static final String SEARCH_FIELD_IMG = "imgUrl2";
	public static final String SEARCH_FIELD_DESCRIPTION = "description";
	
	public static final String LEAVEWORD_READ_URL = "http://sv.yuanpin100.com/read_leave.jspx?ghid=";
}
