RegisterMobile = {};

RegisterMobile.trimSpace = function(str){
	var sname = "";
	if(str != null) sname =  str.replace(/[ ]/g,""); 
	return sname;
}

RegisterMobile.valid_email=function(email) {
	var patten = new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
	return patten.test(email);
}

RegisterMobile.valid_mobile=function(mobile) {
	var patten = new RegExp(/^0?1[358]\d{9}$/);
	return patten.test(mobile);
}


RegisterMobile.checkMobile = function(){
	var mobile = $("#mobile").val();
	if( mobile == null || RegisterMobile.trimSpace(mobile) == ""){
		$("#mobile_div .chek_ture").css("display","none");
		$("#mobile_div .zcsm").css("color","#e4671c").html("手机号不能为空!");
		$("#mobile_div .zcsm").css("display","block");
		return false;
	} else {
		if(!RegisterMobile.valid_mobile(mobile)){
			$("#mobile_div .chek_ture").css("display","none");
			$("#mobile_div .zcsm").css("display","block");
			$("#mobile_div .zcsm").css("color","#e4671c").html("手机号格式不正确!");
			return false;
		} else {
			$.getJSON("/mobile_unique.jspx", {
				mobile : mobile
			}, function(data) {
				if (data == true) {
					$("#mobile_div .zcsm").css("display","none");
					$("#mobile_div .chek_ture").css("display","block");
				} else {
					$("#mobile_div .chek_ture").css("display","none");
					$("#mobile_div .zcsm").css("color","#e4671c").html("手机号已经被占用!");
					$("#mobile_div .zcsm").css("display","block");
				}
			});
		}
	}
	return true;
}


RegisterMobile.checkUsername = function(){
	var username = $("#username").val();
	if( username == null || RegisterMobile.trimSpace(username) == ""){
		$("#username_div .chek_ture").css("display","none");
		$("#username_div .zcsm").css("color","#e4671c").html("昵称不能为空!");
		$("#username_div .zcsm").css("display","block");
		return false;
	} else {
		if(RegisterMobile.trimSpace(username).length >= 2 && RegisterMobile.trimSpace(username).length <= 16){
			$.getJSON("/username_unique.jspx", {
				username : username
			}, function(data) {
				if (data == true) {
					$("#username_div .zcsm").css("display","none");
					$("#username_div .chek_ture").css("display","block");
				} else {
					$("#username_div .chek_ture").css("display","none");
					$("#username_div .zcsm").css("color","#e4671c").html("昵称已经被占用!");
					$("#username_div .zcsm").css("display","block");
					return false;
				}
			});
		} else {
			$("#username_div .chek_ture").css("display","none");
			$("#username_div .zcsm").css("color","#e4671c").html("昵称长度2-16个字符!");
			$("#username_div .zcsm").css("display","block");
			return false;
		}

	}
	return true;
}

RegisterMobile.checkPassword = function(){
	var password = $("#password").val();
	if( password == null || RegisterMobile.trimSpace(password) == ""){
		$("#password_div .chek_ture").css("display","none");
		$("#password_div .zcsm").css("color","#e4671c").html("登录密码不能为空!");
		$("#password_div .zcsm").css("display","block");
		return false;
	} else {
		if(RegisterMobile.trimSpace(password).length >= 4 && RegisterMobile.trimSpace(password).length <= 16){
			$("#password_div .zcsm").css("display","none");
			$("#password_div .chek_ture").css("display","block");
		} else {
			$("#password_div .chek_ture").css("display","none");
			$("#password_div .zcsm").css("color","#e4671c").html("登录密码长度为 4-16个字符!");
			$("#password_div .zcsm").css("display","block");
			return false;
		}
	}
	return true;
}

RegisterMobile.checkRepassword = function(){
	var password = $("#password").val();
	var repassword = $("#repassword").val();
	if( repassword == null || RegisterMobile.trimSpace(repassword) == ""){
		$("#repassword_div .chek_ture").css("display","none");
		$("#repassword_div .zcsm").css("color","#e4671c").html("确认密码不能为空!");
		$("#repassword_div .zcsm").css("display","block");
		return false;
	} else {
		if(password != repassword){
			$("#repassword_div .chek_ture").css("display","none");
			$("#repassword_div .zcsm").css("color","#e4671c").html("登录密码跟确认密码不一致!");
			$("#repassword_div .zcsm").css("display","block");
			return false;
		}else {
			$("#repassword_div .zcsm").css("display","none");
			$("#repassword_div .chek_ture").css("display","block");
		}
	}
	return true;
}

RegisterMobile.checkCaptcha = function(){
	var captcha = $("#captcha").val();
	if( captcha == null || RegisterMobile.trimSpace(captcha) == ""){
		$("#captcha_div .chek_ture").css("display","none");
		$("#captcha_div .zcsm").css("color","#e4671c").html("验证码不能为空!");
		$("#captcha_div .zcsm").css("display","block");
		return false;
	} else {
		if(RegisterMobile.trimSpace(captcha).length == 4) {
			$("#captcha_div .zcsm").css("display","none");
			$("#captcha_div .chek_ture").css("display","block");
		} else {
			$("#captcha_div .chek_ture").css("display","none");
			$("#captcha_div .zcsm").css("color","#e4671c").html("验证码不正确!");
			$("#captcha_div .zcsm").css("display","block");
			return false;
		}
	}
	return true;
}

RegisterMobile.checkAggr = function(){
	if( $("#aggr").attr("checked") == false){
		$("#login_div_left").removeClass("button_sign_left");
		$("#login_div_left").addClass("button_sign_h_left");
		
		$("#login_div_right").removeClass("button_sign_right");
		$("#login_div_right").addClass("button_sign_h_right");
		
		$("#login-button").attr("disabled",true);
	} else {
		$("#login_div_left").removeClass("button_sign_h_left");
		$("#login_div_left").addClass("button_sign_left");
		
		$("#login_div_right").removeClass("button_sign_h_right");
		$("#login_div_right").addClass("button_sign_right");
		
		$("#login-button").attr("disabled",false);
	}
	return true;
}

RegisterMobile.obtainCode=function(){
	 var mobile=$("#mobile").val();
	
	 if($("#mobile_div .chek_ture").css("display") == "none") {
		 $("#mobile").focus();
	 }  else {
		 $.getJSON("/obtain_code.jspx", {
				mobile : mobile
			}, function(data) {
				if (data == true) {
					$("#captcha_div .chek_ture").css("display","none");
					$("#captcha_div .zcsm").css("color","green").html("验证码已下发到您的手机上!");
					$("#captcha_div .zcsm").css("display","block");
				} else {
					$("#captcha_div .chek_ture").css("display","none");
					$("#captcha_div .zcsm").css("color","#e4671c").html("手机号非法,请查证后重试!");
					$("#captcha_div .zcsm").css("display","block");
				}
			});
	 }
	
}

$(document).ready(function(){

	$("#jvForm").submit(function(){

		if(!RegisterMobile.checkMobile()) return false;
		if($("#mobile_div .chek_ture").css("display") == "none") return false;
		
		if(!RegisterMobile.checkUsername()) return false;
		if($("#username_div .chek_ture").css("display") == "none") return false;
		
		if(!RegisterMobile.checkPassword()) return false;
		if($("#password_div .chek_ture").css("display") == "none") return false;
		
		if(!RegisterMobile.checkRepassword()) return false;
		if($("#repassword_div .chek_ture").css("display") == "none") return false;
		
		if(!RegisterMobile.checkCaptcha()) return false;
		if($("#captcha_div .chek_ture").css("display") == "none") return false;
		
		if(!RegisterMobile.checkAggr()) return false;
		if( $("#aggr").attr("checked") == false) return false;
		
		return true;
		
	});
	
	$("#mobile").blur(function(){
		var mobile = $("#mobile").val();
		if( mobile == null || RegisterMobile.trimSpace(mobile) == ""){
			$("#mobile_div .chek_ture").css("display","none");
			$("#mobile_div .zcsm").css("color","#e4671c").html("手机号不能为空!");
			$("#mobile_div .zcsm").css("display","block");
			return false;
		} else {
			if(!RegisterMobile.valid_mobile(mobile)){
				$("#mobile_div .chek_ture").css("display","none");
				$("#mobile_div .zcsm").css("display","block");
				$("#mobile_div .zcsm").css("color","#e4671c").html("手机号格式不正确!");
				return false;
			} else {
				$.getJSON("/mobile_unique.jspx", {
					mobile : mobile
				}, function(data) {
					if (data == true) {
						$("#mobile_div .zcsm").css("display","none");
						$("#mobile_div .chek_ture").css("display","block");
					} else {
						$("#mobile_div .chek_ture").css("display","none");
						$("#mobile_div .zcsm").css("color","#e4671c").html("手机号已经被占用!");
						$("#mobile_div .zcsm").css("display","block");
					}
				});
			}
		}

	});
	
	$("#username").blur(function(){
		var username = $("#username").val();
		if( username == null || RegisterMobile.trimSpace(username) == ""){
			$("#username_div .chek_ture").css("display","none");
			$("#username_div .zcsm").css("color","#e4671c").html("昵称不能为空!");
			$("#username_div .zcsm").css("display","block");
			return false;
		} else {
			if(RegisterMobile.trimSpace(username).length >= 2 && RegisterMobile.trimSpace(username).length <= 16){
				$.getJSON("/username_unique.jspx", {
					username : username
				}, function(data) {
					if (data == true) {
						$("#username_div .zcsm").css("display","none");
						$("#username_div .chek_ture").css("display","block");
					} else {
						$("#username_div .chek_ture").css("display","none");
						$("#username_div .zcsm").css("color","#e4671c").html("昵称已经被占用!");
						$("#username_div .zcsm").css("display","block");
						return false;
					}
				});
			} else {
				$("#username_div .chek_ture").css("display","none");
				$("#username_div .zcsm").css("color","#e4671c").html("昵称长度2-16个字符!");
				$("#username_div .zcsm").css("display","block");
				return false;
			}

		}

	});
	
	$("#password").blur(function(){
		var password = $("#password").val();
		if( password == null || RegisterMobile.trimSpace(password) == ""){
			$("#password_div .chek_ture").css("display","none");
			$("#password_div .zcsm").css("color","#e4671c").html("登录密码不能为空!");
			$("#password_div .zcsm").css("display","block");
			return false;
		} else {
			if(RegisterMobile.trimSpace(password).length >= 4 && RegisterMobile.trimSpace(password).length <= 16){
				$("#password_div .zcsm").css("display","none");
				$("#password_div .chek_ture").css("display","block");
			} else {
				$("#password_div .chek_ture").css("display","none");
				$("#password_div .zcsm").css("color","#e4671c").html("登录密码长度为 4-16个字符!");
				$("#password_div .zcsm").css("display","block");
				return false;
			}
		}
	});
	
	$("#repassword").blur(function(){
		var password = $("#password").val();
		var repassword = $("#repassword").val();
		if( repassword == null || RegisterMobile.trimSpace(repassword) == ""){
			$("#repassword_div .chek_ture").css("display","none");
			$("#repassword_div .zcsm").css("color","#e4671c").html("确认密码不能为空!");
			$("#repassword_div .zcsm").css("display","block");
			return false;
		} else {
			if(password != repassword){
				$("#repassword_div .chek_ture").css("display","none");
				$("#repassword_div .zcsm").css("color","#e4671c").html("登录密码跟确认密码不一致!");
				$("#repassword_div .zcsm").css("display","block");
			}else {
				$("#repassword_div .zcsm").css("display","none");
				$("#repassword_div .chek_ture").css("display","block");
			}
		}
	});
	
	$("#captcha").blur(function(){
		var captcha = $("#captcha").val();
		if( captcha == null || RegisterMobile.trimSpace(captcha) == ""){
			$("#captcha_div .chek_ture").css("display","none");
			$("#captcha_div .zcsm").css("color","#e4671c").html("验证码不能为空!");
			$("#captcha_div .zcsm").css("display","block");
			return false;
		} else {
			if(RegisterMobile.trimSpace(captcha).length == 4) {
				$("#captcha_div .zcsm").css("display","none");
				$("#captcha_div .chek_ture").css("display","block");
			} else {
				$("#captcha_div .chek_ture").css("display","none");
				$("#captcha_div .zcsm").css("color","#e4671c").html("验证码不正确!");
				$("#captcha_div .zcsm").css("display","block");
				return false;
			}
		}
	});
	
	$("#aggr").click(function(){
		if( $("#aggr").attr("checked") == false){
			$("#login_div_left").removeClass("button_sign_left");
			$("#login_div_left").addClass("button_sign_h_left");
			
			$("#login_div_right").removeClass("button_sign_right");
			$("#login_div_right").addClass("button_sign_h_right");
			
			$("#login-button").attr("disabled",true);
		} else {
			$("#login_div_left").removeClass("button_sign_h_left");
			$("#login_div_left").addClass("button_sign_left");
			
			$("#login_div_right").removeClass("button_sign_h_right");
			$("#login_div_right").addClass("button_sign_right");
			
			$("#login-button").attr("disabled",false);
		}
	});
	

});