JiuXFCalendar = {
};

var today = new Date();
var channelId = 68;
var count = 10;
var orderBy = 2;
var curr_pos = -1;
var inDivFlag = false; 
JiuXFCalendar.init = function(str){
		
	 for(var i=0;i<42;i++){
		$("<li><span></span><div><p></p></div></li>").appendTo(".month-body").addClass("month-cell"); 
	 }
	
	  var FullYear = today.getFullYear(); //获取年份
	  var m = today.getMonth();           //获取月号
	
	  var month = today.getMonth()+1;     //获取月份
	  

	  if(month<10){
		 month="0"+month; 
	  }

	  var date = today.getDate();	      //获取日期
	  var day = today.getDay();           //获取星期
	  
	  var monthsNum=[31,28,31,30,31,30,31,31,30,31,30,31];
	  var isleapyear = FullYear%4;        //判断闰年
	  if(isleapyear==0){
		  monthsNum[1]=29;
	  }
  
      if(day==0){
		  day = 7;
	  }
	  var firstDay = day-(date%7-1);       //!important 计算月初星期数

	  if(firstDay==7){                     //如果月初为七，归零
		  firstDay =0;
	  }
	  if(firstDay<0){                       //如果月初为负，加七循环
		  firstDay +=7;
	  }
	  
	var f = firstDay;
	var bf_month = null;
	if(m==0) bf_month = monthsNum[11];
	else bf_month = monthsNum[m-1];
	
	for(var j=0;j<f;j++){
		$("li.month-cell span").eq(j).text(bf_month-f+j+1);
	}
	for(var j=1;j<=monthsNum[m];j++){
		$("li.month-cell span").eq(f).text(j).parent().addClass("pink");
		f++; 
	}
	for(var j=f;j<42;j++){
		$("li.month-cell span").eq(j).text(j-f+1);
	}
	
	var curr_date = new Date();
	var curr_year = curr_date.getFullYear(); //获取年份
	var curr_day = curr_date.getDate();
	var curr_month = curr_date.getMonth()+1;     //获取月份
	
	if(curr_year == FullYear && curr_month == month) {
		$("li.month-cell span").eq(firstDay-1+curr_day).parent().addClass("white");
	}
	

	$("li.month-cell").each(function(){
		
		$(this).bind("mouseover",function(){
				if(curr_pos >-1){
					var xin = (curr_pos%7);
					$("li.month-cell").eq(curr_pos).children(".actioned").removeClass("show");
					$("li.month-cell").eq(curr_pos).children(".actioned").removeClass("w"+xin);
				} 
				
				$(this).children(".actioned").addClass("show");

				curr_pos= (firstDay-1)+parseInt($(this).text());
				
				var xin = (curr_pos%7);
			
				$(this).children(".actioned").addClass("show");
				$(this).children(".actioned").addClass("w"+xin);
			
				
		});
		$(this).bind("mouseout",function(){
			
		});
	});
	
	$(".month-head span.mm").text(FullYear+"年"+month+"月");
	
	$('.month-container').bind("mouseout",function(){
		var xin = (curr_pos%7);
		$("li.month-cell span").eq(curr_pos).parent().children(".actioned").removeClass("w"+xin);
		$("li.month-cell span").eq(curr_pos).parent().children(".actioned").removeClass("show");
	});
};

JiuXFCalendar.pre = function() {

	var day = today.getDate();
	today = new Date(today.getTime()-(day+1)*24*3600*1000);
	$("li.month-cell").remove();
	JiuXFCalendar.init('pre');
	JiuXFCalendar.load_data();
};

JiuXFCalendar.next = function(){
	var day = today.getDate();
	var m = today.getMonth();

	var FullYear = today.getFullYear(); //获取年份
	var monthsNum=[31,28,31,30,31,30,31,31,30,31,30,31];
	var isleapyear = FullYear%4;        //判断闰年
	if(isleapyear==0){
		  monthsNum[1]=29;
	}

	today = new Date(today.getTime()+(monthsNum[m]-day+1)*24*3600*1000);
	$("li.month-cell").remove();
	JiuXFCalendar.init('next');
	JiuXFCalendar.load_data();
};

JiuXFCalendar.label = function(){
	$(".activities li").each(
		function(){

			var oprtime = parseInt($(this).attr('title'));
			
			var date = today.getDate();	    //获取日期
			var day = today.getDay();		//获取星期
			if(day == 0){
				day = 7;
			}
	
			var firstDay = day-(date%7-1);   //!important 计算月初星期数

			if(firstDay==7){                     //如果月初为七，归零
				firstDay =0;
			}
			if(firstDay<0){                       //如果月初为负，加七循环
				  firstDay +=7;
			}

			$("li.month-cell span").eq(firstDay-1 + oprtime).parent().addClass("red");
			$("li.month-cell span").eq(firstDay-1 + oprtime).parent().children("div").addClass("actioned");
			$("li.month-cell span").eq(firstDay-1 + oprtime).parent().children("div").append($(this).html());
		}	
	);
};

JiuXFCalendar.load_data = function(){

		var FullYear = today.getFullYear(); //获取年份
		var m = today.getMonth();           //获取月号
		var month = today.getMonth()+1;     //获取月份

		if(month<10){
			 month="0"+month; 
		}
		var oprtime = FullYear+"-"+month+"-01";
		
		var _url = '/cms_content_page.jspx?channelId='+channelId+'&oprtime='+oprtime+'&count='+count+'&orderBy='+orderBy;

		$.ajax({
	        url : ''+_url,
	        data:{},
	        cache : false, 
	        async : false,
	        type : "GET",
	        dataType : 'html',
	        success : function (data){
	        	$(".activities").html(data);
				JiuXFCalendar.label();
	        }
	});
		
		
};
