Zhaohuimima = {};

Zhaohuimima.trimSpace = function(str){
	var sname = "";
	if(str != null) sname =  str.replace(/[ ]/g,""); 
	return sname;
}

Zhaohuimima.valid_email=function(email) {
	var patten = new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
	return patten.test(email);
}

Zhaohuimima.valid_mobile=function(mobile) {
	var patten = new RegExp(/^0?1[358]\d{9}$/);
	return patten.test(mobile);
}

Zhaohuimima.checkCaptcha = function(){
	var captcha = $("#captcha").val();
	if( captcha == null || Zhaohuimima.trimSpace(captcha) == ""){
		$("#captcha_div .zcsm").css("color","red").html("验证码不能为空!");
		$("#captcha_div .zcsm").css("display","block");
		return false;
	} else {
		$("#captcha_div .zcsm").css("display","none");
	}
	return true;
}

Zhaohuimima.checkEmail=function(){
	var email = $("#email").val();
	if( email == null || Zhaohuimima.trimSpace(email) == ""){
		$("#email_div .chek_ture").css("display","none");
		$("#email_div .zcsm").css("color","red").html("帐号不能为空!");
		$("#email_div .zcsm").css("display","block");
		return false;
	} else {
		if(!Zhaohuimima.valid_mobile(email) && !Zhaohuimima.valid_email(email)){
			$("#email_div .zcsm").css("display","block");
			$("#email_div .chek_ture").css("display","none");
			$("#email_div .zcsm").css("color","red").html("帐号格式不正确!");
			return false;
		} else {
			$("#email_div .chek_ture").css("display","block");
			$("#email_div .zcsm").css("display","none");
		}
	}
	return true;
}

$(document).ready(function(){

	$("#jvForm").submit(function(){
		if(!Zhaohuimima.checkEmail()) return false;
		if(!Zhaohuimima.checkCaptcha()) return false;
		return true;
	});
	
	$("#email").blur(function(){
		var email = $("#email").val();
		if( email == null || Zhaohuimima.trimSpace(email) == ""){
			$("#email_div .chek_ture").css("display","none");
			$("#email_div .zcsm").css("color","red").html("帐号不能为空!");
			$("#email_div .zcsm").css("display","block");
			return false;
		} else {
			if(!Zhaohuimima.valid_mobile(email) && !Zhaohuimima.valid_email(email)){
				$("#email_div .zcsm").css("display","block");
				$("#email_div .chek_ture").css("display","none");
				$("#email_div .zcsm").css("color","red").html("帐号格式不正确!");
				return false;
			} else {
				$("#email_div .chek_ture").css("display","block");
				$("#email_div .zcsm").css("display","none");
			}
		}

	});

	$("#captcha").blur(function(){
		var captcha = $("#captcha").val();
		if( captcha == null || Zhaohuimima.trimSpace(captcha) == ""){
			$("#captcha_div .zcsm").css("color","red").html("验证码不能为空!");
			$("#captcha_div .zcsm").css("display","block");
			return false;
		} else {
			$("#captcha_div .zcsm").css("display","none");
		}
	});


});