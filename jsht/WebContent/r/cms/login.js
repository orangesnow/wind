Login = {};

Login.trimSpace = function(str){
	var sname = "";
	if(str != null) sname =  str.replace(/[ ]/g,""); 
	return sname;
}


Login.valid_email=function(email) {
	var patten = new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
	return patten.test(email);
}

Login.valid_mobile=function(mobile) {
	var patten = new RegExp(/^0?1[358]\d{9}$/);
	return patten.test(mobile);
}


$(document).ready(function(){
	var usernameFlag = true;
	var passwordFlag = true;
	
	$("#jvForm").submit(function(){
		
		if($("#username").val() == null || $("#username").val() == "邮箱 或 手机号" || Login.trimSpace($("#username").val()) == ""){
			if($(".error_left").css("display") == "none"){
				$(".error_left").css("display","block");
			}
			$(".error_rigt").html("帐号不能为空")  ;
			return false;
		} else {
			if(Login.trimSpace($("#username").val()).length >= 3 && Login.trimSpace($("#username").val()).length  <= 100){
				;
			} else {
				return false;
			}
		}
		
		if($("#password").val() == null  || Login.trimSpace($("#password").val()) == ""){
			if($(".error_left").css("display") == "none"){
				$(".error_left").css("display","block");
			}
			$(".error_rigt").html("密码不能为空")  ;
			return false;
		} else {
			if(Login.trimSpace($("#password").val()).length >= 4 && Login.trimSpace($("#password").val()).length <= 16 ){
				;
			} else {
				return false;
			}
			
		}
		
		return true;
	});
	
	$("#username").blur(function(){
		if($("#username").val() == null || $("#username").val() == "邮箱 或 手机号" || Login.trimSpace($("#username").val()) == ""){
			usernameFlag = false;
			if($(".error_left").css("display") == "none"){
				$(".error_left").css("display","block");
			}
			$(".error_rigt").html("帐号不能为空")  ;
			return false;
		} else {
			if(Login.trimSpace($("#username").val()).length >= 3 && Login.trimSpace($("#username").val()).length  <= 100){
				usernameFlag = true;
				
			} else {
				if($(".error_left").css("display") == "none"){
					$(".error_left").css("display","block");
				}
				$(".error_rigt").html("帐号长度3-100个字符")  ;
				return false;
			}
			
		}
		
		if(usernameFlag && passwordFlag) {
			$(".error_left").css("display","none");
		}
		
	});
	
	$("#password").blur(function(){
		if($("#password").val() == null  || Login.trimSpace($("#password").val()) == ""){
			passwordFlag = false;
			if($(".error_left").css("display") == "none"){
				$(".error_left").css("display","block");
			}
			$(".error_rigt").html("密码不能为空") ;
			return false;
		} else {
			if(Login.trimSpace($("#password").val()).length >= 4 && Login.trimSpace($("#password").val()).length <= 16 ){
				passwordFlag = true;
			} else {
				if($(".error_left").css("display") == "none"){
					$(".error_left").css("display","block");
				}
				$(".error_rigt").html("密码长度4-16个字符") ;
				return false;
			}
			
		}
	
		if(usernameFlag && passwordFlag) {
			$(".error_left").css("display","none");
		}
		
	});
	
});