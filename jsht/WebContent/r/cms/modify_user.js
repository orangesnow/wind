ModifyUser = {};

ModifyUser.trimSpace = function(str){
	var sname = "";
	if(str != null) sname =  str.replace(/[ ]/g,""); 
	return sname;
}

ModifyUser.valid_email=function(email) {
	var patten = new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
	return patten.test(email);
}

ModifyUser.valid_mobile=function(mobile) {
	var patten = new RegExp(/^0?1[358]\d{9}$/);
	return patten.test(mobile);
}

ModifyUser.checkUsername = function(){
	var username = $("#username").val();
	var username1 = $("#username1").val();
	
	if( username == null || ModifyUser.trimSpace(username) == ""){
		$("#username_div .chek_ture").css("display","none");
		$("#username_div .zcsm").css("color","red").html("昵称不能为空!");
		$("#username_div .zcsm").css("display","block");
		return false;
	} else {
		if(username == username1){
			$("#username_div .zcsm").css("display","none");
			$("#username_div .chek_ture").css("display","block");
		} else {
			
			$.getJSON("/username_unique.jspx", {
					username : username
			}, function(data) {
				if (data == true) {
					$("#username_div .zcsm").css("display","none");
					$("#username_div .chek_ture").css("display","block");
				} else {
					$("#username_div .chek_ture").css("display","none");
					$("#username_div .zcsm").css("color","red").html("昵称已经被占用!");
					$("#username_div .zcsm").css("display","block");
				}
			});
			
			if($("#username_div .chek_ture").css("display") == "none"){
				return false;
			} 
		
		}
		
	}
	return true;
}

$(document).ready(function(){

	$("#jvForm1").submit(function(){
		if(!ModifyUser.checkUsername()) return false;
		return true;
	});
	
	$("#username").blur(function(){
		var username = $("#username").val();
		var username1 = $("#username1").val();
		
		if( username == null || ModifyUser.trimSpace(username) == ""){
			$("#username_div .chek_ture").css("display","none");
			$("#username_div .zcsm").css("color","red").html("昵称不能为空!");
			$("#username_div .zcsm").css("display","block");
			return false;
		} else {
			if(username == username1){
				$("#username_div .zcsm").css("display","none");
				$("#username_div .chek_ture").css("display","block");
			} else {
				
				$.getJSON("/username_unique.jspx", {
					username : username
				}, function(data) {
				if (data == true) {
					$("#username_div .zcsm").css("display","none");
					$("#username_div .chek_ture").css("display","block");
				} else {
					$("#username_div .chek_ture").css("display","none");
					$("#username_div .zcsm").css("color","red").html("昵称已经被占用!");
					$("#username_div .zcsm").css("display","block");
					return false;
				}
			});
				
			}
		}

	});
	
	

});