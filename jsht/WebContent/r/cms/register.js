Register = {};

Register.trimSpace = function(str){
	var sname = "";
	if(str != null) sname =  str.replace(/[ ]/g,""); 
	return sname;
}

Register.valid_email=function(email) {
	var patten = new RegExp(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]+$/);
	return patten.test(email);
}

Register.checkEmail = function(){
	var email = $("#email").val();
	if( email == null || Register.trimSpace(email) == ""){
		$("#email_div .chek_ture").css("display","none");
		$("#email_div .zcsm").css("color","#e4671c").html("邮箱不能为空!");
		$("#email_div .zcsm").css("display","block");
		return false;
	} else {
		if(!Register.valid_email(email)){
			$("#email_div .chek_ture").css("display","none");
			$("#email_div .zcsm").css("display","block");
			$("#email_div .zcsm").css("color","#e4671c").html("邮箱格式不正确!");
			return false;
		} else {
			$.getJSON("/email_unique.jspx", {
				email : email
			}, function(data) {
				if (data == true) {
					$("#email_div .zcsm").css("display","none");
					$("#email_div .chek_ture").css("display","block");
				} else {
					$("#email_div .chek_ture").css("display","none");
					$("#email_div .zcsm").css("color","#e4671c").html("邮箱已经被占用!");
					$("#email_div .zcsm").css("display","block");
				}
			});
		}
	}
	return true;
}

Register.checkUsername = function(){
	var username = $("#username").val();
	if( username == null || Register.trimSpace(username) == ""){
		$("#username_div .chek_ture").css("display","none");
		$("#username_div .zcsm").css("color","#e4671c").html("昵称不能为空!");
		$("#username_div .zcsm").css("display","block");
		return false;
	} else {
		if(Register.trimSpace(username).length >= 2 && Register.trimSpace(username).length <= 16){
			$.getJSON("/username_unique.jspx", {
				username : username
			}, function(data) {
				if (data == true) {
					$("#username_div .zcsm").css("display","none");
					$("#username_div .chek_ture").css("display","block");
				} else {
					$("#username_div .chek_ture").css("display","none");
					$("#username_div .zcsm").css("color","#e4671c").html("昵称已经被占用!");
					$("#username_div .zcsm").css("display","block");
				}
			});
		} else {
			$("#username_div .chek_ture").css("display","none");
			$("#username_div .zcsm").css("color","#e4671c").html("昵称长度2-16个字符!");
			$("#username_div .zcsm").css("display","block");
			return false;
		}

	}
	return true;
}

Register.checkPassword = function(){
	var password = $("#password").val();
	if( password == null || Register.trimSpace(password) == ""){
		$("#password_div .chek_ture").css("display","none");
		$("#password_div .zcsm").css("color","#e4671c").html("登录密码不能为空!");
		$("#password_div .zcsm").css("display","block");
		return false;
	} else {
		if(Register.trimSpace(password).length >= 4 && Register.trimSpace(password).length <= 16){
			$("#password_div .zcsm").css("display","none");
			$("#password_div .chek_ture").css("display","block");
		} else {
			$("#password_div .chek_ture").css("display","none");
			$("#password_div .zcsm").css("color","#e4671c").html("登录密码长度为 4-16个字符!");
			$("#password_div .zcsm").css("display","block");
			return false;
		}
		
	}
	return true;
}


Register.checkRepassword = function(){
	var password = $("#password").val();
	var repassword = $("#repassword").val();
	if( repassword == null || Register.trimSpace(repassword) == ""){
		$("#repassword_div .chek_ture").css("display","none");
		$("#repassword_div .zcsm").css("color","#e4671c").html("确认密码不能为空!");
		$("#repassword_div .zcsm").css("display","block");
		return false;
	} else {
		if(password != repassword){
			$("#repassword_div .chek_ture").css("display","none");
			$("#repassword_div .zcsm").css("color","#e4671c").html("登录密码跟确认密码不一致!");
			$("#repassword_div .zcsm").css("display","block");
			return false;
		}else {
			$("#repassword_div .zcsm").css("display","none");
			$("#repassword_div .chek_ture").css("display","block");
		}
	}
	return true;
}

Register.checkCaptcha = function(){
	var captcha = $("#captcha").val();
	if( captcha == null || Register.trimSpace(captcha) == ""){
		$("#captcha_div .chek_ture").css("display","none");
		$("#captcha_div .zcsm").css("color","#e4671c").html("验证码不能为空!");
		$("#captcha_div .zcsm").css("display","block");
		return false;
	} else {
		if(Register.trimSpace(captcha).length == 4) {
			$("#captcha_div .zcsm").css("display","none");
			$("#captcha_div .chek_ture").css("display","block");
		} else {
			$("#captcha_div .chek_ture").css("display","none");
			$("#captcha_div .zcsm").css("color","#e4671c").html("验证码不正确!");
			$("#captcha_div .zcsm").css("display","block");
			return false;
		}
	
	}
	return true;
}

Register.checkAggr = function(){
	if( $("#aggr").attr("checked") == false){
		$("#aggr").toggleClass("button_sign_right");
		$("login-button").attr("disabled",true);
		//$("#aggr_div .zcsm").css("color","#e4671c").html("请阅读《酒学坊用户会员协议》 !");
		//$("#aggr_div .zcsm").css("display","block");
		return false;
	} else {
		$("#aggr_div .zcsm").css("display","none");
		//$("#aggr_div .chek_ture").css("display","block");
	}
	return true;
}

$(document).ready(function(){

	$("#jvForm").submit(function(){
		
		if(!Register.checkEmail()) return false;
		if($("#email_div .chek_ture").css("display") == "none") return false;
		
		if(!Register.checkUsername()) return false;
		if($("#username_div .chek_ture").css("display") == "none") return false;
		
		if(!Register.checkPassword()) return false;
		if($("#password_div .chek_ture").css("display") == "none") return false;
		
		if(!Register.checkRepassword()) return false;
		if($("#repassword_div .chek_ture").css("display") == "none") return false;
		
		if(!Register.checkCaptcha()) return false;
		if($("#captcha_div .chek_ture").css("display") == "none") return false;
		
		if(!Register.checkAggr()) return false;
		if( $("#aggr").attr("checked") == false) return false;
		
		return true;
	});
	
	$("#email").blur(function(){
		var email = $("#email").val();
		if( email == null || Register.trimSpace(email) == ""){
			$("#email_div .chek_ture").css("display","none");
			$("#email_div .zcsm").css("color","#e4671c").html("邮箱不能为空!");
			$("#email_div .zcsm").css("display","block");
			return false;
		} else {
			if(!Register.valid_email(email)){
				$("#email_div .chek_ture").css("display","none");
				$("#email_div .zcsm").css("display","block");
				$("#email_div .zcsm").css("color","#e4671c").html("邮箱格式不正确!");
				return false;
			} else {
				$.getJSON("/email_unique.jspx", {
					email : email
				}, function(data) {
					if (data == true) {
						$("#email_div .zcsm").css("display","none");
						$("#email_div .chek_ture").css("display","block");
					} else {
						$("#email_div .chek_ture").css("display","none");
						$("#email_div .zcsm").css("color","#e4671c").html("邮箱已经被占用!");
						$("#email_div .zcsm").css("display","block");
					}
				});
			}
		}

	});
	
	$("#username").blur(function(){
		var username = $("#username").val();
		if( username == null || Register.trimSpace(username) == ""){
			$("#username_div .chek_ture").css("display","none");
			$("#username_div .zcsm").css("color","#e4671c").html("昵称不能为空!");
			$("#username_div .zcsm").css("display","block");
			return false;
		} else {
			if(Register.trimSpace(username).length >= 2 && Register.trimSpace(username).length <= 16){
				$.getJSON("/username_unique.jspx", {
					username : username
				}, function(data) {
					if (data == true) {
						$("#username_div .zcsm").css("display","none");
						$("#username_div .chek_ture").css("display","block");
					} else {
						$("#username_div .chek_ture").css("display","none");
						$("#username_div .zcsm").css("color","#e4671c").html("昵称已经被占用!");
						$("#username_div .zcsm").css("display","block");
						return false;
					}
				});
			} else {
				$("#username_div .chek_ture").css("display","none");
				$("#username_div .zcsm").css("color","#e4671c").html("昵称长度2-16个字符!");
				$("#username_div .zcsm").css("display","block");
				return false;
			}

		}

	});
	
	$("#password").blur(function(){
		var password = $("#password").val();
		if( password == null || Register.trimSpace(password) == ""){
			$("#password_div .chek_ture").css("display","none");
			$("#password_div .zcsm").css("color","#e4671c").html("登录密码不能为空!");
			$("#password_div .zcsm").css("display","block");
			return false;
		} else {
			if(Register.trimSpace(password).length >= 4 && Register.trimSpace(password).length <= 16){
				$("#password_div .zcsm").css("display","none");
				$("#password_div .chek_ture").css("display","block");
			} else {
				$("#password_div .chek_ture").css("display","none");
				$("#password_div .zcsm").css("color","#e4671c").html("登录密码长度为 4-16个字符!");
				$("#password_div .zcsm").css("display","block");
				return false;
			}
		}
	});
	
	$("#repassword").blur(function(){
		var password = $("#password").val();
		var repassword = $("#repassword").val();
		if( repassword == null || Register.trimSpace(repassword) == ""){
			$("#repassword_div .chek_ture").css("display","none");
			$("#repassword_div .zcsm").css("color","#e4671c").html("确认密码不能为空!");
			$("#repassword_div .zcsm").css("display","block");
			return false;
		} else {
			if(password != repassword){
				$("#repassword_div .chek_ture").css("display","none");
				$("#repassword_div .zcsm").css("color","#e4671c").html("登录密码跟确认密码不一致!");
				$("#repassword_div .zcsm").css("display","block");
			}else {
				$("#repassword_div .zcsm").css("display","none");
				$("#repassword_div .chek_ture").css("display","block");
			}
		}
	});
	
	$("#captcha").blur(function(){
		var captcha = $("#captcha").val();
		if( captcha == null || Register.trimSpace(captcha) == ""){
			$("#captcha_div .chek_ture").css("display","none");
			$("#captcha_div .zcsm").css("color","#e4671c").html("验证码不能为空!");
			$("#captcha_div .zcsm").css("display","block");
			return false;
		} else {
			if(Register.trimSpace(captcha).length == 4) {
				$("#captcha_div .zcsm").css("display","none");
				$("#captcha_div .chek_ture").css("display","block");
			} else {
				$("#captcha_div .chek_ture").css("display","none");
				$("#captcha_div .zcsm").css("color","#e4671c").html("验证码不正确!");
				$("#captcha_div .zcsm").css("display","block");
				return false;
			}
		}
	});
	
	$("#aggr").click(function(){
		if( $("#aggr").attr("checked") == false){
			$("#login_div_left").removeClass("button_sign_left");
			$("#login_div_left").addClass("button_sign_h_left");
			
			$("#login_div_right").removeClass("button_sign_right");
			$("#login_div_right").addClass("button_sign_h_right");
			
			$("#login-button").attr("disabled",true);
		} else {
			$("#login_div_left").removeClass("button_sign_h_left");
			$("#login_div_left").addClass("button_sign_left");
			
			$("#login_div_right").removeClass("button_sign_h_right");
			$("#login_div_right").addClass("button_sign_right");
			
			$("#login-button").attr("disabled",false);
		}
	});
	

});