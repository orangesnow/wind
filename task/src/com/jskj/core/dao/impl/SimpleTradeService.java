
package com.jskj.core.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jskj.core.dao.util.BeanFactory;
import com.jskj.core.dao.util.DataBaseService;

public class SimpleTradeService {
	
	private DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");
	
	public int saveSimpleTrade(Integer itemid,String address,String oprtime,Integer isSystem,String title){
		
		Map<String,Object> insertData = new HashMap<String,Object>();
		insertData.put("item_id", itemid);
		insertData.put("oprtime", oprtime);
		insertData.put("address", address);
		insertData.put("is_system", isSystem);
		insertData.put("title", title);
		
		int result = dataBaseService.insertSingleTable("jskj_trade_simple", insertData, true, false);
		
		return result;
	}

	public List<Map<String,Object>> findItemData(int start,int pagesize) {
		List<Map<String,Object>> items = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_item_list where status=1 limit ");
		query.append((start-1)*pagesize).append(",").append(pagesize);

		items = dataBaseService.executeQuery(query.toString());
		
		return items;
	}
	
	public int totalItems(){
		int result = 0;
		StringBuffer query = new StringBuffer();
		query.append("select count(*) cnt from jskj_item_list where status=1 ");
	
		List<Map<String,Object>> items = dataBaseService.executeQuery(query.toString());
		if(items != null && items.size() >0){
			Map<String,Object> item = items.get(0);
			result = (Integer)item.get("cnt");
		}
		
		return result;
	}
	
	public int totalOrderProv(){
		int result = 0;
		StringBuffer query = new StringBuffer();
		query.append("select count(*) cnt from jskj_order_area ");

		List<Map<String,Object>> items = dataBaseService.executeQuery(query.toString());
		if(items != null && items.size() >0){
			Map<String,Object> item = items.get(0);
			result = (Integer)item.get("cnt");
		}
		
		return result;
	}
	
	public Map<String,Object> findOrderProvById(Integer id) {
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_order_area where id = ").append(id);
		
		List<Map<String,Object>> items = dataBaseService.executeQuery(query.toString());
		if(items != null && items.size() >0){
			return items.get(0);
		}
		
		return null;
	}
	
	//0：未同步，1：已同步，2：同步中，3：同步失败
	public List<Map<String,Object>> findNoSyncOrderData(){
		StringBuffer query = new StringBuffer();
		query.append("select * from jskj_trade_info where duty_status=2 limit 0,100");
		
		List<Map<String,Object>> orders = dataBaseService.executeQuery(query.toString());
		
		return orders;
	}
	
	public List<Map<String,Object>> findItemsFromTradeId(String tid) {
		StringBuffer query  = new StringBuffer();
		query.append("select o.*,i.out_item_id,s.out_sku_id from jskj_trade_order o,jskj_item_list i,jskj_item_sku s where o.sku_id=s.sku_id and i.item_id=s.item_id and o.trade_id='").append(tid).append("'");
		
		List<Map<String,Object>> items = dataBaseService.executeQuery(query.toString());
		return items;
		
	}
	
	public void updateOrderDutyStatus(String tid,Integer status) {
		
		Map<String,Object> setData = new HashMap<String,Object>();
		setData.put("duty_status", status);
		
		Map<String,Object> whereData = new HashMap<String,Object>();
		whereData.put("t_id", tid);
		
		dataBaseService.updateTable("jskj_trade_info", setData, whereData);
		
	}
	
}
