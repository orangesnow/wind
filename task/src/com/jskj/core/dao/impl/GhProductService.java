package com.jskj.core.dao.impl;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jskj.core.api.entity.resp.ItemResp;
import com.jskj.core.dao.util.BeanFactory;
import com.jskj.core.dao.util.DataBaseService;

public class GhProductService {

	
	private DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");
	
	public void saveProduct(ItemResp item){
		Map<String,Object> insertData = new HashMap<String,Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		insertData.put("p_id", item.getNum_iid());
		insertData.put("title", item.getTitle());
		if(item.getDesc() != null){
			int len = item.getDesc().length();
			if(len<200){
				insertData.put("remark", item.getDesc().substring(0, len));
			} else {
				insertData.put("remark", item.getDesc().substring(0, 200));
			}
		}
		insertData.put("origin_price", item.getOrigin_price());
		
		insertData.put("outer_id", item.getOuter_id());
		insertData.put("created", item.getCreated());
		insertData.put("detail_url", item.getDetail_url());
		insertData.put("pic_url", item.getPic_url());
		insertData.put("pic_thumb_url", item.getPic_thumb_url());
		insertData.put("num", item.getNum());
		insertData.put("sold_num", item.getSold_num());
		insertData.put("price", item.getPrice());
		insertData.put("post_price", item.getPost_fee());
		insertData.put("status", 1);
		insertData.put("nickname", item.getTitle().subSequence(0, 5));
		dataBaseService.insertSingleTable("gh_product", insertData, false, true);

	}
	
	public void updateProduct(ItemResp item,int pid){
		Map<String,Object> insertData = new HashMap<String,Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		insertData.put("title", item.getTitle());
		if(item.getDesc() != null){
			
			int len = item.getDesc().length();
			if(len<200){
				insertData.put("remark", item.getDesc().substring(0, len));
			} else {
				insertData.put("remark", item.getDesc().substring(0, 200));
			}
		}
		
		insertData.put("origin_price", item.getOrigin_price());
		insertData.put("outer_id", item.getOuter_id());
		insertData.put("created", item.getCreated());
		insertData.put("detail_url", item.getDetail_url());
		insertData.put("pic_url", item.getPic_url());
		insertData.put("pic_thumb_url", item.getPic_thumb_url());
		insertData.put("num", item.getNum());
		insertData.put("sold_num", item.getSold_num());
		insertData.put("price", item.getPrice());
		insertData.put("post_price", item.getPost_fee());
		
		Map<String,Object> whereData = new HashMap<String,Object>();
		whereData.put("p_id", pid);

		dataBaseService.updateTable("gh_product", insertData, whereData);
	}
	
	
	public Map<String,Object> findProduct(int numiid){
	
		List<Map<String,Object>> products = dataBaseService.executeQuery("select * from gh_product where p_id="+numiid);
		
		if(products != null && products.size() >0){
			return products.get(0);
		} else {
			return null;
		}
		
	}
	
	public Map<String,Object> findProductByCode(String code){
		
		String sql = "select * from gh_product where outer_id='"+code+"'";
		System.out.println("sql:"+sql);
		List<Map<String,Object>> products = dataBaseService.executeQuery(sql);
		
		if(products != null && products.size() >0){
			return products.get(0);
		} else {
			return null;
		}
		
	}
	
	
}
