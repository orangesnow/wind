
package com.jskj.core.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jskj.core.dao.util.BeanFactory;
import com.jskj.core.dao.util.DataBaseService;

/**
 * 订单物流信息服务类
 * @author VAIO
 * @since 2015-06-29
 */
public class GhOrderGoodsService {
	
	private DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");
	
	public void saveOrder(String desc,String time,String tid,String outsid){
		
		Map<String,Object> insertData = new HashMap<String,Object>();

		insertData.put("tid", tid);
		insertData.put("out_sid", outsid);
		
		insertData.put("status_desc", desc);
		insertData.put("status_time", time);

		
		dataBaseService.insertSingleTable("gh_order_goods", insertData, false, true);
	}

	public List<Map<String,Object>> findLastLogistics(String tid) {
		
		StringBuffer query = new StringBuffer();
		query.append("select * from gh_order_goods where tid = '").append(tid).append("' order by status_time desc limit 1");
		
		
		List<Map<String,Object>> results = dataBaseService.executeQuery(query.toString());
		
		if(results != null && results.size() >0){
			return results;
		} 
		
		return null;
		
	}

}
