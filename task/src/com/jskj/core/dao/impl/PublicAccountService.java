
package com.jskj.core.dao.impl;

import java.util.List;
import java.util.Map;

import com.jskj.core.dao.util.BeanFactory;
import com.jskj.core.dao.util.DataBaseService;
import com.jskj.core.util.Md5Util;
import com.jskj.core.util.SysConstants;

public class PublicAccountService {
	
	private DataBaseService dataBaseService = (DataBaseService) BeanFactory.getBean("dataBaseService");
	
	public List<Map<String,Object>> findJobList(Integer corpid) {
		
		List<Map<String,Object>> jobsList = null;
		StringBuffer query = new StringBuffer();
		query.append(" select j.*,c.corp_name from wx_employ_job j,wx_employ_corp c");
		query.append(" where j.corp_id=c.corp_id and j.is_check=1 and j.is_display=1 and c.corp_id=").append(corpid);
		query.append(" order by weight desc");
		jobsList = dataBaseService.executeQuery(query.toString());
		
		return jobsList;
	}

	public List<Map<String,Object>>  findCorpList(){
		List<Map<String,Object>> jobsList = null;
		StringBuffer query = new StringBuffer();
		query.append(" select c.* from wx_employ_corp c");
		query.append(" where c.corp_ischeck=1 and corp_status=1");
		
		jobsList = dataBaseService.executeQuery(query.toString());
		
		return jobsList;
	}

	public boolean weixinUserPreBind(String openid,String md5code,String ip){
		boolean result = false;
		
		StringBuffer query = new StringBuffer();
		query.append(" select u.* from wx_employ_user u");
		query.append(" where u.wx_openid='").append(openid).append("'");
		
		List<Map<String,Object>> bindList = dataBaseService.executeQuery(query.toString());
		if(bindList == null || bindList.size() == 0){
			StringBuffer insertSql = new StringBuffer();
			insertSql.append(" insert into wx_employ_user(wx_openid,ip,md5_code,status,is_bind) ");
			insertSql.append(" values('").append(openid).append("','").append(ip).append("','").append(md5code).append("',1,0)");
			int user_id = dataBaseService.executeUpdate(insertSql.toString());
			if(user_id>0) result = true;
		} else {
			Map<String,Object> user = bindList.get(0);
			Integer isBind = (Integer)user.get("is_bind");
			if(isBind == 0){
				StringBuffer updateSql = new StringBuffer();
				updateSql.append(" update wx_employ_user u set u.status=1,u.md5_code = '").append(md5code).append("'");
				updateSql.append(" where wx_openid='").append(openid).append("'");
				int count = dataBaseService.executeUpdate(updateSql.toString());
				if(count>0) result=true;
			}
		
		}
		
		return result;
	}

	public boolean saveUserResume(Integer userId,String name,String mobile,String email,String weixin,int jobid,String md5code){
		boolean result = false;
		StringBuffer updateSql = new StringBuffer();
		
		updateSql.append(" update wx_employ_user set name='").append(name).append("',");
		updateSql.append(" mobile='").append(mobile).append("',email='").append(email).append("',wx_nickname='").append(weixin).append("',");
		updateSql.append(" status=1,is_bind=1");
		updateSql.append(" where user_id = ").append(userId);
		int nums = dataBaseService.executeUpdate(updateSql.toString());

		StringBuffer insertSql = new StringBuffer();
		insertSql.append(" insert into wx_employ_submit(user_id,job_id,opr_time,flag)");
		insertSql.append(" values(").append(userId).append(",").append(jobid).append(",sysdate(),0)");
		int uj_id=dataBaseService.insert(insertSql.toString());
		
		if(nums > 0 && uj_id > 0){
			result = true;
		}
		
		return result;
	}

	public String getMd5Code(String openid){
		return Md5Util.encode(openid+SysConstants.MD5_PASSWORD);
	}
	
	public Map<String,Object> findJobDetail(Integer jobid){
		Map<String,Object> detail = null;
		
		StringBuffer query = new StringBuffer("select j.*,p.corp_name from wx_employ_job j,wx_employ_corp p where p.corp_id=j.corp_id and  j.is_display=1 and j.is_check=1 and  j.job_id=").append(jobid);
		System.out.println("query:"+query.toString());
		List<Map<String,Object>> jobs = dataBaseService.executeQuery(query.toString());
		
		if(jobs != null && jobs.size() >0){
			detail = jobs.get(0);
		}
		
		return detail;
	}
	
	
	public Map<String,Object> findWeixinUserByOpenid(String openid){
		Map<String,Object> user = null;
		
		StringBuffer query = new StringBuffer();
		query.append(" select * from wx_employ_user where wx_openid='").append(openid).append("'");
		
		List<Map<String,Object>> users = dataBaseService.executeQuery(query.toString());
		if(users != null && users.size() >0){
			return users.get(0);
		}
		return user;
	}
	
	public Map<String,Object> findAccountByCode(String code){
		Map<String,Object> account = null;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from wx_public_account where code='").append(code).append("'");
		
		List<Map<String,Object>> accounts = dataBaseService.executeQuery(query.toString());
		if(accounts != null && accounts.size() >0){
			return accounts.get(0);
		}
		return account;
	}
	
	public Map<String,Object> findAccountById(Integer pubType){
		Map<String,Object> account = null;
		
		if(pubType == null) return account;
		
		StringBuffer query = new StringBuffer();
		query.append("select * from wx_public_account where id=").append(pubType);
		
		List<Map<String,Object>> accounts = dataBaseService.executeQuery(query.toString());
		if(accounts != null && accounts.size() >0){
			return accounts.get(0);
		}
		
		return account;
	}

}
