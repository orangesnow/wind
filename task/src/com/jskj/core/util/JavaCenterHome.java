package com.jskj.core.util;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import com.jskj.core.dao.util.PropertiesHelper;

public final class JavaCenterHome {
	
	public static final boolean IN_JCHOME = true;
	public static final String JCH_CHARSET = "GBK";
	public static final String JCH_VERSION = "2.0";
	public static final int JCH_RELEASE = 20110324;
	public static String jchRoot = null;

	public static Map<String, String> jchConfig = new HashMap<String, String>();
	private static Map<String, String> tableNames = new HashMap<String, String>();
	
	public static String getTableName(String name) {
		String tableName = tableNames.get(name);
		if (tableName == null) {
			tableName = jchConfig.get("tablePre") + name;
			tableNames.put(name, tableName);
		}
		return tableName;
	}
		public static String getProperty(String key){
		if(jchConfig.isEmpty()){
			jchConfig = loadProperties();
		}		return  jchConfig.get(key) ;	}
		
	public static Map<String, String> loadProperties(){
		Map<String, String> jchConfig = new HashMap<String, String>();
		
		try {
			PropertiesHelper propHelper = new PropertiesHelper("/config.properties");
			Properties config = propHelper.getProperties();
			Set<Object> keys = config.keySet();
			for (Object key : keys) {
				String k = (String) key;
				String v = (String) config.get(key);
				jchConfig.put(k, v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jchConfig;
	}	
	public static void clearTableNames() {
		tableNames.clear();
	}
	
	public static void setJchRoot(HttpServletRequest request) {
		jchRoot = request.getSession().getServletContext().getRealPath("/");
		if (jchRoot == null) {
			try {
				jchRoot = request.getSession().getServletContext().getResource("/").getPath();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (!jchRoot.endsWith("/"))jchRoot = jchRoot + "/";
	}
	
}