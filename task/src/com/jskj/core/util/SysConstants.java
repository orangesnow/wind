package com.jskj.core.util;

/**
 * 系统常量定义
 * @author jordon
 * @since 2014-06-06
 */
public  interface SysConstants {

	public static final String SPT = "/"; //路径分隔符
	public static final String INDEX = "index"; //索引页
	public static final String DEFAULT = "default"; // 默认模板
	public static final String UTF8 = "UTF-8"; //UTF-8编码
	public static final String MESSAGE = "message"; // 提示信息

	public static final String JSESSION_COOKIE = "JSESSIONID";//cookie中的JSESSIONID名称
	public static final String JSESSION_URL = "jsessionid"; // url中的jsessionid名称
	public static final String POST = "POST";//HTTP POST请求
	public static final String GET = "GET"; // HTTP GET请求
	
	public static final String MD5_PASSWORD="1qaz2wsx!@#";

	public static final int SYNC_FAILURE = 1;
	public static final int REPEAT_COUNT = 3;
	public static final int INTERVAL_CYCLE = 5000;
	
	public static final int STATUS_SEND_OK = 1;
	public static final int STATUS_SEND_FAILURE = 9;
	
	public static final String MENU_QUEUE = "weixin_menu_queue";
	public static final String CUSTOM_MT_QUEUE = "weixin_custom_queue";
	public static final String MT_QUEUE = "weixin_mt_queue";
	public static final String MO_QUEUE = "weixin_mo_queue";
	
	public static String MT_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
	public static String MT_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";
	
	public static int THREAD_SLEEP_TIME = 50;
	
	public static final String HTTP_QUEUE_CHARSET = "GBK";
	public static final int HTTP_QUEUE_SERVER_PORT = 12180;
	public static final boolean HTTP_QUEUE_CHECKPASSWORD = true;
	public static final String HTTP_QUEUE_SERVER_IP = "112.124.68.28";
	public static final String HTTP_QUEUE_PASSWORD = "jiuxf_weixin_123";

	public static final int    MEMCACHED_SERVER_PORT = 12190;
	public static final String MEMCACHED_SERVER_IP = "127.0.0.1";

	public static final String MORE_RESULT_HEAD = "【更多】查看更多与“ ";
	public static final String MORE_RESULT_END = "”相关信息…";
	public static final String MORE_RESULT = "【更多】查看更多与“{content}”相关信息…";
	public static final String SEARCH_FEILD_TOTALCOUNT = "totalCount";
	public static final String SEARCH_FEILD_LIST = "list"; 
	public static final String SEARCH_FIELD_TITLE = "title";
	public static final String SEARCH_FIELD_CTGNAME= "ctgName";
	public static final String SEARCH_FIELD_URL = "url";
	public static final String SEARCH_FIELD_IMG = "imgUrl2";
	public static final String SEARCH_FIELD_DESCRIPTION = "description";
	
	public static final String WX_ALERT_MESSAGE_PREFIX = "wx_alert_message_";
	public static final String WX_ALERT_MESSAGE_FOLLOW = "wx_alert_message_follow";
	public static final String WX_ALERT_MESSAGE_SEARCH = "wx_alert_message_search";
	public static final String WX_ALERT_MESSAGE_LEAVEWORD = "wx_alert_message_leaveword";
	public static final String WX_ALERT_MESSAGE_MORE = "wx_alert_message_more";
	public static final String WX_MEMCACHE_NAME = "wx_memcache_name";
	
	public static final String WX_TOKEN_PREFIX = "wx_token_";
	
	public static final int WX_ALERT_FOLLOWMESSAGE_KEY = 1;
	public static final int WX_ALERT_SEARCHMESSAGE_KEY = 2;
	public static final int WX_ALERT_LEAVEWORDMESSAGE_KEY = 3;
	public static final int WX_ALERT_MOREMESSAGE_KEY = 4;
	
	public static final String HTTP_METHOD_GET = "GET";
	public static final String HTTP_METHOD_POST = "POST";
	
	public static final int MENU_DISPLAY_YES = 1;
	
	public static String MODULE_LEAVEWORD_NO_VOICE = "没有找到你的留言，请查证后重试";
	public static String MODULE_LEAVEWORD_NO_REPLY = "没有正确回复确认码，请查证后重试";

	public static String MODULE_QUEUE_GOODS = "goods_info_queue";
	public static String MODULE_QUEUE_QUESTIONS = "question_info_queue";

	public static String PRODUCT_URL = "http://api.aihuo360.com";
	public static String TEST_URL = "http://test.api.yepcolor.com";

}
