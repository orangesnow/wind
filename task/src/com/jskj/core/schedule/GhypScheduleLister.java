package com.jskj.core.schedule;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.jskj.core.schedule.job.AutoOrderJob;
import com.jskj.core.schedule.job.SyncOrderJob;
import com.jskj.core.schedule.job.SyncProductJob;

public class GhypScheduleLister implements ServletContextListener{


	public static final String SYNC_ORDER_JOB_NAME = "syncOrderJob";
	public static final String SYNC_PRODUCT_JOB_NAME = "syncProductJob";

	public static final String SYNC_AUTOORDER_JOB_NAME = "autoOrderJob";

	private static final Logger logger = Logger.getLogger(GhypScheduleLister.class);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
		try {
			QuartzManager.removeJob(SYNC_ORDER_JOB_NAME);
//			QuartzManager.removeJob(SYNC_PRODUCT_JOB_NAME);
			QuartzManager.removeJob(SYNC_AUTOORDER_JOB_NAME);
		} catch (Exception e) {
			logger.error("destory job!!");
		}

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		SimpleDateFormat DateFormat = new SimpleDateFormat("yyyyMMddHHmmss");  
		String returnstr = DateFormat.format(new Date());         
		
		logger.info(returnstr+"【系统启动】");

		SyncOrderJob orderJob = new SyncOrderJob();
//		SyncProductJob productJob = new SyncProductJob();
		AutoOrderJob autoJob = new AutoOrderJob();
		
		try {

			QuartzManager.addJob(SYNC_ORDER_JOB_NAME, orderJob,"0/5 * * * * ?"); //每5分钟执行一次
			QuartzManager.addJob(SYNC_AUTOORDER_JOB_NAME, autoJob, "0 */30 * * * ?"); //每30分钟执行一次
			
		} catch (Exception e) {
			logger.error("sync exception:"+e.getMessage());
		}
		
	}
	
	public static void main(String[] args) throws Exception{
		SimpleDateFormat DateFormat = new SimpleDateFormat("yyyyMMddHHmmss");  
		String returnstr = DateFormat.format(new Date());         

		System.out.println(returnstr+"【系统启动】");

	}

}