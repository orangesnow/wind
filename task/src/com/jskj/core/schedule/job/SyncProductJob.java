package com.jskj.core.schedule.job;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jskj.core.api.ThirdApiClient;
import com.jskj.core.api.ThirdApiParser;
import com.jskj.core.api.entity.DetailItem;
import com.jskj.core.api.entity.SimpleItem;

/**
 * 同步处理程序
 * @author jordon
 * @since 2014-05-18
 */
public class SyncProductJob implements Job{
	
	private static Logger logger = Logger.getLogger(SyncProductJob.class);

	public static void main(String[] args) throws Exception{
		new SyncProductJob().execute(null);
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		logger.info("SyncProductJob="+sdf.format(new Date()));

		int pageno = 1;
		int pagesize = 10;
		try {
			ThirdApiClient client = new ThirdApiClient(null,null);
	    	String response = client.getProduct(pageno,pagesize);
	    	SimpleItem[]  items = ThirdApiParser.parseItemList(response);
	    	while(items != null){
	    		for(int i=0;i<items.length;i++){
	    			String itemid = items[i].getId();
	    			logger.info("itemid:"+itemid);
	    			String item_detail = client.getProductDetail(itemid);
	    			DetailItem detail = ThirdApiParser.parseDetailItem(item_detail);
	    			String detail_url = detail.getDetail_url();
	    			logger.info("detail_url:"+detail_url);
	    			 String[]  pictures = ThirdApiParser.parseDetailUrl(detail_url);
	    			 if(pictures != null){
	    				 logger.info("pictures[0]:"+pictures[0]);
	    			 }
	    		}
	    		pageno++;
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		logger.info("SyncProductJob end..."+sdf.format(new Date()));
	}
	
	

}

