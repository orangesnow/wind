package com.jskj.core.schedule.job;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jskj.core.api.ThirdApiClient;
import com.jskj.core.dao.impl.SimpleTradeService;
import com.jskj.core.dao.util.BeanFactory;

/**
 * 同步处理程序
 * @author jordon
 * @since 2014-05-18
 */
public class SyncOrderJob implements Job{
	
	private static Logger logger = Logger.getLogger(SyncOrderJob.class);

	public static void main(String[] args) throws Exception{
		new SyncOrderJob().execute(null);
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		logger.info("SyncDutyService="+sdf.format(new Date()));

		//同步任务
		SimpleTradeService tradeService = (SimpleTradeService) BeanFactory.getBean("simpleTradeService");
		List<Map<String,Object>> orders = tradeService.findNoSyncOrderData();
		
		if(orders != null && orders.size() >0){
			for(int i=0;i<orders.size();i++){
				
				boolean flag = false;
				Map<String,Object> order  = orders.get(i);
				String tid = (String)order.get("t_id");
				List<Map<String,Object>> items = tradeService.findItemsFromTradeId(tid);
				ThirdApiClient client = new ThirdApiClient();
				
				try {
					client.syncOrder(order, items);
				} catch (Exception e) {
					e.printStackTrace();
					tradeService.updateOrderDutyStatus(tid, 3);
					flag = true;
				}
				
				if(!flag){
					tradeService.updateOrderDutyStatus(tid, 1);
				}
				
				logger.info("orderid:"+tid);

			}
		}
		
		logger.info("SyncDutyService end..."+sdf.format(new Date()));
	}
	
	

}

