package com.jskj.core.schedule.job;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.jskj.core.dao.impl.SimpleTradeService;
import com.jskj.core.dao.util.BeanFactory;

/**
 * 同步处理程序
 * @author jordon
 * @since 2014-05-18
 */
public class AutoOrderJob implements Job{
	
	private static Logger logger = Logger.getLogger(AutoOrderJob.class);
	public static final int SYNC_TYPE_SHARE = 4;
	public static final int SYNC_TYPE_GOODS = 6;

	public static void main(String[] args) throws Exception{
		new AutoOrderJob().execute(null);
//		
//		for(int i=0;i<100;i++){
//			int result = new AutoOrderJob().peng(1, 2);
//			System.out.println("result:"+result);
//		}
		
	}

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		logger.info("SyncDutyService="+sdf.format(new Date()));

		//同步任务
		SimpleTradeService tradeService = (SimpleTradeService) BeanFactory.getBean("simpleTradeService");
	
		int pagesize = 20;
		
		int count  = tradeService.totalItems();
		int pageno = (count+pagesize-1)/pagesize;
		
		for(int i=0;i<pageno;i++){
			List<Map<String,Object>> items = tradeService.findItemData(i+1, pagesize);
			for(int j=0;j<items.size();j++){
				int flag = peng(1,3);
				if(flag == 1){
					String addr = "未知地区";
					Map<String,Object> item  = items.get(j);
					String title = (String)item.get("title");
					Integer itemid = (Integer)item.get("item_id");
					int provs = tradeService.totalOrderProv();
					int provid = RandomUtils.nextInt(provs)+1;
					Map<String,Object> prov = tradeService.findOrderProvById(provid) ;
					
					if(prov != null && !prov.isEmpty()){
						addr = (String)prov.get("prov");
					}
					
					tradeService.saveSimpleTrade(itemid, addr, sdf.format(new Date()), 1, title);
					
				}
			}
		}

		logger.info("SyncDutyService end..."+sdf.format(new Date()));
		
	}

	
	public int peng(int num,int scope){
		int flag = 0;
		
		int rand = RandomUtils.nextInt(scope);
		if(rand == num) return 1;
		else return flag;
		
	}
	
	
}

