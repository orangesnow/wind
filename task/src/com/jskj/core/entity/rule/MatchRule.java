
package com.jskj.core.entity.rule;

/**
 * @author VAIO
 *
 */
public class MatchRule {
	
	private  int ruleId;
	private String cmdWord;
	private int matchType;
	private int priority;
	private String serviceId;
	private int dealCode;
	private int status;
	
	/**
	 * @return the ruleId
	 */
	public int getRuleId() {
		return ruleId;
	}
	/**
	 * @param ruleId the ruleId to set
	 */
	public void setRuleId(int ruleId) {
		this.ruleId = ruleId;
	}
	/**
	 * @return the cmdWord
	 */
	public String getCmdWord() {
		return cmdWord;
	}
	/**
	 * @param cmdWord the cmdWord to set
	 */
	public void setCmdWord(String cmdWord) {
		this.cmdWord = cmdWord;
	}
	/**
	 * @return the matchType
	 */
	public int getMatchType() {
		return matchType;
	}
	/**
	 * @param matchType the matchType to set
	 */
	public void setMatchType(int matchType) {
		this.matchType = matchType;
	}
	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}
	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	/**
	 * @return the dealCode
	 */
	public int getDealCode() {
		return dealCode;
	}
	/**
	 * @param dealCode the dealCode to set
	 */
	public void setDealCode(int dealCode) {
		this.dealCode = dealCode;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	

}
