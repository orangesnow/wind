package com.jskj.core.entity;

public class GhProduct {

	private String product_id;
	private String product_prop_id;
	private double sale_price;
	private Integer quantity;
	
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getProduct_prop_id() {
		return product_prop_id;
	}
	public void setProduct_prop_id(String product_prop_id) {
		this.product_prop_id = product_prop_id;
	}
	public double getSale_price() {
		return sale_price;
	}
	public void setSale_price(double sale_price) {
		this.sale_price = sale_price;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
