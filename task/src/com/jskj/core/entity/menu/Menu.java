/**
*无线市场版权所有（2011-2020）
*/
package com.jskj.core.entity.menu;

public class Menu {
	
	private Button[] button;

	public Button[] getButton() {
		return button;
	}

	public void setButton(Button[] button) {
		this.button = button;
	}
	
}
