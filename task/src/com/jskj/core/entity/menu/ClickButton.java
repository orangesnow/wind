/**
*无线市场版权所有（2011-2020）
*/
package com.jskj.core.entity.menu;

public class ClickButton extends Button {
	
	private String type;
	private String key;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
}
