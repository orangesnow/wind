package com.jskj.core.entity;

public class GhTradeOrder {
	
	private String line_items_attributes;
	
	private String name;
	private String phone;
	private String shipping_province;
	
	private double shipping_charge;
	private String shipping_city;
	private String shipping_district;
	private String shipping_address;
	
	private String comment;
	private Integer pay_type;
	
	public String getShipping_province() {
		return shipping_province;
	}
	public void setShipping_province(String shipping_province) {
		this.shipping_province = shipping_province;
	}
	public String getShipping_city() {
		return shipping_city;
	}
	public void setShipping_city(String shipping_city) {
		this.shipping_city = shipping_city;
	}
	public String getShipping_district() {
		return shipping_district;
	}
	public void setShipping_district(String shipping_district) {
		this.shipping_district = shipping_district;
	}
	public String getShipping_address() {
		return shipping_address;
	}
	public void setShipping_address(String shipping_address) {
		this.shipping_address = shipping_address;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getLine_items_attributes() {
		return line_items_attributes;
	}
	public void setLine_items_attributes(String line_items_attributes) {
		this.line_items_attributes = line_items_attributes;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getPay_type() {
		return pay_type;
	}
	public void setPay_type(Integer pay_type) {
		this.pay_type = pay_type;
	}
	public double getShipping_charge() {
		return shipping_charge;
	}
	public void setShipping_charge(double shipping_charge) {
		this.shipping_charge = shipping_charge;
	}

}
