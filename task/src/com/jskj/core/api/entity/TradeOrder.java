package com.jskj.core.api.entity;

public class TradeOrder {
	
	private TradeOrderPromotion[] promotionDetails;
	private BuyerMessage[] messages;
	private String ghid;
	private int num_iid;
	private int sku_id;
	private String sku_unique_code;
	private int num;
	private String outer_sku_id;

	private String outer_item_id;
	private String title;
	private String seller_nick;
	private double price;
	private double total_fee;
	private double discount_fee;
	private double payment ;
	private String sku_properties_name;
	private String pic_path;
	private String pic_thumb_path;
	private double fenxiao_price;
	private double fenxiao_payment;
	private Integer itemType;
	
	public TradeOrderPromotion[] getPromotionDetails() {
		return promotionDetails;
	}
	public void setPromotionDetails(TradeOrderPromotion[] promotionDetails) {
		this.promotionDetails = promotionDetails;
	}
	
	public String getGhid() {
		return ghid;
	}
	public void setGhid(String ghid) {
		this.ghid = ghid;
	}
	public String getSku_unique_code() {
		return sku_unique_code;
	}
	public void setSku_unique_code(String sku_unique_code) {
		this.sku_unique_code = sku_unique_code;
	}
	public double getFenxiao_price() {
		return fenxiao_price;
	}
	public void setFenxiao_price(double fenxiao_price) {
		this.fenxiao_price = fenxiao_price;
	}
	public double getFenxiao_payment() {
		return fenxiao_payment;
	}
	public void setFenxiao_payment(double fenxiao_payment) {
		this.fenxiao_payment = fenxiao_payment;
	}
	public Integer getItemType() {
		return itemType;
	}
	public void setItemType(Integer itemType) {
		this.itemType = itemType;
	}
	public BuyerMessage[] getMessages() {
		return messages;
	}
	public void setMessages(BuyerMessage[] messages) {
		this.messages = messages;
	}
	public int getNum_iid() {
		return num_iid;
	}
	public void setNum_iid(int num_iid) {
		this.num_iid = num_iid;
	}
	public int getSku_id() {
		return sku_id;
	}
	public void setSku_id(int sku_id) {
		this.sku_id = sku_id;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getOuter_sku_id() {
		return outer_sku_id;
	}
	public void setOuter_sku_id(String outer_sku_id) {
		this.outer_sku_id = outer_sku_id;
	}
	public String getOuter_item_id() {
		return outer_item_id;
	}
	public void setOuter_item_id(String outer_item_id) {
		this.outer_item_id = outer_item_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSeller_nick() {
		return seller_nick;
	}
	public void setSeller_nick(String seller_nick) {
		this.seller_nick = seller_nick;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(double total_fee) {
		this.total_fee = total_fee;
	}
	public double getDiscount_fee() {
		return discount_fee;
	}
	public void setDiscount_fee(double discount_fee) {
		this.discount_fee = discount_fee;
	}
	public double getPayment() {
		return payment;
	}
	public void setPayment(double payment) {
		this.payment = payment;
	}
	public String getSku_properties_name() {
		return sku_properties_name;
	}
	public void setSku_properties_name(String sku_properties_name) {
		this.sku_properties_name = sku_properties_name;
	}
	public String getPic_path() {
		return pic_path;
	}
	public void setPic_path(String pic_path) {
		this.pic_path = pic_path;
	}
	public String getPic_thumb_path() {
		return pic_thumb_path;
	}
	public void setPic_thumb_path(String pic_thumb_path) {
		this.pic_thumb_path = pic_thumb_path;
	}

}
