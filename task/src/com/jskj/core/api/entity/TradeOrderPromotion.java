package com.jskj.core.api.entity;

import java.util.Date;

public class TradeOrderPromotion {
	
	private String promotion_name;
	private String promotion_type;
	private Date apply_at;
	private double discount_fee;
	
	public String getPromotion_name() {
		return promotion_name;
	}
	public void setPromotion_name(String promotion_name) {
		this.promotion_name = promotion_name;
	}
	public String getPromotion_type() {
		return promotion_type;
	}
	public void setPromotion_type(String promotion_type) {
		this.promotion_type = promotion_type;
	}
	public Date getApply_at() {
		return apply_at;
	}
	public void setApply_at(Date apply_at) {
		this.apply_at = apply_at;
	}
	public double getDiscount_fee() {
		return discount_fee;
	}
	public void setDiscount_fee(double discount_fee) {
		this.discount_fee = discount_fee;
	}

}
