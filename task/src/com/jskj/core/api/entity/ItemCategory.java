package com.jskj.core.api.entity;

/**
 * 商品分类数据结构
 * @author VAIO
 * @since 2014-12-16
 */
public class ItemCategory {
	
	private int cid;
	private int parent_cid;
	private String name;
	private boolean is_parent;
	private ItemCategory[] sub_categories;
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public int getParent_cid() {
		return parent_cid;
	}
	public void setParent_cid(int parent_cid) {
		this.parent_cid = parent_cid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isIs_parent() {
		return is_parent;
	}
	public void setIs_parent(boolean is_parent) {
		this.is_parent = is_parent;
	}
	public ItemCategory[] getSub_categories() {
		return sub_categories;
	}
	public void setSub_categories(ItemCategory[] sub_categories) {
		this.sub_categories = sub_categories;
	}

}
