package com.jskj.core.api.entity;

import java.util.Date;

/**
 * 交易数据结构
 * @author VAIO
 * @since 2014-12-16
 */
public class TradeDetail {
	//交易编号
	private String tid;
	//商品购买数量。当一个trade对应多个order的时候，值为所有商品购买数量之和
	private int num;
	//商品数字编号。当一个trade对应多个order的时候，值为第一个交易明细中的商品的编号
	private int num_iid;
	//商品价格。精确到2位小数；单位：元。当一个trade对应多个order的时候，值为第一个交易明细中的商品的价格
	private double price;
	//商品主图片地址。当一个trade对应多个order的时候，值为第一个交易明细中的商品的图片地址
	private String pic_path;
	//商品主图片缩略图地址
	private String pic_thumb_path;
	//交易标题，以首个商品标题作为此标题的值
	private String title;
	//交易类型。取值范围：FIXED(一口价) GIFT(送礼) COD(货到付款) 
	private String type;
	//微信粉丝ID
	private int weixn_user_id;
	//买家类型，取值范围：0 为未知，1 为微信粉丝，2 为微博粉丝
	private int buyer_type;
	//买家ID，当 buyer_type 为 1 时，buyer_id 的值等于 weixin_user_id 的值
	private int buyer_id;
	//买家昵称
	private String buyer_nick;
	//买家购买附言
	private String buyer_message;
	//卖家备注星标，取值范围 1、2、3、4、5
	private int seller_flag;
	//卖家对该交易的备注
	private String trade_memo;
	//收货人的所在城市
	private String receiver_city;
	//收货人的所在地区
	private String receiver_district;
	//收货人的姓名
	private String receiver_name;
	//收货人的所在省份
	private String receiver_state;
	//收货人的详细地址
	private String receiver_address;
	//收货人的邮编
	private String receiver_zip;
	//收货人的手机号码
	private String receiver_mobile;
	//交易维权状态。0 无维权，1 顾客发起维权，9 商家正在处理，2 顾客拒绝商家的处理结果，3 顾客接受商家的处理结果
	private int feedback;
	//外部交易编号。比如，如果支付方式是微信支付，就是财付通的交易单号
	private String outer_tid;
	/**
	 * 	交易状态。取值范围：
	 * 	TRADE_NO_CREATE_PAY(没有创建支付交易) 
		WAIT_BUYER_PAY(等待买家付款) 
		WAIT_SELLER_SEND_GOODS(等待卖家发货，即：买家已付款) 
		WAIT_BUYER_CONFIRM_GOODS(等待买家确认收货，即：卖家已发货) 
		TRADE_BUYER_SIGNED(买家已签收) 
		TRADE_CLOSED(付款以后用户退款成功，交易自动关闭) 
		TRADE_CLOSED_BY_USER(付款以前，卖家或买家主动关闭交易)
	 */
	private String status;
	/**
	 * 创建交易时的物流方式。取值范围：express（快递），fetch（到店自提）
	 */
	private String shipping_type;
	//运费。单位：元，精确到分
	private double post_fee;
	//商品总价（商品价格乘以数量的总金额）。单位：元，精确到分
	private double total_fee;
	//交易优惠金额（不包含交易明细中的优惠金额）。单位：元，精确到分
	private double discount_fee;
	//实付金额。单位：元，精确到分
	private double payment;
	//交易创建时间
	private Date created;
	//交易更新时间。当交易的：状态改变、备注更改、星标更改 等情况下都会刷新更新时间
	private Date update_time;
	//买家付款时间
	private Date pay_time;
	/**
	 * 	支付类型。取值范围：
		WEIXIN（微信支付），
		ALIPAY（支付宝支付），
		BANKCARDPAY（银行卡支付），
		PEERPAY（代付），
		CODPAY（货到付款）
		BAIDUPAY（百度钱包支付）
	 */
	private String pay_type;
	//卖家发货时间
	private Date consign_time;
	//买家签收时间
	private Date sign_time;
	//买家下单的地区
	private String buyer_area;
	//交易明细列表
	private TradeOrder[] orders;
	//如果是到店自提交易，返回自提详情，否则返回空
	private TradeFetchDetail fetch_detail;
	//	在交易中使用到的卡券的详情，包括：优惠券、优惠码
	private CouponDetail[] coupon_details;
	//在交易中使用到优惠活动详情，包括：满减满送
	private TradePromotion[] promotion_details;
	
	//卖家手工调整订单金额
	private double adjust_fee;
	//商品优惠总额
	private double item_fee;
	//活动优惠总额
	private double promotion_fee;
	//优惠券优惠总额
	private double coupon_fee;
	
	//交易中包含的子交易，目前：仅在送礼订单中会有子交易
	private TradeDetail[] sub_trades;
	private double profit;
	private int handled;

	public TradePromotion[] getPromotion_details() {
		return promotion_details;
	}
	public void setPromotion_details(TradePromotion[] promotion_details) {
		this.promotion_details = promotion_details;
	}
	public double getAdjust_fee() {
		return adjust_fee;
	}
	public void setAdjust_fee(double adjust_fee) {
		this.adjust_fee = adjust_fee;
	}
	public double getItem_fee() {
		return item_fee;
	}
	public void setItem_fee(double item_fee) {
		this.item_fee = item_fee;
	}
	public double getPromotion_fee() {
		return promotion_fee;
	}
	public void setPromotion_fee(double promotion_fee) {
		this.promotion_fee = promotion_fee;
	}
	public double getCoupon_fee() {
		return coupon_fee;
	}
	public void setCoupon_fee(double coupon_fee) {
		this.coupon_fee = coupon_fee;
	}
	public double getProfit() {
		return profit;
	}
	public void setProfit(double profit) {
		this.profit = profit;
	}
	public int getHandled() {
		return handled;
	}
	public void setHandled(int handled) {
		this.handled = handled;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getNum_iid() {
		return num_iid;
	}
	public void setNum_iid(int num_iid) {
		this.num_iid = num_iid;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getPic_path() {
		return pic_path;
	}
	public void setPic_path(String pic_path) {
		this.pic_path = pic_path;
	}
	public String getPic_thumb_path() {
		return pic_thumb_path;
	}
	public void setPic_thumb_path(String pic_thumb_path) {
		this.pic_thumb_path = pic_thumb_path;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getWeixn_user_id() {
		return weixn_user_id;
	}
	public void setWeixn_user_id(int weixn_user_id) {
		this.weixn_user_id = weixn_user_id;
	}
	public int getBuyer_type() {
		return buyer_type;
	}
	public void setBuyer_type(int buyer_type) {
		this.buyer_type = buyer_type;
	}
	public int getBuyer_id() {
		return buyer_id;
	}
	public void setBuyer_id(int buyer_id) {
		this.buyer_id = buyer_id;
	}
	public String getBuyer_nick() {
		return buyer_nick;
	}
	public void setBuyer_nick(String buyer_nick) {
		this.buyer_nick = buyer_nick;
	}
	public String getBuyer_message() {
		return buyer_message;
	}
	public void setBuyer_message(String buyer_message) {
		this.buyer_message = buyer_message;
	}
	public int getSeller_flag() {
		return seller_flag;
	}
	public void setSeller_flag(int seller_flag) {
		this.seller_flag = seller_flag;
	}
	public String getTrade_memo() {
		return trade_memo;
	}
	public void setTrade_memo(String trade_memo) {
		this.trade_memo = trade_memo;
	}
	public String getReceiver_city() {
		return receiver_city;
	}
	public void setReceiver_city(String receiver_city) {
		this.receiver_city = receiver_city;
	}
	public String getReceiver_district() {
		return receiver_district;
	}
	public void setReceiver_district(String receiver_district) {
		this.receiver_district = receiver_district;
	}
	public String getReceiver_name() {
		return receiver_name;
	}
	public void setReceiver_name(String receiver_name) {
		this.receiver_name = receiver_name;
	}
	public String getReceiver_state() {
		return receiver_state;
	}
	public void setReceiver_state(String receiver_state) {
		this.receiver_state = receiver_state;
	}
	public String getReceiver_address() {
		return receiver_address;
	}
	public void setReceiver_address(String receiver_address) {
		this.receiver_address = receiver_address;
	}
	public String getReceiver_zip() {
		return receiver_zip;
	}
	public void setReceiver_zip(String receiver_zip) {
		this.receiver_zip = receiver_zip;
	}
	public String getReceiver_mobile() {
		return receiver_mobile;
	}
	public void setReceiver_mobile(String receiver_mobile) {
		this.receiver_mobile = receiver_mobile;
	}
	public int getFeedback() {
		return feedback;
	}
	public void setFeedback(int feedback) {
		this.feedback = feedback;
	}
	public String getOuter_tid() {
		return outer_tid;
	}
	public void setOuter_tid(String outer_tid) {
		this.outer_tid = outer_tid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getShipping_type() {
		return shipping_type;
	}
	public void setShipping_type(String shipping_type) {
		this.shipping_type = shipping_type;
	}
	public double getPost_fee() {
		return post_fee;
	}
	public void setPost_fee(double post_fee) {
		this.post_fee = post_fee;
	}
	public double getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(double total_fee) {
		this.total_fee = total_fee;
	}
	public double getDiscount_fee() {
		return discount_fee;
	}
	public void setDiscount_fee(double discount_fee) {
		this.discount_fee = discount_fee;
	}
	public double getPayment() {
		return payment;
	}
	public void setPayment(double payment) {
		this.payment = payment;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}
	public Date getPay_time() {
		return pay_time;
	}
	public void setPay_time(Date pay_time) {
		this.pay_time = pay_time;
	}
	public String getPay_type() {
		return pay_type;
	}
	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}
	public Date getConsign_time() {
		return consign_time;
	}
	public void setConsign_time(Date consign_time) {
		this.consign_time = consign_time;
	}
	public Date getSign_time() {
		return sign_time;
	}
	public void setSign_time(Date sign_time) {
		this.sign_time = sign_time;
	}
	public String getBuyer_area() {
		return buyer_area;
	}
	public void setBuyer_area(String buyer_area) {
		this.buyer_area = buyer_area;
	}
	public TradeOrder[] getOrders() {
		return orders;
	}
	public void setOrders(TradeOrder[] orders) {
		this.orders = orders;
	}
	public TradeFetchDetail getFetch_detail() {
		return fetch_detail;
	}
	public void setFetch_detail(TradeFetchDetail fetch_detail) {
		this.fetch_detail = fetch_detail;
	}
	public CouponDetail[] getCoupon_details() {
		return coupon_details;
	}
	public void setCoupon_details(CouponDetail[] coupon_details) {
		this.coupon_details = coupon_details;
	}
	public TradeDetail[] getSub_trades() {
		return sub_trades;
	}
	public void setSub_trades(TradeDetail[] sub_trades) {
		this.sub_trades = sub_trades;
	}

}
