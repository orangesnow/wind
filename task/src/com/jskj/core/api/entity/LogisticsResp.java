package com.jskj.core.api.entity;

public class LogisticsResp {
	
	private String outSid;
	private String companyName;
	private String tid;
	private String status;
	private TransitStepInfo[] traceList;
	
	public String getOutSid() {
		return outSid;
	}
	public void setOutSid(String outSid) {
		this.outSid = outSid;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public TransitStepInfo[] getTraceList() {
		return traceList;
	}
	public void setTraceList(TransitStepInfo[] traceList) {
		this.traceList = traceList;
	}
}
