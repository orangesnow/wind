package com.jskj.core.api.entity;

import java.util.Date;

/**
 * 订单中使用到的卡券的数据结构
 * @author VAIO
 * @since 2014-12-16
 */
public class CouponDetail {
	
	//该组卡券的ID
	private int coupon_id;
	//该组卡券的名称
	private String coupon_name;
	//卡券的类型。可选值：PROMOCARD（优惠券）、PROMOCODE（优惠码）
	private String coupon_type;
	//卡券内容。当卡券类型为优惠码时，值为优惠码字符串
	private String coupon_content;
	//卡券的说明
	private String coupon_description;
	//卡券使用条件说明
	private String coupon_condition;
	//使用时间
	private Date used_at;
	//优惠的金额，单位：元，精确到小数点后两位
	private double discount_fee;
	
	public int getCoupon_id() {
		return coupon_id;
	}
	public void setCoupon_id(int coupon_id) {
		this.coupon_id = coupon_id;
	}
	public String getCoupon_name() {
		return coupon_name;
	}
	public void setCoupon_name(String coupon_name) {
		this.coupon_name = coupon_name;
	}
	public String getCoupon_type() {
		return coupon_type;
	}
	public void setCoupon_type(String coupon_type) {
		this.coupon_type = coupon_type;
	}
	public String getCoupon_content() {
		return coupon_content;
	}
	public void setCoupon_content(String coupon_content) {
		this.coupon_content = coupon_content;
	}
	public String getCoupon_description() {
		return coupon_description;
	}
	public void setCoupon_description(String coupon_description) {
		this.coupon_description = coupon_description;
	}
	public String getCoupon_condition() {
		return coupon_condition;
	}
	public void setCoupon_condition(String coupon_condition) {
		this.coupon_condition = coupon_condition;
	}
	public Date getUsed_at() {
		return used_at;
	}
	public void setUsed_at(Date used_at) {
		this.used_at = used_at;
	}
	public double getDiscount_fee() {
		return discount_fee;
	}
	public void setDiscount_fee(double discount_fee) {
		this.discount_fee = discount_fee;
	}
	
	
	
}
