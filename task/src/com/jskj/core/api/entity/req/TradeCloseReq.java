/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class TradeCloseReq implements Serializable{
	
	private String tid;
	private String closeReason;
	private String fields;
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getCloseReason() {
		return closeReason;
	}
	public void setCloseReason(String closeReason) {
		this.closeReason = closeReason;
	}
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	
}
