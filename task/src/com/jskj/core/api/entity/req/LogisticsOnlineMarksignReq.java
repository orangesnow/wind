/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class LogisticsOnlineMarksignReq implements Serializable {
	
	private String tid;

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}
	
	
}
