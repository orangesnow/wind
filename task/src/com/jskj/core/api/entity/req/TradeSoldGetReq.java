/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class TradeSoldGetReq implements Serializable {
	
	private String fields;
	private String status;
	private String startCreated;
	private String endCreated;
	private String startUpdate;
	private String endUpdate;
	private Integer weixinUserId;
	private String buyerNick;
	private Integer pageNo;
	private Integer pageSize;
	private boolean useHasNext;
	
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStartCreated() {
		return startCreated;
	}
	public void setStartCreated(String startCreated) {
		this.startCreated = startCreated;
	}
	public String getEndCreated() {
		return endCreated;
	}
	public void setEndCreated(String endCreated) {
		this.endCreated = endCreated;
	}
	public String getStartUpdate() {
		return startUpdate;
	}
	public void setStartUpdate(String startUpdate) {
		this.startUpdate = startUpdate;
	}
	public String getEndUpdate() {
		return endUpdate;
	}
	public void setEndUpdate(String endUpdate) {
		this.endUpdate = endUpdate;
	}
	public Integer getWeixinUserId() {
		return weixinUserId;
	}
	public void setWeixinUserId(Integer weixinUserId) {
		this.weixinUserId = weixinUserId;
	}
	public String getBuyerNick() {
		return buyerNick;
	}
	public void setBuyerNick(String buyerNick) {
		this.buyerNick = buyerNick;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public boolean isUseHasNext() {
		return useHasNext;
	}
	public void setUseHasNext(boolean useHasNext) {
		this.useHasNext = useHasNext;
	}

}
