/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class ItemUpdateDelistingReq implements Serializable {
	
	private Integer numIid;
	private String fields;
	
	public Integer getNumIid() {
		return numIid;
	}
	public void setNumIid(Integer numIid) {
		this.numIid = numIid;
	}
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	
}
