package com.jskj.core.api.entity.req;

import java.io.Serializable;

public class ItemCustomGetReq implements Serializable{
	
	private String fields;
	private String outerId;
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	public String getOuterId() {
		return outerId;
	}
	public void setOuterId(String outerId) {
		this.outerId = outerId;
	}
	
}
