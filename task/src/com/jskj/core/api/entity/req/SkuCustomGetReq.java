/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class SkuCustomGetReq implements Serializable{
	
	private String fields;
	private String outerId;
	private Integer numIid;
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	public String getOuterId() {
		return outerId;
	}
	public void setOuterId(String outerId) {
		this.outerId = outerId;
	}
	public Integer getNumIid() {
		return numIid;
	}
	public void setNumIid(Integer numIid) {
		this.numIid = numIid;
	}

}
