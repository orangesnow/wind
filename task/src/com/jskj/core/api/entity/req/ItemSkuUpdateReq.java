/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class ItemSkuUpdateReq implements Serializable{
	
	private Integer numIid;
	private Integer skuId;
	private Integer quantity;
	private Float price;
	private String outerId;
	public Integer getNumIid() {
		return numIid;
	}
	public void setNumIid(Integer numIid) {
		this.numIid = numIid;
	}
	public Integer getSkuId() {
		return skuId;
	}
	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public String getOuterId() {
		return outerId;
	}
	public void setOuterId(String outerId) {
		this.outerId = outerId;
	}
	
}
