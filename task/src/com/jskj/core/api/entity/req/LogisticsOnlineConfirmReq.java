/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class LogisticsOnlineConfirmReq implements Serializable {
	
	private String tid;
	private String outerId;
	private Integer isNoExpress;
	private Integer outStype;
	private String outSid;
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getOuterId() {
		return outerId;
	}
	public void setOuterId(String outerId) {
		this.outerId = outerId;
	}
	public Integer getIsNoExpress() {
		return isNoExpress;
	}
	public void setIsNoExpress(Integer isNoExpress) {
		this.isNoExpress = isNoExpress;
	}
	public Integer getOutStype() {
		return outStype;
	}
	public void setOutStype(Integer outStype) {
		this.outStype = outStype;
	}
	public String getOutSid() {
		return outSid;
	}
	public void setOutSid(String outSid) {
		this.outSid = outSid;
	}
	
	
}
