/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class ItemAddReq implements Serializable {
	
	private Integer cid;
	private Integer promotionCid;
	private String tagIds;
	private Float price;
	private String title;
	private String desc;
	private Integer isVirtual;
	private byte[] images;
	private Float postFee;
	private String skuProperties;
	private String skuOuterIds;
	private String skusWithJson;
	private String originPrice;
	private String buyUrl;
	private String outerId;
	private Integer buyQuota;
	private Integer quantity;
	private Integer hideQuantity;
	private String fields;
	
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Integer getPromotionCid() {
		return promotionCid;
	}
	public void setPromotionCid(Integer promotionCid) {
		this.promotionCid = promotionCid;
	}
	public String getTagIds() {
		return tagIds;
	}
	public void setTagIds(String tagIds) {
		this.tagIds = tagIds;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Integer getIsVirtual() {
		return isVirtual;
	}
	public void setIsVirtual(Integer isVirtual) {
		this.isVirtual = isVirtual;
	}
	public byte[] getImages() {
		return images;
	}
	public void setImages(byte[] images) {
		this.images = images;
	}
	public Float getPostFee() {
		return postFee;
	}
	public void setPostFee(Float postFee) {
		this.postFee = postFee;
	}
	public String getSkuProperties() {
		return skuProperties;
	}
	public void setSkuProperties(String skuProperties) {
		this.skuProperties = skuProperties;
	}
	public String getSkuOuterIds() {
		return skuOuterIds;
	}
	public void setSkuOuterIds(String skuOuterIds) {
		this.skuOuterIds = skuOuterIds;
	}
	public String getSkusWithJson() {
		return skusWithJson;
	}
	public void setSkusWithJson(String skusWithJson) {
		this.skusWithJson = skusWithJson;
	}
	public String getOriginPrice() {
		return originPrice;
	}
	public void setOriginPrice(String originPrice) {
		this.originPrice = originPrice;
	}
	public String getBuyUrl() {
		return buyUrl;
	}
	public void setBuyUrl(String buyUrl) {
		this.buyUrl = buyUrl;
	}
	public String getOuterId() {
		return outerId;
	}
	public void setOuterId(String outerId) {
		this.outerId = outerId;
	}
	public Integer getBuyQuota() {
		return buyQuota;
	}
	public void setBuyQuota(Integer buyQuota) {
		this.buyQuota = buyQuota;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getHideQuantity() {
		return hideQuantity;
	}
	public void setHideQuantity(Integer hideQuantity) {
		this.hideQuantity = hideQuantity;
	}
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	
	
}
