/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class ItemDeleteReq implements Serializable{
	
	private Integer numIid;

	public Integer getNumIid() {
		return numIid;
	}

	public void setNumIid(Integer numIid) {
		this.numIid = numIid;
	}
}
