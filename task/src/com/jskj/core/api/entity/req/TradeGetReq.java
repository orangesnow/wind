/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class TradeGetReq implements Serializable {
	
	private String fields;
	private String tid;
	
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}

}
