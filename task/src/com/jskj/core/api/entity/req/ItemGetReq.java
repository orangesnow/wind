/**
 * 
 */
package com.jskj.core.api.entity.req;

import java.io.Serializable;

/**
 * @author VAIO
 *
 */
public class ItemGetReq implements Serializable {
	
	private String fields;
	private Integer numIid;
	
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	public Integer getNumIid() {
		return numIid;
	}
	public void setNumIid(Integer numIid) {
		this.numIid = numIid;
	}

}
