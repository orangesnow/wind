package com.jskj.core.api.entity;

import java.util.Date;

public class TradePromotion {
	
	// 该优惠活动的ID
	private Integer promotion_id;
	
	//该优惠活动的名称
	private String promotion_name;
	
	//优惠的类型。可选值：FULLREDUCE（满减满送）
	private String promotion_type;
	
	//优惠使用条件说明
	private String promotion_condition;
	
	//使用时间
	private Date used_at;
	
	//优惠的金额，单位：元，精确到小数点后两位
	private double discount_fee;

	public Integer getPromotion_id() {
		return promotion_id;
	}

	public void setPromotion_id(Integer promotion_id) {
		this.promotion_id = promotion_id;
	}

	public String getPromotion_name() {
		return promotion_name;
	}

	public void setPromotion_name(String promotion_name) {
		this.promotion_name = promotion_name;
	}

	public String getPromotion_type() {
		return promotion_type;
	}

	public void setPromotion_type(String promotion_type) {
		this.promotion_type = promotion_type;
	}

	public String getPromotion_condition() {
		return promotion_condition;
	}

	public void setPromotion_condition(String promotion_condition) {
		this.promotion_condition = promotion_condition;
	}

	public Date getUsed_at() {
		return used_at;
	}

	public void setUsed_at(Date used_at) {
		this.used_at = used_at;
	}

	public double getDiscount_fee() {
		return discount_fee;
	}

	public void setDiscount_fee(double discount_fee) {
		this.discount_fee = discount_fee;
	}
	
}
