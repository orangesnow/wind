package com.jskj.core.api.entity;

import java.util.Date;

/**
 * 到店自提详情
 * @author VAIO
 * @since 2014-12-16
 */
public class TradeFetchDetail {
	
	//领取人（即：预约人）的姓名
	private String fetcher_name;
	//领取人的手机号
	private String fetcher_mobile;
	//预约的领取时间
	private Date fetch_time;
	//自提点名称
	private String shop_name;
	//自提点联系方式
	private String shop_mobile;
	//自提点所在省份
	private String shop_state;
	//自提点所在城市
	private String shop_city;
	//自提点所在地区
	private String shop_district;
	//自提点详细地址
	private String shop_address;
	
	public String getFetcher_name() {
		return fetcher_name;
	}
	public void setFetcher_name(String fetcher_name) {
		this.fetcher_name = fetcher_name;
	}
	public String getFetcher_mobile() {
		return fetcher_mobile;
	}
	public void setFetcher_mobile(String fetcher_mobile) {
		this.fetcher_mobile = fetcher_mobile;
	}
	public Date getFetch_time() {
		return fetch_time;
	}
	public void setFetch_time(Date fetch_time) {
		this.fetch_time = fetch_time;
	}
	public String getShop_name() {
		return shop_name;
	}
	public void setShop_name(String shop_name) {
		this.shop_name = shop_name;
	}
	public String getShop_mobile() {
		return shop_mobile;
	}
	public void setShop_mobile(String shop_mobile) {
		this.shop_mobile = shop_mobile;
	}
	public String getShop_state() {
		return shop_state;
	}
	public void setShop_state(String shop_state) {
		this.shop_state = shop_state;
	}
	public String getShop_city() {
		return shop_city;
	}
	public void setShop_city(String shop_city) {
		this.shop_city = shop_city;
	}
	public String getShop_district() {
		return shop_district;
	}
	public void setShop_district(String shop_district) {
		this.shop_district = shop_district;
	}
	public String getShop_address() {
		return shop_address;
	}
	public void setShop_address(String shop_address) {
		this.shop_address = shop_address;
	}
	
}
