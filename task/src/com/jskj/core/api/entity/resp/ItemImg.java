package com.jskj.core.api.entity.resp;

import java.util.Date;


/**
 * 商品图片数据结构
 * @author VAIO
 * @since 2015-03-12
 */
public class ItemImg {
	
	private int id;
	private Date created;
	private String url;
	private String thumburl;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getThumburl() {
		return thumburl;
	}
	public void setThumburl(String thumburl) {
		this.thumburl = thumburl;
	}
	
}
