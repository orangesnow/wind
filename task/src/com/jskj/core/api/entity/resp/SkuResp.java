package com.jskj.core.api.entity.resp;

import java.util.Date;

/**
 * Sku数据结构
 * @author VAIO
 * @since 2014-12-16
 */
public class SkuResp {
	//商家编码（商家为Sku设置的外部编号）
	private String outer_id;
	//Sku的数字ID
	private int sku_id;
	//Sku所属商品的数字编号
	private int num_id;
	//属于这个Sku的商品的数量
	private int quantity;
	//Sku所对应的销售属性的中文名字串
	private String properties_name;
	//商品在付款减库存的状态下，该Sku上未付款的订单数量
	private String properties_name_json;
	//商品在付款减库存的状态下，该Sku上未付款的订单数量
	private int with_hold_quantity;
	//商品的这个Sku的价格；精确到2位小数；单位：元
	private double price ;
	//Sku创建日期，时间格式：yyyy-MM-dd HH:mm:ss
	private Date created;
	//Sku最后修改日期，时间格式：yyyy-MM-dd HH:mm:ss
	private Date modified;
	
	public String getOuter_id() {
		return outer_id;
	}
	public void setOuter_id(String outer_id) {
		this.outer_id = outer_id;
	}
	public int getSku_id() {
		return sku_id;
	}
	public void setSku_id(int sku_id) {
		this.sku_id = sku_id;
	}
	public int getNum_id() {
		return num_id;
	}
	public void setNum_id(int num_id) {
		this.num_id = num_id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getProperties_name() {
		return properties_name;
	}
	public void setProperties_name(String properties_name) {
		this.properties_name = properties_name;
	}
	public int getWith_hold_quantity() {
		return with_hold_quantity;
	}
	public void setWith_hold_quantity(int with_hold_quantity) {
		this.with_hold_quantity = with_hold_quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public String getProperties_name_json() {
		return properties_name_json;
	}
	public void setProperties_name_json(String properties_name_json) {
		this.properties_name_json = properties_name_json;
	}
	
}
