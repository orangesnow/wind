package com.jskj.core.api.entity.resp;

import java.util.Date;

/**
 * 商品二维码数据结构
 * @author VAIO
 * @since 2014-12-16
 */
public class ItemQrcode {
	//商品二维码的ID
	private int id;
	//二维码的名称
	private String name;
	//二维码的描述
	private String desc;
	//商品二维码创建时间，时间格式：yyyy-MM-dd HH:mm:ss
	private Date created;
	/**
	 * 商品二维码类型。可选值：
		GOODS_SCAN_FOLLOW(扫码关注后购买商品) 
		GOODS_SCAN_FOLLOW_DECREASE(扫码关注后减优惠额) 
		GOODS_SCAN_FOLLOW_DISCOUNT(扫码关注后折扣) 
		GOODS_SCAN_DECREASE(扫码直接减优惠额) 
		GOODS_SCAN_DISCOUNT(扫码直接折扣) 
	 */
	private String type;
	//折扣，格式：9.0；单位：折，精确到小数点后 1 位。如果没有折扣，则为 0
	private String discount;
	//减金额优惠，格式：5.00；单位：元；精确到：分。如果没有减额优惠，则为 0
	private double decrease;
	//扫码直接购买的二维码基于这个url生成。如果不是扫码直接购买的类型，则为空
	private String link_url;
	//扫码关注购买的二维码图片地址。如果不是扫码关注购买的类型，则为空
	private String weixin_qrcode_url;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public double getDecrease() {
		return decrease;
	}
	public void setDecrease(double decrease) {
		this.decrease = decrease;
	}
	public String getLink_url() {
		return link_url;
	}
	public void setLink_url(String link_url) {
		this.link_url = link_url;
	}
	public String getWeixin_qrcode_url() {
		return weixin_qrcode_url;
	}
	public void setWeixin_qrcode_url(String weixin_qrcode_url) {
		this.weixin_qrcode_url = weixin_qrcode_url;
	}

}
