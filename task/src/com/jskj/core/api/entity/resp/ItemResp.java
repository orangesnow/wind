package com.jskj.core.api.entity.resp;

import java.util.Date;


/**
 * 商品数据结构
 * @author VAIO
 * @since 2015-03-12
 */
public class ItemResp {
	
	private Integer num_iid;
	private String alias;
	private String title;
	private Integer cid;
	private Integer promotion_cid;
	private String tag_ids;
	private String desc;
	private String origin_price;
	private String outer_id;
	private String outer_buy_url;
	private Integer buy_quota;
	private String created;
	private int is_virtual;
	private int is_listing;
	private int is_lock;
	private String auto_listing_time;
	private String detail_url;
	private String pic_url;
	private String pic_thumb_url;
	private int num;
	private int sold_num;
	private double price;
	private double post_fee;
	private SkuResp[] skus;
	private ItemImg[] item_imgs;
	private ItemQrcode[] item_qrcodes;
	private ItemTag[] item_tags;
	
	public Integer getNum_iid() {
		return num_iid;
	}
	public void setNum_iid(Integer num_iid) {
		this.num_iid = num_iid;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Integer getPromotion_cid() {
		return promotion_cid;
	}
	public void setPromotion_cid(Integer promotion_cid) {
		this.promotion_cid = promotion_cid;
	}
	public String getTag_ids() {
		return tag_ids;
	}
	public void setTag_ids(String tag_ids) {
		this.tag_ids = tag_ids;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getOrigin_price() {
		return origin_price;
	}
	public void setOrigin_price(String origin_price) {
		this.origin_price = origin_price;
	}
	public String getOuter_id() {
		return outer_id;
	}
	public void setOuter_id(String outer_id) {
		this.outer_id = outer_id;
	}
	public String getOuter_buy_url() {
		return outer_buy_url;
	}
	public void setOuter_buy_url(String outer_buy_url) {
		this.outer_buy_url = outer_buy_url;
	}
	public Integer getBuy_quota() {
		return buy_quota;
	}
	public void setBuy_quota(Integer buy_quota) {
		this.buy_quota = buy_quota;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public int getIs_virtual() {
		return is_virtual;
	}
	public void setIs_virtual(int is_virtual) {
		this.is_virtual = is_virtual;
	}
	public int getIs_listing() {
		return is_listing;
	}
	public void setIs_listing(int is_listing) {
		this.is_listing = is_listing;
	}
	public int getIs_lock() {
		return is_lock;
	}
	public void setIs_lock(int is_lock) {
		this.is_lock = is_lock;
	}
	public String getAuto_listing_time() {
		return auto_listing_time;
	}
	public void setAuto_listing_time(String auto_listing_time) {
		this.auto_listing_time = auto_listing_time;
	}
	public String getDetail_url() {
		return detail_url;
	}
	public void setDetail_url(String detail_url) {
		this.detail_url = detail_url;
	}
	public String getPic_url() {
		return pic_url;
	}
	public void setPic_url(String pic_url) {
		this.pic_url = pic_url;
	}
	public String getPic_thumb_url() {
		return pic_thumb_url;
	}
	public void setPic_thumb_url(String pic_thumb_url) {
		this.pic_thumb_url = pic_thumb_url;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getSold_num() {
		return sold_num;
	}
	public void setSold_num(int sold_num) {
		this.sold_num = sold_num;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getPost_fee() {
		return post_fee;
	}
	public void setPost_fee(double post_fee) {
		this.post_fee = post_fee;
	}
	public SkuResp[] getSkus() {
		return skus;
	}
	public void setSkus(SkuResp[] skus) {
		this.skus = skus;
	}
	public ItemImg[] getItem_imgs() {
		return item_imgs;
	}
	public void setItem_imgs(ItemImg[] item_imgs) {
		this.item_imgs = item_imgs;
	}
	public ItemQrcode[] getItem_qrcodes() {
		return item_qrcodes;
	}
	public void setItem_qrcodes(ItemQrcode[] item_qrcodes) {
		this.item_qrcodes = item_qrcodes;
	}
	public ItemTag[] getItem_tags() {
		return item_tags;
	}
	public void setItem_tags(ItemTag[] item_tags) {
		this.item_tags = item_tags;
	}
	
}
