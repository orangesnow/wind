package com.jskj.core.api.entity;

/**
 * 交易明细中买家留言的数据结构
 * @author VAIO
 * @since 2014-12-16
 */
public class BuyerMessage {
	
	/**
	 * 留言的标题
	 */
	private String title;
	
	/**
	 * 留言的内容
	 */
	private String content;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
