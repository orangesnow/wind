package com.jskj.core.api.entity;

public class ItemSku {
	
	private String skuid;
	private String name;
	private String value;
	private String market_price;
	private String retail_price;
	private boolean out_of_stock;
	
	public String getSkuid() {
		return skuid;
	}
	public void setSkuid(String skuid) {
		this.skuid = skuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getMarket_price() {
		return market_price;
	}
	public void setMarket_price(String market_price) {
		this.market_price = market_price;
	}
	public String getRetail_price() {
		return retail_price;
	}
	public void setRetail_price(String retail_price) {
		this.retail_price = retail_price;
	}
	public boolean isOut_of_stock() {
		return out_of_stock;
	}
	public void setOut_of_stock(boolean out_of_stock) {
		this.out_of_stock = out_of_stock;
	}
	
}
