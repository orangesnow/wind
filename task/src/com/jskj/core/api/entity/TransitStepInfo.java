package com.jskj.core.api.entity;
/**
 * 物流跟踪信息数据结构
 * @author VAIO
 * @since 2014-12-16
 */
public class TransitStepInfo {
	
	//状态发生的时间
	private String status_time;
	//状态描述
	private String status_desc;
	
	public String getStatus_time() {
		return status_time;
	}
	public void setStatus_time(String status_time) {
		this.status_time = status_time;
	}
	public String getStatus_desc() {
		return status_desc;
	}
	public void setStatus_desc(String status_desc) {
		this.status_desc = status_desc;
	}

	
}
