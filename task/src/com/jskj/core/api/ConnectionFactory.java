package com.jskj.core.api;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

/**
 * 
 * 
 * @author ���ĳ�
 * @version 1.0
 * @since 2011-5-3
 */
public class ConnectionFactory {

	public static Connection getConnection(String url) {
		return getConnection(url, 3000);
	}

	public static Connection getConnection(String url, int timeout) {
		Connection conn = Jsoup.connect(url)
		.timeout(timeout);
		return conn;
	}

	public static void main(String[] args) {
	}
}
