package com.jskj.core.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.log4j.Logger;

import com.jskj.core.util.Md5Util;



public class ThirdApiClient {
	private static final String Version = "1.0";

    private static final String apiEntry = "http://api.aihuo360.com";

    private static final String format = "json";

    private static final String signMethod = "md5";
    
    private static final String APIKey = "849ad695";
    private static final String SECRETKey = "03174e11fae61a9b5797d15c5dca598b";
	public static Date LastTimeDate = new Date();
	public static long theSerialID = 0L;
    private String appId;
    private String appSecret; 
    
    public static final  Logger logger = Logger.getLogger(ThirdApiClient.class);
    
    public ThirdApiClient(String appId, String appSecret) throws Exception{
    	
        if ("".equals(appId) || "".equals(appSecret)){
            throw new Exception("appId 和 appSecret 不能为空");
        }
        
        this.appId = appId;
        this.appSecret = appSecret;
    }
    
    public ThirdApiClient() {

    }
    
    public static void main(String[] args) throws Exception{
    	ThirdApiClient client = new ThirdApiClient(null,null);
    	String str = client.getOrderList("11");
    	//getProduct(1,1);
    	System.out.println("str:"+str);
    	
    }
    
    /**
     * 获取
     * @param method
     * @param parames
     * @return
     * @throws Exception
     */
    public String getProduct(int pageno,int pagesize) throws Exception{
    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/products?pageno="+pageno+"&pagesize="+pagesize;
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
       
        getMethod.addRequestHeader("APIKey", APIKey);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    /**
     * 获取
     * @param method
     * @param parames
     * @return
     * @throws Exception
     */
    public String getProductDetail(String productid) throws Exception{
    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/products/"+productid;
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
       
        getMethod.addRequestHeader("APIKey", APIKey);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    public int syncOrder(Map<String,Object> order, List<Map<String,Object>> items) throws Exception{
    	int flag = 0;
    	
    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/orders";
        
        HttpClient client = new HttpClient();
        PostMethod postMethod = new PostMethod(url);

        String oid = (String)order.get("o_id");
   
        String recevName = (String)order.get("recev_name");
        String recevAddress = (String)order.get("recev_address");
        String recevMobile = (String)order.get("recev_mobile");
        Integer payType = Integer.parseInt((String)order.get("pay_type"));
        Integer userid = (Integer)order.get("user_id");
     
        Integer postfee =  (Integer)order.get("post_fee"); 
        String tid = (String)order.get("t_id");
        String buyerMessage = (String)order.get("buyer_message");
        
        int loop = 0;
        StringBuffer params = new StringBuffer();
        params.append("device_id=").append(userid).append(",");
        params.append("api_key=").append(APIKey).append(",");
        params.append("coupon=,");
        params.append("name=").append(recevName).append(",");
        params.append("phone=").append(recevMobile).append(",");
        params.append("shipping_province=").append("").append(",");
        params.append("shipping_city=").append("").append(",");
        params.append("shipping_district=").append("").append(",");
        params.append("shipping_address=").append(recevAddress).append(",");
        params.append("shipping_charge=").append(postfee).append(",");
        params.append("pay_type=").append(payType).append(",");
        params.append("comment=").append(buyerMessage).append(","); 
        
        if(items != null && items.size() >0){
        	for(int i=0;i<items.size();i++){
        		Map<String,Object> item = items.get(i);
        		 String  outitemid = (String)item.get("out_item_id");
        		 String  outskuid= (String)item.get("out_sku_id");
        		 String  payment = (String)item.get("payment");
        		 
        		 Integer skuNum = (Integer)item.get("num");
        		 params.append("product_id=").append(outitemid).append(","); 
        		 params.append("product_prop_id=").append(outskuid).append(","); 
        		 params.append("sale_price=").append(payment).append(","); 
        		 params.append("quantity=").append(skuNum).append(","); 
        	}
        }
        
       String[] strArray =  params.toString().split(",");
        
        Arrays.sort(strArray);
        
        JSONObject paramObj = new JSONObject();
        paramObj.put("api_key", APIKey);
        paramObj.put("device_id ", ""+userid);
        paramObj.put("coupon", "");
        paramObj.put("order",  getOrderInfo(order, items));
        paramObj.put("sign", getSign("POST",url,strArray,SECRETKey));
      
        logger.info("post json :"+paramObj.toString());
        RequestEntity requestEntity = new StringRequestEntity(paramObj.toString(),"application/json","UTF-8");
        postMethod.setRequestEntity(requestEntity);
        
		int statusCode = client.executeMethod(postMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			
			//使用流的方式读取也可以如下设置
			InputStream in = postMethod.getResponseBodyAsStream();  
			
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
			
		}

    	logger.info("result:"+result);
    	return flag;
    }
    
    public String getSign(String method,String url,String[] strArray,String secret){
    	String sign = "";
    	StringBuffer source = new StringBuffer();
    	source.append(method).append(url);
    	
    	if(strArray != null && strArray.length >0){
    		for(int i=0;i<strArray.length;i++){
    			source .append(strArray[i]);
    		}
    	}
    	
    	source .append(secret);
    	logger.info("source:"+source);
    	
    	try {
			String encodeStr = URLEncoder.encode(source.toString(), "utf-8");
			logger.info("encodeStr:"+encodeStr);
			sign = Md5Util.encode(encodeStr);
			logger.info("sign:"+sign);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

    	return sign;
    	
    }
    
    public String getOrderList(String userid) throws Exception{

    	StringBuffer result = new StringBuffer();
    	String url = "http://api.aihuo360.com/v2/orders?page=1&per_page=20&device_id="+userid;
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);

		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
			
		}

    	logger.info("result:"+result);
    	
    	return result.toString();
    }
    
    public String getOrderInfo(Map<String,Object> order, List<Map<String,Object>> items) {
    	
    	String oid = (String)order.get("o_id");
         Integer nums = (Integer)order.get("nums");
         String sendName = (String)order.get("send_name");
         String recevName = (String)order.get("recev_name");
         String recevAddress = (String)order.get("recev_address");
         String recevMobile = (String)order.get("recev_mobile");
         Integer payType =Integer.parseInt((String)order.get("pay_type"));
         Integer userid = (Integer)order.get("user_id");
         Double price =Double.parseDouble((String)order.get("price"));
         Integer postfee = (Integer)order.get("post_fee");
         String tid = (String)order.get("t_id");
         String comment = (String)order.get("buyer_message");
         
         JSONObject params = new JSONObject();
         params.put("name", recevName);
         params.put("phone ", recevMobile);
         params.put("shipping_province", "");
         params.put("shipping_city", "");
         params.put("shipping_district", "");
         params.put("shipping_address", recevAddress);
         
         params.put("shipping_charge", postfee);
         params.put("pay_type", payType);
         
         params.put("comment", comment);
         HashMap<String,String> orderHash = new HashMap<String,String>();
         
         if(items != null && items.size() >0){
        	 for(int i=0;i<items.size();i++){
        		 String hashcode = NewSid().substring(0, 13);
        		 JSONObject product = new JSONObject();
        		 Map<String,Object> item = items.get(i);
        		 
        		 String  outitemid = (String)item.get("out_item_id");
        		 String  outskuid= (String)item.get("out_sku_id");
        		 String  payment = (String)item.get("payment");
        		 Integer skuNum = (Integer)item.get("num");
        		
        		 product.put("product_id", outitemid);
        		 product.put("product_prop_id", outskuid);
        	     product.put("sale_price", payment);
        		 product.put("quantity", skuNum);
        		 
        		 orderHash.put(hashcode, product.toString());
        	 }
        	 
        	 JSONObject hashObj =  JSONObject.fromObject(orderHash);
        	 params.put("line_items_attributes", hashObj.toString()) ;
        	 
         }

         return params.toString();
         
    }
    
    public static String NewSid() {
		Date currDate = new Date();
		if (currDate.toString().equals(LastTimeDate.toString())) {
			theSerialID++;
		} else {
			theSerialID = 1L;
			LastTimeDate = new Date();
		}
		String theSerialIDStr = String.valueOf(theSerialID);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String mDateTime = formatter.format(new Date());
		String SerialIDStr = "";
		for (int i = 0; i < 6 - theSerialIDStr.length(); i++)
			SerialIDStr = (new StringBuilder(String.valueOf(SerialIDStr)))
					.append("0").toString();

		SerialIDStr = (new StringBuilder(String.valueOf(SerialIDStr))).append(
				theSerialIDStr).toString();
		return (new StringBuilder(String.valueOf(mDateTime))).append(
				SerialIDStr).toString();
	}
    
    
    public String post(String method, HashMap<String, String> parames, List<String> filePaths, String fileKey) throws Exception{
    	String result = "";
    	String url = apiEntry + getParamStr(method, parames);
    	
    	HttpClient client = new HttpClient();
    	PostMethod postMethod = new PostMethod(url);
    	postMethod.addRequestHeader("APIKey", APIKey);
    	
    	if(null != filePaths && filePaths.size() > 0 && null != fileKey && !"".equals(fileKey)){
    		
    		Part[] parts = new FilePart[filePaths.size()];
    	
    		for(int i = 0; i < filePaths.size(); i++){
	    		File file = new File(filePaths.get(i));
	            parts[i] = new FilePart(fileKey, file);
	    	}
    		
    		postMethod.setRequestEntity(new MultipartRequestEntity(parts,postMethod.getParams()));
    		
    	}
    	
		int statusCode = client.executeMethod(postMethod);  
		if(statusCode == HttpStatus.SC_OK) {  
			result = postMethod.getResponseBodyAsString();
		}
        
        return result;
    }
    
    public String getParamStr(String method, HashMap<String, String> parames){
        String str = "";
        try {
            str = URLEncoder.encode(buildParamStr(buildCompleteParams(method, parames)), "UTF-8")
                    .replace("%3A", ":")
                    .replace("%2F", "/")
                    .replace("%26", "&")
                    .replace("%3D", "=")
                    .replace("%3F", "?");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }
    
    private String buildParamStr(HashMap<String, String> param){
        String paramStr = "";
        Object[] keyArray = param.keySet().toArray();
        for(int i = 0; i < keyArray.length; i++){
            String key = (String)keyArray[i];

            if(0 == i){
                paramStr += (key + "=" + param.get(key));
            }
            else{
                paramStr += ("&" + key + "=" + param.get(key));
            }
        }

        return paramStr;
    }


    private HashMap<String, String> buildCompleteParams(String method, HashMap<String, String> parames) throws Exception{
        HashMap<String, String> commonParams = getCommonParams(method);
        for (String key : parames.keySet()) {
			if(commonParams.containsKey(key)){
				throw new Exception("参数名冲突");
			}
			
			commonParams.put(key, parames.get(key));
		}
        
        commonParams.put(KdtApiProtocol.SIGN_KEY, KdtApiProtocol.sign(appSecret, commonParams));
        return commonParams;
    }

    private HashMap<String, String> getCommonParams(String method){
    	HashMap<String, String> parames = new HashMap<String, String>();
        parames.put(KdtApiProtocol.APP_ID_KEY, appId);
        parames.put(KdtApiProtocol.METHOD_KEY, method);
        parames.put(KdtApiProtocol.TIMESTAMP_KEY, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        parames.put(KdtApiProtocol.FORMAT_KEY, format);
        parames.put(KdtApiProtocol.SIGN_METHOD_KEY, signMethod);
        parames.put(KdtApiProtocol.VERSION_KEY, Version);
        return parames;
    }
}
