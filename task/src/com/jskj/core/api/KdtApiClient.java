package com.jskj.core.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

public class KdtApiClient {
	private static final String Version = "1.0";

    private static final String apiEntry = "http://open.koudaitong.com/api/entry?";

    private static final String format = "json";

    private static final String signMethod = "md5";
    
    private static final String DefaultUserAgent = "KdtApiSdk Client v0.1";

    private String appId;
    private String appSecret;

    public KdtApiClient(String appId, String appSecret) throws Exception{
        if ("".equals(appId) || "".equals(appSecret)){
            throw new Exception("appId 和 appSecret 不能为空");
        }
        
        this.appId = appId;
        this.appSecret = appSecret;
    }
    
    /**
     * 获取
     * @param method
     * @param parames
     * @return
     * @throws Exception
     */
    public String get(String method, HashMap<String,String> parames) throws Exception{
    	StringBuffer result = new StringBuffer();
    	String url = apiEntry + getParamStr(method, parames);
        
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod(url);
       
        getMethod.addRequestHeader("User-Agent", DefaultUserAgent);
 
		int statusCode = client.executeMethod(getMethod);  
		if(statusCode == HttpStatus.SC_OK) { 
			//使用流的方式读取也可以如下设置
			InputStream in = getMethod.getResponseBodyAsStream();  
			//这里的编码规则要与上面的相对应  
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
			
			String s = null;
			while((s = reader.readLine()) != null){
				result.append(s);
			}
			
			reader.close();
			in.close();
		}

		return result.toString();
    }
    
    public String post(String method, HashMap<String, String> parames, List<String> filePaths, String fileKey) throws Exception{
    	String result = "";
    	String url = apiEntry + getParamStr(method, parames);
    	
    	HttpClient client = new HttpClient();
    	PostMethod postMethod = new PostMethod(url);
    	postMethod.addRequestHeader("User-Agent", DefaultUserAgent);
    	
    	if(null != filePaths && filePaths.size() > 0 && null != fileKey && !"".equals(fileKey)){
    		
    		Part[] parts = new FilePart[filePaths.size()];
    	
    		for(int i = 0; i < filePaths.size(); i++){
	    		File file = new File(filePaths.get(i));
	            parts[i] = new FilePart(fileKey, file);
	    	}
    		
    		postMethod.setRequestEntity(new MultipartRequestEntity(parts,postMethod.getParams()));
    		
    	}
    	
		int statusCode = client.executeMethod(postMethod);  
		if(statusCode == HttpStatus.SC_OK) {  
			result = postMethod.getResponseBodyAsString();
		}
        
        return result;
    }
    
    public String getParamStr(String method, HashMap<String, String> parames){
        String str = "";
        try {
            str = URLEncoder.encode(buildParamStr(buildCompleteParams(method, parames)), "UTF-8")
                    .replace("%3A", ":")
                    .replace("%2F", "/")
                    .replace("%26", "&")
                    .replace("%3D", "=")
                    .replace("%3F", "?");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }
    
    private String buildParamStr(HashMap<String, String> param){
        String paramStr = "";
        Object[] keyArray = param.keySet().toArray();
        for(int i = 0; i < keyArray.length; i++){
            String key = (String)keyArray[i];

            if(0 == i){
                paramStr += (key + "=" + param.get(key));
            }
            else{
                paramStr += ("&" + key + "=" + param.get(key));
            }
        }

        return paramStr;
    }


    private HashMap<String, String> buildCompleteParams(String method, HashMap<String, String> parames) throws Exception{
        HashMap<String, String> commonParams = getCommonParams(method);
        for (String key : parames.keySet()) {
			if(commonParams.containsKey(key)){
				throw new Exception("参数名冲突");
			}
			
			commonParams.put(key, parames.get(key));
		}
        
        commonParams.put(KdtApiProtocol.SIGN_KEY, KdtApiProtocol.sign(appSecret, commonParams));
        return commonParams;
    }

    private HashMap<String, String> getCommonParams(String method){
    	HashMap<String, String> parames = new HashMap<String, String>();
        parames.put(KdtApiProtocol.APP_ID_KEY, appId);
        parames.put(KdtApiProtocol.METHOD_KEY, method);
        parames.put(KdtApiProtocol.TIMESTAMP_KEY, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        parames.put(KdtApiProtocol.FORMAT_KEY, format);
        parames.put(KdtApiProtocol.SIGN_METHOD_KEY, signMethod);
        parames.put(KdtApiProtocol.VERSION_KEY, Version);
        return parames;
    }
}
