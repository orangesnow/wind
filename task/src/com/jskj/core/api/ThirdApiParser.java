package com.jskj.core.api;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jskj.core.api.entity.DetailItem;
import com.jskj.core.api.entity.HashImage;
import com.jskj.core.api.entity.ItemSku;
import com.jskj.core.api.entity.SimpleItem;

/**
 * 解析kdt订单数据
 * @author VAIO
 * @since 2014-12-26
 */
public class ThirdApiParser {

	private static final Logger logger = Logger.getLogger(ThirdApiParser.class);
	
	public static SimpleItem[] parseItemList(String response){
		SimpleItem[]  items= null;
		JSONObject jsonObj = JSONObject.fromObject(response);
		JSONArray products = jsonObj.getJSONArray("products");
	
		if(products != null && !products.isEmpty()) {
			int size = products.size();
			items = new SimpleItem[size];
			for(int i=0; i<size; i++){
				JSONObject item = products.getJSONObject(i);
				String id = item.getString("id");
				String title = item.getString("title");
				
				items[i] = new SimpleItem();
				items[i].setId(id);
				items[i].setTitle(title);
			}
		}
		
		return items;
	} 
	
	public static DetailItem parseDetailItem(String response){
		DetailItem detail = new DetailItem();
		JSONObject jsonObj = JSONObject.fromObject(response);
		String id = jsonObj.getString("id");
		detail.setId(id);
		String title = jsonObj.getString("title");
		detail.setTitle(title);
		String market_price = jsonObj.getString("market_price");
		detail.setMarket_price(market_price);
		String retail_price = jsonObj.getString("retail_price");
		detail.setRetail_price(retail_price);
		boolean out_of_stock = jsonObj.getBoolean("out_of_stock");
		detail.setOut_of_stock(out_of_stock);
		boolean auto_pick_up = jsonObj.getBoolean("auto_pick_up");
		detail.setAuto_pick_up(auto_pick_up);
		JSONObject image = jsonObj.getJSONObject("image");
		
		String image_list = image.getString("list");
		detail.setImage_list(image_list);
		String image_grid = image.getString("grid");
		detail.setImage_grid(image_grid);
		
		String detail_link =jsonObj.getString("detail_link") ;
		detail.setDetail_url(detail_link);
		JSONArray product_props = 	jsonObj.getJSONArray("product_props");
		
		if(product_props != null && !product_props.isEmpty()){
			int prop_size = product_props.size();
			ItemSku[] skus = new ItemSku[prop_size];
			for(int i=0;i<prop_size;i++){
				JSONObject prop =product_props.getJSONObject(i);
				String sku_id = prop.getString("id");
				String sku_name = prop.getString("name");
				String sku_value = prop.getString("value");
				String sku_market = prop.getString("market_price");
				String sku_retail = prop.getString("retail_price");
				boolean sku_out_of_stock = prop.getBoolean("out_of_stock");
				
				skus[i]  = new ItemSku();
				skus[i].setMarket_price(sku_market);
				skus[i].setName(sku_name);
				skus[i].setOut_of_stock(sku_out_of_stock);
				skus[i].setRetail_price(sku_retail);
				skus[i].setSkuid(sku_id);
				skus[i].setValue(sku_value);
				
			}
			detail.setSkus(skus);
		}
		
		JSONArray photos = jsonObj.getJSONArray("photos");
		if(photos != null && !photos.isEmpty()){
			int photo_size = photos.size();
			HashImage[] focus_images = new HashImage[photo_size] ;
			for(int i=0;i<photo_size;i++){
				JSONObject photo = photos.getJSONObject(i);
				
				String photo_id = photo.getString("id");
				JSONObject photo_image = photo.getJSONObject("image");
				String iphone_url = photo_image.getString("iphone");
				
				focus_images[i] = new HashImage();
				focus_images[i].setKey(photo_id);
				focus_images[i].setUrl(iphone_url);
				
			}
			detail.setPhotos(focus_images);
		}

		JSONArray labels = jsonObj.getJSONArray("labels");
		if(labels != null && !labels.isEmpty()){
			int label_size = labels.size();
			String [] item_labels = new String[label_size];
			for(int i=0;i<label_size;i++){
				String label = labels.getString(i);
				item_labels[i] = new String();
				item_labels[i] = label;
			}
			detail.setLabels(item_labels);
		}
		
		return detail;
		
	}
	
	public static String[] parseDetailUrl(String listUrl){
		String[] images = null;
		Connection conn = ConnectionFactory.getConnection(listUrl, 5000);
		try {
			Document doc = conn.get();
			Elements imgs = doc.select("img");
			if(imgs != null && !imgs.isEmpty()){
				images = new String[imgs.size()];
				for(int i=0;i<imgs.size();i++){
					Element ele = imgs.get(i);
					String src = ele.attr("src");
					images[i] = src;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return images;
	}
	
	
	public static String decodeStr(String str){
		
		try {
			if(str != null && str.length() >0){
				return new String(str.getBytes(),"utf-8");
			} else {
				
			}
		} catch (Exception e) {
		}
		
		return "";
	}
	

	
}
