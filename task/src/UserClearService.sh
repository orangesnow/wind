cd /usr/local/gh_schedule/
current_datetime=`date +%s`
filedate=`stat /var/logs/schedule_log/checkuser.log | grep Modify | awk '{print $2}'`
filetime=`stat /var/logs/schedule_log/checkuser.log | grep Modify | awk '{split($3,var,".");print var[1]}'`
file_datetime=`date -d "$filedate $filetime" +%s`
timedelta=`expr $current_datetime - $file_datetime`
echo $timedelta
if [ "$timedelta" -gt "5" ];then

kill -9 `ps aux|grep SyncDutyService | head -n 1|awk '{print $2}'`
echo 'kill process SyncDutyService '`date` >>/var/logs/control/checkuser_kill.log

export JAVA_HOME=/usr/java/jdk1.7.0_51
export CLASSPATH=.:$CLASSPATH:/usr/local/gh_schedule/libantlr-2.7.6.jar:/usr/local/gh_schedule/lib/asm-attrs.jar:/usr/local/gh_schedule/lib/asm.jar:/usr/local/gh_schedule/lib/c3p0-0.9.1.2.jar:/usr/local/gh_schedule/lib/commons-beanutils.jar:/usr/local/gh_schedule/lib/commons-codec-1.4.jar:/usr/local/gh_schedule/lib/commons-collections-3.1.jar:/usr/local/gh_schedule/lib/commons-collections-3.2.jar:/usr/local/gh_schedule/lib/commons-httpclient-3.1.jar:/usr/local/gh_schedule/lib/commons-io-1.4.jar:/usr/local/gh_schedule/lib/commons-lang-2.2.jar:/usr/local/gh_schedule/lib/commons-lang-2.4.jar:/usr/local/gh_schedule/lib/commons-logging-1.1.jar:/usr/local/gh_schedule/lib/commons-pool.jar:/usr/local/gh_schedule/lib/dom4j-1.6.1.jar:/usr/local/gh_schedule/lib/ezmorph-1.0.6.jar:/usr/local/gh_schedule/lib/hibernate-3.2.6.ga.jar:/usr/local/gh_schedule/lib/jave-1.0.2.jar:/usr/local/gh_schedule/lib/jedis-2.4.2.jar:/usr/local/gh_schedule/lib/json-lib-2.4-jdk15.jar:/usr/local/gh_schedule/lib/jsoup-1.5.2.jar:/usr/local/gh_schedule/lib/jstl-1.1.2.jar:/usr/local/gh_schedule/lib/jta.jar:/usr/local/gh_schedule/lib/log4j-1.2.16.jar:/usr/local/gh_schedule/lib/log4j-over-slf4j-1.6.1.jar:/usr/local/gh_schedule/lib/metadata-extractor-2.3.1.jar:/usr/local/gh_schedule/lib/mysql-connector-java-5.1.10-bin.jar:/usr/local/gh_schedule/lib/slf4j-api-1.6.1.jar:/usr/local/gh_schedule/lib/spring.aop-3.0.5.jar:/usr/local/gh_schedule/lib/spring.asm-3.0.5.jar:/usr/local/gh_schedule/lib/spring.beans-3.0.5.jar:/usr/local/gh_schedule/lib/spring.context-3.0.5.jar:/usr/local/gh_schedule/lib/spring.context.support-3.0.5.jar:/usr/local/gh_schedule/lib/spring.core-3.0.5.jar:/usr/local/gh_schedule/lib/spring.expression-3.0.5.jar:/usr/local/gh_schedule/lib/spring.jdbc-3.0.5.jar:/usr/local/gh_schedule/lib/spring.orm-3.0.5.jar:/usr/local/gh_schedule/lib/spring.transaction-3.0.5.jar:/usr/local/gh_schedule/lib/spring.web-3.0.5.jar:/usr/local/gh_schedule/lib/spring.web.servlet-3.0.5.jar:/usr/local/gh_schedule/lib/standard-1.1.2.jar:/usr/local/gh_schedule/lib/xstream-1.3.1.jar:/usr/local/gh_schedule/lib/gateway.jar:/usr/local/gh_schedule/lib/gateway.jar:
export PATH=$JAVA_HOME/bin:$PATH

java -Xms256m -Xmx512m com.jiuxf.weixin.schedule.SyncDutyService >>/var/logs/schedule_log/checkuser.log &

fi
